import React, { ReactNode } from "react";

import { Modal, Box } from "@material-ui/core";
import { FeedbackType } from "@peach/checkout-design-system/dist/components/feedback";
import FeedbackScreen from "../screen/FeedbackScreen";

import useStyles from "./feedbackModal.styles";

const getModalStyle = () => {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
};

interface FeedbackModalProps {
  isOpen: boolean;
  // Select variation e.g Warning
  feedback: FeedbackType;
  feedbackTitle: string;
  message: string;
  isTimed?: boolean;
  isBottom?: boolean;

  // States the action the button will take onClick and is button text
  buttonAction: string;
  clickCallback?: () => void;
  clickCallbackGoBack?: () => void;

  errorMessages?: Array<string>;
  text?: string;
  children?: ReactNode;
  fullWidthButton?: boolean;
}

const FeedbackModal = (props: FeedbackModalProps) => {
  const classes = useStyles();

  return (
    <Box>
      <Modal
        open={props.isOpen}
        aria-labelledby={props.feedbackTitle}
        aria-describedby={props.message}
      >
        <Box style={getModalStyle()} className={classes.paper} height="60%">
          <FeedbackScreen
            feedback={props.feedback}
            feedbackTitle={props.feedbackTitle}
            message={props.message}
            buttonAction={props.buttonAction}
            clickCallback={props.clickCallback}
            clickCallbackGoBack={props.clickCallbackGoBack}
            text={props.text}
            errorMessages={props.errorMessages}
            isTimed={props.isTimed || false}
            fullWidthButton={props.fullWidthButton || false}
            isBottom={props.isBottom || false}
          >
            {props.children}
          </FeedbackScreen>
        </Box>
      </Modal>
    </Box>
  );
};

export default FeedbackModal;
