import React, { useContext, useState } from "react";
import { Box, InputLabel, Button } from "@material-ui/core";

import NumberFormat, { NumberFormatValues } from "react-number-format";
import CallIcon from "@material-ui/icons/Call";

import { useStyles, CssTextField } from "./mpesa.styles";

import { CheckoutContext } from "../../../contexts/CheckoutContext";

export interface MobicredProps {
  onClick: (param: string) => void;
}

const title: string = "Mpesa";
const mobileCode: string = "+254";
const buttonAction: string = "Verify";
const maxLength: number = 10;

const Mpesa = (props: MobicredProps) => {
  const checkout = useContext(CheckoutContext);
  const classes = useStyles();

  const [mobileDisplayValue, setMobileDisplayValue] = useState("");
  const [mobileValue, setMobileValue] = useState("");
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const onValueChange = (values: NumberFormatValues): void => {
    const { formattedValue, value } = values;
    setIsButtonDisabled(true);
    setMobileDisplayValue(formattedValue);
    setMobileValue(value);

    if (value.length > maxLength) return;

    if (value.length === maxLength || value.length === maxLength - 1) {
      setIsButtonDisabled(false);
    }
  };

  return (
    <Box>
      <Box p={3} width={1}>
        <Box display="flex" justifyContent="center">
          <img
            data-testid="mpesa-logo"
            src={`${checkout.assetsBasePath || "/"}images/m-pesa.svg`}
            alt=""
          />
        </Box>
        <Box
          display="flex"
          justifyContent="center"
          textAlign="center"
          data-testid="mpesa-mobile-label"
        >
          To begin your payment please enter the mobile number linked with your
          Mpesa account
        </Box>

        <Box mt={3}>
          <form
            onSubmit={(event) => {
              event.preventDefault();
              if (isButtonDisabled) return;
              props.onClick(mobileValue);
            }}
            noValidate
            autoComplete="off"
          >
            <Box style={{ position: "relative" }}>
              <InputLabel className={classes.label} htmlFor={title}>
                Mobile Number
              </InputLabel>

              <NumberFormat
                data-testid="mpesa-mobile-input"
                value={mobileDisplayValue}
                onValueChange={onValueChange}
                type="tel"
                customInput={CssTextField}
                format={`${mobileCode} ####-######`}
                placeholder={`${mobileCode} 1234-123450`}
                allowEmptyFormatting
                mask="_"
                required
                autoFocus
                fullWidth
                id={title}
                aria-describedby={title}
                variant="outlined"
              />
              <Box className={classes.callIcon}>
                <CallIcon />
              </Box>
            </Box>
            <Box mt={2} mb={2} display="flex" justifyContent="flex-end">
              <Button
                data-testid="mpesa-mobile-btn"
                type="button"
                className={classes.button}
                onClick={() => {
                  if (isButtonDisabled) return;
                  props.onClick(mobileValue);
                }}
                variant="contained"
                disableElevation
                fullWidth={false}
                disabled={isButtonDisabled}
              >
                {buttonAction}
              </Button>
            </Box>
          </form>
        </Box>
      </Box>
    </Box>
  );
};

export default Mpesa;
