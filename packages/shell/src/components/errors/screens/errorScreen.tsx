import { FeedbackType, Dialog } from "@peach/checkout-design-system";
import React from "react";

interface LoaderModalProps {
  title: string;
}

const ErrorScreen: React.FunctionComponent<LoaderModalProps> = (
  props: LoaderModalProps
) => {
  return <Dialog type={FeedbackType.WARNING} title={props.title} forceBottom />;
};

export default ErrorScreen;
