import {
  CheckoutEvent,
  CheckoutStatus,
  StatusEvent,
} from "@peach/checkout-lib";
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import ErrorModal from "../../../../components/errors/modals/errorModal";
import LoaderModal from "../../../../components/loaders/loadingModal";
import LoadingScreen from "../../../../components/loaders/loadingScreen";
import {
  CheckoutContext,
  IFrameMethod,
} from "../../../../contexts/CheckoutContext";
import { GoogleAnalyticsContext } from "../../../../contexts/GoogleAnalyticsContext";
import { SelectedPaymentContext } from "../../../../contexts/SelectedPaymentContext";
import { UtilContext } from "../../../../contexts/UtilContext";
import CheckoutService from "../../../../services/checkoutService";
import { GA } from "../../../../utils/googleAnalyticsConstants";
import { MESSAGING } from "../../../../utils/messagingConstants";
import FeedbackPage from "../shared/FeedbackPage";

export interface IframePluginProps {
  method: IFrameMethod;
}
export const IframePlugin = ({ method }: IframePluginProps) => {
  const config = useContext(CheckoutContext);
  const ga = useContext(GoogleAnalyticsContext);
  const selectPayment = useContext(SelectedPaymentContext);
  const { messageUtils, pluginManager } = useContext(UtilContext);
  const checkoutService = useMemo(() => {
    return new CheckoutService();
  }, []);

  useEffect(() => {
    return () => {
      checkoutService.cancelRequest();
      pluginManager.stopPollingListener();
    };
  }, [checkoutService, pluginManager]);

  const ref = useRef<HTMLIFrameElement>(null);

  const [status, setStatus] = useState<CheckoutStatus>("initializing");
  const [code, setCode] = useState<string | undefined>(undefined);
  const [feedback, setFeedback] = useState<{
    heading?: string;
    message?: string | string[];
    continueText?: string;
    cancelText?: string;
  }>({ heading: "", message: "" });
  const [redirectData, setRedirectData] = useState({
    method: "",
    url: "",
    data: {},
  });

  const [cancelTimer, setCancelTimer] = useState<NodeJS.Timeout | undefined>(
    undefined
  );

  const closeModal = useCallback(() => {
    setStatus(undefined);
    ga.trackEvent(
      GA.EVENTS.ERROR_GO_BACK,
      selectPayment?.selected?.name as string
    );
  }, []);

  const reset = useCallback(() => {
    setStatus("initializing");
    setCode(undefined);
    setFeedback({ heading: "", message: "" });
    setRedirectData({ method: "", url: "", data: {} });
    selectPayment.reset();
    ref.current!.contentWindow?.postMessage(
      {
        message: "checkout-bootstrap",
        config,
      },
      method.origin
    );
  }, [setStatus, setCode, setFeedback, setRedirectData, selectPayment, ref]);

  const timeout = useCallback(() => {
    setStatus(undefined);
    selectPayment.select();
  }, []);

  const messages = useMemo(
    () => ({
      ...messageUtils.getErrorMessages(),
      ...messageUtils.getPendingPaymentMessages(),
    }),
    [messageUtils]
  );

  const message = useMemo(() => messages[code || ""] || {}, [messages, code]);

  const updateStatus = useCallback(
    (event: StatusEvent) => {
      setStatus(event.value);
      setFeedback({
        heading: event.heading,
        message: event.message,
        continueText: event.continueText,
        cancelText: event.cancelText,
      });
      setCode(event.code);
      if (["success", "pending"].includes(event.value || "")) {
        selectPayment.setCancelVisibility(false);
      }
    },
    [setStatus, setFeedback, setCode, ga, selectPayment]
  );

  useEffect(() => {
    if (selectPayment.cancellation.state !== "Requested") {
      return;
    }
    if (selectPayment.confirmGoBack) {
      selectPayment.cancellation.deny();

      // ensure that Checkout doesn't get stuck
      // waiting for cancellation from a plugin.
      const timeout = setTimeout(() => {
        selectPayment.cancellation.approve();
        // Log that plugin took long to timeout?
      }, 20_000);
      setCancelTimer(timeout);

      ref.current!.contentWindow?.postMessage(
        {
          message: "checkout-cancellation",
        },
        method.origin
      );

      setStatus("cancelling");
    } else {
      selectPayment.cancellation.approve();
    }

    return () => cancelTimer && clearTimeout(cancelTimer);
  }, [
    selectPayment.cancellation.state,
    selectPayment.confirmGoBack,
    cancelTimer,
  ]);

  useEffect(() => {
    const listener = (e: MessageEvent) => {
      if (
        config.pluginPaymentMethods
          .map((m: any) => m.origin)
          .includes(e.origin) &&
        e.data.message === "push-checkout-events"
      ) {
        e.data.events.forEach((event: CheckoutEvent) => {
          if (event.type === "status" && event.value === "initializing") {
            // Initialise the iframe with the config once it tells us it is initialising.
            ref.current!.contentWindow?.postMessage(
              {
                message: "checkout-bootstrap",
                config,
              },
              method.origin
            );
          }
          switch (event.type) {
            case "status":
              updateStatus(event);
              break;
            case "update-feedback":
              setFeedback(event.feedback);
              break;
            case "checkout-redirect":
              setRedirectData({
                method: event.method,
                url: event.url,
                data: event.data,
              });
              break;
            case "reset":
              reset();
            case "timeout":
              timeout();
              break;
            case "cancelled":
              selectPayment.cancellation.approve();
              if (cancelTimer) {
                clearTimeout(cancelTimer);
                setCancelTimer(undefined);
              }
              break;
            case "set-checkout-confirm":
              selectPayment.setConfirmGoBack(event.confirm);
              break;
          }
        });
      }
    };
    window.addEventListener("message", listener);
    return () => {
      window.removeEventListener("message", listener);
    };
  }, [cancelTimer, reset, setStatus, selectPayment]);

  return (
    <>
      {status === "initializing" && <LoadingScreen widgetName={method.label} />}
      {status === "cancelling" && (
        <LoadingScreen
          widgetName={method.label}
          title="{0} Cancelling"
          label="Please wait while {0} cancels the payment."
        />
      )}

      {status === "pending" && (
        <FeedbackPage
          data-testid="pending-modal"
          messages={feedback}
          redirectUrl={redirectData.url}
          redirectPostData={redirectData.data}
          paymentMethod={method.name}
          transactionId={config.merchantTransactionId}
          code={code || config.code}
          status="pending"
        />
      )}

      {status === "loading" && (
        <LoaderModal
          data-testid="loader-modal"
          open={true}
          title={
            feedback?.heading ||
            message?.heading ||
            MESSAGING.DEFAULT_LOADING_HEADING
          }
          label={
            feedback?.message ||
            message?.message ||
            MESSAGING.DEFAULT_LOADING_MESSAGE
          }
        />
      )}

      {status === "error" && (
        <ErrorModal
          data-testid="error-modal"
          open={true}
          title={
            feedback?.heading ||
            message?.heading ||
            MESSAGING.DEFAULT_ERROR_HEADING
          }
          label={
            Array.isArray(feedback?.message) ? (
              <>
                {feedback?.message.map((message) => {
                  return <p key={message}>{message}</p>;
                })}
              </>
            ) : (
              feedback?.message ||
              message?.message ||
              MESSAGING.DEFAULT_ERROR_MESSAGE
            )
          }
          cancelText={feedback?.cancelText || ""}
          onCancel={() => closeModal()}
          continueText={
            (feedback?.cancelText && feedback?.continueText) ||
            !feedback?.cancelText
              ? "Continue"
              : ""
          }
          onClick={() => reset()}
        />
      )}

      {status === "success" && (
        <FeedbackPage
          data-testid="success-modal"
          paymentMethod={method.name}
          code="000.000.000"
          status="Success"
          isTimed={true}
          redirectPostData={redirectData.data}
          redirectUrl={redirectData.url || config.redirectUrl}
        />
      )}

      <iframe
        ref={ref}
        style={{
          border: "none",
          width: "100%",
          height: "100%",
          visibility: status ? "hidden" : "visible",
          display: status ? "none" : "block",
        }}
        src={method.location}
      />
    </>
  );
};
