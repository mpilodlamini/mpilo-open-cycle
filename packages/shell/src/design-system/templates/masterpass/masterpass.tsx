import React, { useContext } from "react";

import { Box, Button } from "@material-ui/core";

import FileCopyOutlinedIcon from "@material-ui/icons/FileCopyOutlined";
import CheckCircleOutlinedIcon from "@material-ui/icons/CheckCircleOutlined";

import SnackbarContainer from "../../../checkout/selected-payment-method/payment-methods/shared/SnackbarContainer";
import TimeUtil from "../../../utils/timeUtil";

import { MASTERPASS_QR_TOKEN_EXPIRATION_TIME } from "../../../utils/constants";
import { CheckoutContext } from "../../../contexts/CheckoutContext";
import useStyles from "./masterpass.styles";

export interface MasterpassProps {
  timeUtil: TimeUtil;
  qrCode: string;
  qrCodeImageUrl: string;
  copySuccess: boolean;
  onRefreshCode: () => void;
  onCopyCode: () => void;
}

export const Masterpass = (props: MasterpassProps) => {
  const checkout = useContext(CheckoutContext);
  const classes = useStyles();

  const buttonAction: string = !props.copySuccess
    ? "Copy Code"
    : "Code Copied!";
  const qrCodeSrcUrl: string =
    props.qrCodeImageUrl ||
    "https://store-images.s-microsoft.com/image/apps.33967.13510798887182917.246b0a3d-c3cc-46fc-9cea-021069d15c09.392bf5f5-ade4-4b36-aa63-bb15d5c3817a?mode=scale&q=90&h=200&w=200&background=%230078D7";

  const qrCodeFormat: string = props.qrCode.replace(/\d{3,4}?(?=...)/g, "$& ");

  const copyCodeButton = (
    <Button
      type="button"
      className={!props.copySuccess ? classes.button : classes.buttonSuccess}
      onClick={props.onCopyCode}
      variant="contained"
      disableElevation
      fullWidth
      endIcon={
        !props.copySuccess ? (
          <FileCopyOutlinedIcon />
        ) : (
          <CheckCircleOutlinedIcon />
        )
      }
    >
      {buttonAction}
    </Button>
  );

  return (
    <Box width={1} height={1}>
      <Box display="flex" justifyContent="center" mb={1}>
        <img
          src={`${checkout.assetsBasePath || "/"}images/masterpass.svg`}
          alt="Masterpass"
        />
      </Box>
      <Box display="flex" justifyContent="center" mb={3} textAlign="center">
        Go on to the Masterpass App on your phone and enter or scan the code
        below:
      </Box>

      <Box
        display="flex"
        justifyContent="center"
        alignContent="center"
        className={classes.code}
        p={2}
        pl={3}
        pr={3}
      >
        <img
          style={{ height: "100%", maxHeight: "100%", minHeight: "100px" }}
          src={qrCodeSrcUrl}
          alt="qrcodenumber"
          data-testid="qr-code"
        />
      </Box>
      <Box
        width={1}
        pb={2}
        style={{ background: "#EFF0F2", fontWeight: 600 }}
        textAlign="center"
        data-testid="code"
      >
        {qrCodeFormat}
      </Box>

      {copyCodeButton}

      <Box
        display="flex"
        justifyContent="center"
        mb={2}
        mt={1}
        textAlign="center"
        fontWeight="600"
      >
        <Button
          className={classes.link}
          data-testid="RefreshCode"
          onClick={props.onRefreshCode}
        >
          Refresh Code
        </Button>
      </Box>

      <SnackbarContainer
        timeUtil={props.timeUtil}
        time={MASTERPASS_QR_TOKEN_EXPIRATION_TIME}
      />
    </Box>
  );
};

export default Masterpass;
