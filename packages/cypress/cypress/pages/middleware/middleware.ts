export const MIDDLEWARE_CONSTANTS = {
  //Mandatory fields
  predefinedMerchants: 'select[id="predefined-merchants"]',
  secretKey: 'input[id="secret_key"]',
  "authentication.entityId": 'input[id="id_entityId"]',
  amount: 'input[id="id_amount"]',
  currency: 'input[id="id_currency"]',
  paymentType: 'input[id="id_paymentType"]',
  shopperResultUrl: 'input[id="id_shopperResultUrl"]',
  merchantTransactionId: 'input[id="id_merchantTransactionId"]',
  signature: 'input[id="id_x_signature"]',
  nonce: 'input[id="id_nonce"]',

  //Optional fields
  notificationUrl: 'input[id="id_notificationUrl"]',
  cancelUrl: 'input[id="id_cancelUrl"]',
  merchantInvoiceId: 'input[id="id_merchantInvoiceId"]',
  transactionDescription: 'input[id="id_transactionDescription"]',

  ////Customer object
  customerMerchantCustomerId: 'input[id="id_customer.merchantCustomerId"]',
  customerGivenName: 'input[id="id_customer.givenName"]',
  customerSurname: 'input[id="id_customer.surname"]',
  customerMobile: 'input[id="id_customer.mobile"]',
  customerEmail: 'input[id="id_customer.email"]',
  customerStatus: 'input[id="id_customer.status"]',
  customerMerchantCustomerLanguage:
    'input[id="id_customer.merchantCustomerLanguage"]',

  ////Billing object
  billingStreet1: 'input[id="id_billing.street1"]',
  billingStreet2: 'input[id="id_billing.street2"]',
  billingCity: 'input[id="id_billing.city"]',
  billingCompany: 'input[id="id_billing.company"]',
  billingCountry: 'input[id="id_billing.country"]',
  billingState: 'input[id="id_billing.state"]',
  billingPostcode: 'input[id="id_billing.postcode"]',
  //////Billing Customer object
  billingCustomerFax: 'input[id="id_billing.customer.fax"]',
  billingCustomerTaxId: 'input[id="id_billing.customer.taxId"]',
  billingCustomerTaxType: 'input[id="id_billing.customer.taxType"]',
  billingCustomerGivenName: 'input[id="id_billing.customer.givenName"]',
  billingCustomerSurname: 'input[id="id_billing.customer.surname"]',
  billingCustomerPhone: 'input[id="id_billing.customer.phone"]',
  ////Billing object again
  billingHouseNumber1: 'input[id="id_billing.houseNumber1"]',

  ////Shipping object
  shippingStreet1: 'input[id="id_shipping.street1"]',
  shippingStreet2: 'input[id="id_shipping.street2"]',
  shippingCity: 'input[id="id_shipping.city"]',
  shippingCompany: 'input[id="id_shipping.company"]',
  shippingCountry: 'input[id="id_shipping.country"]',
  shippingState: 'input[id="id_shipping.state"]',
  shippingPostcode: 'input[id="id_shipping.postcode"]',
  //////Shipping Customer object
  shippingCustomerFax: 'input[id="id_shipping.customer.fax"]',
  shippingCustomerTaxId: 'input[id="id_shipping.customer.taxId"]',
  shippingCustomerTaxType: 'input[id="id_shippingg.customer.taxType"]',
  shippingCustomerGivenName: 'input[id="id_shipping.customer.givenName"]',
  shippingCustomerSurname: 'input[id="id_shipping.customer.surname"]',
  shippingCustomerPhone: 'input[id="id_shipping.customer.phone"]',
  ////Shipping object again
  shippingHouseNumber1: 'input[id="id_shipping.houseNumber1"]',

  ////Cart object
  cartTax: 'input[id="id_cart.tax"]',
  cartShippingAmount: 'input[id="id_cart.shippingAmountcart.shippingAmount"]',
  cartDiscount: 'input[id="id_cart.discount"]',

  plugin: 'input[id="id_plugin"]',
  defaultPaymentMethod: 'input[id="id_defaultPaymentMethod"]',
  forceDefaultMethod: 'input[id="id_forceDefaultMethod"]',
  createRegistration: 'input[id_createRegistration"]',

  ////CustomParameters
  customerParameterKey: `input[id="id_customParametersKey1"]`,
  customerParameterValue: `input[id="id_customParametersValue1"]`,
  addCustomerParametersButton: `input[id="add-custom-param"]`,
};

export interface MiddlewareObject {
  predefinedMerchants?: string;
  secretKey?: string;
  "authentication.entityId"?: string;
  amount?: string;
  currency?: string;
  paymentType?: string;
  shopperResultUrl?: string;
  merchantTransactionId?: string;

  notificationUrl?: string;
  cancelUrl?: string;
  merchantInvoiceId?: string;
  transactionDescription?: string;

  customerMerchantCustomerId?: string;
  customerGivenName?: string;
  customerSurname?: string;
  customerMobile?: string;
  customerEmail?: string;
  customerStatus?: string;
  customerMerchantCustomerLanguage?: string;

  billingStreet1?: string;
  billingStreet2?: string;
  billingCity?: string;
  billingCompany?: string;
  billingCountry?: string;
  billingState?: string;
  billingPostcode?: string;
  billingCustomerFax?: string;
  billingCustomerTaxId?: string;
  billingCustomerTaxType?: string;
  billingCustomerGivenName?: string;
  billingCustomerSurname?: string;
  billingCustomerPhone?: string;
  billingHouseNumber1?: string;

  shippingStreet1?: string;
  shippingStreet2?: string;
  shippingCity?: string;
  shippingCompany?: string;
  shippingCountry?: string;
  shippingState?: string;
  shippingPostcode?: string;
  shippingCustomerFax?: string;
  shippingCustomerTaxId?: string;
  shippingCustomerTaxType?: string;
  shippingCustomerGivenName?: string;
  shippingCustomerSurname?: string;
  shippingCustomerPhone?: string;
  shippingHouseNumber1?: string;

  cartTax?: string;
  cartShippingAmount?: string;
  cartDiscount?: string;

  plugin?: string;
  defaultPaymentMethod?: string;
  forceDefaultMethod?: string;
  createRegistration?: string;

  buttonId?: string;
}