const oldWayOfProcessingObject = (postData) => {
  let postFormInputs = "";

  // eslint-disable-next-line no-restricted-syntax
  for (const key in postData) {
    // eslint-disable-next-line no-prototype-builtins
    if (postData.hasOwnProperty(key)) {
      postFormInputs += `<input type="hidden" name="${key}" value="${postData[key]}"/>`;
    }
  }

  return postFormInputs;
};

const newWayOfProcessingObject = (postData) => {
  let postFormInputs = "";
  Object.keys(postData).forEach((key) => {
    postFormInputs += `<input type="hidden" name="${key}" value="${postData[key]}"/>`;
  });

  return postFormInputs;
};

const postData = {
  name: "Carl",
  surname: "Cillers",
};

describe("Test the object mapper for hidden element creation", () => {
  it(`should create the same string vales of hidden elements using: ${JSON.stringify(
    postData
  )}`, () => {
    expect(oldWayOfProcessingObject(postData)).toEqual(
      newWayOfProcessingObject(postData)
    );
  });
});
