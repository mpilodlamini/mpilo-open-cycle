import CONSTANTS from "../../../../constantsUtil";
import { When, Then } from "cypress-cucumber-preprocessor/steps";

const onCardSubmitScenario = (scenario) => {
  const cardSubmitScenarios = {
    success: CONSTANTS.PAYMENT_SUCCESS_HEADER,
    unavailable: CONSTANTS.NATIVE_CARD.NUMBERS.UNAVAILABLE,
    notEnrolled: CONSTANTS.NATIVE_CARD.NUMBERS.NOT_ENROLLED_3D,
    notFound: CONSTANTS.NATIVE_CARD.NUMBERS.CARD_HOLDER_NOT_FOUND,
    invalidPayer: CONSTANTS.NATIVE_CARD.NUMBERS.INVALID_PAYER,
    unsupportedDevice: CONSTANTS.NATIVE_CARD.NUMBERS.UNSUPPORTED_USER_DEVICE,
    malformed: CONSTANTS.NATIVE_CARD.NUMBERS.MALFORMED,
  };

  return cardSubmitScenarios[scenario];
};

const onCardSubmitNotification = (notification) => {
  const cardSubmitNotifications = {
    success: CONSTANTS.NATIVE_CARD.NUMBERS.NO_REDIRECT_SUCCESS,
    unavailable: CONSTANTS.NATIVE_CARD.ERROR_MESSAGES.UNAVAILABLE,
    notEnrolled: CONSTANTS.NATIVE_CARD.ERROR_MESSAGES.NOT_ENROLLED_3D,
    notFound: CONSTANTS.NATIVE_CARD.ERROR_MESSAGES.CARD_HOLDER_NOT_FOUND,
    invalidPayer: CONSTANTS.NATIVE_CARD.ERROR_MESSAGES.INVALID_PAYER,
    unsupportedDevice:
      CONSTANTS.NATIVE_CARD.ERROR_MESSAGES.UNSUPPORTED_USER_DEVICE,
    malformed: CONSTANTS.NATIVE_CARD.ERROR_MESSAGES.MALFORMED,
  };

  return cardSubmitNotifications[notification];
};

const enterCardInformation = (cardNumber: string) => {
  cy.intercept(
    CONSTANTS.AJAX_REQUEST_VERB.POST,
    `${Cypress.env("paymentBaseUrl")}/card-payment`
  ).as("cardPaymentRequest");

  cy.get(CONSTANTS.NATIVE_CARD.CARD.NUMBER.ID).clear().type(cardNumber);
  cy.get(CONSTANTS.NATIVE_CARD.CARD.HOLDER.ID)
    .clear()
    .type(CONSTANTS.NATIVE_CARD.CARD.HOLDER.VALUE);
  cy.get(CONSTANTS.NATIVE_CARD.CARD.EXPIRY_DATE.ID)
    .clear()
    .type(CONSTANTS.NATIVE_CARD.CARD.EXPIRY_DATE.VALUE);
  cy.get(CONSTANTS.NATIVE_CARD.CARD.CVV.ID)
    .clear()
    .type(CONSTANTS.NATIVE_CARD.CARD.CVV.VALUE);
  cy.get(CONSTANTS.NATIVE_CARD.CARD.STREET.ID)
    .clear()
    .type(CONSTANTS.NATIVE_CARD.BILLING.STREET1);
  cy.get(CONSTANTS.NATIVE_CARD.CARD.CITY.ID)
    .clear()
    .type(CONSTANTS.NATIVE_CARD.BILLING.CITY);
  cy.get(CONSTANTS.NATIVE_CARD.CARD.POSTAL_CODE.ID)
    .clear()
    .type(CONSTANTS.NATIVE_CARD.BILLING.POSTCODE);
  cy.get(CONSTANTS.NATIVE_CARD.CARD.COUNTRY.ID)
    .type("South Africa")
    .type("{enter}");

  cy.get("button").contains(CONSTANTS.NATIVE_CARD.PAY_NOW_BUTTON).click();
  cy.wait("@cardPaymentRequest"); // TODO: FIX SUBMIT BUTTON ON FE
};

When("I should see that the native card is displayed", () => {
  // enterCardInformation(CONSTANTS.NATIVE_CARD.NUMBERS.NO_REDIRECT_SUCCESS);
  cy.contains(CONSTANTS.NATIVE_CARD.CARD.NUMBER.LABEL).should("be.visible");
  cy.contains(CONSTANTS.NATIVE_CARD.CARD.HOLDER.LABEL).should("be.visible");
  cy.contains(CONSTANTS.NATIVE_CARD.CARD.EXPIRY_DATE.LABEL).should(
    "be.visible"
  );
  cy.contains(CONSTANTS.NATIVE_CARD.CARD.CVV.LABEL).should("be.visible");
  //   enterPinAndVerify(CONSTANTS.MOBICRED.ERROR_PIN);
  //   cy.contains("Payment Failed").should("be.visible");
});

When("I enter card details for payment {string}", (scenario) => {
  enterCardInformation(onCardSubmitScenario(scenario));
});

Then("I should see {string} notification", (notification) => {
  // TODO: REFACTOR
  cy.contains(onCardSubmitNotification(notification)).should("be.visible");
});
