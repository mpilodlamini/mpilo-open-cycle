import React from "react";

import { Box, Grid, Paper } from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useStyles } from "./back-card.styles";

const BackCard = () => {
  const classes = useStyles();
  const isSignatureBp = useMediaQuery("(min-width: 623px)");

  return (
    <Grid container justify="center" className={classes.backCardContainer}>
      <Paper className={classes.backCardPaper}>
        <Grid container direction="column" className={classes.backCard}>
          <Grid className={classes.backCardStrip} item />

          <Box className={classes.backCardWrapper}>
            <Grid
              className={classes.backCardBody}
              container
              item
              alignItems="center"
              spacing={2}
            >
              <Grid item xs={7} sm={8}>
                <Box
                  display={{ xs: "none", sm: "block" }}
                  pl={2}
                  pt={2}
                  pb={2}
                  className={classes.backCardSignature}
                >
                  <Box fontSize="14px">{isSignatureBp && "My"} signature</Box>
                </Box>
              </Grid>

              <Grid item xs={5} sm={4} container justify="center">
                <Box
                  pl={2}
                  pt={2}
                  pb={2}
                  className={classes.backCardCVV}
                  width="56px"
                >
                  123
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Paper>
    </Grid>
  );
};

export default BackCard;
