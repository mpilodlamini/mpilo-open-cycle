import React from "react";
import { Box, Typography, makeStyles } from "@material-ui/core";

export interface MerchantLogoProps {
  alt: string;
  src?: string;
}

const useStyles = makeStyles((theme) => ({
  logo: {
    justifyContent: "center",

    [theme.breakpoints.down("xs")]: {
      justifyContent: "flex-start",
    },
  },
}));

export const MerchantLogo = (props: MerchantLogoProps) => {
  const classes = useStyles();

  return (
    <Box display="flex" className={classes.logo} style={{ minHeight: "36px" }}>
      {props.src ? (
        <img
          height="36"
          width="152"
          src={props.src}
          alt={props.alt}
          title={props.alt}
          data-testid={props.alt}
        />
      ) : (
        <Typography>{props.alt}</Typography>
      )}
    </Box>
  );
};
