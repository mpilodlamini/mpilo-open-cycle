import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import { SecuredBy } from "../../../";

export default {
  title: "Components/Secured By",
} as Meta;

export const Default: Story = () => {
  return <SecuredBy />;
};

Default.args = {};
