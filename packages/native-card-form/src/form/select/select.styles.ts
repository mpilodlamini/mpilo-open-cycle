import { InputLabel, withStyles } from "@material-ui/core";
import {
  border,
  primaryBlue,
  error,
  tertiaryGrey,
} from "@peach/checkout-design-system";

export const CustomInputLabel = withStyles(() => ({
  root: {
    position: "absolute",
    zIndex: 100,
    left: "-6px",
    width: "fit-content",
    paddingLeft: "10px",
    paddingRight: "10px",
    background: "white",
    color: "#0A1F44",
    fontSize: "13px",
    transform: "translate(14px, -6px) scale(0.75)",
  },
}))(InputLabel);

export const getBoxShadowValue = (
  isFocused: boolean,
  isError: boolean
): string => {
  if (!isFocused && !isError) {
    return "";
  }

  let newColor = "transparent";
  if (isError) newColor = "#F03D3D";
  if (isFocused) newColor = "#2684ff";

  return `inset 0 0 0 2px ${newColor}`;
};

/**
 * Custom styling implementation for react-select
 * @see https://react-select.com/styles#provided-styles-and-state
 */
const customStyles = {
  container: (provided: any) => ({
    ...provided,
    fontSize: "12px",
    "@media only screen and (min-width: 600px)": {
      fontSize: "13px",
    },
  }),
  control: (provided: any, state: any) => ({
    ...provided,
    height: "30px", // NOTE: Magic number to match MUI input heights
    borderColor: border,
    color: primaryBlue,
    cursor: "pointer",
    boxShadow: getBoxShadowValue(state.isFocused, state.selectProps.isError),
    ":hover": {
      ...provided[":hover"],
      boxShadow: "inset 0 0 0 2px #2684ff",
    },
    "@media only screen and (min-width: 600px)": {
      height: "52px", // NOTE: Magic number to match MUI input heights
    },
  }),
  indicatorSeparator: (provided: any) => ({
    ...provided,
    display: "none",
  }),
  valueContainer: (provided: any) => ({
    ...provided,
    paddingLeft: "14px",
  }),
  singleValue: (provided: any) => ({
    ...provided,
    color: primaryBlue,
  }),
  placeholder: (provided: any, state: any) => ({
    ...provided,
    color: state.selectProps.isError ? error : primaryBlue,
    fontSize: "13px",
    "@media only screen and (min-width: 600px)": {
      color: tertiaryGrey,
    },
  }),
  menu: (provided: any) => ({
    ...provided,
  }),
  dropdownIndicator: (provided: any) => ({
    ...provided,
    color: primaryBlue,
  }),
};

export default customStyles;
