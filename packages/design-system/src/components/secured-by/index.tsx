import React from "react";
import { Box } from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";
import { bold, primaryBlue } from "../../variables";

import useStyles from "./secured-by.styles";

export const SecuredBy = () => {
  const classes = useStyles();

  return (
    <Box
      component="span"
      fontWeight={bold}
      fontSize={12}
      color="text.disabled"
      display="flex"
      flexWrap="end"
      justifyContent="center"
    >
      <Box
        component="span"
        lineHeight={2}
        display={{ xs: "none", sm: "block", md: "block" }}
      >
        Secured by{" "}
        <Box
          data-testid="secured-by-peach-payments"
          component="span"
          className={classes.highlight}
        >
          Peach Payments
        </Box>
      </Box>
      <Box
        component="span"
        lineHeight={2}
        display={{ xs: "block", sm: "none", md: "none" }}
      >
        Secured
      </Box>
      <Box component="span" className={classes.icon} ml={1}>
        <LockIcon fontSize="small" style={{ color: primaryBlue }} />
      </Box>
    </Box>
  );
};
