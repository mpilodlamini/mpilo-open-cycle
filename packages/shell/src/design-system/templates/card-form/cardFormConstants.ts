export const NUMBER_FIELD_NAME = "number";
export const HOLDER_NAME_FIELD_NAME = "holderName";
export const EXPIRY_DATE_FIELD_NAME = "expiryDate";
export const CVV_FIELD_NAME = "cvv";
export const STREET_FIELD_NAME = "street";
export const CITY_FIELD_NAME = "city";
export const POSTAL_CODE_FIELD_NAME = "postalCode";
export const COUNTRY_FIELD_NAME = "country";
