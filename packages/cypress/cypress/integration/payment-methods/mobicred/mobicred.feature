Feature: Mobicred
    In order to make a purchase
    As a customer
    I want to pay using my mobicred account

    Background: Launch Mobicred modal and login
        Given I Generate Checkout via API using default values
        When I click on "mobicred-btn"
        Then I should see that the element with data-testid "mobicred-login-screen" contains the text "Please sign in to your Mobicred account." to "be.visible"
         And I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"
        When I enter "mail@mail.com" on the field "mobicred-email"
        And I enter "1234" on the field "mobicred-password"
        Then I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"
        When I click on "mobicred-submit-login"
        Then I should see the text "Please enter your OTP"
        And I should see that the "mobicred-otp-submit-verify" button is disabled
        And I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"

    Scenario: Resend OTP and remain on Mobicred otp screen
        When I click on "mobicred-otp-resend-otp"
        Then I should see the text "Please enter your OTP"
        And I should see that the "mobicred-otp-submit-verify" button is disabled
        And I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"


    Scenario: Resend OTP and remain on Mobicred otp screen
        When I click on "mobicred-otp-resend-otp"
        Then I should see the text "Please enter your OTP"
        When I click on "mobicred-otp-resend-otp"
        Then I should see the text "Please enter your OTP"
        When I click on "mobicred-otp-resend-otp"
        Then I should see the text "Please enter your OTP"
        When I click on "mobicred-otp-resend-otp"
        Then I should see the text "Please enter your OTP"
        When I click on "mobicred-otp-resend-otp"
        Then I should see the text "Please enter your OTP"
        When I click on "mobicred-otp-resend-otp"
        Then I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"
        And I should see "warning" containing the text "Card limit has been reached. Please try another card or go back and select another payment method."
        
    
    Scenario: Pending Payment Screen after verifying otp
        When I enter "111111" on the field "mobicred-otp-input"
        And I click on "mobicred-otp-submit-verify"
        Then I should see "pending-feedback-notification" containing the text "Payment Pending"
        And I should see "pending-feedback-notification" containing the text "Return to Store"
        And I should see that the element with data-testid "display-label-go-back" to "not.exist"

    Scenario: Logout of Mobicred
        When I click on "mobicred-otp-different-account-signin"
        Then I should see that the element with data-testid "mobicred-login-screen" contains the text "Please sign in to your Mobicred account." to "be.visible"
        And I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"
       
    Scenario: Payment fails when invalid OTP is entered
        When I enter "654321" on the field "mobicred-otp-input"
        And I click on "mobicred-otp-submit-verify"
        Then I should see "warning" containing the text "Payment Declined"
        And I should see "warning" containing the text "Verification has failed. Please try again or go back and select another payment method."
        And I should see "warning" containing the text "Go Back"
        And I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"

        Scenario: Enter OTP and verify to make successful payment
        When I enter "123456" on the field "mobicred-otp-input"
        And I click on "mobicred-otp-submit-verify"
        Then I should see "success-feedback-notification" containing the text "Payment Successful"
        And I should see "success-feedback-notification" containing the text "Return to Store"
        And I should see that the element with data-testid "display-label-go-back" to "not.exist"

        Scenario: Enter OTP and verify to make successful payment
        When I enter "222222" on the field "mobicred-otp-input"
        And I click on "mobicred-otp-submit-verify"
        Then I should see "success-feedback-notification" containing the text "Payment Successful"
        And I should see "success-feedback-notification" containing the text "Return to Store"
        And I should see that the element with data-testid "display-label-go-back" to "not.exist"
        


        Scenario: Pending Payment Screen after verifying otp with error
        When I enter "777777" on the field "mobicred-otp-input"
        And I click on "mobicred-otp-submit-verify"
        Then I should see "pending-feedback-notification" containing the text "Payment Pending"
        And I should see "pending-feedback-notification" containing the text "Return to Store"
        And I should see that the element with data-testid "display-label-go-back" to "not.exist"

        


