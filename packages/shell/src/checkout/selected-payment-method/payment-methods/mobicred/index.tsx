import React from "react";
import {
  PAYMENT_METHOD_ENUMS,
  PAYMENT_METHOD_NAMES,
} from "../../../../utils/pluginConstants";
import { GA } from "../../../../utils/googleAnalyticsConstants";
import {
  AJAX_DATA_RESULT,
  PEACH_STATE,
  TRANSACTION_STATUS,
} from "../../../../utils/constants";
import { BUTTON_LABELS, MESSAGING } from "../../../../utils/messagingConstants";
import CheckoutService from "../../../../services/checkoutService";
import PluginManager from "../../../../utils/pluginManager";
import MessagesUtils from "../../../../utils/messagesUtil";
import FeedbackPage from "../shared/FeedbackPage";
import LoaderModal from "../../../../components/loaders/loadingModal";
import LoadingScreen from "../../../../components/loaders/loadingScreen";
import MobicredLoginScreen from "../../../../design-system/templates/mobicred/screen/mobicredLoginScreen";
import MobicredOtp from "../../../../design-system/templates/mobicred/otp/mobicredOtp";
import ErrorModal from "../../../../components/errors/modals/errorModal";
import { printInfo } from "../../../../utils/logger";
import TimeUtil from "../../../../utils/timeUtil";
import { CheckoutConfig } from "../../../../contexts/CheckoutContext";
import GoogleAnalyticsUtil from "../../../../utils/googleAnalyticsUtil";
import { SelectedPaymentResponse } from "../../SelectedPayment.interface";

export interface MobicredContainerProps {
  timeUtil: TimeUtil;

  isDefaultAndForced: boolean;

  setCancelVisibility: (state: boolean) => void;
  resetSelected: () => void;

  checkout: CheckoutConfig;
  googleAnalytics: GoogleAnalyticsUtil;
}

export interface MobicredContainerState {
  showLoadingScreen: boolean;
  showLoadingModal: boolean;
  showErrorModal: boolean;
  showFeedbackPage: boolean;
  showMobicredOtpScreen: boolean;

  loadingModalHeading: string;
  loadingModalMessage: string;

  isErrorModalTimed: boolean;
  errorModalHeading: string;
  errorModalMessage: string;
  errorModalActionButton: string;
  errorModalBackButton: string;

  code: string;
  redirectPostData: object;
  redirectUrl: string;
  status: string;

  email: string;
  password: string;
  cellphoneNumber: string;
  transactionId: string;
}

const componentName = "MobicredContainer";

const messagesUtils: MessagesUtils = new MessagesUtils();
const errorMessages = messagesUtils.getErrorMessages();

export class MobicredContainer extends React.Component<
  MobicredContainerProps,
  MobicredContainerState
> {
  private readonly pluginManager: PluginManager = new PluginManager();

  private readonly checkoutService: CheckoutService = new CheckoutService();

  private readonly _loadingScreen = (
    <LoadingScreen widgetName={PAYMENT_METHOD_NAMES.MOBICRED} />
  );

  constructor(props: MobicredContainerProps) {
    super(props);

    this.state = {
      showLoadingScreen: true,
      showLoadingModal: false,
      showErrorModal: false,
      showMobicredOtpScreen: false,
      showFeedbackPage: false,

      loadingModalHeading: "Signing you in",
      loadingModalMessage:
        "Please wait while we attempt to sign you into your account.",

      isErrorModalTimed: false,
      errorModalHeading: MESSAGING.DEFAULT_ERROR_HEADING,
      errorModalMessage: MESSAGING.DEFAULT_ERROR_MESSAGE,
      errorModalActionButton: BUTTON_LABELS.BLANK_LABEL,
      errorModalBackButton: BUTTON_LABELS.GO_BACK,

      code: "",
      status: "",
      redirectUrl: "",
      redirectPostData: {},

      email: "",
      password: "",
      transactionId: "",
      cellphoneNumber: "",
    };
  }

  componentDidMount = (): void => {
    printInfo(componentName, "componentDidMount", "Mounted");

    const loaderTimeout = setTimeout(() => {
      this.setState({ showLoadingScreen: false });
      clearTimeout(loaderTimeout);
    }, 1000);
  };

  signIn = (): void => {
    const postData = {
      merchant_key: this.props.checkout.merchantKey,
      checkout_id: this.props.checkout.checkoutId,
      payment_brand: PAYMENT_METHOD_ENUMS.MOBICRED,
      accountId: this.state.email,
      password: this.state.password,
    };

    const successCallback = (data: any) => {
      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        this.props.timeUtil.updateCheckoutTimer();
        this.pluginManager.createPollingListener();

        let code: string = "";
        let transactionId: string = "";
        let cellphoneNumber: string = "";

        const responseData = data.response;

        if (responseData) {
          code = responseData.response_data.result.code || "";
          transactionId = responseData.response_data.id || "";
          cellphoneNumber =
            responseData.response_data.resultDetails.ExtendedDescription.split(
              "="
            )[1] || "";
        }

        this.setState({
          showMobicredOtpScreen: true,
          showLoadingModal: false,
          code,
          transactionId,
          cellphoneNumber,
          loadingModalHeading: "Mobicred Loading",
          loadingModalMessage:
            "Please wait while the Mobicred Payment Portal loads",
        });

        this.props.googleAnalytics.trackEvent(
          GA.EVENTS.SIGN_IN,
          PAYMENT_METHOD_NAMES.MOBICRED,
          "success"
        );
        this.props.googleAnalytics.trackPage(
          PAYMENT_METHOD_NAMES.MOBICRED_VERIFICATION
        );
      }
    };

    const errorCallback = (data: any) => {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.SIGN_IN,
        PAYMENT_METHOD_NAMES.MOBICRED,
        "failed"
      );
      this.processErrorFromServer(data);
    };

    this.checkoutService.postFormEncodedContent(
      `${this.props.checkout.baseUrl}payment`,
      postData,
      successCallback,
      errorCallback
    );
  };

  hideLoadingModal = (): void => {
    this.setState({ showLoadingModal: false });
  };

  processErrorFromServer = (data: SelectedPaymentResponse): void => {
    this.hideLoadingModal();

    if (data?.response?.result?.code === "100.400.500") {
      //** If payment is pending. Reproducible using 777777 as otp (please see Switch Mock doc)
      this.props.setCancelVisibility(false);
    }

    let code: string = "900.100";
    let status: string = PEACH_STATE.FAILED;
    let isErrorModalTimed: boolean = false;
    let errorModalActionButton: string = BUTTON_LABELS.BLANK_LABEL;
    let errorModalBackButton: string = BUTTON_LABELS.GO_BACK;
    let showFeedbackPage: boolean = false;
    let showErrorModal: boolean = true;
    let redirectUrl: string = "";

    if (data) {
      const responseDataJSON = data.responseJSON;

      if (responseDataJSON) {
        if (responseDataJSON.response.code) {
          code = responseDataJSON.response.code;
        }

        if (responseDataJSON.response.response_data) {
          code = responseDataJSON.response.response_data.result.code;
        }

        if (
          responseDataJSON.response.result &&
          responseDataJSON.response.result.code
        ) {
          code = responseDataJSON.response.result.code;
        }
      }
      const responseData = data.response;

      if (responseData) {
        if (responseData.response_code) {
          code = responseData.response_code;
        }
        if (responseData.result && responseData.result.code) {
          code = responseData.result.code;
        }
      }
    }

    if (code === "800.110.100") {
      isErrorModalTimed = true;
      errorModalActionButton = BUTTON_LABELS.RETURN_TO_STORE;
      errorModalBackButton = "";
    }

    if (code === "100.400.500") {
      if (this.props.checkout.source === PAYMENT_METHOD_NAMES.WIX) {
        redirectUrl = data?.responseJSON?.response?.redirect_url || "";
      }
      status = PEACH_STATE.PENDING;
      showFeedbackPage = true;
      showErrorModal = false;
    }

    if (code === "000.200.000") {
      this.setState({ showMobicredOtpScreen: false });
    }

    const errorModalHeading: string =
      (errorMessages[code] && errorMessages[code].heading) ||
      MESSAGING.DEFAULT_ERROR_HEADING;
    const errorModalMessage: string =
      (errorMessages[code] && errorMessages[code].message) ||
      MESSAGING.DEFAULT_ERROR_MESSAGE;

    if (messagesUtils.errorHeaders.indexOf(errorModalHeading) !== -1) {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.ERROR,
        PAYMENT_METHOD_NAMES.MOBICRED,
        code
      );
      this.props.googleAnalytics.trackErrorEvents(
        errorModalHeading,
        PAYMENT_METHOD_NAMES.MOBICRED,
        code
      );
    } else {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.ERROR,
        PAYMENT_METHOD_NAMES.MASTERPASS,
        code
      );
    }

    this.setState({
      code,
      status,
      redirectUrl,
      isErrorModalTimed,
      errorModalHeading,
      errorModalMessage,
      errorModalActionButton,
      errorModalBackButton,
      showFeedbackPage,
      showLoadingModal: false,
      showErrorModal,
    });
  };

  verifyOtp = (otp: string): void => {
    const postData = {
      checkout_id: this.props.checkout.checkoutId,
      transaction_id: this.state.transactionId,
      otp,
    };

    const successCallback = (data: any): void => {
      this.setState({ showLoadingModal: false });

      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        let redirectUrl: string = "";
        let redirectPostData: object = {};
        const status: string = PEACH_STATE.SUCCESS;

        if (data && data.response) {
          if (data.response.shopper_result_url) {
            redirectUrl = data.response.shopper_result_url;
            redirectPostData = data.response.redirect_post_data
              ? JSON.parse(data.response.redirect_post_data)
              : "";
          }
          if (data.response.redirect_url) {
            redirectUrl = data.response.redirect_url;
          }
        }

        this.props.setCancelVisibility(false);
        this.setState({
          redirectPostData,
          redirectUrl,
          status,
          showLoadingModal: false,
          showFeedbackPage: true,
        });
      }
    };

    const errorCallback = (data: any): void => {
      this.processErrorFromServer(data);
    };

    this.checkoutService.postFormEncodedContent(
      `${this.props.checkout.baseUrl}verify-otp`,
      postData,
      successCallback,
      errorCallback
    );
  };

  resendOtp = (): void => {
    const postData = {
      checkout_id: this.props.checkout.checkoutId,
      transaction_id: this.state.transactionId,
    };

    const successCallback = (data: any): void => {
      this.setState({ showLoadingModal: false });

      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        this.props.timeUtil.resetShortTermTimer();
        this.props.googleAnalytics.trackEvent(
          GA.EVENTS.OPT_RESEND_MODAL,
          PAYMENT_METHOD_NAMES.MOBICRED
        );
      }
    };
    const errorCallback = (data: any): void => {
      this.processErrorFromServer(data);
    };

    this.checkoutService.postFormEncodedContent(
      `${this.props.checkout.baseUrl}resend-otp`,
      postData,
      successCallback,
      errorCallback
    );
  };

  closeErrorModal = (): void => {
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.ERROR_GO_BACK,
      PAYMENT_METHOD_NAMES.MOBICRED
    );
    if (this.state.code === "000.200.000") {
      this.setState({ showErrorModal: false, showMobicredOtpScreen: true });
      return;
    }
    this.setState({ showErrorModal: false, showMobicredOtpScreen: false });

    if (!this.props.isDefaultAndForced && this.state.code !== "100.380.401") {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.COLLAPSE,
        PAYMENT_METHOD_NAMES.MOBICRED
      );
      this.props.resetSelected();
    }
  };

  submitButtonClickCallback = (email: string, password: string): void => {
    const loadingModalHeading: string = "Signing you in";
    const loadingModalMessage: string =
      "Please wait while we attempt to sign you into your account.";

    this.setState(
      {
        email,
        password,
        loadingModalHeading,
        loadingModalMessage,
        showLoadingModal: true,
      },
      () => {
        this.signIn();
      }
    );
  };

  verifyOtpButtonClickCallback = (otp: string): void => {
    this.props.googleAnalytics.trackPaymentInformation();
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.VERIFY,
      PAYMENT_METHOD_NAMES.MOBICRED,
      "otp"
    );

    const loadingModalHeading: string = "Verifying OTP";
    const loadingModalMessage: string =
      "Please wait while the we verify your Mobicred OTP.";

    this.setState(
      {
        loadingModalHeading,
        loadingModalMessage,
        showLoadingModal: true,
      },
      () => {
        this.verifyOtp(otp);
      }
    );
  };

  resendOtpClickCallback = (): void => {
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.RESEND,
      PAYMENT_METHOD_NAMES.MOBICRED,
      "otp"
    );

    const loadingModalHeading: string = "Resending OTP";
    const loadingModalMessage: string =
      "Please wait while the we send a new Mobicred OTP.";

    this.setState(
      {
        loadingModalHeading,
        loadingModalMessage,
        showLoadingModal: true,
      },
      () => {
        this.resendOtp();
      }
    );
  };

  rerenderLoginClickCallback = () => {
    const loadingModalHeading: string = "Signing you in";
    const loadingModalMessage: string =
      "Please wait while we attempt to sign you into your account.";

    this.setState({
      loadingModalHeading,
      loadingModalMessage,
      showMobicredOtpScreen: false,
    });
  };

  cancelCheckout = (): void => {
    this.redirectLoaderOnAction(TRANSACTION_STATUS.CANCELLED);
  };

  componentWillUnmount = (): void => {
    this.checkoutService.cancelRequest();
    this.pluginManager.stopPollingListener();
  };

  redirectLoaderOnAction = (transactionStatus: string): void => {
    const loadingModalHeading: string = `Redirecting to ${this.props.checkout.merchant}`;
    const loadingModalMessage: string = "Please wait you are being redirected.";

    this.setState({
      loadingModalHeading,
      loadingModalMessage,
      showLoadingModal: true,
    });

    this.pluginManager.updateTransactionStatus(
      transactionStatus,
      this.state.transactionId,
      this.state.code
    );
  };

  render = () => {
    const feedbackPage = (
      <FeedbackPage
        paymentMethod={PAYMENT_METHOD_NAMES.MOBICRED}
        code={this.state.code}
        status={this.state.status}
        isTimed
        redirectUrl={this.state.redirectUrl}
        redirectPostData={this.state.redirectPostData}
      />
    );

    const loadingModal = (
      <LoaderModal
        open={this.state.showLoadingModal}
        title={this.state.loadingModalHeading}
        label={this.state.loadingModalMessage}
      />
    );

    const errorModal = (
      <ErrorModal
        open={this.state.showErrorModal}
        title={this.state.errorModalHeading}
        label={this.state.errorModalMessage}
        continueText={this.state.errorModalActionButton}
        cancelText={this.state.errorModalBackButton}
        onClick={this.cancelCheckout}
        onCancel={this.closeErrorModal}
        timed={this.state.isErrorModalTimed}
      />
    );

    const mobicredLoginScreen = (
      <MobicredLoginScreen onClick={this.submitButtonClickCallback} />
    );

    const mobicredOtpScreen = (
      <MobicredOtp
        cellphoneNumber={this.state.cellphoneNumber}
        onVerifyOtp={this.verifyOtpButtonClickCallback}
        onResendOtp={this.resendOtpClickCallback}
        timeUtil={this.props.timeUtil}
        onReRenderLogin={this.rerenderLoginClickCallback}
      />
    );

    return (
      <>
        {this.state.showLoadingScreen && this._loadingScreen}
        {!this.state.showLoadingScreen &&
          !this.state.showMobicredOtpScreen &&
          mobicredLoginScreen}
        {this.state.showMobicredOtpScreen &&
          !this.state.showFeedbackPage &&
          mobicredOtpScreen}
        {this.state.showFeedbackPage && feedbackPage}
        {loadingModal}
        {errorModal}
      </>
    );
  };
}

export default MobicredContainer;
