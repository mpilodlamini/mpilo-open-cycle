import * as React from "react";
import { TextFieldProps as MuiTextFieldProps } from "@material-ui/core/TextField";
import {
  TextField as MuiTextField,
  InputLabel,
  info,
  border,
} from "@peach/checkout-design-system";
import { FieldProps, getIn } from "formik";
import BaseNumberFormat from "react-number-format";
import {
  makeStyles,
  withStyles,
  Box,
  InputAdornment,
  FormHelperText,
} from "@material-ui/core";

const countryCode: string = "+27";

const flagRSA: any = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="28"
    height="19"
    viewBox="0 0 800 534"
  >
    <rect x="0" y="0" width="100%" height="100%" fill="#000C8A" />
    <rect x="0" y="0" width="100%" height="50%" fill="#E1392D" />
    <path
      d="M 0,0 l 160,0 l 266,178 l 374,0 l 0,178 l -374,0 l -266,178 l -160,0 z"
      fill="#FFFFFF"
    />
    <path
      d="M 0,0 l 97 ,0 l 318,213 l 385,0 l 0,108 l -385,0 l -318,213 l -97,0 z"
      fill="#007847"
    />
    <path d="M 0,65 l 304,202 l -304,202 z" fill="#FFB915" />
    <path d="M 0,107 l 240,160 l -240,160 z" fill="#000000" />
  </svg>
);

export interface TextFieldProps
  extends FieldProps,
    Omit<MuiTextFieldProps, "name" | "value" | "error"> {}

export const CssTextField = withStyles({
  root: {
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: border,
        color: "#53627C",
      },
      "&:hover fieldset": {
        borderColor: info,
      },
      "&.Mui-focused fieldset": {
        borderColor: info,
      },
      "& #otp": {
        textAlign: "center",
        letterSpacing: "4px",
      },
    },
  },
})(MuiTextField);

export const useStyles = makeStyles((theme) => ({
  label: {
    fontSize: "14px",
    fontWeight: 600,
    color: "#53627C",
    marginBottom: "4px",
  },
  sublabel: {
    fontSize: "12px",
    fontWeight: 100,
    color: "#53627C",
    marginBottom: "4px",
  },
  countrycode_border: {
    position: "relative",
    width: "80px",
    minWidth: "80px",
    borderRadius: "4px",
    border: "1px solid #C9CED6",
    marginRight: "4px",
  },
  countrycode_block: {
    position: "absolute",
    display: "flex",
    padding: "18.5px 14px",
    height: "100%",
    boxSizing: "border-box",
    marginTop: "-4px",
  },
  countrycode: {
    fontSize: "12px",
    padding: "1px 0 0 6px",
    margin: "0",
    fontWeight: 600,
    [theme.breakpoints.up("sm")]: {
      fontSize: "13px",
    },
  },
  flag: {
    marginTop: "1px",
  },
}));

export function fieldToTextField({
  disabled,
  field: { onBlur: fieldOnBlur, ...field },
  form: { isSubmitting, touched, errors },
  onBlur,
  helperText,
  ...props
}: TextFieldProps): MuiTextFieldProps {
  const fieldError = getIn(errors, field.name);
  const showError = getIn(touched, field.name) && !!fieldError;
  return {
    variant: props.variant,
    error: showError,
    helperText: showError ? fieldError : helperText,
    disabled: disabled ?? isSubmitting,
    onBlur:
      onBlur ??
      function (e) {
        fieldOnBlur(e ?? field.name);
      },
    ...field,
    ...props,
  };
}

const NumberTextField = (props: any) => (
  <CssTextField {...props} inputProps={{ inputMode: "numeric" }} />
);

const CountryCodeField = () => {
  const classes = useStyles();
  return (
    <div className={classes.countrycode_border}>
      <div className={classes.countrycode_block}>
        <div className={classes.flag}>{flagRSA}</div>
        <div
          data-testid="aplus-details-countrycode"
          className={classes.countrycode}
        >
          {countryCode}
        </div>
      </div>
    </div>
  );
};

export function NumberFormat({
  children,
  label,
  sublabel,
  countrycode,
  ...props
}: TextFieldProps & {
  sublabel: string;
  countrycode: boolean;
}) {
  const { helperText, ...innerProps } = fieldToTextField(props);
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column" gridGap="0.25rem">
      {label && (
        <InputLabel error={innerProps.error} className={classes.label}>
          {label}
        </InputLabel>
      )}
      {sublabel && (
        <InputLabel error={innerProps.error} className={classes.sublabel}>
          {sublabel}
        </InputLabel>
      )}

      <Box
        display="flex"
        flexDirection="row"
        gridGap="0.25rem"
        position="relative"
      >
        {countrycode && <BaseNumberFormat customInput={CountryCodeField} />}

        {/* @ts-ignore */}
        <BaseNumberFormat {...innerProps} customInput={NumberTextField} />
      </Box>
      {helperText && (
        <FormHelperText error={innerProps.error}>{helperText}</FormHelperText>
      )}
    </Box>
  );
}

export function TextField({ children, ...props }: TextFieldProps) {
  // @ts-ignore
  return <CssTextField {...fieldToTextField(props)}>{children}</CssTextField>;
}

TextField.displayName = "FormikMaterialUITextField";
