import React, { useContext } from "react";
import {
  Box,
  Collapse,
  Grid,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { SecuredBy } from "@peach/checkout-design-system/dist/components/secured-by";
import { useStyles } from "./payment-method.styles";
import { DisplayLabel } from "./DisplayLabel";
import { SelectedPaymentContext } from "../../contexts/SelectedPaymentContext";
import { StatusContext } from "../../contexts/StatusContext";
import { FeedbackPage } from "./payment-methods/shared/FeedbackPage";
import { CheckoutContext, PaymentMethod } from "../../contexts/CheckoutContext";
import CardContainer from "./payment-methods/card";
import EftSecureContainer from "./payment-methods/eft";
import MasterpassContainer from "./payment-methods/masterpass";
import MobicredContainer from "./payment-methods/mobicred";
import MpesaContainer from "./payment-methods/mpesa";
import OzowContainer from "./payment-methods/ozow";
import { GoogleAnalyticsContext } from "../../contexts/GoogleAnalyticsContext";
import { UtilContext } from "../../contexts/UtilContext";
import { JsonSchemaPlugin } from "./payment-methods/json-schema";
import { IframePlugin } from "./payment-methods/iframe";

export interface SelectedPaymentMethodProps {}

export const SelectedPaymentMethod = () => {
  const config = useContext(CheckoutContext);
  const ga = useContext(GoogleAnalyticsContext);
  const { timeUtil } = useContext(UtilContext);
  const selectedPayment = useContext(SelectedPaymentContext);
  const theme = useTheme();
  const styles = useStyles();
  const isXsBp = useMediaQuery(theme.breakpoints.down("xs"));
  const status = useContext(StatusContext);
  const { present, selected } = useContext(SelectedPaymentContext);
  const paddingBottomBasedOnPaymentSelectedContext = selected ? 0 : 2;
  const phonePaddingBottomBasedOnPaymentSelectedContext = selected ? 8 : 2;

  return (
    <Box
      className={styles.screen}
      pt={2}
      pb={
        isXsBp
          ? phonePaddingBottomBasedOnPaymentSelectedContext
          : paddingBottomBasedOnPaymentSelectedContext
      }
      pl={4}
      pr={4}
      width={1}
    >
      <Collapse
        className={present ? styles.active : styles.inactive}
        in={present}
        collapsedHeight={isXsBp ? "38px" : "50px"}
        timeout={600}
      >
        <Box width={1}>
          <Grid
            container
            alignItems="center"
            style={{ height: isXsBp ? "38px" : "50px" }}
          >
            <Grid item sm={7} xs={7}>
              {!["Success", "Pending"].includes(status.status || "") && (
                <DisplayLabel />
              )}
            </Grid>

            <Grid item sm={5} xs={5}>
              <Box display="flex" justifyContent="flex-end">
                <SecuredBy />
              </Box>
            </Grid>
          </Grid>
          <Box className={styles.scroll} p={0} width={1}>
            {["Success", "Pending", "success", "pending"].includes(
              status.status || ""
            ) ? (
              <FeedbackPage
                redirectUrl={status.data.url}
                redirectPostData={status.data.data}
                paymentMethod={selected?.name}
                transactionId={config.merchantTransactionId}
                code={status.data.code || config.code}
                status={config.status || status.status!}
                isTimed
              />
            ) : (
              <>
                {["CARD", "VISA", "MASTERCARD"].includes(
                  selected?.id.toUpperCase() || ""
                ) && <CardContainer />}
                {selected?.id.toUpperCase() === "EFTSECURE" && (
                  <EftSecureContainer
                    checkout={config}
                    googleAnalytics={ga}
                    timeUtil={timeUtil}
                  />
                )}
                {selected?.id.toUpperCase() === "MASTERPASS" && (
                  <MasterpassContainer
                    checkout={config}
                    googleAnalytics={ga}
                    timeUtil={timeUtil}
                    isDefaultAndForced={
                      !!(selectedPayment.isDefault && selectedPayment.isForced)
                    }
                    resetSelected={selectedPayment.reset}
                    setCancelVisibility={selectedPayment.setCancelVisibility}
                  />
                )}
                {selected?.id.toUpperCase() === "MOBICRED" && (
                  <MobicredContainer
                    checkout={config}
                    googleAnalytics={ga}
                    timeUtil={timeUtil}
                    isDefaultAndForced={
                      !!(selectedPayment.isDefault && selectedPayment.isForced)
                    }
                    resetSelected={selectedPayment.reset}
                    setCancelVisibility={selectedPayment.setCancelVisibility}
                  />
                )}
                {selected?.id.toUpperCase() === "MPESA" && (
                  <MpesaContainer
                    checkout={config}
                    googleAnalytics={ga}
                    timeUtil={timeUtil}
                    isDefaultAndForced={
                      !!(selectedPayment.isDefault && selectedPayment.isForced)
                    }
                    resetSelected={selectedPayment.reset}
                    setCancelVisibility={selectedPayment.setCancelVisibility}
                  />
                )}
                {selected?.id.toUpperCase() === "OZOW" && (
                  <OzowContainer
                    checkout={config}
                    timeUtil={timeUtil}
                    selectedPaymentMethod={
                      selectedPayment.selected?.id.toUpperCase() as PaymentMethod
                    }
                  />
                )}
                {selectedPayment?.selectedPlugin?.type == "json-schema" && (
                  <JsonSchemaPlugin method={selectedPayment.selectedPlugin} />
                )}
                {selectedPayment?.selectedPlugin?.type == "iframe" && (
                  <IframePlugin method={selectedPayment.selectedPlugin} />
                )}
              </>
            )}
          </Box>
        </Box>
      </Collapse>
    </Box>
  );
};
