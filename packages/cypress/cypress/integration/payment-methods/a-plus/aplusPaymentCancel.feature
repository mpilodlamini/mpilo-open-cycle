Feature: A+ Cancel Transactions
    In order to no longer perform an A+ transaction and free my A+ funds
    As a customer
    I want to be able to cancel any payment attempt for my A+ account

    Background: A+ Cancel button on OTP screen
        Given I Generate Checkout via API using default values
        When I click on "aplus-radio-card"

        And I switch to the iframe
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        When I Login to access my A+ account
        And it finishes "loading" in the iframe
        Then I should see the text "One Time Password sent to" in the iframe
        And I should see the text "Please enter your OTP below." in the iframe
        And I should see that the "aplus-otp-btn" button is disabled in the iframe
        And I should see "aplus-otp-timer" containing the text "Session Expires in" in the iframe

        When I click on "display-label-previous"
        Then I should see the text "Cancel Payment?" inside "warning" modal
        And I should see the text "You are about to cancel your payment. Are you sure you want to go back?" inside "warning" modal

    Scenario: Select 'Cancel' to cancel my payment and return to the select payment screen
        When I click the "Cancel" "button" inside "warning" modal
        And it finishes "cancelling" in the iframe
        Then I should see that the element with id "payments-screen" contains the text "Select Payment Method" to "to.visible"

    Scenario: Select 'Go Back' and remain on A+ otp screen
        When I click the "Go Back" "button" inside "warning" modal
        Then I should see the text "One Time Password sent to" in the iframe
        And I should see the text "Please enter your OTP below." in the iframe
        And I should see "aplus-otp-timer" containing the text "Session Expires in" in the iframe


