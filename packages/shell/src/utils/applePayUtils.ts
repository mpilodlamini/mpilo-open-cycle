export default {
  isPaymentAvailable: () =>
    (window as any)?.ApplePaySession?.canMakePayments() || false,
};
