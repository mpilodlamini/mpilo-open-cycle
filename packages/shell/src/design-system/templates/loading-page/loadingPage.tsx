import { Box } from "@material-ui/core";
import {
  FeedbackIcon,
  FeedbackType,
  IconSize,
} from "@peach/checkout-design-system";
import React from "react";
import { white } from "@peach/checkout-design-system";

const LoadingPage = () => {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      width={1}
      style={{ height: "100vh", background: white }}
    >
      <FeedbackIcon feedback={FeedbackType.LOADING} size={IconSize.LARGE} />
    </Box>
  );
};

export default LoadingPage;
