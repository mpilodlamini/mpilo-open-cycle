Feature: Checkout
  Background: Navigate to Checkout
    Given I Generate Checkout via API using default values
        
  Scenario: Checkout should render correctly
    Then I should see the text "Select Payment Method"
    And I should see "header-currency" containing the text "ZAR"
    And I should see "header-total" containing the text "10.00"
    And I should see "banner-cancel" containing the text "Cancel"
    And I should see the text "Secured by"
    And I should see the text "Peach Payments"

  Scenario: Cancel Checkout confirmation should display
    When I click on "banner-cancel"
    Then I should see the text "Cancel Transaction?"
    And I should see the text "You are about to cancel the transaction and return to the merchant. Are you sure you want to do this?"
    And I should see "dialog-cancel" containing the text "Cancel"
    And I should see "dialog-go-back" containing the text "Go Back"

  Scenario: Go Back on Checkout Cancel dialog should just be dismissed
    When I click on "banner-cancel"
    And I click on "dialog-go-back"
    Then I should not see "dialog-go-back"
    And I should see the text "Select Payment Method"
    
  
  Scenario: Cancel Checkout should navigate to ShopperResultUrl
    When I click on "banner-cancel"
    And I click on "dialog-cancel"
    Then I should not see "banner-cancel"
    And I should be navigated to "https://httpbin.org/post" URL

    