import { Box, makeStyles } from "@material-ui/core";
import React from "react";
import { bold } from "../../variables";

export interface TotalProps {
  currency: string;
  total: string;
}

const useStyles = makeStyles(() => ({
  total: {
    textTransform: "uppercase",
    justifyContent: "flex-end",
  },
}));

export const Total = (props: TotalProps) => {
  const classes = useStyles();

  return (
    <Box display="flex" alignItems="center" className={classes.total}>
      <Box
        data-testid="header-currency"
        pr={2}
        color="text.secondary"
        fontSize="caption.fontSize"
        component="span"
      >
        {props.currency}
      </Box>
      <Box
        data-testid="header-total"
        component="span"
        fontWeight={bold}
        fontSize="h6.fontSize"
      >
        {props.total}
      </Box>
    </Box>
  );
};
