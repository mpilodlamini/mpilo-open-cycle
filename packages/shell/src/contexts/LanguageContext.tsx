import { Box } from "@material-ui/core";
import { kMaxLength } from "node:buffer";
import React, { useState, createContext, useContext, useEffect } from "react";

export const LanguageContext = createContext<any>(null);

export function LanguageProvider({ children }) {
  const [userLanguage, setUserLanguage] = useState({
    cancel: "Cancel",
    selectPaymentMethod: "Select Payment Method",
    securedBy: "Secured By",
  });
  let languages: any;
  async function getLanguages(language) {
    languages = await fetch("http://localhost:1337/languages");
    languages = await languages.json();
    setUserLanguage(languages[0][language]);

    // setUserLanguage(languages[0][language]);
    // setUserLanguage(languages[0]["french"]);
  }

  const languageChangeHandler = (e) => {
    getLanguages(e.target.value);
    // setUserLanguage(languages[0]["french"]);
  };

  useEffect(() => {
    getLanguages("english");
  }, []);

  return (
    <>
      <Box>
        <select name="" id="" onChange={languageChangeHandler}>
          <option value="africaans">Africaans</option>
          <option value="english">English</option>
          <option value="french">French</option>
          <option value="portuguese">Portuguese</option>
          <option value="spanish">Spanish</option>
          <option value="zulu">Zulu</option>
        </select>
      </Box>

      <LanguageContext.Provider value={userLanguage}>
        {children}
      </LanguageContext.Provider>
    </>
  );
}
