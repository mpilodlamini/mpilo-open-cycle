import React from "react";
import { AppBar, Box } from "@material-ui/core";
import useStyles from "./header.styles";
import { MerchantLogo, Total } from "../../controls";

export interface HeaderProps {
  currency: string;
  total: string;
  src: string;
  alt: string;
}

export const Header = (props: HeaderProps) => {
  const classes = useStyles();

  return (
    <AppBar position="sticky" className={classes.header} elevation={0}>
      <Box
        display="flex"
        alignItems="center"
        height={1}
        maxWidth={1}
        className={classes.wrapper}
      >
        <Box
          component="span"
          flex="1 0 0px"
          display={{
            xs: "none",
            sm: "block",
            md: "block",
            lg: "block",
          }}
        >
          &nbsp;
        </Box>
        <Box
          component="span"
          flex="1 0 0px"
          justifyContent="center"
          className={classes.logo}
        >
          <MerchantLogo src={props.src} alt={props.alt} />
        </Box>
        <Box justifyContent="flex-end" flex="1 0 0px">
          <Total currency={props.currency} total={props.total} />
        </Box>
      </Box>
    </AppBar>
  );
};

export default Header;
