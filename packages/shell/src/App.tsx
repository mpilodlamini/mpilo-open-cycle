import React, { useContext, useEffect, useState, useReducer } from "react";
import classnames from "classnames";
import { JssProvider } from "react-jss";
import CssBaseline from "@material-ui/core/CssBaseline";
import {} from "@material-ui/core/styles";
import { createGenerateClassName, makeStyles } from "@material-ui/core/styles";
import { Checkout } from "./checkout";
import "./scss/style.scss";
import { PEACH_STATE } from "./utils/constants";
import { isValidBrowser } from "./utils/browser";
import { CHECKOUT_GA_INFORMATION } from "./utils/pluginConstants";
import {
  GoogleAnalyticsContext,
  GoogleAnalyticsProvider,
} from "./contexts/GoogleAnalyticsContext";
import { CheckoutContext, CheckoutProvider } from "./contexts/CheckoutContext";
import { SelectedPaymentProvider } from "./contexts/SelectedPaymentContext";
import { setupBackNavigation } from "./utils/navigation";
import { StatusProvider } from "./contexts/StatusContext";
import { UtilProvider } from "./contexts/UtilContext";
import {
  ThemeProvider,
  BrowserSupportBanner,
} from "@peach/checkout-design-system";
import "./scss/style.scss";
import { LanguageProvider } from "./contexts/LanguageContext";

const generateClassName = createGenerateClassName({
  // @ts-ignore
  dangerouslyUseGlobalCSS: true,
});
export const AppWithProviders = () => {
  return (
    // @ts-ignore
    <JssProvider generateClassName={generateClassName}>
      <ThemeProvider>
        <LanguageProvider>
          <CssBaseline />
          <GoogleAnalyticsProvider>
            <CheckoutProvider>
              <SelectedPaymentProvider>
                <StatusProvider>
                  <UtilProvider>
                    <App />
                  </UtilProvider>
                </StatusProvider>
              </SelectedPaymentProvider>
            </CheckoutProvider>
          </GoogleAnalyticsProvider>
        </LanguageProvider>
      </ThemeProvider>
    </JssProvider>
  );
};

const useStyles = makeStyles(() => {
  return {
    bannerAdjustment: {
      height: "calc(100vh - (10vh + 34px))",
      minHeight: "calc(100vh - (10vh + 34px))",
    },
  };
});

export const App = () => {
  const ga = useContext(GoogleAnalyticsContext);
  const config = useContext(CheckoutContext);
  const styles = useStyles();
  const [showBrowserSupportBanner, setBrowserSupportBanner] = useState(
    !isValidBrowser()
  );

  useEffect(() => {
    ga.trackBeginCheckout(config.amount, config.merchantCurrencyCode);
    ga.setDimensions();
    if (
      !config.defaultPaymentMethod &&
      config.status !== PEACH_STATE.SUCCESS &&
      config.status !== PEACH_STATE.PENDING
    ) {
      ga.trackPage(CHECKOUT_GA_INFORMATION.NAME);
    }
    setupBackNavigation(ga);
  }, [config, ga]);

  return (
    <>
      {showBrowserSupportBanner && (
        <BrowserSupportBanner onClick={() => setBrowserSupportBanner(false)} />
      )}
      <Checkout
        className={classnames({
          [styles.bannerAdjustment]: showBrowserSupportBanner,
        })}
      >
        <></>
      </Checkout>
      <div id="redirectFormContainer" />
    </>
  );
};
