import React, { createContext, ReactNode, useMemo } from "react";
import mockGlobalCheckoutObject from "../utils/mockGlobalCheckoutObject";
import { JSONSchema7 } from "json-schema";
import { CheckoutConfig as BaseCheckoutConfig } from "@peach/checkout-lib";

export type PaymentMethod =
  | "Card"
  | "ApplePay"
  | "OZOW"
  | "Visa"
  | "Mastercard"
  | "Mobicred"
  | "EFTSecure"
  | "Masterpass"
  | "Diners Club"
  | "1ForYou"
  | "mPesa";

export type JSONSchemaMethod = {
  type: "json-schema";
  schema: JSONSchema7;
  destination: string;
  name: string;
  label: string;
  image: string;
  submit_btn: string;
};

export type IFrameMethod = {
  type: "iframe";
  name: string;
  label: string;
  image: string;
  origin: string;
  location: string;
  path: string;
};

export type PluginPaymentMethod = { id: string } & (
  | JSONSchemaMethod
  | IFrameMethod
);

export type CheckoutConfig = BaseCheckoutConfig & {
  defaultPaymentMethod?: PaymentMethod;
  paymentMethods: PaymentMethod[];
  pluginPaymentMethods: PluginPaymentMethod[];
};

// @ts-ignore
export const CHECKOUT = ((window as Window).GLOBAL_CHECKOUT_OBJECT ||
  mockGlobalCheckoutObject) as CheckoutConfig;

export const CheckoutContext = createContext<CheckoutConfig>(CHECKOUT);

export const CheckoutProvider = ({ children }: { children: ReactNode }) => {
  const paymentMethods = useMemo(() => {
    if (CHECKOUT.forceDefaultMethod && CHECKOUT.defaultPaymentMethod) {
      return [CHECKOUT.defaultPaymentMethod];
    }

    return CHECKOUT.paymentMethods || [];
  }, []);

  const config = useMemo(() => {
    const checkoutContent = CHECKOUT || mockGlobalCheckoutObject;
    const defaultPaymentMethod = CHECKOUT.status
      ? (checkoutContent.paymentBrand as PaymentMethod)
      : CHECKOUT.defaultPaymentMethod;

    return Object.freeze({
      ...checkoutContent,
      paymentMethods,
      defaultPaymentMethod,
      pluginPaymentMethods: checkoutContent.pluginPaymentMethods.map(
        (paymentMethod, i) => {
          paymentMethod.id = `${paymentMethod.name}-${i}`;
          return paymentMethod;
        }
      ),
    });
  }, [paymentMethods]);

  return (
    <CheckoutContext.Provider value={config}>
      {children}
    </CheckoutContext.Provider>
  );
};
