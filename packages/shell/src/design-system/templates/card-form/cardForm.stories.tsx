import React from "react";
import { Meta, Story } from "@storybook/react";

import CardForm from "./cardForm";
import { CARD_BRANDS } from "../../../utils/constants";

export default {
  title: "Payments / Card Form",
  component: CardForm,
  argTypes: {
    cardBrands: {
      control: { type: "check", options: CARD_BRANDS },
    },
    setCvvLabel: { action: "clicked" },
  },
  args: {
    cardBrands: CARD_BRANDS,
  },
} as Meta;

const Template: Story<any> = (args) => <CardForm {...args} />;

export const Primary = Template.bind({});
