import { Story } from "@storybook/react/types-6-0";
import React from "react";
import { FeedbackMessage, FeedbackMessageProps } from "../../../";

export default {
  title: "Components / FeedbackMessage",
  component: FeedbackMessage,
};

export const Default: Story<FeedbackMessageProps> = (
  props: FeedbackMessageProps
) => <FeedbackMessage {...props} />;

Default.args = {
  messages: ["Test error message", "another message"],
};
