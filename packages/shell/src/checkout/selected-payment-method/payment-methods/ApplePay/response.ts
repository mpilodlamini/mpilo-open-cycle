export interface APIResponse<T> {
  status?: "success" | "failure";
  response?: T;
  error?: any;
}

export interface Payment {
  redirect_url: string;
  response_code: string;
  response_data: {
    paymentBrand: string;
    result: {
      code: string;
      description: string;
    };
  };
  redirect_post_data: any;
  post_data: any;
}

export interface PaymentsResponse extends APIResponse<Payment> {
  message?: string;
}
