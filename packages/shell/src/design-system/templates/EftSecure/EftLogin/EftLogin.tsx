import React from "react";
import {
  Box,
  Container,
  AppBar,
  Collapse,
  makeStyles,
  Grid,
  Button,
} from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";

import "../../../../scss/style.scss";
import { white, bold, text, primaryBlue } from "../../../../variables";
// NOTE: DO NOT CHANGE CLASSNAMES OR HTML STRUCTURE
// all changes must be affected through scss files

const useStyles = makeStyles((theme) => ({
  wrapper: {
    minHeight: "100vh",

    [theme.breakpoints.down("xs")]: {
      alignItems: "flex-start",
    },
  },
  container: {
    borderRadius: "8px",
    height: "calc(100vh - 10vh)",
    overflow: "hidden",
    boxShadow: "0px 0px 20px 0px rgba(10,31,68,0.1)",
    maxWidth: "764px",
    position: "relative",

    [theme.breakpoints.down("xs")]: {
      height: "calc(100vh - 10vh)",
    },
  },
  logo: {
    justifyContent: "center",

    [theme.breakpoints.down("xs")]: {
      justifyContent: "flex-start",
    },
  },
  total: {
    textTransform: "uppercase",
    justifyContent: "flex-end",
  },
  header: {
    borderBottom: "1px solid #e0e0e8",
    minHeight: "85px",
    zIndex: 25,
  },
  headerWrapper: {
    padding: "0 32px",

    [theme.breakpoints.down("xs")]: {
      padding: "0 16px",
    },
  },
  headerLogo: {
    justifyContent: "center",

    [theme.breakpoints.down("sm")]: {
      justifyContent: "flex-start",
    },
  },
  button: {
    textTransform: "capitalize",
    minWidth: "200px",
    justifyContent: "flex-start",
    padding: "0",

    [theme.breakpoints.down("xs")]: {
      textAlign: "left",
      paddingLeft: "0",
    },
  },
  screen: {
    backgroundColor: white,
    borderTopLeftRadius: "8px",
    borderTopRightRadius: "8px",
    boxShadow: "0 -2px 4px rgba(0,0,0,0.12)",
    zIndex: 999,
    position: "absolute",
    bottom: "0",

    [theme.breakpoints.down("xs")]: {
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
  content: {
    height: "calc((100vh - 10vh) - (24px + 90px))",

    [theme.breakpoints.down("xs")]: {
      height: "calc((100vh - 10vh) - (24px + 90px))",
    },
  },
  scroll: {
    height: "calc((100vh - 10vh) - (85px + 35px + 16px + 16px))",
    overflowY: "auto",
    maxWidth: "100%",
  },
  active: {
    "& .expandIcon": {
      transform: "rotate(180deg)",
      transition: "all 0.8s ease",
    },
  },
  inactive: {
    "& .expandIcon": {
      transition: "all 0.8s ease",
    },
  },
  highlight: {
    color: text,
  },
  icon: {
    width: "20px",
  },
}));

const EftLogin = () => {
  const classes = useStyles();

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      className={classes.wrapper}
    >
      <Container disableGutters={true} className={classes.container}>
        <AppBar position="sticky" className={classes.header} elevation={0}>
          <Box
            display="flex"
            alignItems="center"
            height={1}
            maxWidth={1}
            className={classes.headerWrapper}
            style={{ minHeight: "85px" }}
          >
            <Box
              component="span"
              flexGrow={2}
              display={{
                xs: "none",
                sm: "block",
                md: "block",
                lg: "block",
              }}
            >
              &nbsp;
            </Box>
            <Box
              component="span"
              flexGrow={1}
              justifyContent="center"
              className={classes.headerLogo}
            >
              <Box
                display="flex"
                className={classes.logo}
                style={{ minHeight: "36px" }}
              >
                <img
                  height="36"
                  width="152"
                  src="https://www.peachpayments.com/assets/images/peach-payments-logo.svg"
                  alt="img"
                />
              </Box>
            </Box>
            <Box justifyContent="flex-end" flexGrow={1}>
              <Box display="flex" alignItems="center" className={classes.total}>
                <Box
                  pr={2}
                  color="text.secondary"
                  fontSize="caption.fontSize"
                  component="span"
                >
                  ZAR
                </Box>
                <Box component="span" fontWeight={bold} fontSize="h6.fontSize">
                  299.99
                </Box>
              </Box>
            </Box>
          </Box>
        </AppBar>
        <Box className={classes.screen} p={2} pl={4} pr={4} width={1}>
          <Collapse
            className={classes.active}
            in={true}
            collapsedHeight="50px"
            timeout={600}
          >
            <Box className={classes.content} width={1}>
              <Box display="flex" alignItems="center" height="50px">
                <Grid container alignItems="center">
                  <Grid item sm={7} xs={7}>
                    <Box display="flex" alignItems="center">
                      <Button className={classes.button} disableElevation>
                        <ExpandLessIcon
                          style={{ marginLeft: "-5px" }}
                          className="expandIcon"
                        />
                        <b>Go Back</b>
                      </Button>
                    </Box>
                  </Grid>
                  <Grid item sm={5} xs={5}>
                    <Box display="flex" justifyContent="flex-end">
                      <Box
                        component="span"
                        fontWeight={bold}
                        fontSize={12}
                        color="text.disabled"
                        display="flex"
                        flexWrap="end"
                        justifyContent="center"
                      >
                        <Box
                          component="span"
                          lineHeight={2}
                          display={{ xs: "none", sm: "block", md: "block" }}
                        >
                          Secured by{" "}
                          <Box component="span" className={classes.highlight}>
                            Peach Payments
                          </Box>
                        </Box>
                        <Box
                          component="span"
                          lineHeight={2}
                          display={{ xs: "block", sm: "none", md: "none" }}
                        >
                          Secured
                        </Box>
                        <Box component="span" className={classes.icon} ml={1}>
                          <LockIcon
                            fontSize="small"
                            style={{ color: primaryBlue }}
                          />
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>

              <Box className={classes.scroll} p={2} width={1}>
                {/* Content goes here */}
                <div className="inner-content eft" id="eft">
                  <div className="eft_main">
                    <div className="eft_content">
                      <div className="eft_header">
                        <img
                          src="https://hosted-payment-page-demo-assets.s3.amazonaws.com/images/absa.png"
                          className="eft_logo"
                          alt=""
                        />
                      </div>
                      <p id="eftx-title">Login</p>
                      <p id="eftx-subtitle">
                        Login using your Absa internet banking profile.
                        <span className="eftx-note">
                          None of your credentials will be stored. We simply
                          facilitate the transaction on your behalf to confirm
                          the transaction instantly.
                        </span>
                      </p>

                      <form id="eftx-form" style={{ display: "block" }}>
                        <label>Access Account Number</label>
                        <input
                          type="text"
                          name="account_number"
                          value=""
                          placeholder="Enter anything..."
                          className="eftx-input eftx-account_number"
                          style={{
                            backgroundImage:
                              "url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAkCAYAAADo6zjiAAAAAXNSR0IArs4c6QAAAbNJREFUWAntV8FqwkAQnaymUkpChB7tKSfxWCie/Yb+gbdeCqGf0YsQ+hU95QNyDoWCF/HkqdeiIaEUqyZ1ArvodrOHxanQOiCzO28y781skKwFW3scPV1/febP69XqarNeNTB2KGs07U3Ttt/Ozp3bh/u7V7muheQf6ftLUWyYDB5yz1ijuPAub2QRDDunJsdGkAO55KYYjl0OUu1VXOzQZ64Tr+IiPXedGI79bQHdbheCIAD0dUY6gV6vB67rAvo6IxVgWVbFy71KBKkAFaEc2xPQarXA931ot9tyHphiPwpJgSbfe54Hw+EQHMfZ/msVEEURjMfjCjbFeG2dFxPo9/sVOSYzxmAwGIjnTDFRQLMQAjQ5pJAQkCQJ5HlekeERxHEsiE0xUUCzEO9AmqYQhiF0Oh2Yz+ewWCzEY6aYKKBZCAGYs1wuYTabKdNNMWWxnaA4gp3Yry5JBZRlWTXDvaozUgGTyQSyLAP0dbb3DtQlmcan0yngT2ekE9ARc+z4AvC7nauh9iouhpcGamJeX8XF8MaClwaeROWRA7nk+tUnyzGvZrKg0/40gdME/t8EvgG0/NOS6v9NHQAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%",
                          }}
                        />

                        <label>Pin</label>
                        <input
                          type="password"
                          placeholder="Enter anything..."
                          name="pin"
                          className="eftx-input eftx-pin"
                          style={{
                            backgroundImage:
                              "url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAkCAYAAADo6zjiAAAAAXNSR0IArs4c6QAAAbNJREFUWAntV8FqwkAQnaymUkpChB7tKSfxWCie/Yb+gbdeCqGf0YsQ+hU95QNyDoWCF/HkqdeiIaEUqyZ1ArvodrOHxanQOiCzO28y781skKwFW3scPV1/febP69XqarNeNTB2KGs07U3Ttt/Ozp3bh/u7V7muheQf6ftLUWyYDB5yz1ijuPAub2QRDDunJsdGkAO55KYYjl0OUu1VXOzQZ64Tr+IiPXedGI79bQHdbheCIAD0dUY6gV6vB67rAvo6IxVgWVbFy71KBKkAFaEc2xPQarXA931ot9tyHphiPwpJgSbfe54Hw+EQHMfZ/msVEEURjMfjCjbFeG2dFxPo9/sVOSYzxmAwGIjnTDFRQLMQAjQ5pJAQkCQJ5HlekeERxHEsiE0xUUCzEO9AmqYQhiF0Oh2Yz+ewWCzEY6aYKKBZCAGYs1wuYTabKdNNMWWxnaA4gp3Yry5JBZRlWTXDvaozUgGTyQSyLAP0dbb3DtQlmcan0yngT2ekE9ARc+z4AvC7nauh9iouhpcGamJeX8XF8MaClwaeROWRA7nk+tUnyzGvZrKg0/40gdME/t8EvgG0/NOS6v9NHQAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;",
                            cursor: "auto",
                          }}
                        />

                        <label>User number</label>
                        <input
                          type="text"
                          name="user_number"
                          placeholder="Enter anything..."
                          value="1"
                          className="eftx-input eftx-user_number"
                          style={{
                            backgroundImage:
                              "url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAkCAYAAADo6zjiAAAAAXNSR0IArs4c6QAAAbNJREFUWAntV8FqwkAQnaymUkpChB7tKSfxWCie/Yb+gbdeCqGf0YsQ+hU95QNyDoWCF/HkqdeiIaEUqyZ1ArvodrOHxanQOiCzO28y781skKwFW3scPV1/febP69XqarNeNTB2KGs07U3Ttt/Ozp3bh/u7V7muheQf6ftLUWyYDB5yz1ijuPAub2QRDDunJsdGkAO55KYYjl0OUu1VXOzQZ64Tr+IiPXedGI79bQHdbheCIAD0dUY6gV6vB67rAvo6IxVgWVbFy71KBKkAFaEc2xPQarXA931ot9tyHphiPwpJgSbfe54Hw+EQHMfZ/msVEEURjMfjCjbFeG2dFxPo9/sVOSYzxmAwGIjnTDFRQLMQAjQ5pJAQkCQJ5HlekeERxHEsiE0xUUCzEO9AmqYQhiF0Oh2Yz+ewWCzEY6aYKKBZCAGYs1wuYTabKdNNMWWxnaA4gp3Yry5JBZRlWTXDvaozUgGTyQSyLAP0dbb3DtQlmcan0yngT2ekE9ARc+z4AvC7nauh9iouhpcGamJeX8XF8MaClwaeROWRA7nk+tUnyzGvZrKg0/40gdME/t8EvgG0/NOS6v9NHQAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;",
                          }}
                        />

                        <input
                          type="hidden"
                          name="step"
                          id="step"
                          value="demo-auth"
                        />
                        <input
                          type="hidden"
                          name="complete_url"
                          id="complete_url"
                          value="https://qaapi.ppay.io/v1/verify/eftsecure/?peach_transaction_id=e13cea99a4244d139d9d9b106568d7c3"
                        />
                      </form>

                      <div id="eftx-button-container">
                        <button
                          type="submit"
                          name="submit"
                          value="1"
                          className="eftx-submit submitBtn"
                          disabled={true}
                        >
                          Login
                        </button>
                        <a
                          className="eftx-cancel-transaction"
                          href="https://qaapi.ppay.io/v1/verify/eftsecure/?peach_transaction_id=e13cea99a4244d139d9d9b106568d7c3"
                          style={{ display: "none" }}
                        >
                          Cancel
                        </a>
                        <a
                          href="https://www.google.com"
                          id="selectAnotherBank"
                          className="select_bank"
                        >
                          Select another bank
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </Box>
            </Box>
          </Collapse>
        </Box>
      </Container>
    </Box>
  );
};

export default EftLogin;
