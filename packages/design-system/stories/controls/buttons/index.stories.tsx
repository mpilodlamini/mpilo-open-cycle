import React from "react";
import {
  CancelButton,
  CancelButtonProps,
  GoBackButton,
  GoBackButtonProps,
  PrimaryButton,
  PrimaryButtonProps,
} from "../../../src/controls/buttons";

export default {
  title: "Controls/Buttons",
  component: PrimaryButton,
  subcomponents: { GoBackButton, CancelButton },

  parameters: {
    componentSubtitle: "Buttons how do they work?",
  },
};

export const Primary = (args: PrimaryButtonProps) => (
  <PrimaryButton {...args} />
);

Primary.args = {
  label: "Click me",
};

Primary.parameters = {
  docs: {
    storyDescription: "We will follow Design Patterns of Material UI",
  },
};

export const GoBack = (args: GoBackButtonProps) => <GoBackButton {...args} />;

GoBack.args = {
  label: "Go Back",
};

GoBack.parameters = {
  docs: {
    storyDescription:
      "This controls the go back functionality within a one page app. This also affects browser back functionality",
  },
};

export const Cancel = (args: CancelButtonProps) => <CancelButton {...args} />;

Cancel.parameters = {
  docs: {
    storyDescription:
      "This cancels the entire transaction, redirecting the user back to Merchant Store",
  },
};
