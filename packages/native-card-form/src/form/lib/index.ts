import { CARD } from "./cards";
import countryList, { CountryType } from "./country-list";
import { formats as spacingFormats } from "./spacing-formats";

export const findCardBrandType = (
  number: string,
  cardBrands: string[],
  regexPatterns: typeof CARD.BRAND_DETECTION = CARD.BRAND_DETECTION
): string => {
  if (
    regexPatterns.AMERICAN_EXPRESS.test(number) &&
    cardBrands.indexOf(CARD.ENUM.AMERICAN_EXPRESS) !== -1
  ) {
    return CARD.ENUM.AMERICAN_EXPRESS;
  }

  if (
    regexPatterns.VISA.test(number) &&
    cardBrands.indexOf(CARD.ENUM.VISA) !== -1
  ) {
    return CARD.ENUM.VISA;
  }

  if (
    regexPatterns.MASTERCARD.test(number) &&
    cardBrands.indexOf(CARD.ENUM.MASTERCARD) !== -1
  ) {
    return CARD.ENUM.MASTERCARD;
  }

  if (
    regexPatterns.DINERS_CLUB.test(number) &&
    cardBrands.indexOf(CARD.ENUM.DINERS_CLUB) !== -1
  ) {
    return CARD.ENUM.DINERS_CLUB;
  }

  return "All Cards";
};

export const getCountryFromValue = (
  selectedValue: string,
  list: CountryType[] = countryList
) => {
  if (!selectedValue || list.length === 0) {
    return "";
  }

  return list.find(({ value }) => value === selectedValue);
};

export const getCardNumberFormat = ({
  selectedCardBrand,
  spacingFormatList = spacingFormats,
  brandNames = CARD.ENUM,
}: {
  selectedCardBrand: string;
  spacingFormatList?: typeof spacingFormats;
  brandNames?: typeof CARD.ENUM;
}): string => {
  const { amex, visa, masterCard, dinersClub, generic } = spacingFormatList;

  const { AMERICAN_EXPRESS, VISA, MASTERCARD, DINERS_CLUB } = brandNames;

  switch (selectedCardBrand) {
    case AMERICAN_EXPRESS:
      return amex.default.format;
    case VISA:
      return visa.default.format;
    case MASTERCARD:
      return masterCard.default.format;
    case DINERS_CLUB:
      return dinersClub.default.format;
    default:
      return generic.default.format;
  }
};

export default {
  findCardBrandType,
};
