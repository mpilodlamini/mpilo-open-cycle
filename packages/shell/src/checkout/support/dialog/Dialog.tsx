import React, { ReactNode } from "react";
import classnames from "classnames";
import { Box, BoxProps } from "@material-ui/core";
import { useStyles } from "./dialog.styles";
import { bold } from "@peach/checkout-design-system/dist/variables";

export interface DialogProps extends BoxProps {
  children?: ReactNode;
  icon: ReactNode;
  subtitle: ReactNode;
}

export const Dialog = ({
  children,
  title,
  subtitle,
  icon,
  className,
  ...props
}: DialogProps) => {
  const styles = useStyles();
  return (
    <>
      <Box
        {...props}
        display="flex"
        flexDirection="column"
        alignItems="flex-start"
        className={classnames(styles.wrapper, className)}
      >
        <Box alignSelf="flex-start" width={1} mb={3}>
          <Box display="flex" justifyContent="center">
            {icon}
          </Box>
        </Box>

        <Box textAlign="center" width={1} mb={2}>
          <Box component="span" className={styles.pageTitle} fontWeight={bold}>
            {title}
          </Box>
        </Box>

        <Box
          className={styles.message}
          fontSize="14px"
          textAlign="center"
          alignSelf="center"
          marginBottom={3}
        >
          {subtitle}
        </Box>

        {children}
      </Box>
    </>
  );
};
