import React, {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { usePrevious } from "../hooks/use-previous";
import {
  CheckoutConfig,
  CheckoutContext,
  PluginPaymentMethod,
} from "./CheckoutContext";

export type SelectedPaymentMethod = {
  id: string;
  name: string;
  label: string;
};

export type CancellationState = "Requested" | "Approved" | "Denied" | "";

export interface SelectedPaymentContextState {
  reset: () => void;
  selected?: SelectedPaymentMethod;
  selectedPlugin?: PluginPaymentMethod;
  select: (paymentMethod?: SelectedPaymentMethod) => void;
  toggle: () => void;
  toggleCancelVisibility: () => void;
  setCancelVisibility: (visibility: boolean) => void;
  previous?: SelectedPaymentMethod;
  present?: boolean;
  isDefault?: boolean;
  cancelVisible: boolean;
  isForced?: boolean;
  confirmGoBack?: boolean;
  setConfirmGoBack: (value: boolean) => void;
  cancellation: {
    state: CancellationState;
    request: () => void;
    approve: () => void;
    deny: () => void;
  };
}

export const SelectedPaymentContext =
  createContext<SelectedPaymentContextState>({
    reset: () => {},
    selected: undefined,
    select: () => {},
    toggle: () => {},
    toggleCancelVisibility: () => {},
    setCancelVisibility: () => {},
    previous: undefined,
    isDefault: false,
    isForced: false,
    cancelVisible: true,
    confirmGoBack: false,
    setConfirmGoBack: () => {},
    cancellation: {
      request: () => {},
      approve: () => {},
      deny: () => {},
      state: "",
    },
  });

export const SelectedPaymentProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const config = useContext(CheckoutContext);
  const [cancelVisible, setCancelVisible] = useState(true);
  const [confirmGoBack, setConfirmGoBack] = useState(false);
  const [selected, select] = useState<SelectedPaymentMethod | undefined>(
    getDefaultPaymentMethod(config)
  );
  const previous = usePrevious(selected);
  const isDefault = useMemo(() => {
    return !!config.defaultPaymentMethod;
  }, [config]);
  const isForced = useMemo(() => config.forceDefaultMethod, [config]);
  const toggle = useCallback(() => select(previous), [select, previous]);
  const toggleCancelVisibility = useCallback(
    () => setCancelVisible((cancelVisible) => !cancelVisible),
    [setCancelVisible]
  );
  const selectedPlugin = useMemo(
    () =>
      config.pluginPaymentMethods.find(
        (m) => m.id.toUpperCase() === selected?.id.toUpperCase()
      ),
    [selected, config]
  );
  const [cancellationState, setCancellationState] =
    useState<CancellationState>("");

  const reset = useCallback(() => {
    setCancellationState("");
    setConfirmGoBack(false);
    setCancelVisible(true);
  }, [setCancellationState, setConfirmGoBack, setCancelVisible]);

  return (
    <SelectedPaymentContext.Provider
      value={{
        reset,
        selected,
        select,
        selectedPlugin,
        toggleCancelVisibility,
        setCancelVisibility: setCancelVisible,
        toggle,
        cancelVisible,
        previous,
        present: !!selected,
        isDefault,
        isForced,
        confirmGoBack,
        setConfirmGoBack,
        cancellation: {
          request: () => setCancellationState("Requested"),
          approve: () => setCancellationState("Approved"),
          deny: () => setCancellationState("Denied"),
          state: cancellationState,
        },
      }}
    >
      {children}
    </SelectedPaymentContext.Provider>
  );
};

function getDefaultPaymentMethod(
  config: CheckoutConfig
): SelectedPaymentMethod | undefined {
  if (!config.defaultPaymentMethod) {
    return undefined;
  }
  // Use plugin methods by default
  // then fallback to normal payment methods.
  return (
    config.pluginPaymentMethods.find(
      (m) => m.name.toUpperCase() === config.defaultPaymentMethod?.toUpperCase()
    ) || {
      id: config.defaultPaymentMethod,
      name: config.defaultPaymentMethod,
      label: config.defaultPaymentMethod,
    }
  );
}
