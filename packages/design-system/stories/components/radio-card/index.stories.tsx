import React, { useState } from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import { RadioCard, RadioCardProps } from "../../../";

export default {
  title: "Components/RadioCard",
} as Meta;

export const Default: Story<RadioCardProps> = (props) => {
  const [checked, setChecked] = useState(false);
  return (
    <RadioCard
      {...props}
      checked={checked}
      onClick={() => setChecked(!checked)}
    />
  );
};

Default.args = {
  id: "something",
  onClick: () => {},
  title: "Hello darkness my old friend",
  label: "Label",
  alt: "Title",
  src: ["/example.png"],
  checked: true,
};
