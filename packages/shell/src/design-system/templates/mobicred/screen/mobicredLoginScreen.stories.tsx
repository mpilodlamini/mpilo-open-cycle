import React from "react";
import MobicredLoginScreen from "./mobicredLoginScreen";

export default {
  title: "Payments / Mobicred login screen",
  parameters: {
    component: MobicredLoginScreen,
    design: {
      type: "figma",
      url:
        "https://www.figma.com/file/UUlUU69A35kmtZf303GbZH/Checkout_Final_V4.5?node-id=0%3A3098",
    },
  },
};

export const Login = (args: any) => <MobicredLoginScreen {...args} />;
