import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useMemo,
} from "react";
import GoogleAnalyticsUtil from "../utils/googleAnalyticsUtil";
import { CheckoutContext } from "./CheckoutContext";

export const GoogleAnalyticsContext = createContext(new GoogleAnalyticsUtil());

export const GoogleAnalyticsProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const config = useContext(CheckoutContext);
  const ga = useMemo(() => {
    const ga = new GoogleAnalyticsUtil();
    return ga;
  }, []);

  useEffect(() => {
    const listener = (e: MessageEvent) => {
      if (
        config.pluginPaymentMethods
          .map((m: any) => m.origin)
          .includes(e.origin) &&
        e.data.message === "push-checkout-events"
      ) {
        e.data.events.forEach((event) => {
          switch (event.type) {
            case "track-page":
              ga.trackPage(event.title);
              break;
            case "track-event":
              ga.trackEvent(
                event.event.action,
                event.event.category,
                event.event.label
              );
              break;
            case "track-purchase":
              ga.trackPurchase(
                event.event.value,
                event.event.currency,
                event.event.transactionId
              );
              break;
            case "track-payment-information":
              ga.trackPaymentInformation();
              break;
            case "track-error-events":
              ga.trackErrorEvents(
                event.event.heading,
                event.event.category,
                event.event.code
              );
          }
        });
      }
    };
    window.addEventListener("message", listener);
    return () => {
      window.removeEventListener("message", listener);
    };
  }, [ga]);

  return (
    <GoogleAnalyticsContext.Provider value={ga}>
      {children}
    </GoogleAnalyticsContext.Provider>
  );
};
