import { Story } from "@storybook/react/types-6-0";
import React from "react";
import { PaymentMethodSelect, PaymentSelectProps } from "../../../";

const emptyFunction = () => {};

export default {
  title: "Components / PaymentMethodSelect",
};

export const Default: Story<PaymentSelectProps> = (
  args: PaymentSelectProps
) => <PaymentMethodSelect {...args} />;

Default.args = {
  title: "Select a payment method",
  items: [
    {
      label: "Ozow",
      id: "ozow",
      alt: "",
      src: ["https://www.merchantequip.com/images/logos/small-visa-mc.gif"],
      onClick: emptyFunction,
      checked: false,
    },
    {
      label: "Mobicred",
      id: "mobicred",
      src: ["https://www.merchantequip.com/images/logos/small-visa-mc.gif"],
      alt: "",
      checked: false,
      onClick: emptyFunction,
    },
    {
      label: "EFTSecure",
      id: "eftsecure",
      src: [""],
      alt: "Visa Card, Mastercard, Amex",
      checked: false,
      onClick: emptyFunction,
    },
  ],
};
