import React from "react";

export interface PostFormInputsProp {
  fieldsCollection: Array<{ name: string; value: string }>;
}

const PostFormInputs = (props: PostFormInputsProp) => {
  return (
    <>
      {props.fieldsCollection.map((currentField) => {
        return (
          <input
            type="text"
            name={currentField.name}
            key={currentField.name}
            value={currentField.value}
          />
        );
      })}
    </>
  );
};

export default PostFormInputs;
