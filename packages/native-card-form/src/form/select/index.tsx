import React, { useMemo } from "react";
import Base from "react-select";
import { Box } from "@material-ui/core";
import customStyles, { CustomInputLabel } from "./select.styles";

export const Select = (props: any) => {
  const {
    isXsBp,
    label,
    isTouched,
    hasValue,
    isError,
    errorMessage,
  }: {
    label: string;
    isXsBp?: boolean;
    isTouched?: boolean;
    hasValue?: boolean;
    isError?: boolean;
    errorMessage?: string;
  } = props;

  const isLabelPresent = useMemo(() => {
    if (errorMessage === "Required") {
      return false;
    }

    return isXsBp && (hasValue || isTouched);
  }, [isTouched, hasValue, isXsBp, errorMessage]);

  return (
    <>
      {isLabelPresent && (
        <Box position="relative">
          <CustomInputLabel error={isError} shrink={isTouched}>
            {errorMessage || label}
          </CustomInputLabel>
        </Box>
      )}

      <Base {...props} isError={isError} styles={customStyles} />
    </>
  );
};
