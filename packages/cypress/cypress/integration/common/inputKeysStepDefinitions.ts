import { When } from "cypress-cucumber-preprocessor/steps";

When("I enter {string} on the field {string}", (inputText, element) => {
  cy.getElementByDataTestId(element).type(inputText);
});
