import React from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";

export interface WithMediaQueryInterface {
  readonly isXsBp?: boolean;
  readonly isSmBp?: boolean;
  readonly isMdBp?: boolean;
  readonly isLgBp?: boolean;
  readonly mediaQuery: any;
}

/**
 * This HOC allows class components to use MUI useMediaQuery hook.
 * More info: https://github.com/mui-org/material-ui/issues/17350
 */
const withMediaQuery = (mediaQueries: Array<string> = []) => (
  Component: typeof React.Component
) => (props: any) => {
  const theme = useTheme();
  const isXsBp = useMediaQuery(theme.breakpoints.down("xs"));
  const isSmBp = useMediaQuery(theme.breakpoints.down("sm"));
  const isMdBp = useMediaQuery(theme.breakpoints.down("md"));
  const isLgBp = useMediaQuery(theme.breakpoints.down("lg"));

  const media: any = {};

  mediaQueries.forEach((mediaQuery, index) => {
    media[index] = useMediaQuery(mediaQuery);
  });

  return (
    <Component
      isXsBp={isXsBp}
      isSmBp={isSmBp}
      isMdBp={isMdBp}
      isLgBp={isLgBp}
      mediaQuery={media}
      {...props}
    />
  );
};

export default withMediaQuery;
