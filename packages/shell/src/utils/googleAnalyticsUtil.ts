import pluginConstants, {
  CHECKOUT_GA_INFORMATION,
  PAYMENT_METHOD_ENUMS,
} from "./pluginConstants";
import { GA } from "./googleAnalyticsConstants";
import ApplePayUtils from "./applePayUtils";
import { printInfo } from "./logger";
import { CHECKOUT } from "../contexts/CheckoutContext";

interface GoogleAnalyticsUtil {
  trackPage(title: string): void;

  trackBeginCheckout(value: string, currency: string): void;

  trackPurchase(value: string, currency: string, transactionId: string): void;

  setCheckoutOption(value: string, paymentMethod: string): void;

  trackPaymentInformation(): void;

  trackEvent(action: string, category: string, label?: string): void;

  trackErrorEvents(heading: string, category: string, code: string): void;
}

const className: string = "GoogleAnalyticsUtil";
const trackingId: string = CHECKOUT.trackingId || "";
// @ts-ignore
const googleAnalytics = window?.gtag || null;

class GoogleAnalyticsUtil implements GoogleAnalyticsUtil {
  trackPage = (title: string): void => {
    printInfo(
      className,
      "trackPage",
      `Logging ${title} page with Google Analytics`
    );
    if (!googleAnalytics) return;

    let processedPage = " ";

    if (title) {
      processedPage = pluginConstants.convertPluginNameToGAStandards(title);
      processedPage =
        processedPage === PAYMENT_METHOD_ENUMS.MOBICRED
          ? PAYMENT_METHOD_ENUMS.MOBICRED_SIGN_IN
          : processedPage;
    }

    googleAnalytics("config", trackingId, {
      page_title: processedPage,
    });
  };

  trackBeginCheckout = (value: string, currency: string): void => {
    if (!googleAnalytics) return;

    printInfo(
      className,
      "trackBeginCheckout",
      `Logging ${value} ${currency} Checkout details with Google Analytics`
    );

    googleAnalytics("event", "begin_checkout", {
      value,
      currency,
    });
  };

  trackPurchase = (
    value: string,
    currency: string,
    transactionId: string
  ): void => {
    if (!googleAnalytics) return;

    printInfo(
      className,
      "trackPurchase",
      `Logging ${value} ${currency} ${transactionId} purchase details with Google Analytics`
    );

    googleAnalytics("event", "purchase", {
      value,
      currency,
      transaction_id: transactionId,
    });
  };

  setCheckoutOption = (value: string, paymentMethod: string): void => {
    if (!googleAnalytics) return;

    printInfo(
      className,
      "setCheckoutOption",
      `Logging ${value} ${paymentMethod} checkout options details with Google Analytics`
    );

    googleAnalytics("event", "set_checkout_option", {
      checkout_step: 1,
      checkout_option: paymentMethod,
      value,
    });
  };

  trackPaymentInformation = (): void => {
    if (!googleAnalytics) return;

    printInfo(
      className,
      "trackPaymentInformation",
      "Logging add_payment_info details with Google Analytics"
    );

    googleAnalytics("event", "add_payment_info");
  };

  trackEvent = (action: string, category: string, label?: string): void => {
    if (!googleAnalytics) return;

    printInfo(
      className,
      "trackEvent",
      `Tracking user event - {${action} ${category} ${label || ""}}`
    );

    const gaLabel = label || " ";
    const value = parseInt(CHECKOUT.amount, 10) || 0;
    const processedCategory = category
      ? pluginConstants.convertPluginNameToGAStandards(category)
      : " ";

    googleAnalytics("event", action, {
      event_category: processedCategory,
      event_label: gaLabel,
      value,
    });
  };

  trackErrorEvents(heading: string, category: string, code: string): void {
    printInfo(className, "trackErrorEvents", "Tracking user errors event");

    switch (heading) {
      case "Login Failed":
        this.trackEvent(GA.EVENTS.FAILED_LOGIN_MODAL, category, code);
        break;
      case "Payment Timeout":
        this.trackEvent(GA.EVENTS.TIME_OUT_MODAL, category, code);
        break;
      case "Payment Error":
        this.trackEvent(GA.EVENTS.ERROR_MODAL, category, code);
        break;
      case "Payment Cancelled":
        this.trackEvent(GA.EVENTS.CANCEL_MODAL, category);
        break;
      case "Payment Declined":
        this.trackEvent(GA.EVENTS.DECLINED_MODAL, category, code);
        break;
      case "OTP Incorrect":
        this.trackEvent(GA.EVENTS.OTP_VERIFY_RETRY_MODAL, category, code);
        break;
      case "OTP Limit Exceeded":
        this.trackEvent(GA.EVENTS.OTP_LIMIT_MODAL, category, code);
        break;
      case "Payment Failed":
        this.trackEvent(GA.EVENTS.FAILED_MODAL, category, code);
        break;
      default:
        this.trackEvent(GA.EVENTS.ERROR, category, code);
    }
  }

  setDimensions = (): void => {
    if (!googleAnalytics) return;
    printInfo(className, "setDimensions", "Setting up Checkout dimensions");
    printInfo(className, "dimension1", CHECKOUT.merchant);
    printInfo(className, "dimension2", CHECKOUT.source);
    printInfo(className, "dimension3", CHECKOUT.merchantTransactionId);
    printInfo(className, "dimension4", `${CHECKOUT.version}-v3`);
    printInfo(className, "dimension6", CHECKOUT.environment || "demo");
    printInfo(
      className,
      "dimension7",
      `${ApplePayUtils.isPaymentAvailable()} `
    );

    googleAnalytics("set", "dimension1", CHECKOUT.merchant);
    googleAnalytics("set", "dimension2", CHECKOUT.source);
    googleAnalytics("set", "dimension3", CHECKOUT.merchantTransactionId);
    googleAnalytics("set", "dimension4", CHECKOUT.version);
    googleAnalytics(
      "set",
      "dimension5",
      pluginConstants.convertPluginNameToGAStandards(
        CHECKOUT.defaultPaymentMethod ||
          CHECKOUT.paymentBrand ||
          CHECKOUT_GA_INFORMATION.NAME
      )
    );
    googleAnalytics("set", "dimension6", CHECKOUT.environment || "demo");
    googleAnalytics("set", "dimension7", ApplePayUtils.isPaymentAvailable());
  };
}

export default GoogleAnalyticsUtil;
