import React, {
  useEffect,
  useState,
  useMemo,
  useCallback,
  useContext,
} from "react";
import NumberFormat from "react-number-format";
import { useFormik } from "formik";
import {
  Box,
  Grid,
  Paper,
  InputAdornment,
  Theme,
  useTheme,
  useMediaQuery,
  Button,
} from "@material-ui/core";
import {
  TextField,
  InputLabel,
  InputFieldErrorMessage,
} from "@peach/checkout-design-system";
import { Select } from "./select";
import { CARD } from "./lib/cards";
import useStyles from "./card-form.styles";
import { cardSchema } from "./lib/card-vaildation";
import {
  findCardBrandType,
  getCardNumberFormat,
  getCountryFromValue,
} from "./lib";
import countryList, { CountryType } from "./lib/country-list";
import { ConfigContext } from "../contexts/ConfigContext";
import OneForYouContainer from "../one-for-you";
import { GoogleAnalyticsContext } from "../../../shell/src/contexts/GoogleAnalyticsContext";

export interface CardPostData {
  brand: string;
  number: string;
  holder: string;
  expiryMonth: string;
  expiryYear: string;
  street1: string;
  city: string;
  postalCode: string;
  country: string;
  cvv: string;
}

const NUMBER_FIELD_NAME = "number";
const HOLDER_NAME_FIELD_NAME = "holderName";
const EXPIRY_DATE_FIELD_NAME = "expiryDate";
const CVV_FIELD_NAME = "cvv";
const STREET_FIELD_NAME = "street";
const CITY_FIELD_NAME = "city";
const POSTAL_CODE_FIELD_NAME = "postalCode";
const COUNTRY_FIELD_NAME = "country";
export interface CardFormProps {
  handleOnSubmit: (parameters: CardPostData) => void;
}

export const CardForm = (props: CardFormProps) => {
  const checkout = useContext(ConfigContext);
  const ga = useContext(GoogleAnalyticsContext);
  const cardBrands = checkout.cardBrands;
  const {
    handleSubmit,
    values,
    errors,
    handleChange,
    handleBlur,
    isSubmitting,
    isValid,
    touched,
    setFieldTouched,
    setFieldValue,
  } = useFormik({
    initialValues: {
      isAmex: false,
      brand: "",
      number: "",
      holderName: "",
      expiryDate: "",
      cvv: "",
      street: checkout.billingStreet1 || "",
      city: checkout.billingCity || "",
      postalCode: checkout.billingPostcode || "",
      country: checkout.billingCountry || "",
    },
    enableReinitialize: true,
    validationSchema: cardSchema(cardBrands),
    onSubmit: async (formValues) => {
      const expiryMonth = formValues.expiryDate.substr(0, 2);
      const expiryYear = formValues.expiryDate.substr(2, 2);

      await props.handleOnSubmit({
        number: formValues.number,
        holder: formValues.holderName,
        brand: formValues.brand,
        expiryMonth,
        expiryYear,
        street1: formValues.street,
        city: formValues.city,
        postalCode: formValues.postalCode,
        country: formValues.country,
        cvv: formValues.cvv,
      });
    },
  });

  // NOTE: handle side effect of card number changing
  const selectedBrand = useMemo(() => {
    return findCardBrandType(values.number, cardBrands);
  }, [values.number, cardBrands]);

  const cardNumberFormat: string = useMemo(
    () =>
      getCardNumberFormat({
        selectedCardBrand: selectedBrand,
      }),
    [selectedBrand]
  );

  const isAmex = useMemo(
    () => selectedBrand === CARD.ENUM.AMERICAN_EXPRESS,
    [selectedBrand]
  );

  const { breakpoints }: Theme = useTheme();
  const classes = useStyles();
  const [isFeedBackModalVisible, setIsFeedBackModalVisible] = useState(false);
  const isXsBp = useMediaQuery(breakpoints.down("xs"));
  const isGreaterThan340bp = useMediaQuery("(min-width:340px)");
  const [cvvLabel, setCvvLabel] = useState("CVV");

  const textFieldSize = useMemo(() => (isXsBp ? "small" : "medium"), [isXsBp]);
  const hasTouchedValues = useMemo(
    () => Object.keys(touched).length > 0,
    [touched]
  );

  const handleToggleFeedBackModalVisibility = useCallback(
    () => setIsFeedBackModalVisible(!isFeedBackModalVisible),
    [isFeedBackModalVisible]
  );

  useEffect(() => {
    setFieldValue("isAmex", isAmex, false);
  }, [isAmex, setFieldValue]);

  useEffect(() => {
    setFieldValue("brand", selectedBrand, false);
  }, [selectedBrand, setFieldValue]);

  useEffect(() => {
    let newCvvLabel: string = "";

    if (isXsBp) {
      if (isGreaterThan340bp) {
        newCvvLabel = "Security Code";
      } else {
        newCvvLabel = "CVV";
      }

      if (errors?.cvv && touched.cvv) {
        newCvvLabel = errors.cvv;
      }
    }

    setCvvLabel(newCvvLabel);
  }, [isXsBp, isGreaterThan340bp, touched.cvv, errors, errors.cvv]);

  const countryPlaceHolderValue = useMemo(() => {
    if (!isXsBp) {
      return "South Africa";
    }

    if (touched.country && errors.country) {
      return errors.country;
    }

    return "Country";
  }, [isXsBp, errors.country, touched.country]);

  const handleOnCountryChange = (
    country: CountryType | null,
    onChangeCB: Function,
    inputName = COUNTRY_FIELD_NAME
  ): void => {
    if (!country) return;

    const { value } = country;
    onChangeCB(inputName)(value);
  };

  /*  MPILO + SUSAN CHANGES*/
  const [canGoBack, setCanGoBack] = useState(true);
  const [showCancelButton, setShowCancelButton] = useState(true);
  const [showBlockingModal, setShowBlockingModal] = useState(false);

  const setCancelVisibility = (): void => {
    setShowCancelButton(!showCancelButton);
  };

  return (
    <>
      {/* <OneForYouContainer
        checkout={checkout}
        googleAnalytics={ga}
        isDefaultAndForced={false}
        timeUtil={}
        setCancelVisibility={setCancelVisibility}
        toggleIsSelected={}
      /> */}
      <form onSubmit={handleSubmit} data-testid="qa-native-card-form">
        <Grid container>
          <Grid item xs={12} container>
            <Paper className={classes.paper} elevation={3}>
              <Box className={classes.brandLogosContainer}>TODO</Box>

              <Box
                pt={!isXsBp ? 3 : 0}
                pr={!isXsBp ? 4 : 0}
                pb={!isXsBp ? 2 : 0}
                pl={!isXsBp ? 4 : 0}
              >
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Box>
                      <InputLabel
                        error={Boolean(touched.number && !!errors.number)}
                        htmlFor={NUMBER_FIELD_NAME}
                      >
                        Card Number
                      </InputLabel>

                      <Box className={classes.textFieldContainer}>
                        <NumberFormat
                          type="tel"
                          customInput={TextField}
                          className={isAmex ? classes.numberFieldIsAmex : ""}
                          onValueChange={({ value }) =>
                            handleChange(NUMBER_FIELD_NAME)(value)
                          }
                          onBlur={handleBlur}
                          error={Boolean(touched.number && !!errors.number)}
                          helperText={
                            touched.number && errors.number && !isXsBp
                              ? errors.number
                              : ""
                          }
                          isNumericString
                          value={values.number}
                          autoComplete="cc-number"
                          id={NUMBER_FIELD_NAME}
                          name={NUMBER_FIELD_NAME}
                          aria-describedby={NUMBER_FIELD_NAME}
                          variant="outlined"
                          data-testid={NUMBER_FIELD_NAME}
                          fullWidth
                          required
                          format={cardNumberFormat}
                          label={
                            touched.number && errors.number && isXsBp
                              ? errors.number
                              : isXsBp && "Card Number"
                          }
                          // TODO: https://github.com/s-yadav/react-number-format/issues/422
                          // @ts-ignore
                          size={textFieldSize}
                        />
                      </Box>
                    </Box>
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <InputLabel
                      error={Boolean(touched.holderName && !!errors.holderName)}
                      htmlFor={HOLDER_NAME_FIELD_NAME}
                    >
                      Card Holder Name
                    </InputLabel>

                    <Box className={classes.textFieldContainer}>
                      <TextField
                        name={HOLDER_NAME_FIELD_NAME}
                        id={HOLDER_NAME_FIELD_NAME}
                        aria-describedby={HOLDER_NAME_FIELD_NAME}
                        variant="outlined"
                        autoComplete="cc-name"
                        placeholder="John Doe"
                        required
                        fullWidth
                        onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
                          const val = (e.target.value || "").replace(
                            /\s+/gi,
                            " "
                          );
                          setFieldValue(HOLDER_NAME_FIELD_NAME, val.trim());
                          handleBlur(e);
                        }}
                        onChange={handleChange}
                        value={values.holderName}
                        size={textFieldSize}
                        inputProps={{
                          autoComplete: "cc-name",
                        }}
                        error={Boolean(
                          touched.holderName && !!errors.holderName
                        )}
                        helperText={
                          !isXsBp ? touched.holderName && errors.holderName : ""
                        }
                        label={
                          isXsBp && touched.holderName && errors.holderName
                            ? errors.holderName
                            : isXsBp && "Card Holder Name"
                        }
                      />
                    </Box>
                  </Grid>

                  <Grid item xs={6} sm={3}>
                    <InputLabel
                      error={Boolean(touched.expiryDate && !!errors.expiryDate)}
                      htmlFor={EXPIRY_DATE_FIELD_NAME}
                    >
                      Expiry Date
                    </InputLabel>

                    <Box className={classes.textFieldContainer}>
                      <NumberFormat
                        type="tel"
                        name={EXPIRY_DATE_FIELD_NAME}
                        id={EXPIRY_DATE_FIELD_NAME}
                        aria-describedby={EXPIRY_DATE_FIELD_NAME}
                        autoComplete="cc-exp"
                        variant="outlined"
                        placeholder="MM / YY"
                        format="## / ##"
                        // TODO: https://github.com/s-yadav/react-number-format/issues/422
                        // @ts-ignore
                        size={textFieldSize}
                        required
                        fullWidth
                        customInput={TextField}
                        onBlur={handleBlur}
                        value={values.expiryDate}
                        onValueChange={({ value }) =>
                          handleChange(EXPIRY_DATE_FIELD_NAME)(value)
                        }
                        error={Boolean(
                          touched.expiryDate && !!errors.expiryDate
                        )}
                        helperText={
                          touched.expiryDate && !isXsBp && errors.expiryDate
                        }
                        label={
                          touched.expiryDate && isXsBp && errors.expiryDate
                            ? errors.expiryDate
                            : isXsBp && "Expiry Date"
                        }
                      />
                    </Box>
                  </Grid>

                  <Grid item xs={6} sm={3}>
                    <InputLabel
                      error={Boolean(!!errors.cvv && touched.cvv)}
                      htmlFor={CVV_FIELD_NAME}
                    >
                      Security Code
                    </InputLabel>

                    <Box className={classes.textFieldContainer}>
                      <NumberFormat
                        customInput={TextField}
                        className={`${isAmex ? classes.cvvFieldIsAmex : ""} ${
                          classes.cvvField
                        }`}
                        onBlur={handleBlur}
                        onValueChange={({ value }) =>
                          handleChange(CVV_FIELD_NAME)(value)
                        }
                        type="tel"
                        name={CVV_FIELD_NAME}
                        id={CVV_FIELD_NAME}
                        aria-describedby={CVV_FIELD_NAME}
                        variant="outlined"
                        autoComplete="cc-csc"
                        required
                        fullWidth
                        value={values.cvv}
                        error={Boolean(touched.cvv && !!errors.cvv)}
                        helperText={
                          touched.cvv && errors.cvv && !isXsBp ? errors.cvv : ""
                        }
                        placeholder={isAmex ? "----" : "---"}
                        format={isAmex ? "####" : "###"}
                        // TODO: https://github.com/s-yadav/react-number-format/issues/422
                        // @ts-ignore
                        size={textFieldSize}
                        label={cvvLabel}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment
                              position="end"
                              className={classes.tooltipIconContainer}
                            >
                              <Box
                                className={classes.tooltipPsuedoButton}
                                onClick={() =>
                                  setIsFeedBackModalVisible(
                                    !isFeedBackModalVisible
                                  )
                                }
                              />
                              TODO Icon
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Box>
                  </Grid>

                  <Grid item xs={12}>
                    <InputLabel
                      error={Boolean(errors.street && touched.street)}
                      htmlFor={STREET_FIELD_NAME}
                    >
                      Street
                    </InputLabel>

                    <Box className={classes.textFieldContainer}>
                      <TextField
                        name={STREET_FIELD_NAME}
                        id={STREET_FIELD_NAME}
                        aria-describedby={STREET_FIELD_NAME}
                        variant="outlined"
                        autoComplete="street-address"
                        placeholder="2 inifinite loop"
                        required
                        fullWidth
                        // TODO could be a re-usable function
                        onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
                          const val = (e.target.value || "").replace(
                            /\s+/gi,
                            " "
                          );
                          setFieldValue(STREET_FIELD_NAME, val.trim());
                          handleBlur(e);
                        }}
                        onChange={handleChange}
                        value={values.street}
                        size={textFieldSize}
                        error={Boolean(touched.street && !!errors.street)}
                        helperText={
                          !isXsBp ? touched.street && errors.street : ""
                        }
                        label={
                          isXsBp && touched.street && errors.street
                            ? errors.street
                            : isXsBp && "Street"
                        }
                      />
                    </Box>
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <InputLabel
                      error={Boolean(!!errors.city && touched.city)}
                      htmlFor={CITY_FIELD_NAME}
                    >
                      City
                    </InputLabel>

                    <Box className={classes.textFieldContainer}>
                      <TextField
                        name={CITY_FIELD_NAME}
                        id={CITY_FIELD_NAME}
                        aria-describedby={CITY_FIELD_NAME}
                        variant="outlined"
                        autoComplete="address-level2"
                        placeholder="Cape Town"
                        required
                        fullWidth
                        onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
                          const val = (e.target.value || "").replace(
                            /\s+/gi,
                            " "
                          );
                          setFieldValue(CITY_FIELD_NAME, val.trim());
                          handleBlur(e);
                        }}
                        onChange={handleChange}
                        value={values.city}
                        size={textFieldSize}
                        error={Boolean(touched.city && !!errors.city)}
                        helperText={!isXsBp ? touched.city && errors.city : ""}
                        label={
                          isXsBp && touched.city && errors.city
                            ? errors.city
                            : isXsBp && "City"
                        }
                      />
                    </Box>
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <InputLabel
                      error={Boolean(!!errors.postalCode && touched.postalCode)}
                      htmlFor={POSTAL_CODE_FIELD_NAME}
                    >
                      Postal Code
                    </InputLabel>

                    <Box className={classes.textFieldContainer}>
                      <TextField
                        name={POSTAL_CODE_FIELD_NAME}
                        id={POSTAL_CODE_FIELD_NAME}
                        aria-describedby={POSTAL_CODE_FIELD_NAME}
                        variant="outlined"
                        autoComplete="postal-code"
                        placeholder="7492"
                        required
                        fullWidth
                        onBlur={(e: React.FocusEvent<HTMLTextAreaElement>) => {
                          const val = (e.target.value || "").replace(
                            /\s+/gi,
                            " "
                          );
                          setFieldValue(POSTAL_CODE_FIELD_NAME, val.trim());
                          handleBlur(e);
                        }}
                        onChange={handleChange}
                        value={values.postalCode}
                        size={textFieldSize}
                        inputProps={{
                          autoComplete: "postal-code",
                        }}
                        error={Boolean(
                          touched.postalCode && !!errors.postalCode
                        )}
                        helperText={
                          !isXsBp ? touched.postalCode && errors.postalCode : ""
                        }
                        label={
                          isXsBp && touched.postalCode && errors.postalCode
                            ? errors.postalCode
                            : isXsBp && "Postal Code"
                        }
                      />
                    </Box>
                  </Grid>

                  <Grid item xs={12}>
                    <InputLabel
                      error={Boolean(!!errors.country && touched.country)}
                      htmlFor={COUNTRY_FIELD_NAME}
                    >
                      Country
                    </InputLabel>

                    <Box className={classes.textFieldContainer}>
                      <Select
                        labelId={`${COUNTRY_FIELD_NAME}-label`}
                        name={COUNTRY_FIELD_NAME}
                        id={COUNTRY_FIELD_NAME}
                        defaultValue={getCountryFromValue(values.country)}
                        menuPlacement="auto"
                        autoComplete="country-name"
                        options={countryList}
                        placeholder={countryPlaceHolderValue}
                        label="Country"
                        isXsBp={isXsBp}
                        isTouched={touched.country}
                        hasValue={values.country}
                        isError={Boolean(touched.country && !!errors.country)}
                        errorMessage={errors.country}
                        onBlur={() => setFieldTouched(COUNTRY_FIELD_NAME)}
                        onChange={(selectedCountry: CountryType) =>
                          handleOnCountryChange(selectedCountry, handleChange)
                        }
                      />
                      {!isXsBp && touched.country && !!errors.country && (
                        <InputFieldErrorMessage>
                          {errors.country}
                        </InputFieldErrorMessage>
                      )}
                    </Box>
                  </Grid>
                </Grid>

                <Grid item xs={12} className={classes.layoutFooter}>
                  <Box display="flex" justifyContent="flex-end" mt={2}>
                    <Button
                      type="submit"
                      data-testid="submit-button"
                      className={classes.button}
                      disabled={isSubmitting || !hasTouchedValues || !isValid}
                      variant="contained"
                      disableElevation
                      fullWidth={isXsBp}
                    >
                      Pay Now
                    </Button>
                  </Box>
                </Grid>
              </Box>
            </Paper>
          </Grid>
        </Grid>
      </form>
    </>
  );
};
