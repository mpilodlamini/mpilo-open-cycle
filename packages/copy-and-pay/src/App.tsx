import React from "react";
import "whatwg-fetch";
import { ConfigProvider } from "./contexts/ConfigContext";
import { ThemeProvider } from "@peach/checkout-design-system";
import Card from "./index";

function App() {
  return (
    <ConfigProvider>
      <ThemeProvider>
        <Card />
      </ThemeProvider>
    </ConfigProvider>
  );
}

export default App;
