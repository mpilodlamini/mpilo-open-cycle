const styles = () => ({
  wrapper: {
    height: "100%",
  },
  message: {
    maxWidth: "500px",
  },
  modal: {
    marginTop: "24px",
  },
  bottom: {
    alignSelf: "flex-end",
    paddingTop: "24px",
    paddingBottom: "16px",
  },
  anchor: {
    display: "flex",
    height: "100%",
  },
});

export default styles;
