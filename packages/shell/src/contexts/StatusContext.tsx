import { CheckoutStatus } from "@peach/checkout-lib";
import React, {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useState,
} from "react";
import { CheckoutContext } from "./CheckoutContext";

export interface StatusData {
  data?: any;
  url?: string;
  code?: string;
}

export interface StatusContextState {
  status?: CheckoutStatus;
  data: StatusData;
  set(status: CheckoutStatus | undefined, data?: StatusData): void;
}

export const StatusContext = createContext<StatusContextState>({
  set: () => {},
  data: {},
});

export const StatusProvider = ({ children }: { children: ReactNode }) => {
  const config = useContext(CheckoutContext);
  const [status, setStatus] = useState<CheckoutStatus | undefined>(
    config.status
  );
  const [data, setData] = useState<StatusData>({});
  const set = useCallback(
    (status, data = {}) => {
      setStatus(status);
      setData(data);
    },
    [setStatus, setData]
  );
  return (
    <StatusContext.Provider
      value={{
        status,
        data,
        set,
      }}
    >
      {children}
    </StatusContext.Provider>
  );
};
