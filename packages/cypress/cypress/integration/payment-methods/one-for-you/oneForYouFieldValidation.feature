Feature: One For You

  Background: Launch One For You Modal
    Given I Generate Checkout via API using default values
    When I click on "1ForYou-radio-card"
    And I switch to the iframe
    Then I should see "1ForYou-warning" containing the text "Partial Payments Not Supported" in the iframe
    Then I should see that the "1ForYou-submit" button is disabled in the iframe

  Scenario Outline: Redeem button remains disabled when an invalid voucher pin length is entered
    When I enter "<InvalidPin>" on the field "1ForYou-pin" in the iframe
    Then I should see that the "1ForYou-submit" button is disabled in the iframe
    Examples:
      | InvalidPin       |
      | 1234567890       |
      | qwertyuiopzxcvbn |

