import WS from "jest-websocket-mock";
import initiateWebsocket from "../websockets";

const websocketServerUrl = "ws://localhost:1234";
const server = new WS(websocketServerUrl);
const sendMessage = {
  action: "sendMessage",
  transaction_id: "29239392",
};

describe("Websocket tests", () => {
  it("should have an unsuccessful setup due to incorrect url", async () => {
    const incorrectUrl = "meh.com";

    const onMessageCallback = (data: any) => data;

    try {
      initiateWebsocket(incorrectUrl, sendMessage, onMessageCallback, "end");
    } catch (error) {
      expect(error.message).toEqual("Setup error");
    }
  });

  it("should communicate with the mock websocket server", async () => {
    let messageReceived = "";
    const receiveMessage = "Send message";

    const onMessageCallback = (data: any) => {
      messageReceived = data;
    };

    initiateWebsocket(
      websocketServerUrl,
      sendMessage,
      onMessageCallback,
      "end"
    );
    await server.connected;

    expect(messageReceived).toEqual("");

    server.send(receiveMessage);

    await expect(server).toReceiveMessage(JSON.stringify(sendMessage));
    await expect(messageReceived).toEqual(receiveMessage);
  });

  it("should close the socket connection", async () => {
    let messageReceived = "";
    const onMessageCallback = (data: any) => {
      messageReceived = data;
    };

    const client = initiateWebsocket(
      websocketServerUrl,
      sendMessage,
      onMessageCallback,
      "end"
    );
    await server.connected;

    server.send("end");
    await expect(messageReceived).toEqual("end");
    await expect(client.readyState).toEqual(WebSocket.CLOSING);

    try {
      client.send("hello");
    } catch (error) {
      expect(error.message).toEqual(
        "WebSocket is already in CLOSING or CLOSED state"
      );
    }
  });
});
