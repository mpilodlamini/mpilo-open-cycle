import { Box } from "@material-ui/core";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import React, { useContext } from "react";
import { CheckoutContext } from "../../contexts/CheckoutContext";

const PayOnCardHtml = () => {
  const checkout = useContext(CheckoutContext);

  return (
    <Box id="cards">
      <Box display="flex" justifyContent="center">
        <CreditCardIcon fontSize="large" />
      </Box>
      <Box display="flex" justifyContent="center" textAlign="center">
        <p style={{ fontSize: "14px" }}>
          Please enter your card details below to complete the payment.
        </p>
      </Box>
      <form
        action={`${checkout.baseUrl}/checkout?checkoutId=${checkout.checkoutId}`}
        className="paymentWidgets m-t-20"
      >
        <p style={{ display: "none" }}>VISA MASTER AMEX</p>
      </form>
    </Box>
  );
};

export default PayOnCardHtml;
