import React from "react";
import Mpesa from "./mpesa";

export default {
  title: "payments / Mpesa",
  parameters: {
    component: Mpesa,
  },
};
export const standard = (args: any) => <Mpesa {...args} />;

standard.args = {};
