import { Box, Button, Grid, InputLabel } from "@material-ui/core";
import { SnackbarStatic } from "@peach/checkout-design-system";
import React, { useContext, useState } from "react";
import NumberFormat from "react-number-format";
import { CheckoutContext } from "../../../contexts/CheckoutContext";
import { CssTextField, useStyles } from "./oneForYou.styles";

export interface OneForYouProps {
  /**
   * Functionality of the Redeem button
   */
  onClick: (param: string) => void;
}

const buttonAction = "Redeem";
const title = "1ForYou";
const maxLength = 16;

const inputProps: object = {
  style: {
    paddingTop: "10px",
    paddingBottom: "10px",
  },
  maxLength,
};

const OneForYou = (props: OneForYouProps) => {
  const checkout = useContext(CheckoutContext);
  const [pinValue, setPinValue] = useState("");
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const classes = useStyles();

  const onChange = (event: any) => {
    setIsButtonDisabled(true);
    const pinOnChangeValue = event.target.value;
    setPinValue(pinOnChangeValue);

    if (pinOnChangeValue.length === maxLength) {
      setIsButtonDisabled(false);
    }
  };

  const processPinContent = (): void => {
    if (isButtonDisabled) return;
    props.onClick(pinValue);
  };

  const onSubmit = (event: any): void => {
    event.preventDefault();
    processPinContent();
  };

  const button = (
    <Button
      type="button"
      data-testid={`submit-${title}`}
      className={classes.button}
      onClick={processPinContent}
      autoFocus
      disabled={isButtonDisabled}
      variant="contained"
      disableElevation
      fullWidth
    >
      {buttonAction}
    </Button>
  );

  return (
    <Box width={1} height={1}>
      <Box display="flex" justifyContent="center" mb={3}>
        <img
          src={`${checkout.assetsBasePath || "/"}images/1foryou.svg`}
          alt="1foryou"
        />
      </Box>

      <SnackbarStatic
        title="Partial Payments Not Supported"
        label="Your voucher amount must match the purchase amount or your payment will be declined. You will not be charged."
        dataTestId="one-for-you-warning"
      />

      <Box mt={3}>
        <form onSubmit={onSubmit} noValidate autoComplete="off">
          <InputLabel className={classes.label} htmlFor={title}>
            Please enter your 16 digit Voucher Pin
          </InputLabel>

          <Grid container>
            <Grid item md={10} xs={12}>
              <Box data-testid={`pin-${title}`} className={classes.textField}>
                <NumberFormat
                  type="tel"
                  customInput={CssTextField}
                  onChange={onChange}
                  required
                  autoFocus
                  fullWidth
                  id={title}
                  aria-describedby={title}
                  variant="outlined"
                  placeholder="1234 5678 8909 6806"
                  inputProps={inputProps}
                />
              </Box>
            </Grid>
            <Grid item md={2} xs={12}>
              {button}
            </Grid>
          </Grid>
        </form>
      </Box>
    </Box>
  );
};

export default OneForYou;
