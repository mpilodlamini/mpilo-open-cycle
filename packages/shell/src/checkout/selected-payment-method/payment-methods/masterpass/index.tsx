import $ from "jquery";
import React from "react";
import { Masterpass } from "../../../../design-system/templates/masterpass/masterpass";
import {
  PAYMENT_METHOD_NAMES,
  PAYMENT_METHOD_ENUMS,
} from "../../../../utils/pluginConstants";
import {
  AJAX_DATA_RESULT,
  PEACH_STATE,
  TRANSACTION_STATUS,
} from "../../../../utils/constants";
import { BUTTON_LABELS, MESSAGING } from "../../../../utils/messagingConstants";
import { GA } from "../../../../utils/googleAnalyticsConstants";
import { AJAX } from "../../../../utils/ajaxConstants";
import LoadingScreen from "../../../../components/loaders/loadingScreen";
import LoaderModal from "../../../../components/loaders/loadingModal";
import CheckoutService from "../../../../services/checkoutService";
import PluginManager from "../../../../utils/pluginManager";
import ErrorScreen from "../../../../components/errors/screens/errorScreen";
import ErrorModal from "../../../../components/errors/modals/errorModal";
import MessagesUtils from "../../../../utils/messagesUtil";
import FeedbackPage from "../shared/FeedbackPage";
import { printInfo } from "../../../../utils/logger";
import TimeUtil from "../../../../utils/timeUtil";
import { CheckoutConfig } from "../../../../contexts/CheckoutContext";
import GoogleAnalyticsUtil from "../../../../utils/googleAnalyticsUtil";
import { Component } from "react";

export interface MasterPassContainerProps {
  timeUtil: TimeUtil;
  isDefaultAndForced: boolean;

  resetSelected: () => void;
  setCancelVisibility: (state: boolean) => void;

  checkout: CheckoutConfig;
  googleAnalytics: GoogleAnalyticsUtil;
}

export interface MasterPassContainerState {
  showSuccessScreen: boolean;
  showLoadingScreen: boolean;
  showLoadingModal: boolean;
  showErrorModal: boolean;
  showErrorScreen: boolean;

  loadingModalHeading: string;
  loadingModalMessage: string;

  errorModalHeading: string;
  errorModalMessage: string;
  errorModalActionButton: string;
  errorModalBackButton: string;
  isErrorModalTimed: boolean;

  code: string;
  transactionId: string;
  qrCodeImageUrl: string;
  qrCode: string;
  copySuccess: boolean;
}

const componentName = "MasterpassContainer";
const paymentBrand = PAYMENT_METHOD_ENUMS.MASTERPASS;

const messagesUtils: MessagesUtils = new MessagesUtils();
const errorMessageObject = messagesUtils.getErrorMessages();

export class MasterpassContainer extends Component<
  MasterPassContainerProps,
  MasterPassContainerState
> {
  private readonly checkoutService: CheckoutService = new CheckoutService();

  private readonly _pluginManager: PluginManager = new PluginManager(
    this.props.timeUtil
  );

  private _masterpassTimeout: number | NodeJS.Timeout | null = null;

  private readonly _loadingScreen = (
    <LoadingScreen widgetName={PAYMENT_METHOD_NAMES.MASTERPASS} />
  );

  private readonly _errorScreen = (
    <ErrorScreen
      title={`The ${PAYMENT_METHOD_NAMES.MASTERPASS} payment method is unavailable`}
    />
  );

  constructor(props: MasterPassContainerProps) {
    super(props);

    this.state = {
      showLoadingScreen: true,
      showSuccessScreen: false,
      showLoadingModal: false,
      showErrorModal: false,
      showErrorScreen: false,

      isErrorModalTimed: false,

      loadingModalHeading: MESSAGING.DEFAULT_LOADING_HEADING,
      loadingModalMessage: MESSAGING.DEFAULT_LOADING_MESSAGE,

      errorModalHeading: MESSAGING.DEFAULT_ERROR_HEADING,
      errorModalMessage: MESSAGING.DEFAULT_ERROR_MESSAGE,
      errorModalActionButton: "",
      errorModalBackButton: BUTTON_LABELS.GO_BACK,

      code: "",
      qrCode: "",
      copySuccess: false,
      transactionId: "",
      qrCodeImageUrl: "",
    };
  }

  componentDidMount(): void {
    printInfo(componentName, "componentDidMount", "Mounted");

    this.getMasterpassQrCode();
  }

  componentWillUnmount = (): void => {
    this.checkoutService.cancelRequest();
    this._pluginManager.stopPollingListener();
  };

  getMasterpassQrCode = () => {
    const url = `${this.props.checkout.baseUrl}payment`;

    const postData = {
      merchant_key: this.props.checkout.merchantKey,
      checkout_id: this.props.checkout.checkoutId,
      payment_brand: paymentBrand,
    };

    const successCallback = (data: any) => {
      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        this.props.googleAnalytics.trackPaymentInformation();

        this.props.timeUtil.resetShortTermTimer();
        this.props.timeUtil.updateCheckoutTimer();
        this._pluginManager.createPollingListener();

        const code =
          (data &&
            data.response &&
            data.response.response_data &&
            data.response.response_data.connectorTxID2) ||
          "";
        const qrCode = code;
        const transactionId = data.response.response_data.id;
        const qrCodeImageUrl = `${this.props.checkout.switchBaseUrl}/rates/masterpass/qr/?code=${code}`;

        this.setState({
          code,
          qrCode,
          transactionId,
          qrCodeImageUrl,
          showLoadingScreen: false,
          showLoadingModal: false,
        });

        this.getMasterpassStatus();
      }
    };

    const errorCallback = (data: any): void => {
      let code: string = "900.100";
      let isErrorModalTimed: boolean = false;
      let errorModalActionButton: string = "";
      let errorModalBackButton: string = BUTTON_LABELS.GO_BACK;

      if (data) {
        if (data.responseJSON && data.responseJSON.response) {
          if (
            data.responseJSON.response.code &&
            data.responseJSON.response.code === "800.110.100"
          ) {
            code = "800.110.100";
            isErrorModalTimed = true;
            errorModalActionButton = BUTTON_LABELS.RETURN_TO_STORE;
            errorModalBackButton = "";
          }
          if (data.responseJSON.response.response_data) {
            code = data.responseJSON.response.response_data.result.code;
          }
        }
      }
      const errorModalHeading =
        (errorMessageObject[code] && errorMessageObject[code].heading) ||
        MESSAGING.DEFAULT_ERROR_HEADING;
      const errorModalMessage =
        (errorMessageObject[code] && errorMessageObject[code].message) ||
        MESSAGING.DEFAULT_ERROR_MESSAGE;

      if (messagesUtils.errorHeaders.indexOf(errorModalHeading) !== -1) {
        this.props.googleAnalytics.trackEvent(
          GA.EVENTS.ERROR,
          PAYMENT_METHOD_NAMES.MASTERPASS
        );
        this.props.googleAnalytics.trackErrorEvents(
          errorModalHeading,
          PAYMENT_METHOD_NAMES.MASTERPASS,
          code
        );
      } else {
        this.props.googleAnalytics.trackEvent(
          GA.EVENTS.ERROR,
          PAYMENT_METHOD_NAMES.MASTERPASS
        );
      }

      this.setState({
        code,
        errorModalHeading,
        errorModalBackButton,
        errorModalMessage,
        errorModalActionButton,
        isErrorModalTimed,
        showLoadingScreen: false,
        showLoadingModal: false,
        showErrorScreen: true,
        showErrorModal: true,
      });
    };

    this.checkoutService.postFormEncodedContent(
      url,
      postData,
      successCallback,
      errorCallback
    );
  };

  successScreenRedirectCallBack = (): void => {
    this.redirectLoaderOnAction(TRANSACTION_STATUS.SUCCESSFUL);
  };

  getMasterpassStatus = (): void => {
    if (this._masterpassTimeout) {
      clearInterval(this._masterpassTimeout as NodeJS.Timeout);
    }

    const url: string = `${this.props.checkout.switchBaseUrl}/internal/verify/${this.state.transactionId}/status/`;
    const successCallback = (data: any): void => {
      if (data.status === AJAX.CODES.SUCCESS) {
        const code: string = data.data.result_code;
        if (AJAX.SUCCESS_RESULT_CODES.indexOf(code) !== -1) {
          clearInterval(this._masterpassTimeout as NodeJS.Timeout);
          this.props.setCancelVisibility(false);
          this.setState({ code, showSuccessScreen: true });
        }
      }
    };

    this._masterpassTimeout = setInterval(() => {
      this.checkoutService.getContent(url, successCallback);
    }, 5000);
  };

  cancelCheckout = (): void => {
    this.redirectLoaderOnAction(TRANSACTION_STATUS.CANCELLED);
  };

  copyToClipboard = (): void => {
    const textField = document.createElement("textarea");
    textField.innerText = this.state.qrCode;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand("copy");
    // TODO: Native implementation is failing on IE11.
    // Need a better solution that does not require Jquery
    $("textarea").remove();
  };

  closeErrorModal = (): void => {
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.ERROR_GO_BACK,
      PAYMENT_METHOD_NAMES.MASTERPASS
    );
    this.setState({ showErrorModal: false });

    if (!this.props.isDefaultAndForced) {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.COLLAPSE,
        PAYMENT_METHOD_NAMES.MASTERPASS
      );
      this.props.resetSelected();
    }
  };

  copyToClipboardClick = (): void => {
    this.setState({ copySuccess: true });
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.COPY_CODE,
      PAYMENT_METHOD_NAMES.MASTERPASS
    );
    this.copyToClipboard();
  };

  refreshQrCodeClick = (): void => {
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.REFRESH_CODE,
      PAYMENT_METHOD_NAMES.MASTERPASS
    );
    const loadingModalHeading: string = "Refreshing code";
    const loadingModalMessage: string =
      "Please wait while we fetch a new QR code.";
    this.setState({
      loadingModalHeading,
      loadingModalMessage,
      showLoadingModal: true,
      copySuccess: false,
    });
    this.getMasterpassQrCode();
  };

  redirectLoaderOnAction = (transactionStatus: string): void => {
    const loadingModalHeading = `Redirecting to ${this.props.checkout.merchant}`;
    const loadingModalMessage = "Please wait you are being redirected";
    this.setState({
      loadingModalHeading,
      loadingModalMessage,
      showLoadingModal: true,
    });
    this._pluginManager.updateTransactionStatus(
      transactionStatus,
      this.state.transactionId,
      this.state.code
    );
  };

  render() {
    const feedbackPage = (
      <FeedbackPage
        paymentMethod={PAYMENT_METHOD_NAMES.MASTERPASS}
        code={this.state.code}
        status={PEACH_STATE.SUCCESS}
        onRedirect={this.successScreenRedirectCallBack}
        isTimed
      />
    );

    const masterpassWidget = (
      <Masterpass
        qrCode={this.state.qrCode}
        qrCodeImageUrl={this.state.qrCodeImageUrl}
        onCopyCode={this.copyToClipboardClick}
        onRefreshCode={this.refreshQrCodeClick}
        timeUtil={this.props.timeUtil}
        copySuccess={this.state.copySuccess}
      />
    );

    const loadingModal = (
      <LoaderModal
        open={this.state.showLoadingModal}
        title={this.state.loadingModalHeading}
        label={this.state.loadingModalMessage}
      />
    );

    const errorModal = (
      <ErrorModal
        open={this.state.showErrorModal}
        title={this.state.errorModalHeading}
        label={this.state.errorModalMessage}
        continueText={this.state.errorModalActionButton}
        cancelText={this.state.errorModalBackButton}
        onClick={this.cancelCheckout}
        onCancel={this.closeErrorModal}
        timed={this.state.isErrorModalTimed}
      />
    );

    return (
      <>
        {!this.state.showLoadingScreen &&
          !this.state.showErrorScreen &&
          !this.state.showSuccessScreen &&
          masterpassWidget}
        {this.state.showSuccessScreen && feedbackPage}
        {this.state.showLoadingScreen && this._loadingScreen}
        {this.state.showErrorScreen && this._errorScreen}
        {loadingModal}
        {errorModal}
      </>
    );
  }
}

export default MasterpassContainer;
