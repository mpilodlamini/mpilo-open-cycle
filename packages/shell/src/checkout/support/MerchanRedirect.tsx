import React, {
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { Box, BoxProps, Button, CircularProgress } from "@material-ui/core";
import { useStyles } from "./support.styles";
import { info } from "@peach/checkout-design-system/dist/variables";
import { CheckoutContext } from "../../contexts/CheckoutContext";
import constants from "../../utils/constants";
import { GoogleAnalyticsContext } from "../../contexts/GoogleAnalyticsContext";
import { Redirector } from "../../service-components/Redirector";
import pluginConstants from "../../utils/pluginConstants";
import { RedirectForm } from "../../service-components/RedirectForm";
import { Dialog } from "./dialog/Dialog";

export interface MerchantRedirectProps extends BoxProps {
  children?: ReactNode;
  status: string;
  paymentMethod: string;
  url: string;
  data: any;
}

export const MerchantRedirect = ({
  children,
  status,
  paymentMethod,
  url,
  className,
  data,
  ...props
}: MerchantRedirectProps) => {
  const styles = useStyles();
  const config = useContext(CheckoutContext);
  const ga = useContext(GoogleAnalyticsContext);
  const [time, setTime] = useState(5);

  useEffect(() => {
    const timer = setInterval(() => {
      if (time > 0) {
        setTime((time) => time - 1);
      }
    }, 1000);
    return () => {
      if (timer) {
        clearInterval(timer);
      }
    };
  }, [setTime, time]);

  const redirectMethod = useMemo(() => {
    if (
      config.source !== pluginConstants.NAMES.SHOPIFY &&
      config.source !== pluginConstants.NAMES.SBTECH &&
      config.source !== pluginConstants.NAMES.WIX &&
      data &&
      Object.keys(data).length > 0
    ) {
      return "POST";
    }
    return "GET";
  }, [config, data]);

  const logRedirect = useCallback(() => {
    if (config.isCheckoutInTest) {
      return;
    }
    ga.trackEvent(
      {
        [constants.PEACH_STATE.PENDING]: "pending__goback",
      }[status] || "success_goback",
      paymentMethod
    );
  }, [status, config, ga, paymentMethod]);

  return (
    <>
      <Dialog
        title={`Redirecting to ${config.merchant}`}
        icon={
          <CircularProgress style={{ color: info, height: 50, width: 50 }} />
        }
        subtitle={
          <>
            We&apos;re taking you back to {config.merchant}
            <span>
              {" "}
              in {time} seconds. If nothing happens, click the button.
            </span>
          </>
        }
        {...props}
      >
        <Box width={1} className={styles.margin}>
          <Box display="flex" justifyContent="center">
            <RedirectForm
              data={data}
              url={url}
              method={redirectMethod}
              onSubmit={() => logRedirect()}
            >
              <Button
                className={styles.button}
                variant="contained"
                disableElevation
                type="submit"
              >
                Go Back
              </Button>
            </RedirectForm>
          </Box>
        </Box>
        <div style={{ display: "none" }}>
          {time <= 0 && (
            <Redirector
              data={data}
              url={url}
              method={redirectMethod}
              onSubmit={() => logRedirect()}
            />
          )}
        </div>
        {children}
      </Dialog>
    </>
  );
};
