import CONSTANTS from "../../../../constantsUtil";
import { When, Then } from "cypress-cucumber-preprocessor/steps";

When("I Login to access my A+ account", () => {
  enterDetailsAndVerify();
});

export const enterDetailsAndVerify = (): void => {
  cy.iframe()
    .intercept(
      CONSTANTS.AJAX_REQUEST_VERB.POST,
      `${Cypress.env("paymentBaseUrl")}/payment`
    )
    .as("paymentMethodRequest");
  cy.iframe().contains(CONSTANTS.APLUS.SIGN_IN_MESSAGE_1);
  cy.iframe().contains(CONSTANTS.APLUS.SIGN_IN_MESSAGE_2);
  cy.getIFrameElementByDataTestId(
    CONSTANTS.APLUS.CARD_NUMBER.ID
  ).type(CONSTANTS.APLUS.CARD_NUMBER.VALUE);
  cy.getIFrameElementByDataTestId(
    CONSTANTS.APLUS.EXPIRY_DATE.ID
  ).type(CONSTANTS.APLUS.EXPIRY_DATE.VALUE);
  cy.getIFrameElementByDataTestId(
    CONSTANTS.APLUS.CELLPHONE_NUMBER.ID
  ).type(CONSTANTS.APLUS.CELLPHONE_NUMBER.VALUE);
  cy.getIFrameElementByDataTestId("aplus-details-btn")
    .contains(CONSTANTS.APLUS.VERIFY_BUTTON_LABEL)
    .click();

  cy.iframe().wait("@paymentMethodRequest");
};

When("I enter OTP {string} and click verify", (otp) => {
  enterOtpAndVerify(otp);
});

export const enterOtpAndVerify = (otp: string): void => {
  cy.iframe()
    .intercept(
      CONSTANTS.AJAX_REQUEST_VERB.POST,
      `${Cypress.env("paymentBaseUrl")}/verify-otp`
    )
    .as("verifyOtpRequest");
  cy.getIFrameElementByDataTestId(CONSTANTS.APLUS.OTP.ID).type(otp);
  cy.getIFrameElementByDataTestId("aplus-otp-btn")
    .contains(CONSTANTS.APLUS.PAY_NOW_BUTTON)
    .click();

  cy.iframe().wait("@verifyOtpRequest");
};

When("I click resend otp in iframe", () => {
  cy.iframe()
    .intercept(
      CONSTANTS.AJAX_REQUEST_VERB.POST,
      `${Cypress.env("paymentBaseUrl")}/resend-otp`
    )
    .as("resendOtpRequest");

  cy.getIFrameElementByDataTestId("aplus-otp-resendotp")
    .contains(CONSTANTS.APLUS.RESEND_OTP)
    .click();
  cy.iframe().wait("@resendOtpRequest");
});
