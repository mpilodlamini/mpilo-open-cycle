import { makeStyles } from "@material-ui/core";
import { text } from "../../variables";

const useStyles = makeStyles(() => ({
  highlight: {
    color: text,
  },
  icon: {
    width: "20px",
  },
}));

export default useStyles;
