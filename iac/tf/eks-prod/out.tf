output "one_for_you_sandbox" {
  value = module.one_for_you_sandbox.load_balancer_ingress
}

output "a_plus_sandbox" {
  value = module.a_plus_sandbox.load_balancer_ingress
}

output "one_for_you_live" {
  value = module.one_for_you_live.load_balancer_ingress
}

output "a_plus_live" {
  value = module.a_plus_live.load_balancer_ingress
}
