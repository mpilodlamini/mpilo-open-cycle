export const MIDDLEWARE_SBTECH_CONSTANTS = {
  //Mandatory fields
  secretKey: 'input[id="secret_key"]',
  "authentication.entityId": 'input[id="id_entityId"]',
  amount: 'input[id="id_amount"]',
  currency: 'input[id="id_currency"]',
  paymentType: 'input[id="id_paymentType"]',
  shopperResultUrl: 'input[id="id_shopperResultUrl"]',

  merchantTransactionId: 'input[id="id_merchantTransactionId"]',
  signature: 'input[id="id_x_signature"]',
  nonce: 'input[id="id_nonce"]',

  notificationUrl: 'input[id="id_notificationUrl"]',
  customerMerchantCustomerId: 'input[id="id_customer.merchantCustomerId"]',
  defaultPaymentMethod: 'input[id="id_defaultPaymentMethod"]',

  //Optional fields
  cancelUrl: 'input[id="id_cancelUrl"]',
  merchantInvoiceId: 'input[id="id_merchantInvoiceId"]',
  transactionDescription: 'input[id="id_transactionDescription"]',

  ////Customer object
  customerGivenName: 'input[id="id_customer.givenName"]',
  customerSurname: 'input[id="id_customer.surname"]',
  customerMobile: 'input[id="id_customer.mobile"]',
  customerEmail: 'input[id="id_customer.email"]',
  customerStatus: 'input[id="id_customer.status"]',

  ////Billing object
  billingStreet1: 'input[id="id_billing.street1"]',
  billingStreet2: 'input[id="id_billing.street2"]',
  billingCity: 'input[id="id_billing.city"]',
  billingCompany: 'input[id="id_billing.company"]',
  billingCountry: 'input[id="id_billing.country"]',
  billingState: 'input[id="id_billing.state"]',
  billingPostcode: 'input[id="id_billing.postcode"]',

  ////Shipping object
  shippingStreet1: 'input[id="id_shipping.street1"]',
  shippingStreet2: 'input[id="id_shipping.street2"]',
  shippingCity: 'input[id="id_shipping.city"]',
  shippingCompany: 'input[id="id_shipping.company"]',
};

export interface MiddlewareSBTechObject {
  secretKey?: string;
  "authentication.entityId"?: string;
  amount?: string;
  currency?: string;
  paymentType?: string;
  shopperResultUrl?: string;
  merchantTransactionId?: string;
  notificationUrl?: string;
  customerMerchantCustomerId?: string;
  defaultPaymentMethod?: string;

  cancelUrl?: string;
  merchantInvoiceId?: string;
  transactionDescription?: string;

  customerGivenName?: string;
  customerSurname?: string;
  customerMobile?: string;
  customerEmail?: string;
  customerStatus?: string;

  billingStreet1?: string;
  billingStreet2?: string;
  billingCity?: string;
  billingCompany?: string;
  billingCountry?: string;
  billingState?: string;
  billingPostcode?: string;

  shippingStreet1?: string;
  shippingStreet2?: string;
  shippingCity?: string;
  shippingCompany?: string;

  buttonId?: string;
}
