import { FeedbackType, ModalDialog } from "@peach/checkout-design-system";
import React, { ReactNode } from "react";

interface LoaderModalProps {
  open: boolean;
  title: string;
  label: ReactNode;
}

const LoaderModal: React.FunctionComponent<LoaderModalProps> = (
  props: LoaderModalProps
) => {
  return (
    <ModalDialog
      type={FeedbackType.LOADING}
      title={props.title}
      label={props.label}
      open={props.open}
      forceBottom
    />
  );
};

export default LoaderModal;
