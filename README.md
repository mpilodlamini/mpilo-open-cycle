# Checkout

The project can be run as a **_rework_** (Dockerized) version

## Running Rework (Docker needed)

Install docker from the official docker website found on `https://docs.docker.com/get-docker/`

- Windows OS - `https://docs.docker.com/docker-for-windows/install/`
- MacOS (Intel Chip or Apple Chip) - `https://docs.docker.com/docker-for-mac/install/`
- Linux OS Distribution - `hhttps://docs.docker.com/engine/install/`

After successfully installing Docker on local machine switch to the **_develop_** branch by running `git checkout develop` on your terminal or CMD
Proceed to execute the commands below

```
docker compose build
docker compose run shell yarn
docker compose up -d
open https://localhost:3000
```

## Peach Payments Checkout Page

Payments portal for clients to have access to multiple 3rd party vendors in one central place.
Formerly known as the hpp, hosted payments page.
See [confluence](https://peachpayments.atlassian.net/wiki/spaces/Checkout/overview) to access additional documentation

## Available Scripts

In the project directory, you can run, although it is best to run it in docker, especially on M1 Mac:

### `yarn install`

Installs all the project dependencies. (Internet and Nodejs is required for this step)

Nodejs setup guide: [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

### `yarn test`

Launches the Jest test runner in the interactive watch mode.<br />

### `yarn cypress:open`

Launches the Cypress browser automation test runner in the interactive watch mode.<br />
By default the automation tests are run on the demo environment. Appending :qa to the command will the QA tests.

### `yarn cypress:run`

Launches the Cypress headless automation test runner in the interactive watch mode.<br />
After all the tests run a folder called `reports` will be auto generated and inside the folder is another folder called `cucumber-htmlreport.html` and it contains a the file `index.html` as well as other files. To view the test report on a chrome browser, run the command `yarn cypress:open-cucumber-report`.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and has all hashed values removed by the rename-build-files script.
This is done to ensure there are no deployment dependencies on the Checkout Back End responsible for serving the
index.html file.<br />

### `yarn run-build`

Runs the production build locally.

### `yarn storybook`

Starts up the Checkout UI component library. This is used to create, test and interact with React presentational
component built in isolation.

### `yarn get-checkout`

Generates a new Checkout in the demo environment and returns the Checkout Id.

## Technologies

- [React](https://reactjs.org/docs/getting-started.html) Front end library
- [Typescript](https://www.typescriptlang.org/) Type checking
- [Material UI](https://material-ui.com/) UI component library
- [Formik](https://formik.org/) Form handling
- [Yup](https://www.npmjs.com/package/yup) Form schema validation
- [Cypress](https://docs.cypress.io/guides/overview/why-cypress) Automation Testing
- [Jest](https://jestjs.io/docs/getting-started) Unit testing
- [React testing library](https://testing-library.com/docs/react-testing-library/intro/)
- [JSDOM](https://github.com/jsdom/jsdom) Emulates the DOM without having to invoke the browser. In this case allows test files to access global objects set by index.html file.
- [Docker](https://docs.docker.com/get-docker/) Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers.
- [Vite](https://vitejs.dev/) Front End Build tool used instead of `Create React App` see [React](https://reactjs.org/docs/getting-started.html).
- [Storybook](https://storybook.js.org/) Storybook is an open source tool for building UI components and pages in isolation. It streamlines UI development, testing, and documentation.
- [Caddy](https://caddyserver.com/) Caddy is a powerful, enterprise-ready, open source web server with automatic HTTPS.
