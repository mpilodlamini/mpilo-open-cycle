const CryptoJS = require("crypto-js");
const fetch = require("node-fetch");
const fs = require("fs/promises");

const entityId = "8ac7a4ca694cec2601694cf5f9360026";
const secret = "ca3d3f623a7111e9baa702de7a5a0a6e";

const merchantTransactionId = Math.floor(Math.random() * 1000000);
const nonce = Math.floor(Math.random() * 1000000);

const body = {
  "authentication.entityId": entityId,
  amount: 10.0,
  currency: "ZAR",
  paymentType: "DB",
  signature: "",
  shopperResultUrl: "https://httpbin.org/post",
  merchantTransactionId,
  nonce,
  notificationUrl: "",
  cancelUrl: "",
  merchantInvoiceId: "",
  transactionDescription: "",
  "customer.merchantCustomerId": "",
  "customer.givenName": "",
  "customer.surname": "",
  "customer.mobile": "",
  "customer.email": "",
  "customer.status": "",
  "customer.merchantCustomerLanguage": "",
  "billing.street1": "",
  "billing.street2": "",
  "billing.city": "",
  "billing.company": "",
  "billing.country": "",
  "billing.state": "",
  "billing.postcode": "",
  "billing.customer.fax": "",
  "billing.customer.taxType": "",
  "billing.customer.givenName": "",
  "billing.customer.surname": "",
  "billing.customer.phone": "",
  "billing.houseNumber1": "",
  "shipping.street1": "",
  "shipping.street2": "",
  "shipping.city": "",
  "shipping.company": "",
  "shipping.country": "",
  "shipping.state": "",
  "shipping.postcode": "",
  "shipping.customer.givenName": "",
  "shipping.customer.surname": "",
  "shipping.customer.phone": "",
  "shipping.customer.fax": "",
  "shipping.customer.taxId": "",
  "shipping.customer.taxType": "",
  "shipping.customer.houseNumber1": "",
  "cart.tax": "",
  "cart.shippingAmount": "",
  "cart.discount": "",
  plugin: "",
  defaultPaymentMethod: "",
  forceDefaultMethod: "",
  createRegistration: "",
};

//loop through all available body attributes and create an array with the populated values (not empty strings)
const keyValues = Object.keys(body)
  .map((key) => body[key] && `${key}${body[key]}`)
  .sort()
  .join("");
//joins all the elements of the array into a single string with no comma separators
const hash = CryptoJS.HmacSHA256(keyValues, secret);

body.signature = hash.toString(CryptoJS.enc.Hex);

fetch("https://checkout-demo.ppay.io/checkout", {
  method: "POST",
  body: new URLSearchParams(body),
  headers: {
    Accept: "application/json",
    Referer: "34.247.96.206",
    Origin: "34.247.96.206",
  },
})
  .then((response) =>
    response.ok ? response.json() : Promise.reject(response)
  )
  .then(async (json) => {
    const checkout = json["checkoutId"];

    console.log(checkout);

    const file = await fs.readFile("./packages/shell/index.html");
    const content = file.toString();

    await fs.writeFile(
      "./packages/shell/index.html",
      content.replace(/checkoutId: "\w*",/, `checkoutId: "${checkout}",`)
    );
  })
  .catch((error) => console.error(error));
