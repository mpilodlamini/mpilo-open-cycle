import * as React from "react";
import { TextFieldProps as MuiTextFieldProps } from "@material-ui/core/TextField";
import { TextField as MuiTextField } from "@peach/checkout-design-system";
import { FieldProps, getIn } from "formik";

export interface TextFieldProps
  extends FieldProps,
    Omit<MuiTextFieldProps, "name" | "value" | "error"> {}

export function MuiField({
  children,
  disabled,
  field: { onBlur: fieldOnBlur, ...field },
  form: { isSubmitting, touched, errors },
  onBlur,
  helperText,
  ...props
}: TextFieldProps) {
  const fieldError = getIn(errors, field.name);
  const showError = getIn(touched, field.name) && !!fieldError;
  return (
    <MuiTextField
      variant={props.variant}
      error={showError}
      helperText={showError ? fieldError : helperText}
      disabled={disabled ?? isSubmitting}
      onBlur={onBlur ? onBlur : (e: any) => fieldOnBlur(e ?? field.name)}
      {...field}
      {...props}
    >
      {children}
    </MuiTextField>
  );
}
