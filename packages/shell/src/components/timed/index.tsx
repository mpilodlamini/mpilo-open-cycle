import React, { useCallback, useEffect, useMemo, useState } from "react";
import constants from "../../utils/constants";
import { printInfo } from "../../utils/logger";
import PluginManager from "../../utils/pluginManager";
import TimeUtil from "../../utils/timeUtil";

export interface TimedProps {
  redirectPostData?: {};
  redirectUrl?: string;
  onTimeout?: () => void;
  open: boolean;

  time?: number;
}

const componentName = "Timed";

const pluginManager: PluginManager = new PluginManager();
const timeUtil: TimeUtil = new TimeUtil();

export const withTimer =
  (Component: React.FunctionComponent<any>) =>
  (props: TimedProps & { [key: string]: any }) => {
    const [timer, setTimer] = useState(props.time ?? constants.REDIRECT_TIMER);

    const { redirectUrl, redirectPostData, onTimeout, ...remaining } = props;

    const redirect = useCallback(() => {
      if (onTimeout) {
        onTimeout();
      } else {
        pluginManager.redirectToPluginSpecificUrl(
          redirectUrl,
          redirectPostData
        );
      }
    }, [onTimeout, redirectUrl, redirectPostData]);

    const updateTime = useCallback(
      (newTime: number): void => {
        setTimer(newTime);

        if (newTime <= 0) {
          redirect();
        }
      },
      [redirect]
    );

    useEffect(() => {
      printInfo(componentName, "componentDidMount", "Mounted");

      timeUtil.createTimer(constants.REDIRECT_TIMER, updateTime);

      return () => timeUtil.stopShortTermTimer();
    }, [props.open, updateTime]);

    const timerDisplay = useMemo(
      () => (
        <span> in {timer} seconds. If nothing happens, click the button.</span>
      ),
      [timer]
    );

    let label = null;
    if (remaining.label) {
      label = (
        <>
          {remaining.label}
          {timerDisplay}
        </>
      );
    }
    return <Component {...remaining} label={label} />;
  };

export default withTimer;
