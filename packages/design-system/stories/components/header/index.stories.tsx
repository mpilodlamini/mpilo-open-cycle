import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import { Header, HeaderProps } from "../../../";

export default {
  title: "Components/Header",
} as Meta;

export const Default: Story<HeaderProps> = (props) => {
  return <Header {...props} />;
};

Default.args = {
  currency: "ZAR",
  total: "100.00",
  alt: "Peach Payments",
  src:
    "https://www.peachpayments.com/hubfs/raw_assets/public/Peach_Payments_December2020/images/peach-payments-logo.svg",
};
