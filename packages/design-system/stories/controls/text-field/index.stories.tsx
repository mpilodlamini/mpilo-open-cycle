import { TextFieldProps } from "@material-ui/core/TextField";
import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import { TextField } from "../../../";

export default {
  title: "Controls/TextField",
} as Meta;

export const Default: Story<TextFieldProps> = (props) => {
  return <TextField {...props} />;
};

Default.args = {
  placeholder: "Maximus",
  disabled: false,
  label: "Enter your name",
};
