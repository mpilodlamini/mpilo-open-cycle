Feature: Native Card
    In order to make a purchase
    As a customer
    I want to pay using my native card account

    Background: Launch Native Card modal and login
        Given I Generate Checkout via API using default values
        When I click on "card-btn"
        Then I should see that the element with data-testid "qa-native-card-form" contains the text "Card Number" to "be.visible"

    Scenario Outline: Native Card <heading>
        When I enter "<cardNumber>" on the field "native-card-number"
        And I enter "T Automation" on the field "native-card-holderName"
        And I enter "1225" on the field "native-card-expiryDate"
        And I enter "123" on the field "native-card-cvv"
        And I click on "native-card-submit-button"
        Then I should see the text "<heading>" inside "<container>" modal

        Examples:
            | cardNumber       | heading                | container      | message                                                                                                               |
            | 4012888888881881 | Unsupported Device     | warning        | 3DSecure not possible on this device. Try again with a different device or go back and select another payment method. |
            | 5457116362416571 | 3DSecure Not Supported | warning        | 3DSecure 2.0 is not enabled on this card. Try again or go back and select another payment method.                     |
            | 4263982640269299 | Card Not Supported     | warning        | This card is not supported. Try again or go back and select another payment method.                                   |
            | 4917484589897107 | Payment Failed         | warning        | Payment failed due to failed 3DSecure response. Try again or go back and select another payment method.               |
            | 4012888888881881 | Unsupported Device     | warning        | 3DSecure not possible on this device. Try again with a different device or go back and select another payment method. |
            | 4024007179651566 | Payment Successful     | success-screen | You will automatically be redirected back to                                                                          |

