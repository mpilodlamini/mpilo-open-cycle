import React from "react";
import LoadingPage from "./loadingPage";

export default {
  title: "organisms / Loading Page",
  component: LoadingPage,
};

export const Loading = (args: any) => <LoadingPage {...args} />;
