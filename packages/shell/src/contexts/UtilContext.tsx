import React, {
  useState,
  createContext,
  ReactNode,
  useContext,
  useMemo,
  useCallback,
} from "react";
import MessagesUtils from "../utils/messagesUtil";
import PluginManager from "../utils/pluginManager";
import TimeUtil from "../utils/timeUtil";
import { CheckoutContext } from "./CheckoutContext";

export interface UtilContextState {
  timeUtil: TimeUtil;
  pluginManager: PluginManager;
  messageUtils: MessagesUtils;
}

export const UtilContext = createContext<UtilContextState>({
  timeUtil: new TimeUtil(),
  pluginManager: new PluginManager(),
  messageUtils: new MessagesUtils(),
});

export const UtilProvider = ({ children }: { children: ReactNode }) => {
  const config = useContext(CheckoutContext);
  const timeUtil = useMemo(() => new TimeUtil(), []);
  const pluginManager = useMemo(() => new PluginManager(timeUtil), [timeUtil]);
  const messageUtils = useMemo(
    () =>
      new MessagesUtils(
        config.amount,
        config.merchantCurrencyCode,
        config.merchant
      ),
    [config]
  );
  return (
    <UtilContext.Provider
      value={{
        timeUtil,
        pluginManager,
        messageUtils,
      }}
    >
      {children}
    </UtilContext.Provider>
  );
};
