import React, { HTMLAttributes } from "react";
import { Button, makeStyles } from "@material-ui/core";
import { white, info } from "../../../variables";

export type PrimaryButtonProps = HTMLAttributes<HTMLButtonElement> & {
  label: string;
  fullWidth?: boolean;
  onClick: () => void;
};

const customStyles = makeStyles(() => ({
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },
  },
}));

export const PrimaryButton = (props: PrimaryButtonProps) => {
  const classes = customStyles();

  return (
    <Button
      className={classes.button}
      onClick={props.onClick}
      variant="contained"
      disableElevation
      fullWidth={props.fullWidth ?? false}
    >
      {props.label}
    </Button>
  );
};
