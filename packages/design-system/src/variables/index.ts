/* Colours */
export const white = "#FFFFFF";
export const primaryBlue = "#0A1F44";
export const secondaryGrey = "#53627C";
export const tertiaryGrey = "#8A94A6";
export const orange = "#E1733F";
export const red = "#F03D3D";
export const primaryGreen = "#4CAF50";
export const secondaryGreen = "#54C683"; // Hover
export const tertiaryGreen = "#6FCF97";

/* Functional colours */
export const backgroundGrey = "#F5F7FA";
export const secondaryBackgroundGrey = "#FAFAFA";
export const border = "#C9CED6";

/* Typography */
export const text = primaryBlue;

/* Feedback */
export const info = "#1C7ED6";
export const error = "#F03D3D";
export const success = primaryGreen;
export const warning = "#FF9800";

/* Typography */
export const bold = "600";
