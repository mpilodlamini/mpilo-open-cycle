import { makeStyles, createStyles, Theme } from "@material-ui/core";
import { primaryBlue } from "../../variables";

const useStyles = makeStyles(({ breakpoints }: Theme) =>
  createStyles({
    root: {
      color: primaryBlue,
      fontSize: "14px",
      fontWeight: 600,
      marginBottom: "4px",
      display: "none",
      [breakpoints.up("sm")]: {
        display: "block",
      },
    },
  })
);

export default useStyles;
