declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-cy attribute.
     * @example cy.dataCy('greeting')
     */
    getIframeBody(): Chainable;
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to send a Post request to the Checkout endpoint to generate.
     * @example cy.generateAPICheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount: "10.00",
        currency: "ZAR",
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/post",
        merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8)
      });
     */
     generateAPICheckout(options): Chainable
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to send a navigate to our Middleware page, populating its fields and pressing Post in order to send a request to the Checkout endpoint to generate.
     * @example cy.generateMiddlewareCheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount: "10.00",
        currency: "ZAR",
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/post",
        merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8)
      });
     */
      generateMiddlewareCheckout(options): Chainable
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to send a navigate to our Wix Middleware page, populating its fields and pressing Post in order to send a request to the Checkout endpoint to generate.
     * @example cy.generateMiddlewareWixCheckout({
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        wixTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8),
        orderId: "123",
        amount: "10.00",
        currency: "ZAR",
        orderReturnSuccessUrl: "https://httpbin.org/get",
        orderReturnErrorUrl: "https://httpbin.org/get",
        orderReturnCancelUrl: "https://httpbin.org/get",
        orderReturnPendingUrl: "https://httpbin.org/get",
        orderDescriptionItemsName: "Shoes",
        orderDescriptionItemsId: "10",
        orderDescriptionItemsPrice: "10.00",
        notificationUrl: "https://httpbin.org/get",
      });
     */
      generateMiddlewareWixCheckout(options): Chainable
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to send a navigate to our Middleware page, populating its fields and pressing Post in order to send a request to the Checkout endpoint to generate.
     * @example cy.generateMiddlewareSbTechCheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount: "10.00",
        currency: "ZAR",
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/get",
        merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8),
        notificationUrl: "https://httpbin.org/get",
      });
     */
      generateMiddlewareSbTechCheckout(options): Chainable
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to send a navigate to our Middleware page, populating its fields and pressing Post in order to send a request to the Checkout endpoint to generate.
     * @example cy.generateMiddlewareShopifyCheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount: "10.00",
        currency: "ZAR",
        reference: "Cy-Checkout-" + Cypress._.random(0, 1e8),
        test: ""+ Cypress._.random(0, 1e5),
        urlCallback: "https://httpbin.org/get",
      });
     */
      generateMiddlewareShopifyCheckout(options): Chainable
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by id attribute.
     * @example cy.dataCy('greeting')
     */
     getElementById(Id, option?): Chainable
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-testid attribute.
     * @example cy.getElementByDataTestId(element).type(inputText);
     */
     getElementByDataTestId(dataTestId, option?): Chainable
  }
}

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-testid attribute within an iFrame.
     * @example cy.getIFrameElementByDataTestId(element).should("be.disabled");
     */
     getIFrameElementByDataTestId(dataTestId, option?): Chainable
  }
}