Feature: A+ Resend OTP Requests for invalid scenarios
    In order to handle failed resend attempts
    As a customer
    I want to to be notified accordingly

    Scenario Outline: A+ Resend OTP Tests with merchantTransId <merchantTransId>
        Given I Generate Checkout via API using "<merchantTransId>" as my merchant Transaction Id
        When I click on "aplus-radio-card"

        And I switch to the iframe
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        When I Login to access my A+ account
        And it finishes "loading" in the iframe
        Then I should see that the "aplus-otp-btn" button is disabled in the iframe
        And I should see "aplus-otp-timer" containing the text "Session Expires in" in the iframe

        When I click resend otp in iframe
        And it finishes "loading" in the iframe
        Then I should see the text "<errorHeading>"
        And I should see the text "Continue" inside "warning" modal
        When I click the "Continue" inside "warning" modal
        
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe
        
        Examples:
            | merchantTransId | errorHeading              |
            | test-2222       | Payment Declined          |
            | test-3333       | Incorrect Payment Details |
            | test-4444       | Payment Error             |
            | test-5555       | Payment Timeout           |
            | test-6666       | Payment Failed            |
            | test-7777       | A+ unavailable            |
            | test-8888       | Payment Declined          |
            | test-9999       | Payment Declined          |
            | test-1122       | A+ unavailable            |