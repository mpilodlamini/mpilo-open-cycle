export const CARD = Object.freeze({
  REGEX_PATTERN: {
    AMERICAN_EXPRESS: /^(?:3[47][0-9]{13})$/,
    VISA: /^(?:4[0-9]{12}(?:[0-9]{3})?)$/,
    MASTERCARD: /^(?:5[1-5][0-9]{14})$/,
    DINERS_CLUB: /^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$/,
  },
  BRAND_DETECTION: {
    AMERICAN_EXPRESS: /^(?:3[47][0-9]{2})/,
    VISA: /^(?:4[0-9]{3})/,
    MASTERCARD: /^(?:5[1-5][0-9]{2})/,
    DINERS_CLUB: /^(?:3(?:0[0-5]|[68][0-9])[0-9])/,
  },
  ENUM: {
    AMERICAN_EXPRESS: "AMERICANEXPRESS",
    VISA: "VISA",
    MASTERCARD: "MASTERCARD",
    DINERS_CLUB: "DINERSCLUB",
  },
  NAME: {
    AMERICAN_EXPRESS: "American Express",
    VISA: "Visa",
    MASTERCARD: "Mastercard",
    DINERS_CLUB: "Diners Club",
  },
  MAX_LENGTH: {
    AMERICAN_EXPRESS: 15,
    VISA: 16,
    MASTERCARD: 16,
    DINERS_CLUB: 14,
  },
  MAX_CVV_LENGTH: {
    AMERICAN_EXPRESS: 4,
    VISA: 3,
    MASTERCARD: 3,
    DINERS_CLUB: 3,
  },
  BRAND_COLLECTION: ["AMERICANEXPRESS", "VISA", "MASTERCARD", "DINERSCLUB"],
  transformBrandEnumToName: (enumValue: string) => {
    switch (enumValue) {
      case "AMERICANEXPRESS":
        return "American Express";
      case "VISA":
        return "Visa";
      case "MASTERCARD":
        return "Mastercard";
      case "DINERSCLUB":
        return "Diners Club";
      default:
        return "";
    }
  },
});
