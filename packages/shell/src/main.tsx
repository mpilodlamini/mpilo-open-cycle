import React from "react";
import "whatwg-fetch";
import { AppWithProviders } from "./App";
import WebFont from "webfontloader";
import * as Sentry from "@sentry/browser";
import { render } from "react-snapshot";

WebFont.load({
  google: {
    families: ["Open Sans: 400,600", "san-serif"],
  },
});

// @ts-ignore
const GLOBAL_CHECKOUT_OBJECT = window.GLOBAL_CHECKOUT_OBJECT || {};

Sentry.init({
  dsn: GLOBAL_CHECKOUT_OBJECT.sentryId,
  environment: GLOBAL_CHECKOUT_OBJECT.environment,
});

render(<AppWithProviders />, document.getElementById("root"));
