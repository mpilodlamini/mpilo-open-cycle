import { When } from "cypress-cucumber-preprocessor/steps";

When("I click on {string}", (element) => {
  cy.getElementByDataTestId(element).click();
});

When("I click the {string} inside {string} modal", (element, container) => {
  cy.getElementByDataTestId(container).contains(element).click();
});

When("I click the {string} {string} inside {string} modal", (element, htmlTag, container) => {
  cy.getElementByDataTestId(container).contains(htmlTag, element).click();
});

When("I click on {string} id", (id) => {
  cy.getElementById(id).click();
});

When("I click on {string} inside {string} id", (element, id) => {
  cy.getElementById(id).contains(element).click();
});

When("I scroll to {string}", (element) => {
  cy.get("button").contains(element).scrollIntoView();
});

When("I scroll to data-testid {string}", (element) => {
  cy.getElementByDataTestId(element).scrollIntoView();
});
