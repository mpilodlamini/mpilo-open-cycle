import { CHECKOUT } from "../contexts/CheckoutContext";

export const printInfo = (
  className: string,
  functionName: string,
  message: string | {} | []
): void => {
  if (!CHECKOUT.isCheckoutInTest) return;
  // eslint-disable-next-line no-console
  console.info(`${className} - ${functionName} : ${message}`);
};

export const printError = (
  className: string,
  functionName: string,
  message: string | {} | []
): void => {
  if (!CHECKOUT.isCheckoutInTest) return;
  // eslint-disable-next-line no-console
  console.error(`${className} - ${functionName} : ${message}`);
};

export const printWarning = (
  className: string,
  functionName: string,
  message: string | {} | []
): void => {
  if (!CHECKOUT.isCheckoutInTest) return;
  // eslint-disable-next-line no-console
  console.warn(`${className} - ${functionName} : ${message}`);
};
