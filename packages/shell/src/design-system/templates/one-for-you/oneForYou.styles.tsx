import { makeStyles, withStyles, TextField, Theme } from "@material-ui/core";
import { info, white, border } from "@peach/checkout-design-system";

export const useStyles = makeStyles((theme: Theme) => ({
  label: {
    fontSize: "14px",
    fontWeight: 600,
    color: "#53627C",
    marginBottom: "8px",
  },
  textField: {
    marginRight: "8px",

    [theme.breakpoints.down("sm")]: {
      marginRight: "0",
      marginBottom: "8px",
    },
  },
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },
    "&:disabled": {
      borderColor: "#C9CED6",
      cursor: "not-allowed",
    },
  },
}));

export const CssTextField = withStyles({
  root: {
    "& .MuiOutlinedInput-root": {
      paddingLeft: "calc(48% - 100px)",
      paddingRight: "calc(48% - 100px)",

      "@media (max-width: 420px)": {
        paddingLeft: "calc(48% - 100px)",
        paddingRight: "calc(48% - 100px)",
      },
      "& fieldset": {
        borderColor: border,
        color: "#53627C",
      },
      "&:hover fieldset": {
        borderColor: info,
      },
      "&.Mui-focused fieldset": {
        borderColor: info,
      },
    },
  },
})(TextField);
