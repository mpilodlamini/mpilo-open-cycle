import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import { MerchantLogo, MerchantLogoProps } from "../../../";

export default {
  title: "Controls/MerchantLogo",
} as Meta;

export const Default: Story<MerchantLogoProps> = (props) => {
  return <MerchantLogo {...props} />;
};

Default.args = {
  alt: "Peach Payments",
  src:
    "https://www.peachpayments.com/hubfs/raw_assets/public/Peach_Payments_December2020/images/peach-payments-logo.svg",
};
