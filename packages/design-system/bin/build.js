const esbuild = require("esbuild");
const fs = require("fs/promises");
const path = require("path");
const { nodeExternalsPlugin } = require("esbuild-node-externals");
const glob = require("glob");

const pglob = (toGlob) =>
  new Promise((resolve, reject) => {
    return glob(toGlob, (err, matches) => {
      if (err) {
        return reject(err);
      }
      return resolve(matches);
    });
  });

const main = async () => {
  try {
    const [tsx, ts] = await Promise.all([
      pglob("src/**/*.tsx"),
      pglob("src/**/*.ts"),
    ]);

    const base = {
      entryPoints: [...ts, ...tsx],
      bundle: true,
      platform: "browser",
      watch: process.env.NODE_ENV === "production" ? false : true,
      plugins: [nodeExternalsPlugin()],
      sourcemap: true,
    };

    await fs.rmdir(path.resolve(__dirname, "../dist"), { recursive: true });
    await Promise.all([
      // esbuild.build({ ...base, format: "cjs", outdir: "dist" }),
      esbuild.build({ ...base, format: "esm", outdir: "dist" }),
    ]);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
};

main();
