// TODO: this file needs a grand refactor

import { CHECKOUT } from "../contexts/CheckoutContext";

export interface ErrorMessageObject {
  [key: string]: { message: string; heading: string };
}

export class MessagesUtils {
  private readonly _amount: string;

  private readonly _merchantCurrencyCode: string;

  private readonly _merchant: string;

  readonly errorHeaders = [
    "Login Failed",
    "Payment Timeout",
    "Payment Error",
    "Payment Cancelled",
    "Payment Declined",
    "OTP Limit Exceeded",
    "Payment Failed",
  ];

  private readonly _successCodes: Array<string> = [
    "000.000.000",
    "000.100.110",
  ];

  constructor(
    amount = CHECKOUT.amount,
    merchant_currency_code = CHECKOUT.merchantCurrencyCode,
    merchant = CHECKOUT.merchant
  ) {
    this._amount = amount;
    this._merchantCurrencyCode = merchant_currency_code;
    this._merchant = merchant;
  }

  getSuccessCodes = (): string[] => this._successCodes;

  getNativeCardErrorMessages = (): ErrorMessageObject => ({
    ...this.get3DSecureErrorMessages(),
    ...this.getNativeCardBankErrorMessages(),
    ...this.getNativeCardSystemErrors(),
    ...this.getNativeCardAddressValidationErrorMessages(),
    ...this.get3DSecureRejectionErrorMessages(),
    "900.100": {
      message:
        "Transaction failed due to an internal error. Please go back to try again or select a different payment method.",
      heading: "Payment Error",
    },
  });

  get3DSecureRejectionErrorMessages = (): ErrorMessageObject => ({
    "100.380.401": {
      heading: "Payment Declined",
      message:
        "User authentication has failed. Please try again or go back and select another payment method.",
    },
    "100.390.105": {
      heading: "Technical Error",
      message:
        "There has been a technical error with the 3DSecure system. Please try again or go back and select another payment method.",
    },
    "100.390.106": {
      heading: "Payment Declined",
      message:
        "There has been an issue with the 3DSecure configuration. Contact the Merchant or go back and try another payment method.",
    },
    "100.390.107": {
      heading: "Payment Declined",
      message:
        "User authentication is currently unavailable. Please try again or go back and select another payment method.",
    },
    "100.390.111": {
      heading: "Payment Declined",
      message:
        "A communication error has occurred with VISA/Mastercard. Please try again or go back and select another payment method.",
    },
    "100.390.112": {
      heading: "Technical Error",
      message:
        "An error has occurred with the 3DSecure system. Please try again or go back and select another payment method.",
    },
    "100.390.113": {
      heading: "Device Not Supported",
      message:
        "3DSecure is not supported on this device. Please try again with another device or go back and select another payment method.",
    },
    "100.390.118": {
      heading: "Payment Declined",
      message:
        "Suspected Fraud. Please contact your bank for further information. Try again with another card or go back and select another payment method.",
    },
  });

  getNativeCardAddressValidationErrorMessages = (): ErrorMessageObject => ({
    "800.400.100": {
      heading: "Payment Declined",
      message:
        "Card address validation has failed. Please try again or go back and select another payment method.",
    },
    "800.400.101": {
      heading: "Invalid Street Address",
      message:
        "Card address validation has failed. Please try again or go back and select another payment method.",
    },
    "800.400.102": {
      heading: "Invalid Street Address",
      message:
        "Card address validation has failed. Please try again or go back and select another payment method.",
    },
    "800.400.104": {
      heading: "Invalid Postal Cod",
      message:
        "Card address validation has failed. Please try again or go back and select another payment method.",
    },
    "800.400.110": {
      heading: "Invalid Card Address",
      message:
        "Card address validation has failed. Please try again or go back and select another payment method.",
    },
    "800.400.150": {
      heading: "Invalid Card Address",
      message:
        "Card address validation has failed. Please try again or go back and select another payment method.",
    },
    "800.400.151": {
      heading: "Invalid Card Address",
      message:
        "Card address validation has failed. Please try again or go back and select another payment method",
    },
  });

  getNativeCardSystemErrors = (): ErrorMessageObject => ({
    "600.100.100": {
      heading: "Internal Error",
      message:
        "Please contact your Merchant or go back and select another payment method.",
    },
    "800.500.100": {
      heading: "Payment Declined",
      message:
        "Payment for an unknown reason. Please try again or go back and select another payment method.",
    },
    "800.500.110": {
      heading: "Unable to Process Payment",
      message:
        "Please contact your Merchant for support or go back and select another payment method.",
    },
    "800.600.100": {
      heading: "Payment Declined",
      message:
        "Payment is already being processed. Please wait before attempting again.",
    },
    "800.800.400": {
      heading: "System in Maintenance",
      message:
        "Please wait before trying again or go back and select another payment method.",
    },
    "800.800.800": {
      heading: "System Unavailable",
      message:
        "Please wait before trying again or go back and select another payment method.",
    },
    "800.800.801": {
      heading: "System in Maintenance",
      message:
        "Payment cannot be processed while in maintenance. Please try again later or go back and select another payment method.",
    },
  });

  get3DSecureErrorMessages = (): ErrorMessageObject => ({
    "000.400.101": {
      heading: "3DSecure Not Available",
      message:
        "3DSecure is not available on this card. Try again or select another payment method.",
    },
    "000.400.102": {
      heading: "3DSecure Not Supported",
      message:
        "3DSecure not enabled on this card. Try again or go back and select a different payment method.",
    },
    "000.400.103": {
      heading: "3DSecure Failed",
      message:
        "There was a technical error with 3DSecure. Try again or select another payment method.",
    },
    "000.400.104": {
      heading: "Payment Failed",
      message:
        "3DSecure is not configured correctly on this channel. Please contact the Merchant to resolve this issue.",
    },
    "000.400.105": {
      heading: "Unsupported Device",
      message:
        "3DSecure not possible on this device. Try again with a different device or go back and select another payment method.",
    },
    "000.400.106": {
      heading: "Payment Failed",
      message:
        "Payment failed due to failed 3DSecure response. Try again or go back and select another payment method.",
    },
    "000.400.107": {
      heading: "Payment Failed",
      message:
        "Transaction failed due to a communication error. Try again or go back and select another payment method.",
    },
    "000.400.108": {
      heading: "Card Not Supported",
      message:
        "This card is not supported. Try again or go back and select another payment method.",
    },
    "000.400.109": {
      heading: "3DSecure Not Supported",
      message:
        "3DSecure 2.0 is not enabled on this card. Try again or go back and select another payment method.",
    },
    "000.400.200": {
      heading: "Payment Failed",
      message:
        "Transaction failed due to a communication error. Try again or go back and select a different payment method.",
    },
  });

  getNativeCardBankErrorMessages = (): ErrorMessageObject => ({
    "800.100.100": {
      heading: "Payment Declined",
      message:
        "Payment declined for unknown reasons. Try again or select another payment method.",
    },
    "800.100.151": {
      heading: "Payment Declined",
      message:
        "Invalid card used. Please try another card or select another payment method",
    },
    "800.100.152": {
      heading: "Payment Declined",
      message:
        "Rejected by authorization system. Go back and try another payment method or cancel payment.",
    },
    "800.100.155": {
      heading: "Payment Declined",
      message:
        "Payment amount exceeds available credit. Try again with another card or go back and select another payment method.",
    },
    "800.100.157": {
      heading: "Payment Declined",
      message:
        "Incorrect expiry date. Try again or go back and select another payment method.",
    },
    "800.100.160": {
      heading: "Payment Declined",
      message:
        "This card has been blocked. Please try another card or go back and select another payment method.",
    },
    "800.100.161": {
      heading: "Payment Declined",
      message:
        "Too many failed attempts. Go back and select another payment method or cancel payment.",
    },
    "800.100.162": {
      heading: "Payment Declined",
      message:
        "Card limit has been reached. Please try another card or go back and select another payment method.",
    },
    "800.100.163": {
      heading: "Payment Declined",
      message:
        "Card limit has been reached. Please try another card or go back and select another payment method.",
    },
    "800.100.165": {
      heading: "Payment Declined",
      message:
        "This card has been marked as lost. Please contact your bank, try another card or go back and select another payment method.",
    },
    "800.100.166": {
      heading: "Payment Declined",
      message:
        "Verification has failed. Please try again or go back and select another payment method.",
    },
    "800.100.168": {
      heading: "Payment Declined",
      message:
        "This card has been restricted. Please try another card or go back and try another payment method.",
    },
    "800.100.169": {
      heading: "Card Not Supported",
      message:
        "Please try a different card or go back and select another payment method.",
    },
    "800.100.172": {
      heading: "Payment Declined",
      message:
        "Account has been blocked. Please try again with a card for a different account or go back and select another payment method.",
    },
    "800.100.175": {
      heading: "Card Not Supported",
      message:
        "This card brand is not supported. Please try another card or go back and select another payment method.",
    },
    "800.100.179": {
      heading: "Payment Declined",
      message:
        "Withdrawal amount limit has been exceeded. Please try another card or go back and select another payment method.",
    },
    "800.100.192": {
      heading: "Payment Declined",
      message:
        "Invalid security code. Try again or go back and select another payment method.",
    },
    "800.100.200": {
      heading: "Payment Declined",
      message: "Please try again or go back and select another payment method.",
    },
    "800.100.202": {
      heading: "Payment Declined",
      message:
        "Account unavailable. Please try another card associated with a different account or go back and select another payment method.",
    },
    "800.100.203": {
      heading: "Insufficient Funds",
      message:
        "Please try another card or go back and select another payment method.",
    },
    "800.100.208": {
      heading: "Payment Declined",
      message:
        "Direct debit is not enabled for this account. Please try another card or go back and select another payment method.",
    },
    "800.100.500": {
      heading: "Payment Declined",
      message:
        "Recurring payment has been stopped on this card. Please try another card or go back and select another payment method.",
    },
    "800.100.501": {
      heading: "Payment Declined",
      message:
        "Recurring payment has been stopped on this card. Please try another card or go back and select another payment method.",
    },
    "800.700.100": {
      heading: "Payment Declined",
      message:
        "Payment is currently being processed. Please try again later or go back and select another payment method.",
    },
    "800.800.102": {
      heading: "Invalid Street Address",
      message:
        "Invalid street address provided. Please try again or go back and select another payment method .",
    },
    "800.800.202": {
      heading: "Invalid Postal Code",
      message:
        "Invalid postal code provided. Please try again or go back and select another payment method.",
    },
    "800.800.302": {
      heading: "Invalid City",
      message:
        "Invalid City provided. Please try again or go back and select another payment method.",
    },
  });

  getErrorMessages = (): ErrorMessageObject => ({
    "100.380.401": {
      message:
        "Authentication of this payment has failed. Please go back to try again or select a different payment method.",
      heading: "Payment Failed",
    },
    "100.380.501": {
      message:
        "Authentication of this payment has failed. Please go back to try again or select a different payment method.",
      heading: "Payment Timeout",
    },
    "900.100": {
      message:
        "Transaction failed due to an internal error. Please go back to try again or select a different payment method.",
      heading: "Payment Error",
    },
    "100.396.101": {
      message:
        "You have cancelled your payment attempt. You can try again or select a different payment method.",
      heading: "Payment Cancelled",
    },
    "100.396.104": {
      message:
        "You have cancelled your payment attempt. You can try again or select a different payment method.",
      heading: "Payment Cancelled",
    },
    "800.100.152": {
      message: `Your payment for ${this._amount} ${this._merchantCurrencyCode} was declined. Please go back to try again or select a different payment method.`,
      heading: "Payment Declined",
    },
    "800.100.166": {
      message:
        "Authentication of this payment has failed. Please go back to try again or select a different payment method.",
      heading: "Payment Failed",
    },
    "800.100.162": {
      message:
        "You have exceeded the allowed number of OTP re-sends for this order. Should you wish to fulfill the order please go back and try again.",
      heading: "OTP Limit Exceeded",
    },
    "900.300.600": {
      message:
        "Authentication of this payment has failed. Please go back to try again or select a different payment method.",
      heading: "Payment Timeout",
    },
    "900.100.301": {
      message:
        "Your voucher credit does not match your purchase amount. Your payment has been declined. You can try again or select another payment method.",
      heading: "Payment Declined",
    },
    "900.100.400": {
      message:
        "Something went wrong! Our support team will be in touch with further details.",
      heading: "Payment Error",
    },
    "800.110.100": {
      message:
        "Your payment attempt has expired and you have not been charged. Please go back and try again.",
      heading: "Payment Timeout",
    },
    "000.200.000": {
      message:
        "OTP Verification has failed. Please resend the OTP and try again.",
      heading: "Payment Failed",
    },
    "900.100.201": {
      message:
        "Authentication of this payment has failed. Please go back to try again or select a different payment method.",
      heading: "Payment Declined",
    },
    "100.390.105": {
      message:
        "Authentication of this payment has failed because of a bank error. Please go back to try again or select a different payment method.",
      heading: "Payment Error",
    },
    "700.500.001": {
      message:
        "Transaction failed due to an internal error. Please go back to try again or select a different payment method.",
      heading: "Payment Error",
    },
    ...this.get3DSecureErrorMessages(),
    ...this.get3DSecureRejectionErrorMessages(),
    ...this.getNativeCardBankErrorMessages(),
  });

  getPendingPaymentMessages = (): ErrorMessageObject => ({
    "100.400.500": {
      message: `Your payment for ${this._amount} ${this._merchantCurrencyCode} is pending approval. Please contact ${this._merchant} to confirm your payment status. You will automatically be re-directed back to ${this._merchant}.`,
      heading: "Payment Pending",
    },
    "900.100.400": {
      message: `Processing of this payment has failed due to an internal error. Please contact ${this._merchant} for support.`,
      heading: "Payment Pending",
    },
  });

  getErrorCodes = (): Array<string> => [
    "100.396.101",
    "100.396.104",
    "800.100.152",
    "800.100.16",
    "900.300.600",
    "100.380.401",
    "100.380.501",
    "100.390.105",
    "700.500.001",
    "900.100.200",
    "800.100.162",
    "900.100.201",
  ];
}

export default MessagesUtils;
