import { Meta, Story } from "@storybook/react/types-6-0";
import { BrowserSupportBanner, BrowserSupportBannerProps } from "../../../";

export default {
  title: "Components/BrowserSupportBanner",
} as Meta;

export const Default: Story<BrowserSupportBannerProps> = (props) => {
  return <BrowserSupportBanner {...props} />;
};

Default.args = {
  onClick: () => alert("Supported"),
};
