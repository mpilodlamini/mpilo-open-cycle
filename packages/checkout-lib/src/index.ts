export type CheckoutStatus =
  | "loading"
  | "error"
  | "success"
  | "pending"
  | "initializing"
  | "cancelling"
  | undefined;

export type StatusEvent = {
  type: "status";
  value: CheckoutStatus;
  code?: string;
  heading?: string;
  message?: string | string[];
  continueText?: string;
  cancelText?: string;
};

export type FeedbackEvent = {
  type: "update-feedback";
  feedback: {
    heading?: string;
    message?: string | string[];
    continueText?: string;
    cancelText?: string;
  };
};

export type CheckoutConfirm = {
  type: "set-checkout-confirm";
  confirm: boolean;
};

export type GAEvent =
  | { type: "track-page"; title: string }
  | {
      type: "track-event";
      event: { action: string; category: string; label?: string };
    }
  | {
      type: "track-purchase";
      event: {
        value: string;
        currency: string;
        transactionId: string;
      };
    }
  | { type: "track-payment-information" }
  | {
      type: "track-error-events";
      event: {
        heading:
          | "Login Failed"
          | "Payment Timeout"
          | "Payment Error"
          | "Payment Cancelled"
          | "Payment Declined"
          | "OTP Limit Exceeded"
          | "Payment Failed"
          | string;
        category: string;
        code: string;
      };
    };

export type CheckoutRedirectEvent = {
  type: "checkout-redirect";
  url: string;
  data?: any;
} & ({ method: "GET" } | { method: "POST" });

export type CheckoutEvent =
  | StatusEvent
  | FeedbackEvent
  | { type: "reset" }
  | { type: "timeout" }
  | GAEvent
  | CheckoutRedirectEvent
  | CheckoutConfirm
  | { type: "cancelled" };

export interface PluginPaymentMethod {
  name: string;
  label: string;
  image: string;
  type: string;
  health?: string;
  accessible?: boolean;
  origin: string;
  location: string;
  id?: string;
}

export interface CheckoutConfig {
  isNativeCardForm: boolean;
  isCheckoutInTest: boolean;
  forceDefaultMethod: boolean;
  version: string;
  assetsBasePath: string;
  payonBaseUrl: string;
  payonActionUrl: string;
  cancelUrl: string;
  successUrl: string;
  code: string;
  merchant: string;
  merchantCurrencyCode: string;
  merchantTransactionId: string;
  merchantLogo: string;
  merchantKey: string;
  merchantInvoiceId: string;
  privateKey: string;
  baseUrl: string;
  shopperResultUrl: string;
  siteCode: string;
  status?: CheckoutStatus;
  switchBaseUrl: string;
  countryCode: string;
  currencyCode: string;
  amount: string;
  paymentBrand: string;
  checkoutId: string;
  source: string;
  notifyUrl: string;
  trackingId: string;
  environment: string;
  title: string;
  redirectPostData: {};
  errorMessages: string[];
  billingCountry: string;
  billingCity: string;
  billingStreet1: string;
  billingPostcode: string;
  customerEmail: string;
  merchantInfo: [];
  redirectUrl: string;
  sentryId: string;
  reRenderCheckout: boolean;
  responseData: any;
  paymentMethods: string[] | any;
  pluginPaymentMethods: PluginPaymentMethod[] | any;
}

export type Subscription = "checkout-bootstrap" | "checkout-cancellation";

export type SubscribeEvent = { origin: string; data: { message: string } } & {
  data: {
    message: Subscription;
    config: CheckoutConfig;
  };
};

export const Checkout = (checkoutOrigin: string) => {
  const publish = (events: CheckoutEvent[]) =>
    window.parent.postMessage(
      {
        message: "push-checkout-events",
        events,
      },
      checkoutOrigin,
      []
    );

  const subscribe = <T extends Subscription>(
    message: T,
    handler: (event: { data: { message: T } } & SubscribeEvent) => void
  ): ((e: { data: { message: T } } & SubscribeEvent) => void) => {
    const listener = async (e: { data: { message: T } } & SubscribeEvent) => {
      if (e.origin === checkoutOrigin && e.data.message === message) {
        handler(e);
      }
    };
    window.addEventListener("message", listener);
    return listener;
  };

  const unsubscribe = <T extends Subscription>(
    listener: (event: { data: { message: T } } & SubscribeEvent) => void
  ) => {
    window.removeEventListener("message", listener);
  };

  return {
    /**
     * The low level mechanism that plugins use to communicate with Checkout.
     * You will rarely need to drop down to this level unless you are doing something complicated.
     * You can publish multiple events at one.
     *
     * @param events A list of events to send to checkout
     * @returns
     */
    publish,
    /**
     * Tells Checkout that your plugin is ready
     * to start receiving events and indicates that it's loading
     */
    initialize: () => {
      return publish([{ type: "status", value: "initializing" }]);
    },
    /**
     * Tells Checkout that your plugin is finished
     * loading and should be displayed to the user
     */
    initialized: () => {
      return publish([{ type: "status", value: undefined }]);
    },
    /**
     * Indicates to Checkout that your plugin
     * is in a loading state
     *
     * @param heading The heading of the loading modal
     * @param message The message body of the loading modal
     * @returns void
     */
    loading: (heading?: string, message?: string) => {
      return publish([{ type: "status", value: "loading", heading, message }]);
    },
    /**
     * Indicates to Checkout that your plugin has
     * successfully completed a payment
     *
     * !!! This will redirect the user back to the store they came from
     *
     * @param heading The heading of the modal
     * @param message The message body of the modal
     * @returns void
     */
    success: (heading?: string, message?: string) => {
      return publish([{ type: "status", value: "success", heading, message }]);
    },
    /**
     * Indicates to Checkout that your plugin
     * Has encountered an error it can't recover from
     * and allows the user to restart their payment
     *
     * @param heading The heading of the  modal
     * @param message The message body of the  modal
     * @returns
     */
    error: (heading?: string, message?: string) => {
      return publish([{ type: "status", value: "error", heading, message }]);
    },
    /**
     * Indicates to Checkout that your plugin has
     * finished cancelling
     *
     * @returns void
     */
    cancelled: () => {
      return publish([{ type: "cancelled" }]);
    },
    /**
     * This allows you to subscribe to events from Checkout, this returns the instance of your handler if you need to unsubscribe.
     * @param event the event to subscribe to
     * @param handler the event handler
     */
    subscribe,
    /**
     * This allows you to subscribe to events from Checkout, this returns the instance of your handler if you need to unsubscribe.
     * @param event the event to subscribe to
     * @param handler the event handler
     * @returns an event handler
     */
    on: subscribe,
    /**
     * This allows you to unsubscribe your handler.
     * @param handler the event handler
     */
    unsubscribe,
    /**
     * This allows you to unsubscribe your handler.
     * @param handler the event handler
     */
    off: unsubscribe,
    /**
     * This allows you to redirect your user
     */
    redirect: {
      /**
       * This allows you to redirect your user with a get request
       * @param url url to direct to
       */
      get: (url: string) =>
        publish([{ type: "checkout-redirect", url, method: "GET" }]),
      /**
       * This allows you to redirect your user with a post request
       * @param url url to direct to
       * @param data the post body
       */
      post: (url: string, data?: any) =>
        publish([{ type: "checkout-redirect", url, method: "POST", data }]),
    },
    /**
     * Google analytics helpers
     */
    ga: {
      trackPage: (title: string) => publish([{ type: "track-page", title }]),
      trackEvent: (event: {
        action: string;
        category: string;
        label?: string;
      }) => publish([{ type: "track-event", event }]),
      trackPurchase: (event: {
        value: string;
        currency: string;
        transactionId: string;
      }) => publish([{ type: "track-purchase", event }]),
      trackPaymentInformation: () =>
        publish([{ type: "track-payment-information" }]),
      trackErrorEvents: (event: {
        heading: string;
        category: string;
        code: string;
      }) => publish([{ type: "track-error-events", event }]),
      events: {
        error_go_back: "error_goback",
        collapse: "collapse",
        copy_code: "copy_code",
        error: "error",
        verify: "verify",
        resend: "resend",
        refresh_code: "refresh_code",
        sign_in: "sign_in",
        opt_resend_modal: "otpResend_modal",
        error_card_widget: "error_cardwidget",
        pay: "pay",
        cancel_payment: "cancel_payment",
        select_eft_bank: "select_eftbank",
        select_new_bank: "select_newbank",
        failed_modal: "failed_modal",
        otp_limit_modal: "otpLimit_modal",
        declined_modal: "declined_modal",
        cancel_modal: "cancel_modal",
        error_modal: "error_modal",
        time_out_modal: "timeout_modal",
        failed_login_modal: "failedLogin_modal",
        continue: "continue",
        try_again: "tryagain",
        partial_payment_model: "partialpayment_modal",
        payment_pending: "payment_pending",
        payment_success: "payment_success",
        cancel_checkout: "cancel_checkout",
        cancel_confirm: "cancel_confirm",
        cancel_go_back: "cancel_goBack",
        cancel_browser_back: "cancel_browser_back",
        success_browser_back: "success_browser_back",
      },
    },
  };
};

export default Checkout;
