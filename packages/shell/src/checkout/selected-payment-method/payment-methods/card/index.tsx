import $ from "jquery";
import CheckoutService from "../../../../services/checkoutService";
import MessagesUtils, {
  ErrorMessageObject,
} from "../../../../utils/messagesUtil";
import PluginManager from "../../../../utils/pluginManager";
import { PAYMENT_METHOD_NAMES } from "../../../../utils/pluginConstants";
import { GA } from "../../../../utils/googleAnalyticsConstants";
import LoaderModal from "../../../../components/loaders/loadingModal";
import {
  AJAX_DATA_RESULT,
  PEACH_STATE,
  TRANSACTION_STATUS,
} from "../../../../utils/constants";
import ErrorModal from "../../../../components/errors/modals/errorModal";
import PostForm from "../../../../components/form/postForm";
import { getBrowserSpecificInformation } from "../../../../utils/browser";
import CardRenderElements from "./card-payment";
import { useCardState } from "./hooks";
import { GoogleAnalyticsContext } from "../../../../contexts/GoogleAnalyticsContext";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";
import { CardPostData } from "../../../../design-system/templates/card-form/cardFormInterfaces";
import TimeUtil from "../../../../utils/timeUtil";
import { UtilContext } from "../../../../contexts/UtilContext";
import React, { useContext, useMemo, useCallback, useEffect } from "react";

export interface CardProps {}

const paymentBrand: { [key: string]: string } = {
  AMERICANEXPRESS: "AMEX",
  VISA: "VISA",
  MASTERCARD: "MASTER",
  DINERSCLUB: "DINERS",
};

const messagesUtils: MessagesUtils = new MessagesUtils();
const errorMessages: ErrorMessageObject = messagesUtils.getErrorMessages();
const cardPaymentErrorNames: Array<string> = [
  "InvalidCheckoutIdError",
  "PciIframeSubmitError",
  "PciIframeCommunicationError",
  "WidgetError",
];

let domElementCheckInterval: NodeJS.Timeout | null = null;

/**
 * Card component container for the card widget from CopyAndPay
 */
const Card = (props: CardProps) => {
  const googleAnalytics = useContext(GoogleAnalyticsContext);
  const checkout = useContext(CheckoutContext);
  const { timeUtil } = useContext(UtilContext);

  const checkoutService: CheckoutService = useMemo(
    () => new CheckoutService(),
    []
  );

  const pluginManager: PluginManager = useMemo(
    () => new PluginManager(timeUtil),
    [timeUtil]
  );

  const [
    {
      showCardCaptureScreen,
      showLoadingScreen,
      showLoadingModal,
      showErrorModal,
      showErrorScreen,
      showSuccessScreen,
      loadingModalHeading,
      loadingModalMessage,
      errorModalHeading,
      errorModalMessage,
      errorModalActionButton,
      errorModalBackButton,
      threeDSecureRedirectUrl,
      threeDSecurePostData,
      code,
      transactionId,
      redirectUrl,
      redirectPostData,
    },
    action,
  ] = useCardState();

  /**
   * Updates the global wpwlOptions object when the script has been loaded/is ready
   */
  const onReady = useCallback(() => {
    const wpwlOptions = {
      style: "plain",
      iframeStyles: {
        "card-number-placeholder": {
          "font-size": "14px",
          color: "#8a94a6",
          "font-family": "Open Sans",
        },
        "cvv-placeholder": {
          "font-size": "14px",
          color: "#8a94a6",
          "font-family": "Open Sans",
        },
      },
      autofocus: "card.number",
      brandDetection: true,
      errorMessages: {
        cardHolderError: "Invalid card holder name",
        cvvError: "Invalid CVV entered",
        cardNumberError: "Invalid card number",
        expiryMonthError: "Invalid expiry date",
        expiryYearError: "Invalid expiry date",
      },
      onReady: () => {
        // Not the cleanest approach
        // This event fires first
        // We need to stop showing the Loading Modal here if the widget does not render

        if ($("button:contains('Pay now')").length === 0) {
          domElementCheckInterval = setInterval(() => {
            // The only way to know if it has not rendered properly is by looking for this phrase
            if ($("div:contains('Payment cannot be completed')").length !== 0) {
              // If this phrase is present the onReadyIframeCommunication will never fire
              // and the loader will never stop
              clearInterval(domElementCheckInterval as NodeJS.Timeout);
              action({ type: "stop-loading-modal" });
            }
          }, 100);
        }
      },
      onReadyIframeCommunication: () => {
        if (domElementCheckInterval) {
          clearInterval(domElementCheckInterval as NodeJS.Timeout);
        }

        domElementCheckInterval = setInterval(() => {
          if ($(".spinner").length === 0) {
            clearInterval(domElementCheckInterval as NodeJS.Timeout);
            action({ type: "stop-loading-modal" });
          }
        }, 100);
      },
      onBeforeSubmitCard: () => {},
      onBeforeSubmitVirtualAccount: () => {},
      onError: (error: any) => {
        if (cardPaymentErrorNames.indexOf(error.name) !== -1) {
          googleAnalytics.trackEvent(
            GA.EVENTS.ERROR_CARD_WIDGET,
            PAYMENT_METHOD_NAMES.CARD,
            error.name
          );
        }
      },
      onAfterSubmit: () => {
        timeUtil.updateCheckoutTimer();
        pluginManager.createPollingListener();
        pluginManager.updateTransactionStatus("awaiting_confirmation");
      },
    };

    wpwlOptions.onReady();
    wpwlOptions.onBeforeSubmitCard = () => {
      googleAnalytics.trackEvent(GA.EVENTS.PAY, PAYMENT_METHOD_NAMES.CARD);
      return true;
    };

    // Attach to the local card setup object to the global window object
    // @ts-ignore
    window.wpwlOptions = wpwlOptions;
  }, [action, pluginManager, timeUtil, googleAnalytics]);

  /**
   * CopyAndPay widget requires a global object for it to be initialized
   * The global setup object is attached to the Javascript object and passed to CopyAndPay to allow customization
   * via a HTML script tag
   * @param checkoutId
   */
  const loadCopyAndPayWidgetJS = useCallback(
    (checkoutId: string) => {
      onReady();

      const script = document.createElement("script");

      script.setAttribute("async", "");
      script.onerror = (): void => {
        action({ type: "show-error-screen" });
      };
      script.src = `${checkout.payonBaseUrl}/v1/paymentWidgets.js?checkoutId=${checkoutId}`;

      document.body.appendChild(script);
    },
    [action, onReady, checkout.payonBaseUrl]
  );

  /**
   * Fetches the payon checkout id from the Checkout backend on success
   * @param checkoutId
   */
  const getCheckoutId = useCallback(
    (checkoutId: string): void => {
      const postData = { checkout_id: checkoutId };

      const successCallback = (result: any): void => {
        if (
          !(
            result &&
            result.response &&
            result.response.result &&
            result.response.result.code &&
            errorMessages[result.response.result.code]
          )
        ) {
          timeUtil.updateCheckoutTimer();
          pluginManager.createPollingListener();
          loadCopyAndPayWidgetJS(result.response.payon_checkout_id);
        }
      };

      const errorCallback = (data: any): void => {
        if (
          data &&
          data.responseJSON &&
          data.responseJSON.response.code &&
          data.responseJSON.response.code === "800.110.100"
        ) {
          pluginManager.redirectToPluginSpecificUrl();
          return;
        }

        if (
          data &&
          data.responseJSON &&
          data.responseJSON.response.response_data
        ) {
          const { response_code } =
            data.responseJSON.response.response_data.result;
          if (response_code === "800.110.100") {
            pluginManager.redirectToPluginSpecificUrl();
          }
        }

        action({ type: "show-error-screen" });
      };

      checkoutService.postFormEncodedContent(
        `${checkout.baseUrl}payon-checkout`,
        postData,
        successCallback,
        errorCallback
      );
    },
    [
      action,
      pluginManager,
      loadCopyAndPayWidgetJS,
      timeUtil,
      checkout.baseUrl,
      checkoutService,
    ]
  );

  const serverError = useCallback(
    (serverErrorCode: string): void => {
      action({
        type: "server-error",
        code: serverErrorCode,
      });

      googleAnalytics.trackEvent(
        GA.EVENTS.ERROR,
        PAYMENT_METHOD_NAMES.CARD,
        code
      );
      googleAnalytics.trackErrorEvents(
        GA.EVENTS.ERROR_GO_BACK,
        PAYMENT_METHOD_NAMES.CARD,
        code
      );
    },
    [action, code, googleAnalytics]
  );

  const postNativeCardContent = useCallback(
    (parameters: CardPostData): void => {
      googleAnalytics.trackPaymentInformation();
      googleAnalytics.trackEvent(GA.EVENTS.PAY, PAYMENT_METHOD_NAMES.CARD);

      action({
        type: "start-loading-modal",
      });

      const postData = {
        checkoutId: checkout.checkoutId,
        paymentBrand: paymentBrand[parameters.brand],
        "card.expiryMonth": parameters.expiryMonth,
        "card.expiryYear": `20${parameters.expiryYear}`,
        "card.number": parameters.number,
        "card.holder": parameters.holder,
        "card.cvv": parameters.cvv,
        "customer.email": checkout.customerEmail,
        "billing.city": parameters.city,
        "billing.country": parameters.country,
        "billing.street1": parameters.street1,
        "billing.postcode": parameters.postalCode,
        ...getBrowserSpecificInformation(),
      };

      const successCallback = (data: any) => {
        if (data && data.status === AJAX_DATA_RESULT.SUCCESS) {
          timeUtil.updateCheckoutTimer();
          pluginManager.createPollingListener();

          const successTransactionId = data.response?.response_data?.id || "";
          const successCode = data.response?.response_data?.result?.code || "";
          const successThreeDSecureRedirectUrl =
            data.response?.response_data?.redirect?.url || "";
          const successThreeDSecurePostData =
            data.response?.response_data?.redirect?.parameters || [];
          const successRedirectPostData =
            data.response?.redirect_post_data || {};
          const successRedirectUrl = data.response?.redirect_url || "";

          const isValidSuccessCode = new RegExp(
            "^(000.000.|000.100.1|000.[36])|^(000.400.0[^3]|000.400.100)"
          );

          const isSuccessFlow = isValidSuccessCode.test(successCode);

          if (successThreeDSecureRedirectUrl) {
            action({
              type: "start-3ds-redirect",
              code: successCode,
              transactionId: successTransactionId,
              url: successThreeDSecureRedirectUrl,
              postData: successThreeDSecurePostData,
            });
          } else if (isSuccessFlow) {
            action({
              type: "3ds-success",
              code: successCode,
              postData: successRedirectPostData,
              url: successRedirectUrl,
            });
          } else {
            serverError(successCode);
          }
        }
      };

      const errorCallback = (): void => {
        serverError("900.100");
      };

      checkoutService.postFormEncodedContent(
        `${checkout.baseUrl}card-payment`,
        postData,
        successCallback,
        errorCallback
      );
    },
    [
      action,
      pluginManager,
      serverError,
      googleAnalytics,
      timeUtil,
      checkout.baseUrl,
      checkout.checkoutId,
      checkout.customerEmail,
      checkoutService,
    ]
  );

  const redirectLoaderOnAction = useCallback(
    (transactionStatus: string): void => {
      action({
        type: "start-loading-modal",
        heading: `Redirecting to ${checkout.merchant}`,
        message: "Please wait you are being redirected.",
      });

      pluginManager.updateTransactionStatus(
        transactionStatus,
        transactionId,
        code
      );
    },
    [action, code, transactionId, pluginManager, checkout.merchant]
  );

  const closeErrorModal = useCallback((): void => {
    googleAnalytics.trackEvent(
      GA.EVENTS.ERROR_GO_BACK,
      PAYMENT_METHOD_NAMES.CARD
    );
    action({ type: "hide-error-modal" });
    if (code !== "100.380.401") {
      googleAnalytics.trackEvent(GA.EVENTS.COLLAPSE, PAYMENT_METHOD_NAMES.CARD);
    }
  }, [action, code, googleAnalytics]);

  const redirectToClient = useCallback(
    (status: string): void => {
      action({
        type: "start-loading-modal",
        heading: `Redirecting to ${checkout.merchant}`,
        message: `We're taking you back to ${checkout.merchant}`,
      });

      if (status === PEACH_STATE.PENDING) {
        pluginManager.updateTransactionStatus(TRANSACTION_STATUS.CANCELLED);
      } else {
        pluginManager.redirectToPluginSpecificUrl(
          redirectUrl,
          redirectPostData
        );
      }
    },
    [action, redirectUrl, redirectPostData, pluginManager, checkout.merchant]
  );

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (!checkout.isNativeCardForm) {
      getCheckoutId(checkout.checkoutId);
    } else {
      action({ type: "start-loading-screen" });
      timer = setTimeout(() => {
        action({ type: "stop-loading-screen" });
      }, 1000);
    }

    return () => {
      checkoutService.cancelRequest();
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [
    action,
    getCheckoutId,
    checkout.checkoutId,
    checkout.isNativeCardForm,
    checkoutService,
  ]);

  const threeDSecureForm = threeDSecureRedirectUrl ? (
    <PostForm
      redirectUrl={threeDSecureRedirectUrl}
      fieldsCollection={threeDSecurePostData}
    />
  ) : null;

  return (
    <>
      <CardRenderElements
        showLoadingScreen={showLoadingScreen}
        showErrorScreen={showErrorScreen}
        showSuccessScreen={showSuccessScreen}
        showCardCaptureScreen={showCardCaptureScreen}
        code={code}
        redirectUrl={redirectUrl}
        redirectPostData={redirectPostData}
        redirectToClient={redirectToClient}
        postNativeCardContent={postNativeCardContent}
      />

      {threeDSecureForm}

      <LoaderModal
        open={showLoadingModal}
        title={loadingModalHeading}
        label={loadingModalMessage}
      />

      <ErrorModal
        open={showErrorModal}
        title={errorModalHeading}
        label={errorModalMessage}
        continueText={errorModalActionButton}
        cancelText={errorModalBackButton}
        onClick={() => redirectLoaderOnAction(TRANSACTION_STATUS.CANCELLED)}
        onCancel={closeErrorModal}
      />
    </>
  );
};

export default Card;
