import React, { HTMLAttributes, useEffect, useRef } from "react";
import { RedirectForm } from "./RedirectForm";

export interface RedirectorProps extends HTMLAttributes<HTMLFormElement> {
  url: string;
  data: { [index: string]: any };
  method: "GET" | "POST";
}

export const Redirector = (props: RedirectorProps) => {
  const formRef = useRef<HTMLFormElement>(null);

  useEffect(() => {
    if (formRef.current) {
      formRef.current.submit();
    }
  }, []);

  return <RedirectForm {...props} ref={formRef} />;
};
