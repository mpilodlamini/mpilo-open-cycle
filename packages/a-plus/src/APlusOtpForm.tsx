import React, { useContext } from "react";
import {
  tertiaryGreen,
  white,
  border,
} from "@peach/checkout-design-system/dist/variables";
import {
  makeStyles,
  withStyles,
  Theme,
  Box,
  Grid,
  Button,
} from "@material-ui/core";
import { Formik, Field } from "formik";
import * as yup from "yup";
import { NumberFormat } from "./MUITextField";
import { SnackbarStatic, FeedbackType } from "@peach/checkout-design-system";
import { ConfigContext } from "./contexts/ConfigContext";
import { useRef } from "react";
import { useEffect } from "react";

export const useStyles = makeStyles((theme: Theme) => ({
  label: {
    fontSize: "14px",
    fontWeight: 600,
    color: "#53627C",
    marginBottom: "8px",
  },
  textField: {
    marginRight: "8px",

    [theme.breakpoints.down("xs")]: {
      marginRight: "0",
      marginBottom: "8px",
    },
  },
  textNote: {
    color: "#8A94A6",
    textAlign: "center",
  },
  button: {
    border: `1px solid ${tertiaryGreen}`,
    backgroundColor: tertiaryGreen,
    color: white,
    textTransform: "none",
    width: "auto",

    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },

    "&:hover": {
      borderColor: "#d5d5d5",
    },
    "&:disabled": {
      borderColor: "#C9CED6",
      cursor: "not-allowed",
    },
  },
  link: {
    color: "#8A94A6",
    textTransform: "none",
    textDecoration: "underline",
    fontWeight: 600,
    paddingLeft: "5px",
    "&:hover": {
      cursor: "pointer",
    },
  },
  snackbarTimer: {
    width: "100%",
    position: "relative",
    marginTop: "30px",
  },
  aplusIcon: {
    width: "68px",
    height: "42px",
    marginLeft: "8px",
  },
}));

export const APlusOtpForm = ({
  onSubmit,
  onResend,
  otpResend,
  otpVerify,
  cellNumber,
}: {
  onSubmit: (values: { otp: string }) => void;
  onResend: () => void;
  otpResend: number;
  otpVerify: boolean;
  cellNumber: string;
}) => {
  const classes = useStyles();
  const top = useRef<HTMLImageElement | null>(null);
  const {
    time: { timeLeft },
  } = useContext(ConfigContext);

  useEffect(() => {
    setTimeout(() => {
      top?.current?.scrollIntoView({ behavior: "smooth" });
    });
  }, []);

  return (
    <Box width={1} height={1}>
      <Box display="flex" justifyContent="center" mb={3}>
        <img
          ref={top}
          data-testid="aplus-otp-logo"
          src="images/a+.svg"
          alt="A+"
          className={classes.aplusIcon}
        />
      </Box>

      <Box mt={3}>
        <Formik
          enableReinitialize
          validationSchema={yup.object({
            otp: yup
              .string()
              .required("Required")
              .min(7)
              .matches(
                /^([0-9])\s([0-9])\s([0-9])\s([0-9])$/,
                "Invalid OTP, must be a 4 digit number."
              ),
          })}
          initialValues={{ otp: "" }}
          onSubmit={onSubmit}
        >
          {(props) => (
            <form onSubmit={props.handleSubmit}>
              <Box mb={2} className={classes.textField}>
                <Box mb={4} textAlign="center" data-testid="aplus-otp-label">
                  One Time Password sent to <b>{cellNumber}</b>. Please enter
                  your OTP below.
                </Box>
              </Box>
              <Grid container>
                <Grid item sm={12} xs={12}>
                  <Box mb={2}>
                    <Field
                      label="Enter OTP"
                      data-testid="aplus-otp-input"
                      type="text"
                      name="otp"
                      component={NumberFormat}
                      fullWidth
                      id="otp"
                      format="# # # #"
                      aria-describedby="otp"
                      variant="outlined"
                      placeholder="_ _ _ _"
                    />
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box mb={2} data-testid="aplus-otp-resendotp">
                    {otpResend > 0 && (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                        }}
                      >
                        <div className={classes.textNote}>
                          Didn’t receive an OTP?
                        </div>
                        <a onClick={onResend} className={classes.link}>
                          Resend OTP
                        </a>
                      </div>
                    )}
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box style={{ textAlign: "right", width: "100%" }}>
                    <Button
                      data-testid="aplus-otp-btn"
                      type="submit"
                      className={classes.button}
                      variant="contained"
                      disableElevation
                      disabled={
                        !otpVerify ||
                        !props.dirty ||
                        !props.isValid ||
                        props.isSubmitting
                      }
                    >
                      Pay Now
                    </Button>
                  </Box>
                </Grid>
                <Box
                  data-testid="aplus-otp-timer"
                  className={classes.snackbarTimer}
                >
                  <SnackbarStatic
                    label="Session Expires in"
                    title=""
                    time={timeLeft}
                    timed
                    feedback={FeedbackType.TIMER}
                  />
                </Box>
              </Grid>
            </form>
          )}
        </Formik>
      </Box>
    </Box>
  );
};
