import React, { useContext, useEffect, useMemo } from "react";
import { makeStyles, Box } from "@material-ui/core";
import { PEACH_STATE } from "../../../../utils/constants";
import FeedbackScreen from "../../../../design-system/organisims/feedback/screen/FeedbackScreen";
import PluginManager from "../../../../utils/pluginManager";
import { printInfo } from "../../../../utils/logger";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";
import { GoogleAnalyticsContext } from "../../../../contexts/GoogleAnalyticsContext";
import { GA } from "../../../../utils/googleAnalyticsConstants";
import MessagesUtils from "../../../../utils/messagesUtil";

interface FeedbackPageProps {
  status: string;
  code: string;
  messages?: { heading?: string; message?: string | string[] };
  paymentMethod?: string;
  isOpen?: boolean;
  onRedirect?: () => void;
  redirectPostData?: {};
  redirectUrl?: string;
  isTimed?: boolean;
  transactionId?: string;
}

const useStyles = makeStyles(() => ({
  container: {
    height: "100%",
    padding: "0",
  },
}));

const componentName = "FeedbackPage";

export const FeedbackPage = ({
  paymentMethod,
  code,
  messages,
  onRedirect,
  redirectPostData,
  redirectUrl,
  status,
  transactionId,
  isTimed,
}: FeedbackPageProps) => {
  const classes = useStyles();
  const checkout = useContext(CheckoutContext);
  const ga = useContext(GoogleAnalyticsContext);
  useEffect(() => {
    printInfo(componentName, "componentDidMount", "Mounted");

    if (status === PEACH_STATE.PENDING) {
      ga.trackPage(GA.PAGES.PAYMENT_PENDING);
      ga.trackEvent(GA.EVENTS.PAYMENT_PENDING, paymentMethod || " ", code);
    } else if (status === PEACH_STATE.SUCCESS) {
      ga.trackPurchase(
        checkout.amount || "",
        checkout.merchantCurrencyCode || " ",
        transactionId || " "
      );
      ga.setCheckoutOption(`${checkout.amount}`, paymentMethod || " ");
      ga.trackPage(GA.PAGES.PAYMENT_SUCCESS);
      ga.trackEvent(GA.EVENTS.PAYMENT_SUCCESS, paymentMethod || " ", code);
    }
  }, [ga, checkout, paymentMethod, transactionId]);

  const messagesUtils = useMemo(() => new MessagesUtils(), []);

  const feedbackTitle = useMemo(() => {
    const pending = messagesUtils.getPendingPaymentMessages();
    if (messages) {
      return messages.heading;
    }
    if (status === PEACH_STATE.PENDING) {
      return pending && pending[code] && pending[code].heading
        ? pending[code].heading
        : "Payment Pending";
    }
    return "Payment Successful";
  }, [messagesUtils, code]);

  const message = useMemo(() => {
    const pending = messagesUtils.getPendingPaymentMessages();
    if (messages) {
      return messages.message;
    }
    if (status === PEACH_STATE.PENDING) {
      return (
        (pending && pending[code] && pending[code].message) ||
        `Your payment is pending approval. Please contact ${checkout.merchant} to confirm payment status. You will automatically be redirected back to ${checkout.merchant}`
      );
    }
    return `You will automatically be redirected back to ${checkout.merchant}`;
  }, [messagesUtils, code]);

  const goBackFunction = (): void =>
    ga.trackEvent("success_goback", paymentMethod || " ", code);

  return (
    <Box
      maxWidth={1}
      height={1}
      className={classes.container}
      id="success-screen"
      data-testid="success-screen"
    >
      <FeedbackScreen
        feedback={status.toLocaleLowerCase() as any}
        feedbackTitle={feedbackTitle!}
        message={message!}
        buttonAction="Return to Store"
        text=""
        isTimed={!!isTimed}
        clickCallbackGoBack={goBackFunction}
        clickCallback={() => {
          if (onRedirect) {
            onRedirect();
          } else {
            const pluginManager = new PluginManager();
            pluginManager.redirectToPluginSpecificUrl(
              redirectUrl,
              redirectPostData
            );
          }
          ga.trackEvent("success_goback", paymentMethod || " ", code);
        }}
        redirectPostData={redirectPostData}
        redirectUrl={redirectUrl}
        status={status}
        paymentMethod={paymentMethod}
        fullWidthButton
        isBottom
      />
    </Box>
  );
};

export default FeedbackPage;
