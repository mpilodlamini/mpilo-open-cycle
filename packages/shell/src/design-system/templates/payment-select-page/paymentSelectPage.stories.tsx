import React from "react";
import PaymentSelectPage from "./paymentSelectPage";

export default {
  title: "templates / Checkout",
  component: PaymentSelectPage,
};

export const Checkout = (args: any) => <PaymentSelectPage {...args} />;
const emptyFunction = () => {};

Checkout.args = {
  paymentItem: [
    {
      pluginLabel: "Ozow",
      pluginId: "ozow",
      imageUrl: [
        "https://www.merchantequip.com/images/logos/small-visa-mc.gif",
      ],
      imageAlternative: "",
      isChecked: false,
      clickCallback: emptyFunction,
    },
    {
      pluginLabel: "Mobicred",
      pluginId: "mobicred",
      imageUrl: [
        "https://www.merchantequip.com/images/logos/small-visa-mc.gif",
      ],
      imageAlternative: "",
      isChecked: false,
      clickCallback: emptyFunction,
    },
    {
      pluginLabel: "EFTSecure",
      pluginId: "eftsecure",
      imageUrl: [""],
      imageAlternative: "Visa Card, Mastercard, Amex",
      isChecked: false,
      clickCallback: emptyFunction,
    },
  ],
};
