import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  header: {
    borderBottom: "1px solid #e0e0e8",
    minHeight: "72px",
    zIndex: 25,

    [theme.breakpoints.up("sm")]: {
      minHeight: "100px",
    },
  },
  wrapper: {
    padding: "0 32px",
    minHeight: "90px",

    [theme.breakpoints.down("xs")]: {
      padding: "0 16px",
    },

    [theme.breakpoints.up("sm")]: {
      minHeight: "105px",
    },
  },
  logo: {
    justifyContent: "center",

    [theme.breakpoints.down("sm")]: {
      justifyContent: "flex-start",
    },
  },
}));

export default useStyles;
