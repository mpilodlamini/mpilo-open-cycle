import { CHECKOUT } from "../contexts/CheckoutContext";

export const CHECKOUT_GA_INFORMATION = Object.freeze({
  ENUM: "CHECKOUT",
  NAME: "Checkout",
});

export const EMPTY_FUNCTION = (): void => {};

export const PAYMENT_METHOD_NAMES = Object.freeze({
  CHECKOUT: "Checkout",
  SHOPIFY: "Shopify",
  OZOW: "Ozow",
  MASTERPASS: "Masterpass",
  MOBICRED: "Mobicred",
  MOBICRED_SIGN_IN: "Mobicred Sign In",
  MOBICRED_VERIFICATION: "Mobicred Verification",
  CARD: "Card",
  EFT_SECURE: "EFT Secure",
  MPESA: "M-Pesa",
  WIX: "Wix",
  SBTECH: "sbtech",
  ONE_FOR_YOU: "1ForYou",
  APPLE_PAY: "Apple Pay",
});

export const ACTION_NAMES = Object.freeze({
  PAYMENT_SUCCESS: "Payment Success",
  PAYMENT_PENDING: "Payment Pending",
  PAYMENT_FAILED: "Payment Failed",
});

export const PAYMENT_METHOD_ENUMS = {
  PAYMENT_SUCCESS: "PAYMENTSUCCESS",
  PAYMENT_PENDING: "PAYMENTPENDING",
  PAYMENT_FAILED: "PAYMENTFAILED",
  CHECKOUT: "CHECKOUT",
  OZOW: "OZOW",
  EFT_SECURE: "EFTSECURE",
  CARD: "CARD",
  MOBICRED: "MOBICRED",
  MOBICRED_SIGN_IN: "MOBICREDSIGNIN",
  MOBICRED_VERIFICATION: "MOBICREDVERIFICATION",
  VISA: "VISA",
  MASTERCARD: "MASTERCARD",
  MASTERPASS: "MASTERPASS",
  AMERICAN_EXPRESS: "AMERICANEXPRESS",
  ONE_FOR_YOU: "1FORYOU",
  ONE_VOUCHER: "1VOUCHER",
  MPESA: "MPESA",
  M_PESA_VERIFICATION: "M-PESAVERIFICATION",
  M_PESA_ACCOUNT: "M-PESAACCOUNT",
  APPLE_PAY: "APPLEPAY",
};

export const URLS = {
  OZOW_URL: "https://pay.ozow.com/",
};

export interface PaymentMethod {
  order: number;
  id: string;
  title: string;
  label: string;
  src: string[];
  alt: string;
}

export const SETUP_INFORMATION: { [key: string]: PaymentMethod } = {
  MOBICRED: {
    order: 4,
    label: "Mobicred",
    id: "mobicred-btn",
    title: "Pay with Mobicred",
    src: [`${CHECKOUT.assetsBasePath}images/mobicred_36.svg`],
    alt: "Mobicred Icon",
  },
  CARD: {
    order: 1,
    label: "Card",
    id: "card-btn",
    title: "Pay with Card",
    src: [],
    alt: "Card Icon",
  },
  EFTSECURE: {
    order: 2,
    label: "EFT Secure",
    id: "eft-secure-btn",
    title: "Pay with EFT Secure",
    src: [`${CHECKOUT.assetsBasePath}images/eft.svg`],
    alt: "EFT Secure Icon",
  },
  MASTERPASS: {
    order: 3,
    label: "Masterpass",
    id: "masterpass-btn",
    title: "Pay with Masterpass",
    src: [`${CHECKOUT.assetsBasePath}images/mc.svg`],
    alt: "Masterpass Icon",
  },
  MPESA: {
    order: 5,
    label: "M-Pesa",
    id: "mpesa-secure-btn",
    title: "Pay with M-Pesa",
    src: [`${CHECKOUT.assetsBasePath}images/m-pesa.svg`],
    alt: "EFT Secure Icon",
  },
  OZOW: {
    order: 6,
    label: "Ozow",
    id: "ozow-btn",
    title: "Pay with Ozow",
    src: [`${CHECKOUT.assetsBasePath}images/ozow-on-white.svg`],
    alt: "Ozow Icon",
  },
  "1FORYOU": {
    order: 7,
    label: "1ForYou (1Voucher)",
    id: "1ForYou-btn",
    title: "Pay with 1ForYou",
    src: [`${CHECKOUT.assetsBasePath}images/1foryou.svg`],
    alt: "1ForYou Icon",
  },
  APPLEPAY: {
    order: 8,
    label: "Apple Pay",
    id: "apple-pay-btn",
    title: "Pay with Apple Pay",
    src: [`${CHECKOUT.assetsBasePath}images/apple-pay-white.svg`],
    alt: "Apple Pay Icon",
  },
};

const convertPluginNameToGAStandards = (pluginName: string) => {
  if (!pluginName) return "";

  const purgedPluginName = pluginName.toUpperCase().replace(/\s/g, "");

  switch (purgedPluginName) {
    case PAYMENT_METHOD_ENUMS.PAYMENT_SUCCESS:
      return ACTION_NAMES.PAYMENT_SUCCESS;
    case PAYMENT_METHOD_ENUMS.PAYMENT_PENDING:
      return ACTION_NAMES.PAYMENT_PENDING;
    case PAYMENT_METHOD_ENUMS.PAYMENT_FAILED:
      return ACTION_NAMES.PAYMENT_FAILED;
    case PAYMENT_METHOD_ENUMS.CHECKOUT:
      return PAYMENT_METHOD_NAMES.CHECKOUT;
    case PAYMENT_METHOD_ENUMS.OZOW:
      return PAYMENT_METHOD_NAMES.OZOW;
    case PAYMENT_METHOD_ENUMS.EFT_SECURE:
      return PAYMENT_METHOD_NAMES.EFT_SECURE;
    case PAYMENT_METHOD_ENUMS.CARD:
    case PAYMENT_METHOD_ENUMS.VISA:
    case PAYMENT_METHOD_ENUMS.MASTERCARD:
    case PAYMENT_METHOD_ENUMS.AMERICAN_EXPRESS:
      return PAYMENT_METHOD_NAMES.CARD;
    case PAYMENT_METHOD_ENUMS.ONE_FOR_YOU:
    case PAYMENT_METHOD_ENUMS.ONE_VOUCHER:
      return PAYMENT_METHOD_NAMES.ONE_FOR_YOU;
    case PAYMENT_METHOD_ENUMS.MASTERPASS:
      return PAYMENT_METHOD_NAMES.MASTERPASS;
    case PAYMENT_METHOD_ENUMS.MOBICRED:
      return PAYMENT_METHOD_NAMES.MOBICRED;
    case PAYMENT_METHOD_ENUMS.MOBICRED_SIGN_IN:
      return PAYMENT_METHOD_NAMES.MOBICRED_SIGN_IN;
    case PAYMENT_METHOD_ENUMS.MOBICRED_VERIFICATION:
      return PAYMENT_METHOD_NAMES.MOBICRED_VERIFICATION;
    case PAYMENT_METHOD_ENUMS.MPESA:
      return PAYMENT_METHOD_NAMES.MPESA;
    case PAYMENT_METHOD_ENUMS.M_PESA_ACCOUNT:
      return "M-Pesa Account";
    case PAYMENT_METHOD_ENUMS.M_PESA_VERIFICATION:
      return "M-Pesa Verification";
    case PAYMENT_METHOD_ENUMS.APPLE_PAY:
      return PAYMENT_METHOD_NAMES.APPLE_PAY;
    default:
      return pluginName;
  }
};

export default {
  NAMES: PAYMENT_METHOD_NAMES,
  URLS,
  ENUMS: PAYMENT_METHOD_ENUMS,
  convertPluginNameToGAStandards,
};
