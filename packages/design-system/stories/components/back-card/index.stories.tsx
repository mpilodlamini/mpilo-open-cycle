import React from "react";
import { BackCard } from "../../../";

export default {
  title: "Components / BackCard",
  component: BackCard,
};

export const Default = () => <BackCard />;
