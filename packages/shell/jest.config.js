const config = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  collectCoverage: false,
  clearMocks: true,
  globals: {},
  transform: {
    "^.+\\.(ts|tsx)?$": "ts-jest",
    "^.+\\.(js|jsx)$": "babel-jest",
  },
  moduleNameMapper: {
    "\\.(css|less|sass|scss)$": "<rootDir>/src/__mocks__/styles.ts",
    "\\.(gif|ttf|eot|svg)$": "<rootDir>/src/__mocks__/files.ts",
  },
  setupFilesAfterEnv: ["<rootDir>/src/setup.jest.ts"],
};

module.exports = config;
