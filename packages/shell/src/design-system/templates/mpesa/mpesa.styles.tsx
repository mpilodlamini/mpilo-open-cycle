import { withStyles, TextField, makeStyles } from "@material-ui/core";
import { border, info, white } from "@peach/checkout-design-system";

export const CssTextField = withStyles({
  root: {
    "& .MuiOutlinedInput-root": {
      paddingLeft: "calc(48% - 80px)",
      paddingRight: "calc(48% - 80px)",

      "@media (max-width: 420px)": {
        paddingLeft: "calc(48% - 90px)",
        paddingRight: "calc(48% - 90px)",
      },
      "& fieldset": {
        borderColor: border,
        color: "#53627C",
      },
      "&:hover fieldset": {
        borderColor: info,
      },
      "&.Mui-focused fieldset": {
        borderColor: info,
      },
    },
  },
})(TextField);

export const useStyles = makeStyles(() => ({
  label: {
    fontSize: "14px",
    fontWeight: 600,
    color: "#53627C",
    marginBottom: "8px",
  },
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },
    "&:disabled": {
      borderColor: border,
    },
  },
  callIcon: {
    position: "absolute",
    top: "46%",
    right: "15px",
  },
}));
