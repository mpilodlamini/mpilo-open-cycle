import * as CryptoJS from "crypto-js";
import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

Given("I have Generated Checkout from the Middleware page", () =>
  generateCheckoutFromMiddleware()
);

Given(
  "I have Generated Checkout from the Middleware page using {string} as my test amount",
  (amount) => {
    generateCheckoutFromMiddleware({ amount });
  }
);

Given(
  "I have Generated Checkout from the Middleware page using {string} as my test amount and {string} as my currency",
  (amount, currency) => {
    generateCheckoutFromMiddleware({ amount, currency });
  }
);

Given(
  "I have Generated Checkout from the Middleware page using {string} as my merchant Transaction Id",
  (merchantTransactionId) => {
    generateCheckoutFromMiddleware({ merchantTransactionId });
  }
);

//TODO: REFACTOR
Given(
  "I have Generated Checkout from the Middleware page using set secret and entity key",
  () => {
    generateCheckoutFromMiddleware({
      secretKey: "ca3d3f623a7111e9baa702de7a5a0a6e",
      entityId: "8ac7a4ca694cec2601694cf5f9360026",
      customerEmail: "test@test.com",
      billingStreet1: "1 Test lane",
      billingCountry: "ZA",
      billingCity: "Cape Town",
      billingPostcode: "7700",
    });
  }
);

Given(
  "I have Generated Checkout from the Middleware page using {string} as my test amount and set secret and entity key",
  (amount) => {
    generateCheckoutFromMiddleware({
      secretKey: "ca3d3f623a7111e9baa702de7a5a0a6e",
      entityId: "8ac7a4ca694cec2601694cf5f9360026",
      amount,
    });
  }
);

Given(
  "I have Generated Checkout from the Middleware page using {string} as my test amount and {string} as my currency and set secret and entity key",
  (amount, currency) => {
    generateCheckoutFromMiddleware({
      secretKey: "ca3d3f623a7111e9baa702de7a5a0a6e",
      entityId: "8ac7a4ca694cec2601694cf5f9360026",
      amount,
      currency,
    });
  }
);

Given(
  "I have Generated Checkout from the Middleware page using {string} as my merchant Transaction Id and set secret and entity key",
  (merchantTransactionId) => {
    generateCheckoutFromMiddleware({
      secretKey: "ca3d3f623a7111e9baa702de7a5a0a6e",
      entityId: "8ac7a4ca694cec2601694cf5f9360026",
      merchantTransactionId,
    });
  }
);

When("I generate Checkout from the Middleware page", () =>
  generateCheckoutFromMiddleware()
);

When("I wait {string} seconds to finish loading", (sec) => {
  const millisecondsTowait = sec * 1000;
  cy.wait(millisecondsTowait);
});

Given("I test locally", () => {
  cy.visit("https://localhost:3000/");
});

function generateCheckoutFromMiddleware(
  params: {
    secretKey?: string;
    merchantTransactionId?: string;
    entityId?: string;
    [key: string]: any;
  } = {}
) {
  // If env is configured as json
  // checkout api will return the checkout object as json
  // we can then take the json and set the global checkout
  // object on the index.html for local
  const asLocal = Cypress.env("asLocal") || false;

  if (asLocal) {
    const merchantTransactionId =
      params?.merchantTransactionId || Math.floor(Math.random() * 1000000);
    const nonce = Math.floor(Math.random() * 1000000);

    const options = {
      "authentication.entityId": params?.entityId || Cypress.env("entityId"),
      amount: 14.99,
      currency: "ZAR",
      paymentType: "DB",
      signature: "",
      shopperResultUrl: "https://httpbin.org/post",
      merchantTransactionId,
      nonce,
      notificationUrl: "",
      cancelUrl: "",
      merchantInvoiceId: "",
      transactionDescription: "",
      "customer.merchantCustomerId": "",
      "customer.givenName": "",
      "customer.surname": "",
      "customer.mobile": "",
      "customer.email": "",
      "customer.status": "",
      "customer.merchantCustomerLanguage": "",
      "billing.street1": "",
      "billing.street2": "",
      "billing.city": "",
      "billing.company": "",
      "billing.country": "",
      "billing.state": "",
      "billing.postcode": "",
      "billing.customer.fax": "",
      "billing.customer.taxType": "",
      "billing.customer.givenName": "",
      "billing.customer.surname": "",
      "billing.customer.phone": "",
      "billing.houseNumber1": "",
      "shipping.street1": "",
      "shipping.street2": "",
      "shipping.city": "",
      "shipping.company": "",
      "shipping.country": "",
      "shipping.state": "",
      "shipping.postcode": "",
      "shipping.customer.givenName": "",
      "shipping.customer.surname": "",
      "shipping.customer.phone": "",
      "shipping.customer.fax": "",
      "shipping.customer.taxId": "",
      "shipping.customer.taxType": "",
      "shipping.customer.houseNumber1": "",
      "cart.tax": "",
      "cart.shippingAmount": "",
      "cart.discount": "",
      plugin: "",
      defaultPaymentMethod: "",
      forceDefaultMethod: "",
      createRegistration: "",
      ...params,
    };

    //loop through all available body attributes and create an array with the populated values (not empty strings)
    const body = Object.keys(options)
      .map((key) => options[key] && `${key}${options[key]}`)
      .sort();
    //joins all the elements of the array into a single string with no comma separators
    const hash = CryptoJS.HmacSHA256(
      body.join(""),
      params?.secretKey || Cypress.env("secretKey")
    );

    options.signature = hash.toString(CryptoJS.enc.Hex);

    // const form = new FormData();
    // Object.keys(options).forEach((key) => {
    //   form.append(key, options[key]);
    // });

    cy.request({
      method: "POST",
      url: Cypress.env("paymentBaseUrl") + "/checkout",
      form: true,
      body: options,
      headers: {
        Accept: "application/json",
        Referer: "34.247.96.206",
        Origin: "34.247.96.206",
      },
    }).then((response) => {
      if (response.isOkStatusCode) {
        // replace plugins with local variants
        const plugins: [] = response.body.pluginPaymentMethods.map((p: any) => {
          const pluginDomain: string = p.origin.split(".")[0].split("://")[1];
          const plugin = pluginDomain.substr(0, pluginDomain.lastIndexOf("-"));
          const splitter = p.image.split("/");
          const image = splitter[splitter.length - 1];
          return {
            ...p,
            origin: "https://localhost:3000",
            location: `https://localhost:3000/${plugin}/${plugin}`,
            image: `https://localhost:3000/${plugin}/images/${image}`,
          };
        });

        response.body.pluginPaymentMethods = plugins;

        cy.visit(Cypress.config().baseUrl, {
          onBeforeLoad: (win) =>
            // @ts-expect-error
            (win.GLOBAL_CHECKOUT_OBJECT = response.body),
        });
      }
    });

    return;
  }

  cy.generateMiddlewareCheckout(params)
}
