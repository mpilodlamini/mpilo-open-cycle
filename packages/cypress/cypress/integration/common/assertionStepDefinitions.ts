import { Then, When } from "cypress-cucumber-preprocessor/steps";
import CONSTANTS from "../../constantsUtil";

Then(
  "I should see {string} containing the text {string}",
  (element, textContent) => {
    cy.getElementByDataTestId(element).contains(textContent);
  }
);

Then(
  "I should see that the element with id {string} contains the text {string}",
  (id, textContent) => {
    cy.getElementById(id).contains(textContent);
  }
);

Then(
  "I should see that the element with id {string} contains the text {string} to {string}",
  (id, textContent, cypresChainable) => {
    cy.getElementById(id)
      .contains(textContent)
      .should(cypresChainable);
  }
);

Then(
  "I should see that the element with data-testid {string} contains the text {string} to {string}",
  (id, textContent, cypresChainable) => {
    cy.getElementByDataTestId(id)
      .contains(textContent)
      .should(cypresChainable);
  }
);

Then(
  "I should see that the element with data-testid {string} to {string}",
  (id, cypresChainable) => {
    cy.getElementByDataTestId(id).should(cypresChainable);
  }
);

Then("I should see that the {string} button is disabled", (element) => {
  cy.getElementByDataTestId(element).should("be.disabled");
});

Then("I should see payment {string} notification", (paymentNotification) => {
  switch (paymentNotification) {
    case "payment successful":
      cy.contains(CONSTANTS.PAYMENT_SUCCESS_HEADER).should("be.visible");
      break;

    case "payment declined":
      cy.contains(CONSTANTS.PAYMENT_DECLINED_HEADER).should("be.visible");
      break;

    case "no balance":
      cy.contains(CONSTANTS.ONE_FOR_YOU.NO_AVAILABLE_BALANCE.DIALOG_HEADER).should(
        "be.visible"
      );
      break;

    case "invalid voucher":
      cy.contains(CONSTANTS.ONE_FOR_YOU.INVALID_VOUCHER.DIALOG_HEADER).should(
        "be.visible"
      );
      cy.contains(
        "Please make sure you have entered your PIN correctly. Please go back to try again or select a different payment method."
      ).should("be.visible");
      break;

    case "payment error":
      cy.contains(CONSTANTS.PAYMENT_ERROR_HEADER).should("be.visible");
      cy.contains(
        "Transaction failed due to an internal error. Please go back to try again or select a different payment method."
      ).should("be.visible");
      break;

    case "voucher expired":
      cy.contains(CONSTANTS.ONE_FOR_YOU.EXPIRED_VOUCHER.DIALOG_HEADER).should(
        "be.visible"
      );
      cy.contains(
        "Please contact 1ForYou or try again with a different voucher or select a different payment method."
      ).should("be.visible");
      break;
    case "amount not matching":
      cy.contains(CONSTANTS.PAYMENT_DECLINED_HEADER).should("be.visible");
      cy.contains(
        "Your voucher credit does not match your purchase amount. Your payment has been declined. You can try again or select another payment method."
      ).should("be.visible");
      break;

    case "payment pending":
      cy.wait(5000); //TODO: FIND AN IMPLICIT WAIT INSTEAD OF EXPLICIT
      cy.contains(CONSTANTS.PAYMENT_PENDING_HEADER, { timeout: 20000 }).should(
        "be.visible"
      );
      cy.get("button")
        .contains(CONSTANTS.REDIRECT_BUTTON_LABEL)
        .should("be.visible")
        .click();
      cy.wait(5000); //TODO: FIND AN IMPLICIT WAIT INSTEAD OF EXPLICIT
      cy.location().should((locationObject) => {
        expect(locationObject.href.toString()).to.contain(
          CONSTANTS.POST_TEST_URL
        );
      });
      break;
    default:
      break;
  }
});

Then("I should see the text {string}", (text) => {
  cy.contains(text).should("be.visible");
});

Then("I should see the text {string} in the iframe", (text) => {
  cy.iframe().contains(text).should("be.visible");
});

Then(
  "I should see the text {string} inside {string} modal",
  (element, container) => {
    cy.getElementByDataTestId(container)
      .contains(element)
      .should("be.visible");
  }
);

Then("I should not see {string}", (element) => {
  cy.getElementByDataTestId(element).should(
    "not.exist"
  );
});

Then("I should be navigated to {string} URL", (url) => {
  cy.url().should("eq", url);
});



