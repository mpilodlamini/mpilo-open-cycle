export interface CheckoutData {
  isNativeCardForm: boolean;
  isCheckoutInTest: boolean;
  forceDefaultMethod: boolean;

  version: string;
  assetsBasePath: string;
  payonBaseUrl: string;
  payonActionUrl: string;
  cancelUrl: string;
  successUrl: string;
  code: string;
  merchant: string;
  merchantCurrencyCode: string;
  merchantTransactionId: string;
  merchantLogo: string;
  merchantKey: string;
  merchantInvoiceId: string;
  privateKey: string;
  baseUrl: string;
  shopperResultUrl: string;
  siteCode: string;
  status: string;
  switchBaseUrl: string;
  countryCode: string;
  currencyCode: string;
  amount: string;
  paymentBrand: string;
  checkoutId: string;
  source: string;
  notifyUrl: string;
  trackingId: string;
  environment: string;
  defaultPaymentMethod: string;
  title: string;

  redirectPostData: {};
  errorMessages: Array<string>;
  paymentMethods: Array<string>;

  billingCountry: string;
  billingCity: string;
  billingStreet1: string;
  billingPostcode: string;
  customerEmail: string;
}
