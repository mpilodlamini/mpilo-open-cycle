import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import React, { ReactNode } from "react";

export { CssBaseline };

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#FCFCFD",
    },
    secondary: {
      main: "#f1f1f1",
    },
    action: {
      main: "#f1f1f1",
    },
    text: {
      primary: "#0A1F44",
      secondary: "#0D55CF",
    },
  },
  typography: {
    fontFamily: ["'Open sans'", "Arial", "sans-serif"].join(", "),
    fontSize: 16,
    caption: {
      fontSize: 12,
    },
  },
  overrides: {
    MuiPaper: {
      elevation3: "0px 0px 16px 0px rgba(83,98,124,0.15)",
    },
    MuiButton: {},
    MuiAppBar: {
      colorPrimary: {
        backgroundColor: "#FFF",
      },
    },
    MuiRadio: {
      root: {
        color: "#C9CED6",
      },
    },
  },
} as any);

export const ThemeProvider = ({ children }: { children: ReactNode }) => (
  <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
);
