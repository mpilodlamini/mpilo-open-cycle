import { printError, printInfo } from "./logger";

const pollingInterval = 15000; // 15 seconds

/**
 * Setup the websocket communication channel
 * @param url to connect to
 * @param onOpenMessage message to be sent on the channel
 * @param onMessageCallback handles the message received from the sender
 * @param endMessage matc message to identify the channel should be closed
 */
const initiateWebsocket = (
  url: string,
  onOpenMessage: string | {},
  onMessageCallback: (data: any) => void,
  endMessage: string
) => {
  try {
    const websocket = new WebSocket(url);

    websocket.onopen = () => {
      printInfo("Websocket", "onopen", "sending message");
      websocket.send(JSON.stringify(onOpenMessage));
    };

    websocket.onmessage = (event) => {
      printInfo("Websocket", "onmessage", "sending message");
      let retrySendMessage: any | undefined;

      onMessageCallback(event.data);

      if (event.data === endMessage) {
        if (retrySendMessage) clearInterval(retrySendMessage);
        websocket.close();
      }

      retrySendMessage = setInterval(() => {
        websocket.send(JSON.stringify(onOpenMessage));
      }, pollingInterval);
    };

    websocket.onclose = () => {
      printInfo("Websocket", "onclose", "sending message");
      websocket.close(); // TODO send code?
    };

    websocket.onerror = (event) => {
      printError("Websocket", "onerror", event);
      throw new Error("Websocket error occurred");
    };

    return websocket;
  } catch (error) {
    printError("Websocket", "catch error", error);
    throw new Error("Setup error");
  }
};

export default initiateWebsocket;
