Feature: A+ Payment Requests for invalid scenarios
    In order to handle failed payment attempts
    As a customer
    I want to to be notified accordingly

    Scenario Outline: A+ <testAmount> Payment Request Tests that reset the payment type on Fail
        Given I Generate Checkout via API using "<testAmount>" as my test amount
        When I click on "aplus-radio-card"

        And I switch to the iframe
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        When I Login to access my A+ account
        And it finishes "loading" in the iframe
        Then I should see the text "<errorHeading>"
        And I should see the text "Continue" inside "warning" modal
        When I click the "Continue" inside "warning" modal

        And it finishes "loading" in the iframe

        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe
        Examples:
            | testAmount | errorHeading              | 
            | 1.01       | Incorrect Payment Details | 
            | 2.01       | Payment Failed            | 
            | 3.01       | Payment Error             | 
            | 4.01       | Payment Timeout           | 
            | 5.01       | Payment Declined          | 
            | 6.01       | Payment Declined          | 
            | 7.01       | Payment Failed            | 
            | 8.01       | Payment Failed            | 
            | 9.01       | Payment Declined          | 
            | 12.01      | Payment Declined          | 
            | 13.01      | Payment Declined          | 
            | 14.01      | Insufficient Funds        | 
            | 16.01      | A+ unavailable            | 
            | 17.01      | A+ unavailable            | 

