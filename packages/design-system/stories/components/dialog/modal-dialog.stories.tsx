import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import { FeedbackType, ModalDialog, ModalDialogProps } from "../../../";

export default {
  title: "Components / ModalDialog",
  component: ModalDialog,
} as Meta;

export const Default: Story<ModalDialogProps> = (props: ModalDialogProps) => {
  return <ModalDialog {...props} />;
};

Default.args = {
  open: true,
  type: FeedbackType.INFO,
  title: "Dialog Title",

  errorMessages: [],
  forceBottom: false,
  continueText: null,

  label: <p>Dialog Label</p>,
  children: <p>Dialog body</p>,

  onClick: () => {},
  onCancel: () => {},
};

export const Error: Story<ModalDialogProps> = (props: ModalDialogProps) => {
  return <ModalDialog {...props} />;
};

Error.args = {
  open: true,
  type: FeedbackType.ERROR,
  title: "Error",

  errorMessages: [
    "An error has occurred.",
    "Please contact your administrator.",
  ],
  forceBottom: false,
  continueText: null,

  label: <p>Dialog Label</p>,
  children: <p>Dialog body</p>,

  onClick: () => {},
  onCancel: () => {},
};

export const Cancel: Story<ModalDialogProps> = (props: ModalDialogProps) => {
  return <ModalDialog {...props} />;
};

Cancel.args = {
  open: true,
  type: FeedbackType.WARNING,
  title: "Cancel Transaction?",

  forceBottom: false,
  continueText: "Cancel",

  children: (
    <p>
      You are about to cancel the transaction and return to the merchant. Are
      you sure you want to do this?
    </p>
  ),

  onClick: () => {},
  onCancel: () => {},
};
