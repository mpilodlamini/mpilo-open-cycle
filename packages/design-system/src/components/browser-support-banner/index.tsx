import { Box } from "@material-ui/core";
import CloseOutlinedIcon from "@material-ui/icons/CloseOutlined";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import React from "react";
import { bold } from "../../variables";
import useStyles from "./index.styles";

export interface BrowserSupportBannerProps {
  onClick: () => void;
}

export const BrowserSupportBanner = (props: BrowserSupportBannerProps) => {
  const classes = useStyles();

  return (
    <Box
      display="flex"
      textAlign="center"
      className={classes.browserBanner}
      p={1}
    >
      <Box display="flex" justifyContent="center" flexGrow={1}>
        <Box display="flex" alignItems="center">
          <InfoOutlinedIcon style={{ width: "15px", height: "15px" }} />
          <Box
            display={{ xs: "none", sm: "block", md: "block", lg: "block" }}
            ml={1}
            mr={1}
            component="span"
            fontWeight={bold}
          >
            Browser Not Supported.
          </Box>
          <Box
            display={{ xs: "block", sm: "none", md: "none", lg: "none" }}
            ml={1}
            mr={1}
            component="span"
            fontWeight={bold}
          >
            Browser Not Supported
          </Box>
          <Box display={{ xs: "none", sm: "block", md: "block", lg: "block" }}>
            For the best payment experience please upgrade your browser version.
          </Box>
        </Box>
      </Box>
      <Box display="flex" justifyContent="flex-end" alignItems="center">
        <CloseOutlinedIcon
          onClick={props.onClick}
          style={{ width: "15px", height: "15px", cursor: "pointer" }}
        />
      </Box>
    </Box>
  );
};
