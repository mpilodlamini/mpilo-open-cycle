import { withStyles } from "@material-ui/core/styles";
import BaseTextField from "@material-ui/core/TextField";
import {
  border,
  info,
  white,
  red,
  secondaryGrey,
  primaryBlue,
  tertiaryGrey,
  error,
} from "../../variables";

export const TextField = withStyles((theme) => ({
  root: {
    "& .MuiOutlinedInput-root": {
      backgroundColor: white,

      "& .MuiOutlinedInput-notchedOutline": {
        borderColor: border,
        color: secondaryGrey,
      },
      "&:hover .MuiOutlinedInput-notchedOutline, &.Mui-focused.Mui-focused .MuiOutlinedInput-notchedOutline":
        {
          borderColor: info,
        },
      "&.Mui-error:hover .MuiOutlinedInput-notchedOutline, &.Mui-error .MuiOutlinedInput-notchedOutline, &.Mui-error.Mui-focused .MuiOutlinedInput-notchedOutline, &.Mui-error.Mui-active .MuiOutlinedInput-notchedOutline":
        {
          borderColor: red,
        },
      "&.Mui-focused .MuiOutlinedInput-notchedOutline, &:Mui-focused:hover .MuiOutlinedInput-notchedOutline, &.Mui-active .MuiOutlinedInput-notchedOutline, &.Mui-focused.Mui-focused .MuiOutlinedInput-notchedOutline":
        {
          borderColor: info,
        },
    },
    "& .MuiFormLabel-root": {
      color: primaryBlue,
      letterSpacing: "normal",
      "&.Mui-error": {
        color: error,
      },
    },
    "& .MuiInputLabel-outlined": {
      fontSize: "13px",
      "& .MuiInputLabel-asterisk": {
        display: "none",
      },
      [theme.breakpoints.up("sm")]: {
        fontSize: "inherit",
      },
    },
    "& .Mui-error.MuiInputLabel-shrink": {
      color: red,
      fontSize: "16px",
    },
    "& input": {
      color: primaryBlue,
      fontSize: "12px",
      [theme.breakpoints.up("sm")]: {
        fontSize: "13px",
      },
    },
    "& input::placeholder": {
      color: tertiaryGrey,
      opacity: 1,
    },
    "& input[name='number']": {
      letterSpacing: "2px",
      "@media screen and (-ms-high-contrast: active), screen and (-ms-high-contrast: none)":
        {
          /* IE11 specific styles go here */
          width: "100% !important",
          letterSpacing: "3px !important",
        },
      [theme.breakpoints.up("sm")]: {
        letterSpacing: "8px",
      },
      [theme.breakpoints.up("md")]: {
        letterSpacing: "10px",
      },
    },
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline, & .MuiOutlinedInput-root.Mui-error .MuiOutlinedInput-notchedOutline":
      {
        borderWidth: "2px",
      },
  },
}))(BaseTextField);
