import {
  CallbackErrorObject,
  PayOnCheckoutErrorCallBack,
  PayOnCheckoutSuccessCallBack,
} from "../interfaces/PayOn.interface";
import { checkout } from "../lib/checkout";

class PayOnService {
  getErrorObject = (error: CallbackErrorObject) => {
    /**
     * Couldn't reproduce a case locally where the response object comes back with the following structure:
     * error.response.data
     * the error object comes back as error.response.msg please see PayOnCheckoutErrorCallBack in PayOn.interface.ts
     * Might not be needing this check however since we cant test all possible errors codes locally leaving it as is
     * So that any GOTCHAS may fail gracefully
     * */
    return error?.response?.data || error;
  };

  postFormEncodedContent = async (
    url: string,
    postData: {
      checkout_id: string;
    },
    successCallback: (response: PayOnCheckoutSuccessCallBack) => void,
    errorCallback?: (error: PayOnCheckoutErrorCallBack) => void
  ) => {
    try {
      const res = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams(postData),
      });
      if (res.ok) {
        const data = await res.json();
        return successCallback(data.response);
      } else {
        const errorObject = this.getErrorObject(await res.json());
        if (errorCallback) {
          return errorCallback(errorObject);
        }
      }
    } catch (e: unknown) {
      if (e instanceof Error) {
        checkout.publish([
          {
            type: "status",
            value: "error",
            message: e.message,
          },
        ]);
      }
    }
  };
}

export default PayOnService;
