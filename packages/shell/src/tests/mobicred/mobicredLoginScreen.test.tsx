import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";
import renderer from "react-test-renderer";

import MobicredLoginScreen from "../../design-system/templates/mobicred/screen/mobicredLoginScreen";
const {
  getByPlaceholderText,
  getByText,
  getByLabelText,
  findAllByText,
  findByText,
} = screen;
describe("Mobicred login screen", () => {
  it("renders the correct input fields", async () => {
    const onClick = jest.fn();
    render(<MobicredLoginScreen onClick={onClick} />);
    expect(getByPlaceholderText("email@address.co.za")).toBeInTheDocument();
    expect(getByPlaceholderText("password")).toBeInTheDocument();
    expect(getByText("Sign In")).toBeInTheDocument();
  });

  it("email address and password field show errors when blank", async () => {
    const onClick = jest.fn();
    render(<MobicredLoginScreen onClick={onClick} />);
    getByLabelText("Email Address*").focus();
    getByPlaceholderText("password").focus();
    getByPlaceholderText("password").blur();
    const validationErrorHTML = await findAllByText("Required");
    const emailRequiredError = validationErrorHTML[0];
    const passwordRequiredError = validationErrorHTML[1];

    expect(validationErrorHTML).not.toHaveLength(0);
    expect(validationErrorHTML).toHaveLength(2);
    expect(emailRequiredError).toBeInTheDocument();
    expect(passwordRequiredError).toBeInTheDocument();
    expect(emailRequiredError.innerHTML).toEqual("Required");
    expect(passwordRequiredError.innerHTML).toEqual("Required");
  });

  it("prevents invalid email format", async () => {
    const onClick = jest.fn();
    render(<MobicredLoginScreen onClick={onClick} />);
    const emailInputField = getByPlaceholderText("email@address.co.za");
    const passwordInputField = getByPlaceholderText("password");
    emailInputField.focus();
    fireEvent.change(emailInputField, { target: { value: "email.com" } });
    fireEvent.change(passwordInputField, { target: { value: "password123" } });
    passwordInputField.focus();
    const validationErrorHTML = await findByText("Invalid email address");

    expect(await findByText("Invalid email address")).toBeInTheDocument();
    expect(document.querySelector("button.MuiButtonBase-root")).toBeDisabled();
    expect(validationErrorHTML).toBeInTheDocument();
    expect(validationErrorHTML.innerHTML).toEqual("Invalid email address");
  });

  it("should enable the sign in button when username and password fields are valid", async () => {
    const onClick = jest.fn();
    render(<MobicredLoginScreen onClick={onClick} />);
    fireEvent.change(getByPlaceholderText("email@address.co.za"), {
      target: { value: "email@mail.com" },
    });
    fireEvent.change(getByPlaceholderText("password"), {
      target: { value: "password123" },
    });

    expect(
      await document.querySelector("button.MuiButtonBase-root")
    ).not.toHaveAttribute('disabled=""');
  });
});
