import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import { CustomSelect, SelectProps } from "../../../";

export default {
  title: "Components/Select",
} as Meta;

export const Default: Story<SelectProps> = (props: SelectProps) => {
  return <CustomSelect {...props} />;
};

Default.args = {
  label: "",
  errorMessage: "",
  isError: false,
  isXsBp: false,
  defaultValue: { value: "1", label: "One" },
  options: [
    { value: "1", label: "One" },
    { value: "2", label: "Two" },
  ],
  onChange: (change) => {},
};
