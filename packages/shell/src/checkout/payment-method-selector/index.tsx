import React, { useCallback, useContext, useMemo } from "react";
import { Box, FormControl, Grid, makeStyles } from "@material-ui/core";
import {
  CheckoutContext,
  PluginPaymentMethod,
} from "../../contexts/CheckoutContext";
import Banner from "./Banner";
import { RadioCard } from "@peach/checkout-design-system/dist/components/radio-card";
import { CARD } from "../../utils/constants";
import { ApplePayButton } from "../selected-payment-method/payment-methods/ApplePay";
import { SelectedPaymentContext } from "../../contexts/SelectedPaymentContext";

import { LanguageContext } from "../../contexts/LanguageContext";
const useStyles = makeStyles((theme) => ({
  wrapper: {
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
  root: {
    overflowY: "auto",
    height: "calc(100% - 130px)",
    paddingBottom: "60px",
    [theme.breakpoints.down("xs")]: {
      height: "calc(100% - 18px)",
    },
  },
}));

export const PaymentMethodSelector = () => {
  const styles = useStyles();
  const config = useContext(CheckoutContext);
  const {
    selected: selectedPaymentMethod,
    select: selectPaymentMethod,
    previous,
  } = useContext(SelectedPaymentContext);

  const language = useContext(LanguageContext);

  console.log("MPIIILL-->", language);

  // setting up a map for O(1) access
  const available = useMemo(() => {
    return config.paymentMethods.reduce(
      (agg: { [index: string]: boolean }, method) => {
        agg[method.toUpperCase().toUpperCase().replace(/\s/g, "")] = true;
        return agg;
      },
      {}
    );
  }, [config]);

  const availableCards = useMemo(() => {
    return {
      [CARD.ENUM.VISA]: `${config.assetsBasePath}images/visa.svg`,
      [CARD.ENUM.MASTERCARD]: `${config.assetsBasePath}images/mc.svg`,
      [CARD.ENUM.AMERICAN_EXPRESS]: `${config.assetsBasePath}images/amex.svg`,
      [CARD.ENUM
        .DINERS_CLUB]: `${config.assetsBasePath}images/diners-club-icon.svg`,
    };
  }, [config]);

  const hasApplePay = useMemo(() => {
    try {
      return (
        available.APPLEPAY && (window as any).ApplePaySession.canMakePayments()
      );
    } catch (e) {
      return false;
    }
  }, [available]);

  const hasCard = useMemo(() => {
    return config.paymentMethods.find((m) => {
      return availableCards[m.toUpperCase().toUpperCase().replace(/\s/g, "")];
    });
  }, [config, availableCards]);

  const cardImages = useMemo(() => {
    return config.paymentMethods.reduce((images: string[], m) => {
      const card = m.toUpperCase().toUpperCase().replace(/\s/g, "");
      if (availableCards[card]) {
        images.push(availableCards[card]);
      }
      return images;
    }, []);
  }, [config, availableCards]);

  const select = useCallback(
    (method) => () => selectPaymentMethod(method),
    [selectPaymentMethod]
  );
  const hasBeenSelected = useCallback(
    (method) => selectedPaymentMethod?.id === method || previous?.id === method,
    [selectedPaymentMethod?.id, previous?.id]
  );
  return (
    <Box className={styles.root} id="payments-screen">
      {/* <Banner title="Select Payment Method" /> */}
      <Banner title={language.selectPaymentMethod} />

      <Box className={styles.wrapper} pr={4} pl={4}>
        <FormControl component="fieldset" fullWidth>
          <Grid container spacing={2}>
            {hasApplePay && (
              <Grid item xs={12} sm={12}>
                <ApplePayButton />
              </Grid>
            )}

            {hasCard && (
              <Grid item xs={12} sm={6}>
                <RadioCard
                  src={cardImages}
                  label="Card"
                  id="card-btn"
                  dataTestId="card-btn"
                  title="Pay with Card"
                  alt="Card Icons"
                  checked={hasBeenSelected("Card")}
                  onClick={select({ id: "Card", name: "Card", label: "Card" })}
                />
              </Grid>
            )}

            {available.EFTSECURE && (
              <Grid item xs={12} sm={6}>
                <RadioCard
                  label="EFT Secure"
                  id="eft-secure-btn"
                  dataTestId="eft-secure-btn"
                  title="Pay with EFT Secure"
                  src={[`${config.assetsBasePath}images/eft.svg`]}
                  alt="EFT Secure Icon"
                  onClick={select({
                    id: "EFTSecure",
                    name: "EFTSecure",
                    label: "EFTSecure",
                  })}
                  checked={hasBeenSelected("EFTSecure")}
                />
              </Grid>
            )}
            {available.MASTERPASS && (
              <Grid item xs={12} sm={6}>
                <RadioCard
                  label="Masterpass"
                  id="masterpass-btn"
                  dataTestId="masterpass-btn"
                  title="Pay with Masterpass"
                  src={[`${config.assetsBasePath}images/mc.svg`]}
                  alt="Masterpass Icon"
                  onClick={select({
                    id: "Masterpass",
                    name: "Masterpass",
                    label: "Masterpass",
                  })}
                  checked={hasBeenSelected("Masterpass")}
                />
              </Grid>
            )}
            {available.MOBICRED && (
              <Grid item xs={12} sm={6}>
                <RadioCard
                  label="Mobicred"
                  id="mobicred-btn"
                  dataTestId="mobicred-btn"
                  title="Pay with Mobicred"
                  src={[`${config.assetsBasePath}images/mobicred_36.svg`]}
                  alt="Mobicred Icon"
                  onClick={select({
                    id: "Mobicred",
                    name: "Mobicred",
                    label: "Mobicred",
                  })}
                  checked={hasBeenSelected("Mobicred")}
                />
              </Grid>
            )}
            {available.MPESA && (
              <Grid item xs={12} sm={6}>
                <RadioCard
                  label="M-Pesa"
                  id="mpesa-secure-btn"
                  dataTestId="mpesa-secure-btn"
                  title="Pay with M-Pesa"
                  src={[`${config.assetsBasePath}images/m-pesa.svg`]}
                  alt="EFT Secure Icon"
                  onClick={select({
                    id: "mPesa",
                    name: "mPesa",
                    label: "mPesa",
                  })}
                  checked={hasBeenSelected("mPesa")}
                />
              </Grid>
            )}
            {available.OZOW && (
              <Grid item xs={12} sm={6}>
                <RadioCard
                  label="Ozow"
                  id="ozow-btn"
                  title="Pay with Ozow"
                  src={[`${config.assetsBasePath}images/ozow-on-white.svg`]}
                  alt="Ozow Icon"
                  onClick={select({ id: "OZOW", name: "OZOW", label: "OZOW" })}
                  checked={hasBeenSelected("OZOW")}
                />
              </Grid>
            )}
            {config.pluginPaymentMethods.map(
              (paymentMethod: PluginPaymentMethod) => {
                return (
                  <Grid item xs={12} sm={6} key={paymentMethod.id}>
                    <RadioCard
                      label={paymentMethod.label}
                      id={`${paymentMethod.name}-btn`}
                      dataTestId={`${paymentMethod.name}-radio-card`}
                      title={`Pay with ${paymentMethod.label}`}
                      src={
                        paymentMethod.name === "CNP"
                          ? cardImages
                          : [paymentMethod.image]
                      }
                      alt={`${paymentMethod.label} Icon`}
                      onClick={select({
                        id: paymentMethod.id,
                        name: paymentMethod.name,
                        label: paymentMethod.label,
                      })}
                      checked={hasBeenSelected(paymentMethod.id)}
                    />
                  </Grid>
                );
              }
            )}
          </Grid>
        </FormControl>
      </Box>
    </Box>
  );
};
