export const AJAX = Object.freeze({
  RESULT_STATUS: {
    SUCCESSFUL: "successful",
    CANCELLED: "cancelled",
  },
  DATA_RESULT: {
    SUCCESS: "success",
    FAILURE: "failure",
  },
  CODES: {
    SUCCESS: 200,
  },
  SUCCESS_RESULT_CODES: ["000.000.000", "000.100.110"],
});

export default { AJAX };
