output "load_balancer_ingress" {
  value = kubernetes_service.this.status.0.load_balancer.0.ingress
}

/*output "deployment_namespace" {
    value = kubernetes_namespace.this.metadata.0.name
}*/

output "container_port" {
  value = var.container_port
}

output "container_image" {
  value = var.container_image
}
