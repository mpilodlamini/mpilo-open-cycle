import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import { TitleBar, TitleProps } from "../../../";

export default {
  title: "Controls/TitleBar",
} as Meta;

export const Default: Story<TitleProps> = (props) => {
  return <TitleBar {...props} />;
};

Default.args = {
  title: "Peach Payments",
};
