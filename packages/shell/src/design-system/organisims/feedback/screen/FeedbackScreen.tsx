import React from "react";
import { Box, WithStyles, withStyles } from "@material-ui/core";
import {
  FeedbackIcon,
  FeedbackType,
  IconSize,
  TitleBar,
  PrimaryButton,
  GoBackButton,
  FeedbackMessage,
} from "@peach/checkout-design-system";
import PluginManager from "../../../../utils/pluginManager";
import TimeUtil from "../../../../utils/timeUtil";
import constants from "../../../../utils/constants";
import GoogleAnalyticsUtil from "../../../../utils/googleAnalyticsUtil";
import styles from "./feedbackScreen.styles";

export interface FeedbackScreenProps {
  feedback: FeedbackType;
  feedbackTitle: string;
  message: string | string[];
  isTimed: boolean;
  isBottom: boolean;
  fullWidthButton: boolean;
  buttonAction: string;

  clickCallback?: () => void;
  clickCallbackGoBack?: () => void;

  errorMessages?: Array<string>;
  text?: string;
  redirectPostData?: {};
  redirectUrl?: string;
  status?: string;
  paymentMethod?: string;
  globalGoogleAnalytics?: GoogleAnalyticsUtil;
}

interface FeedbackScreenStateInterface {
  timer: number;
}

class FeedbackScreen extends React.Component<
  FeedbackScreenProps & WithStyles,
  FeedbackScreenStateInterface
> {
  private readonly _timeUtil: TimeUtil = new TimeUtil();

  private readonly _pluginManager: PluginManager = new PluginManager();

  constructor(props: FeedbackScreenProps & WithStyles) {
    super(props);
    this.state = {
      timer: constants.REDIRECT_TIMER,
    };
  }

  updateTime = (newTime: number): void => {
    this.setState({ timer: newTime });
    if (newTime === 0) {
      this.redirect();
    }
  };

  redirectClickCallback = (): void => {
    const action =
      this.props.status === constants.PEACH_STATE.PENDING
        ? "pending_goback"
        : "success_goback";
    if (this.props.globalGoogleAnalytics) {
      this.props.globalGoogleAnalytics.trackEvent(
        action,
        this.props.paymentMethod || " "
      );
    }
    this.redirect();
  };

  redirect = (): void => {
    if (this.props.buttonAction && this.props.clickCallback) {
      this.props.clickCallback();
    } else {
      this._pluginManager.redirectToPluginSpecificUrl(
        this.props.redirectUrl,
        this.props.redirectPostData
      );
    }
  };

  componentDidMount = (): void => {
    if (this.props.isTimed) {
      this._timeUtil.createTimer(constants.REDIRECT_TIMER, this.updateTime);
    }
  };

  render = () => {
    const { classes } = this.props;
    const timerDisplay = this.props.isTimed ? (
      <span>
        &nbsp;in {this.state.timer} seconds. If nothing happens, click the
        button.
      </span>
    ) : null;

    const buttonCallback: () => void = this.redirectClickCallback;
    const clickCallbackGoBack = this.props.clickCallbackGoBack || (() => {});

    return (
      <Box
        display="flex"
        flexDirection="column"
        alignItems="flex-start"
        className={classes.wrapper}
        data-testid={`${this.props.feedback}-feedback-notification`}
      >
        <Box alignSelf="flex-start" width={1} mb={3}>
          <FeedbackIcon size={IconSize.LARGE} feedback={this.props.feedback} />
        </Box>

        <Box textAlign="center" width={1} mb={2}>
          <TitleBar title={this.props.feedbackTitle} />
        </Box>

        <Box
          className={classes.message}
          fontSize="14px"
          textAlign="center"
          alignSelf="center"
        >
          {this.props.message}
          {timerDisplay}
        </Box>

        {this.props.children}

        {this.props.errorMessages ? (
          <Box width={1} pl={3} pr={3}>
            <FeedbackMessage messages={this.props.errorMessages} />
          </Box>
        ) : null}

        {this.props.isBottom ? <Box className={classes.anchor} /> : null}
        <Box
          width={1}
          className={this.props.isBottom ? classes.bottom : classes.modal}
        >
          {this.props.buttonAction ? (
            <Box display="flex" justifyContent="center">
              <PrimaryButton
                fullWidth={this.props.fullWidthButton}
                onClick={buttonCallback}
                label={this.props.buttonAction}
              />
            </Box>
          ) : null}

          {this.props.text ? (
            <Box
              display="flex"
              justifyContent="center"
              mt={1}
              width={1}
              pl={3}
              pr={3}
              mb={3}
            >
              <GoBackButton
                onClick={clickCallbackGoBack}
                label={this.props.text}
              />
            </Box>
          ) : null}
        </Box>
      </Box>
    );
  };
}

// @ts-ignore
export default withStyles(styles)(FeedbackScreen);
