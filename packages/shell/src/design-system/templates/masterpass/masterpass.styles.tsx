import { makeStyles } from "@material-ui/core";
import { info, white, success, text } from "@peach/checkout-design-system";

const useStyles = makeStyles(() => ({
  code: {
    background: "#EFF0F2",
    height: "calc(100% - 300px)",
    minHeight: "130px",
    borderTopLeftRadius: "4px",
    borderTopRightRadius: "4px",
  },
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },
    "&:disabled": {
      borderColor: "#C9CED6",
      cursor: "not-allowed",
    },
  },
  buttonSuccess: {
    border: `1px solid ${success}`,
    backgroundColor: success,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },

    "&:disabled": {
      borderColor: "#C9CED6",
      cursor: "not-allowed",
    },
  },
  link: {
    color: text,
    textTransform: "none",
    textDecoration: "underline",
  },
}));

export default useStyles;
