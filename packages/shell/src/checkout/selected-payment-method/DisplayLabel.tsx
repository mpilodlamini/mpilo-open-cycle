import { Box, Button } from "@material-ui/core";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import React, { useContext, useEffect, useState } from "react";
import ErrorModal from "../../components/errors/modals/errorModal";
import { CheckoutContext } from "../../contexts/CheckoutContext";
import { GoogleAnalyticsContext } from "../../contexts/GoogleAnalyticsContext";
import { LanguageContext } from "../../contexts/LanguageContext";
import { SelectedPaymentContext } from "../../contexts/SelectedPaymentContext";
import { StatusContext } from "../../contexts/StatusContext";
import { GA } from "../../utils/googleAnalyticsConstants";
import { CancelButton } from "../CancelButton";
import { useStyles } from "./payment-method.styles";

const displayName: { [index: string]: string } = {
  Card: "Card",
  ApplePay: "ApplePay",
  OZOW: "OZOW",
  Visa: "Visa",
  Mastercard: "Mastercard",
  Mobicred: "Mobicred",
  EFTSecure: "EFT Secure",
  Masterpass: "Masterpass",
  "Diners Club": "Diners Club",
  "1ForYou": "1ForYou",
  mPesa: "M-Pesa",
  APPLEPAY: "ApplePay",
  CARD: "Card",
  VISA: "Visa",
  MASTERCARD: "Mastercard",
  MOBICRED: "Mobicred",
  EFTSECURE: "EFT Secure",
  MASTERPASS: "Masterpass",
  "DINERS CLUB": "Diners Club",
  "1FORYOU": "1ForYou",
  MPESA: "M-Pesa",
};

export const DisplayLabel = () => {
  const styles = useStyles();
  const [showModal, setShowModal] = useState(false);
  const checkout = useContext(CheckoutContext);
  const language = useContext(LanguageContext);
  const status = useContext(StatusContext);
  const ga = useContext(GoogleAnalyticsContext);
  const {
    previous,
    select,
    selected,
    isForced,
    confirmGoBack,
    cancelVisible,
    setConfirmGoBack,
    cancellation,
  } = useContext(SelectedPaymentContext);

  useEffect(() => {
    if (cancellation.state === "Approved") {
      select();
      setConfirmGoBack(false);
      ga.trackEvent(
        GA.EVENTS.CANCEL_CONFIRM,
        (selected?.name || previous?.name) as string
      );
      cancellation.deny();
    }
  }, [cancellation.state, select, setConfirmGoBack, ga, selected, previous]);

  if (
    (previous?.id || selected?.id) &&
    cancelVisible &&
    !["success", "pending", "Success", "Pending"].includes(status.status || "")
  ) {
    return (
      <>
        {showModal && (
          <ErrorModal
            data-testid="error-modal"
            open={true}
            title="Cancel Payment?"
            label="You are about to cancel your payment. Are you sure you want to go back?"
            continueText="Cancel"
            onClick={() => {
              cancellation.request();
              setShowModal(false);
            }}
            cancelText="Go Back"
            onCancel={() => {
              setShowModal(false);
              ga.trackEvent(
                GA.EVENTS.CANCEL_GO_BACK,
                (selected?.name || previous?.name) as string
              );
            }}
          />
        )}
        <Box display="flex" alignItems="center">
          {isForced && <CancelButton />}
          {!isForced && (
            <Button
              data-testid="display-label-go-back"
              className={styles.button}
              onClick={() => {
                if (selected && confirmGoBack) {
                  setShowModal(true);
                  ga.trackEvent(
                    GA.EVENTS.CANCEL_MODAL,
                    (selected?.name || previous?.name) as string
                  );
                } else {
                  previous?.id === selected?.id
                    ? select(undefined)
                    : select(previous);
                }
              }}
              disableElevation
            >
              <ExpandLessIcon
                style={{ marginLeft: "-5px" }}
                className="expandIcon"
              />
              <b>{language.goBack}</b>
              {/* <b>
                {selected?.id
                  ? "Go Back"
                  : displayName[previous?.label || ""] || previous?.label}
              </b> */}
            </Button>
          )}
        </Box>
      </>
    );
  }
  return null;
};
