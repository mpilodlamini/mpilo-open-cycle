variable "eks_cluster_name" {
  description = "The name of the cluster we would like to deploy EKS resources to."
}

variable "replicas_amount" {
  description = "Amount specified in your replica set setting."
}

variable "env" {
  description = "The environment in which the app is running."
}

variable "app" {
  description = "The name of the app which is also used as the label for the replica set and container spec."
}

variable "container_image" {
  description = "Name of the container image."
}

variable "container_image_pull_policy" {
  description = "Always | IfNotPresent | Never"
  default     = "Always"
}

variable "container_port" {
  default     = 80
  description = "The port exposed by the container."
}

variable "load_balancer_port" {
  default     = 80
  description = "The port exposed by the load balancer."
}

variable "cpu_limit" {
  default = "0.5"
}

variable "memory_limit" {
  default = "512Mi"
}

variable "resource_request_cpu" {
  default = "250m"
}

variable "resource_request_memory" {
  default = "50Mi"
}

variable "file_system_id" {
  description = "The EFS id"
}

variable "env_vars" {
  description = "Environmental variables to make available within the containers"
  default     = []
}

variable "volume_size" {
  default = 0.5
}

variable "mount_path" {
  default = "/data"
}

variable "mounts" {
  description = "Map of mount paths within the container and in the volume."
  default = {}
}

variable "acm_cert_arn" {
  description = "ACM certificate arn to enable TLS."
}

locals {
  volume_name       = "${var.app}-${var.env}-pv"
  volume_claim_name = "${var.app}-${var.env}-pvc"
}
