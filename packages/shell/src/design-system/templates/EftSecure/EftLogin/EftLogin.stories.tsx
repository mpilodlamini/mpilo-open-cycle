import React from "react";

import EftLogin from "./EftLogin";

export default {
  title: "payments / Eft login",
  component: EftLogin,
};

export const Login = (args: any) => <EftLogin {...args} />;
