import { FeedbackType, Dialog } from "@peach/checkout-design-system";
import React, { useMemo } from "react";

interface LoadingScreenProps {
  widgetName: string;
  title?: string;
  label?: string;
}

const LoadingScreen: React.FunctionComponent<LoadingScreenProps> = ({
  widgetName,
  title = "{0} Loading",
  label = "Please wait while {0} payment loads.",
}: LoadingScreenProps) => {
  const dialogTitle = useMemo(
    () => title.replace("{0}", widgetName),
    [title, widgetName]
  );
  const dialogLabel = useMemo(
    () => label.replace("{0}", widgetName),
    [label, widgetName]
  );

  return (
    <Dialog
      title={dialogTitle}
      label={dialogLabel}
      type={FeedbackType.LOADING}
      forceBottom
    />
  );
};

export default LoadingScreen;
