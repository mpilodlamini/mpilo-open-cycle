import { Box } from "@material-ui/core";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import React, { useMemo } from "react";
import { PayOnProps } from "./interfaces/PayOn.interface";

const mapCardBrands: any = {
  americanexpress: "AMEX",
  dinersclub: "DINERS",
  mastercard: "MASTER",
  visa: "VISA",
};

const PayOnCardHtml = ({ checkout }: PayOnProps) => {
  /*  Reason for the below is CnPs data brands:
   * accepts American Express, Master Card, and Diners Club as "AMEX MASTER DINERS" case sensitive string
   * as such the below code aims to filter the paymentMethods array down to the string "AMEX MASTER DINERS VISA"
   * Please see "https://peachpayments.docs.oppwa.com/tutorials/integration-guide/customisation" under Brand Options
   * We can possibly avoid the below by passing in [AMEX, MASTER, DINERS, VISA] from Global Checkout Object
   */

  const supportedCards = useMemo(
    () =>
      checkout.paymentMethods
        .reduce((cardBrands: string[], paymentType) => {
          const filterCardBrand =
            mapCardBrands[paymentType.replace(/\s/g, "").toLowerCase()];
          if (filterCardBrand) {
            cardBrands.push(filterCardBrand);
          }
          return cardBrands;
        }, [])
        .join(" "),
    [checkout]
  );

  return (
    <Box id="cards">
      <Box display="flex" justifyContent="center">
        <CreditCardIcon fontSize="large" />
      </Box>
      <Box
        display="flex"
        justifyContent="center"
        textAlign="center"
        data-testid="card-form-title"
      >
        <p style={{ fontSize: "14px" }}>
          Please enter your card details below to complete the payment.
        </p>
      </Box>
      <form
        data-brands={supportedCards}
        action={`${checkout.baseUrl}/checkout?checkoutId=${checkout.checkoutId}`}
        className="paymentWidgets m-t-20"
      >
        <p style={{ display: "none" }}>VISA MASTER AMEX</p>
      </form>
    </Box>
  );
};

export default PayOnCardHtml;
