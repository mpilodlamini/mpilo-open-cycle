Feature: Masterpass
    In order to make a purchase
    As a customer
    I want to pay using my masterpass account

    Background: Launch Masterpass modal and scan code
        Given I Generate Checkout via API using default values
        When I click on "masterpass-btn"
        Then I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"
        When it finishes "loading"
        Then I should see the text "Go on to the Masterpass App on your phone and enter or scan the code below:"
        And I should see a QR code
        And I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"

    Scenario: Refresh code and see a new QR code
        When I click on "RefreshCode"
        When QR code finishes generating
        Then I should see a QR code
