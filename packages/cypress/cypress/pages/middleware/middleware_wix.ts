export const MIDDLEWARE_WIX_CONSTANTS = {
  //Mandatory fields
  "authentication.entityId": 'input[id="id_entityId"]',
  wixTransactionId: 'input[id="id_wixTransactionId"]',
  orderId: 'input[id="id_order_Id"]',
  amount: 'input[id="id_totalAmount"]',
  currency: 'input[id="id_currency"]',
  orderReturnSuccessUrl: 'input[id="id_successUrl"]',
  orderReturnErrorUrl: 'input[id="id_errorUrl"]',
  orderReturnCancelUrl: 'input[id="id_cancelUrl"]',
  orderReturnPendingUrl: 'input[id="id_pendingUrl"]',
  orderDescriptionItemsName: 'input[id="id_name"]',
  orderDescriptionItemsId: 'input[id="id_item_id"]',
  orderDescriptionItemsPrice: 'input[id="id_price"]',
  notificationUrl: 'input[id="id_notificationUrl"]'
  //Optional fields
  ////...
};

export interface MiddlewareWixObject {
  "authentication.entityId"?: string;
  wixTransactionId?: string;
  orderId?: string;
  amount?: string;
  currency?: string;
  orderReturnSuccessUrl?: string;
  orderReturnErrorUrl?: string;
  orderReturnCancelUrl?: string;
  orderReturnPendingUrl?: string;
  orderDescriptionItemsName?: string;
  orderDescriptionItemsId?: string;
  orderDescriptionItemsPrice?: string;
  notificationUrl?: string;

  buttonId?: string;
}