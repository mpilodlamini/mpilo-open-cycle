import axios, { Canceler } from "axios";
// @ts-ignore
import qs from "querystringify";
import { printInfo, printWarning, printError } from "../utils/logger";

const CancelToken = axios.CancelToken;

interface CallbackErrorObject {
  response: { data: object } | undefined;
}

const className: string = "CheckoutService";

class CheckoutService {
  private readonly _className: string = "CheckoutService";

  private _cancel: Canceler | null = null;

  private readonly _cancelToken = new CancelToken((c: Canceler) => {
    this._cancel = c;
    printInfo(className, "cancelToken", "Cancel token assigned");
  });

  private readonly _fromUrlEncodedConfigHeader: object = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    cancelToken: this._cancelToken,
  };

  getErrorObject = (error: CallbackErrorObject) => {
    return (error && error.response && error.response.data) || error;
  };

  cancelRequest = (): void => {
    const functionName = "cancelRequest";

    if (!this._cancel) {
      printWarning(className, functionName, "No cancel object exists");
      return;
    }

    printInfo(className, functionName, "Calling cancel token");
    this._cancel();
  };

  postContent = (
    url: string,
    postData: object,
    successCallback: (response: object) => void,
    errorCallback?: (error: object | string | undefined) => void
  ) => {
    const functionName = "postContent";
    printInfo(className, functionName, `Posting to ${url}`);

    axios
      .post(url, postData, { cancelToken: this._cancelToken })
      .then((response: { data: object }) => {
        return successCallback(response.data);
      })
      .catch((error: CallbackErrorObject) => {
        const errorObject = this.getErrorObject(error);

        printWarning(className, functionName, errorObject);
        if (errorCallback) {
          return errorCallback(errorObject);
        }
        return errorObject;
      });
  };

  postRawFormEncodedContent = (
    url: string,
    postData: string,
    successCallback: (response: any) => void,
    errorCallback?: (error: object) => void
  ) => {
    axios
      .post(url, postData, this._fromUrlEncodedConfigHeader)
      .then((response: { data: object }) => {
        return successCallback(response.data);
      })
      .catch((error: CallbackErrorObject) => {
        const errorObject = this.getErrorObject(error);

        if (errorCallback) {
          return errorCallback(errorObject);
        }
        return errorObject;
      });
  };

  postFormEncodedContent = (
    url: string,
    postData: object,
    successCallback: (response: any) => void,
    errorCallback?: (error: object) => void
  ) => {
    const functionName = "postFormEncodedContent";
    printInfo(className, functionName, `Posting to ${url}`);

    const postFormEncodedData = qs.stringify(postData);

    axios
      .post(url, postFormEncodedData, this._fromUrlEncodedConfigHeader)
      .then((response: { data: object }) => {
        return successCallback(response.data);
      })
      .catch((error: CallbackErrorObject) => {
        const errorObject = this.getErrorObject(error);

        printWarning(className, functionName, errorObject);
        if (errorCallback) {
          return errorCallback(errorObject);
        }
        return errorObject;
      });
  };

  startPolling = (
    url: string,
    checkoutId: string,
    successCallback: (result: {}) => void
  ) => {
    const functionName = "startPolling";

    printInfo(
      className,
      functionName,
      `Starting polling for iphone ${url}/status/checkout?checkoutId=${checkoutId}`
    );

    axios
      .get(
        `${url}/status/checkout?checkoutId=${checkoutId}`,
        this._fromUrlEncodedConfigHeader
      )
      .then((result: object) => {
        printInfo(className, functionName, JSON.stringify(result));
        return successCallback(result);
      })
      .catch((error: CallbackErrorObject) => {
        const errorObject = this.getErrorObject(error);

        printError(className, functionName, errorObject);
        return errorObject;
      });
  };

  getContent = (
    url: string,
    successCallback: (result: {}) => void,
    errorCallback?: (error: object) => void
  ) => {
    const functionName = "getContent";
    printInfo(className, functionName, `Fetching data from ${url}`);

    axios
      .get(url, { cancelToken: this._cancelToken })
      .then((result: object) => {
        printInfo(className, functionName, JSON.stringify(result));
        return successCallback(result);
      })
      .catch((error: CallbackErrorObject) => {
        const errorObject = this.getErrorObject(error);
        printWarning(className, functionName, errorObject);

        if (errorCallback) {
          return errorCallback(errorObject);
        }

        return errorObject;
      });
  };

  redirect = (url: string): void => {
    printInfo(className, "className", `Redirecting to ${url}`);

    window.location.replace(url);
  };
}

export default CheckoutService;
