import React from "react";
import Masterpass from "./masterpass";
import TimeUtil from "../../../utils/timeUtil";

export default {
  title: "payments / Masterpass",
  parameters: {
    component: Masterpass,
  },
};
export const standard = (args: any) => <Masterpass {...args} />;

standard.args = {
  qrCode: "12312321432",
  timeUtil: new TimeUtil(),
};
