import { Button, makeStyles } from "@material-ui/core";
import {
  border,
  error,
  white,
} from "@peach/checkout-design-system/dist/variables";
import axios from "axios";
import React, { useContext, useState } from "react";
import { CheckoutContext } from "../contexts/CheckoutContext";
import { GoogleAnalyticsContext } from "../contexts/GoogleAnalyticsContext";
import { LanguageContext } from "../contexts/LanguageContext";
import { SelectedPaymentContext } from "../contexts/SelectedPaymentContext";
import { GA } from "../utils/googleAnalyticsConstants";
import { CancelTransaction } from "./support/CancelTransaction";
import { MerchantRedirect } from "./support/MerchanRedirect";
import { Modal } from "./support/modal/Modal";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
  button: {
    border: `1px solid ${border}`,
    backgroundColor: white,
    color: "#8A94A6",
    textTransform: "none",
    paddingLeft: "24px",
    paddingRight: "24px",

    "&:hover": {
      backgroundColor: error,
      borderColor: error,
      color: white,
    },

    [theme.breakpoints.down("xs")]: {
      fontSize: "13px",
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
}));

export const CancelButton = () => {
  const styles = useStyles();
  const config = useContext(CheckoutContext);
  const language = useContext(LanguageContext);
  const ga = useContext(GoogleAnalyticsContext);
  const [cancelModal, showCancelModal] = useState(false);
  const [redirectModal, showRedirectModal] = useState(false);
  const [url, setUrl] = useState(config.cancelUrl || config.shopperResultUrl);
  const [data, setData] = useState({});
  const { selected, cancelVisible } = useContext(SelectedPaymentContext);
  const cancel = async () => {
    try {
      const response = await axios.post(
        `${config.baseUrl}checkout-status`,
        new URLSearchParams({
          checkout_id: config.checkoutId,
          checkout_status: "cancelled",
        }),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
        }
      );

      const resp = response.data.response;

      ga.trackEvent(
        GA.EVENTS.CANCEL_GO_BACK,
        (config.paymentBrand || selected?.name || selected?.id) as string
      );

      if (resp.redirect_post_data) {
        setData(JSON.parse(resp.redirect_post_data));
      }

      if (resp.redirect_url) {
        setUrl(resp.redirect_url);
      }
    } catch (e) {
      setUrl(config.cancelUrl || config.shopperResultUrl);
      setData({});
    } finally {
      showRedirectModal(true);
      showCancelModal(false);
    }
  };

  return (
    <>
      {cancelVisible && (
        <Button
          data-testid="banner-cancel"
          className={styles.button}
          variant="contained"
          size="small"
          disableElevation
          onClick={() => showCancelModal(true)}
        >
          {language.cancel}
        </Button>
      )}
      <Modal open={cancelModal} onClose={() => showCancelModal(false)}>
        <CancelTransaction
          onConfirm={cancel}
          onCancel={() => {
            showCancelModal(false);
          }}
        />
      </Modal>

      {redirectModal && (
        <Modal open={redirectModal} onClose={() => showRedirectModal(false)}>
          <MerchantRedirect
            url={url}
            data={data}
            status="cancelled"
            paymentMethod={
              config.paymentBrand || selected?.name || selected?.id || ""
            }
          />
        </Modal>
      )}
    </>
  );
};
