import React from "react";
import {
  Box,
  Container,
  AppBar,
  Collapse,
  makeStyles,
  Grid,
  Button,
} from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";

import "../../../../scss/style.scss";
import { white, bold, text, primaryBlue } from "../../../../variables";
// NOTE: DO NOT CHANGE CLASSNAMES OR HTML STRUCTURE
// all changes must be affected through scss files

const useStyles = makeStyles((theme) => ({
  wrapper: {
    minHeight: "100vh",

    [theme.breakpoints.down("xs")]: {
      alignItems: "flex-start",
    },
  },
  container: {
    borderRadius: "8px",
    height: "calc(100vh - 10vh)",
    overflow: "hidden",
    boxShadow: "0px 0px 20px 0px rgba(10,31,68,0.1)",
    maxWidth: "764px",
    position: "relative",

    [theme.breakpoints.down("xs")]: {
      height: "calc(100vh - 10vh)",
    },
  },
  logo: {
    justifyContent: "center",

    [theme.breakpoints.down("xs")]: {
      justifyContent: "flex-start",
    },
  },
  total: {
    textTransform: "uppercase",
    justifyContent: "flex-end",
  },
  header: {
    borderBottom: "1px solid #e0e0e8",
    minHeight: "85px",
    zIndex: 25,
  },
  headerWrapper: {
    padding: "0 32px",

    [theme.breakpoints.down("xs")]: {
      padding: "0 16px",
    },
  },
  headerLogo: {
    justifyContent: "center",

    [theme.breakpoints.down("sm")]: {
      justifyContent: "flex-start",
    },
  },
  button: {
    textTransform: "capitalize",
    minWidth: "200px",
    justifyContent: "flex-start",
    padding: "0",

    [theme.breakpoints.down("xs")]: {
      textAlign: "left",
      paddingLeft: "0",
    },
  },
  screen: {
    backgroundColor: white,
    borderTopLeftRadius: "8px",
    borderTopRightRadius: "8px",
    boxShadow: "0 -2px 4px rgba(0,0,0,0.12)",
    zIndex: 999,
    position: "absolute",
    bottom: "0",

    [theme.breakpoints.down("xs")]: {
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
  content: {
    height: "calc((100vh - 10vh) - (24px + 90px))",

    [theme.breakpoints.down("xs")]: {
      height: "calc((100vh - 10vh) - (24px + 90px))",
    },
  },
  scroll: {
    height: "calc((100vh - 10vh) - (85px + 35px + 16px + 16px))",
    overflowY: "auto",
    maxWidth: "100%",
  },
  active: {
    "& .expandIcon": {
      transform: "rotate(180deg)",
      transition: "all 0.8s ease",
    },
  },
  inactive: {
    "& .expandIcon": {
      transition: "all 0.8s ease",
    },
  },
  highlight: {
    color: text,
  },
  icon: {
    width: "20px",
  },
}));

const EftLoader = () => {
  const classes = useStyles();

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      className={classes.wrapper}
    >
      <Container disableGutters={true} className={classes.container}>
        <AppBar position="sticky" className={classes.header} elevation={0}>
          <Box
            display="flex"
            alignItems="center"
            height={1}
            maxWidth={1}
            className={classes.headerWrapper}
            style={{ minHeight: "85px" }}
          >
            <Box
              component="span"
              flexGrow={2}
              display={{
                xs: "none",
                sm: "block",
                md: "block",
                lg: "block",
              }}
            >
              &nbsp;
            </Box>
            <Box
              component="span"
              flexGrow={1}
              justifyContent="center"
              className={classes.headerLogo}
            >
              <Box
                display="flex"
                className={classes.logo}
                style={{ minHeight: "36px" }}
              >
                <img
                  height="36"
                  width="152"
                  src="https://www.peachpayments.com/assets/images/peach-payments-logo.svg"
                  alt="img"
                />
              </Box>
            </Box>
            <Box justifyContent="flex-end" flexGrow={1}>
              <Box display="flex" alignItems="center" className={classes.total}>
                <Box
                  pr={2}
                  color="text.secondary"
                  fontSize="caption.fontSize"
                  component="span"
                >
                  ZAR
                </Box>
                <Box component="span" fontWeight={bold} fontSize="h6.fontSize">
                  299.99
                </Box>
              </Box>
            </Box>
          </Box>
        </AppBar>
        <Box className={classes.screen} p={2} pl={4} pr={4} width={1}>
          <Collapse
            className={classes.active}
            in={true}
            collapsedHeight="50px"
            timeout={600}
          >
            <Box className={classes.content} width={1}>
              <Box display="flex" alignItems="center" height="50px">
                <Grid container alignItems="center">
                  <Grid item sm={7} xs={7}>
                    <Box display="flex" alignItems="center">
                      <Button className={classes.button} disableElevation>
                        <ExpandLessIcon
                          style={{ marginLeft: "-5px" }}
                          className="expandIcon"
                        />
                        <b>Go Back</b>
                      </Button>
                    </Box>
                  </Grid>
                  <Grid item sm={5} xs={5}>
                    <Box display="flex" justifyContent="flex-end">
                      <Box
                        component="span"
                        fontWeight={bold}
                        fontSize={12}
                        color="text.disabled"
                        display="flex"
                        flexWrap="end"
                        justifyContent="center"
                      >
                        <Box
                          component="span"
                          lineHeight={2}
                          display={{ xs: "none", sm: "block", md: "block" }}
                        >
                          Secured by{" "}
                          <Box component="span" className={classes.highlight}>
                            Peach Payments
                          </Box>
                        </Box>
                        <Box
                          component="span"
                          lineHeight={2}
                          display={{ xs: "block", sm: "none", md: "none" }}
                        >
                          Secured
                        </Box>
                        <Box component="span" className={classes.icon} ml={1}>
                          <LockIcon
                            fontSize="small"
                            style={{ color: primaryBlue }}
                          />
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>

              <Box className={classes.scroll} p={2} width={1}>
                {/* Content goes here */}
                <div className="eft-loading">
                  <div className="center-con-in">
                    <div className="loader_wrapper hide">
                      <svg
                        viewBox="22 22 44 44"
                        style={{ height: "50px", width: "50px" }}
                      >
                        <circle
                          className="MuiCircularProgress-circleIndeterminate-10"
                          cx="44"
                          cy="44"
                          r="18"
                          stroke="#1c7ed6"
                          strokeWidth="4"
                          fill="none"
                        ></circle>
                      </svg>
                      <h2 className="text-content">EFT Secure Loading</h2>
                      <p>Please wait while EFT Secure loads</p>
                    </div>
                  </div>
                </div>
              </Box>
            </Box>
          </Collapse>
        </Box>
      </Container>
    </Box>
  );
};

export default EftLoader;
