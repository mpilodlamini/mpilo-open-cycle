Feature: A+ Payment Requests with Invalid Credentials 
    In order to ensure the correct credentials have been submitted
    As a customer
    I want to be notified that the incorrect credentails have been submitted for my A+ Account

    Scenario Outline: A+ <testAmount> Payment Request Tests with error modal
        Given I Generate Checkout via API using "<testAmount>" as my test amount
        When I click on "aplus-radio-card"

        And I switch to the iframe
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        When I Login to access my A+ account
        And it finishes "loading" in the iframe
        Then I should see the text "<errorHeading>"
        And I should see the text "Go Back" inside "warning" modal

        When I click the "Go Back" inside "warning" modal
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        Examples:
            | testAmount | errorHeading        |
            | 10.01      | Invalid Credentials |
            | 11.01      | Invalid Credentials |
            | 15.01      | Invalid Credentials |

