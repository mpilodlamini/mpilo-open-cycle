import React, { useMemo } from "react";
import { createContext, ReactNode, useEffect, useState } from "react";
import { checkout } from "../lib/checkout";
import { CheckoutConfig as BaseConfig } from "@peach/checkout-lib";

export type CheckoutConfig = {
  config: BaseConfig;
  sessionId: string;
  time: {
    timer: number;
    timeLeft: string;
    setTimer: (fn: (number: number) => number) => void;
  };
  transaction: {
    transactionId: string | null;
    setTransactionId: (id: string) => void;
  };
};

export const ConfigContext = createContext<CheckoutConfig>({
  config: {} as any,
  sessionId: "",
  time: {} as any,
  transaction: {} as any,
});

const initialize = async (config: BaseConfig) => {
  try {
    const response = await fetch(`${config.baseUrl}aplus/initiate-session`, {
      method: "POST",
      body: new URLSearchParams({
        checkout_id: config.checkoutId,
      }),
    });

    if (response.ok) {
      // success, we have session.
      const json: { session_id: string } = await response.json();
      checkout.ga.trackPaymentInformation();
      checkout.ga.trackPage("A+ Account");
      checkout.initialized();

      return json.session_id;
    }

    const json: { code: string; message: string } = await response.json();
    // we have no session.
    checkout.publish([
      {
        type: "status",
        value: "error",
        code: json.code,
        message: json.message,
      },
    ]);
  } catch (e: unknown) {
    if (e instanceof Error) {
      checkout.publish([
        {
          type: "status",
          value: "error",
          message: e.message,
        },
      ]);
    }
  }
  return undefined;
};

export const ConfigProvider = ({ children }: { children: ReactNode }) => {
  const [loading, setLoading] = useState(true);
  const [config, setConfig] = useState<any>({});
  const [transactionId, setTransactionId] = useState<string | null>(null);
  const [timer, setTimer] = useState(1 * 60 * 15);
  const [intervalState, setIntervalState] = useState<number | null>(null);
  const timeLeft = useMemo(() => {
    if (timer <= 0) {
      return "0 seconds";
    }
    const minutes = Math.floor(timer / 60);
    const seconds = timer % 60;
    const minutesDisplay = minutes < 10 ? `0${minutes}` : minutes;
    const secondsDisplay = seconds < 10 ? `0${seconds}` : seconds;
    if (timer <= 60) {
      return `${secondsDisplay} seconds`;
    }
    return `${minutesDisplay}:${secondsDisplay}`;
  }, [timer]);

  useEffect(() => {
    if (config.sessionId) {
      const interval = setInterval(() => {
        setTimer((time) => {
          if (time <= 0) {
            clearInterval(interval);
            checkout.publish([
              {
                type: "timeout",
              },
            ]);
            checkout.ga.trackEvent({
              action: checkout.ga.events.time_out_modal,
              category: "aplus",
              label: checkout.ga.events.time_out_modal,
            });
            return 0;
          }
          return time - 1;
        });
      }, 1000);
      setIntervalState(interval);
      return () => {
        clearInterval(interval);
      };
    }
  }, [setTimer, config.sessionId]);

  useEffect(() => {
    const canceller = checkout.subscribe("checkout-cancellation", async () => {
      await fetch(
        `${config.config.baseUrl}transaction/${transactionId}/cancel`,
        {
          method: "POST",
          body: new URLSearchParams({
            checkoutId: config.config.checkoutId,
          }),
        }
      );
      // Needs to cancel regardless of the status
      checkout.cancelled();
    });

    return () => {
      checkout.unsubscribe(canceller);
    };
  }, [transactionId, config]);

  useEffect(() => {
    checkout.initialize();
    const bootstrapper = checkout.subscribe("checkout-bootstrap", (e) => {
      intervalState && clearInterval(intervalState);
      setLoading(true);
      setTransactionId(null);
      setIntervalState(null);
      initialize(e.data.config).then((sessionId) => {
        setConfig({ ...e.data, sessionId });
        setLoading(false);
      });
    });

    return () => {
      checkout.unsubscribe(bootstrapper);
    };
  }, [setLoading, setConfig]);

  if (loading) {
    return null;
  }

  return (
    <ConfigContext.Provider
      value={{
        ...config,
        time: { timer, timeLeft, setTimer },
        transaction: { transactionId, setTransactionId },
      }}
    >
      {children}
    </ConfigContext.Provider>
  );
};
