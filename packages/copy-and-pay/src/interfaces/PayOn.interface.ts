import { CheckoutConfig } from "@peach/checkout-lib";

export interface PayOnCheckoutErrorCallBack {
  code?: string;
  error: string;
  name?: string;
  response: { msg: string };
  status: string;
  responseJSON?: {
    response: {
      code: string;
      response_data: {
        result: string;
      };
    };
  };
}

export interface PayOnCheckoutSuccessCallBack {
  result: {
    code: string;
    description: string;
  };
  buildNumber: string;
  timestamp: string;
  ndc: string;
  id: string;
  payon_checkout_id: string;
  response?: {
    result: {
      code: string;
    };
  };
}

export interface OnReadyError {
  name: string;
  response: { response_code: string };
}

export interface CallbackErrorObject {
  response: { data: PayOnCheckoutErrorCallBack };
}
export interface PayOnProps {
  checkout: CheckoutConfig;
}
