import React, {
  useContext,
  useMemo,
  useCallback,
  useEffect,
  useState,
} from "react";
import $ from "jquery";
import PayOnService from "./services/PayOnService";
import PayOnCardHtml from "./payOnCardHtml";
import { ConfigContext } from "./contexts/ConfigContext";
import { checkout } from "./lib/checkout";
import {
  OnReadyError,
  PayOnCheckoutErrorCallBack,
  PayOnCheckoutSuccessCallBack,
} from "./interfaces/PayOn.interface";

const cardPaymentErrorNames: Array<string> = [
  "InvalidCheckoutIdError",
  "PciIframeSubmitError",
  "PciIframeCommunicationError",
  "WidgetError",
];

let domElementCheckInterval: any | null = null;

/**
 * Card component container for the card widget from CopyAndPay
 */
const Card = () => {
  const { config } = useContext(ConfigContext);
  const checkoutService: PayOnService = useMemo(() => new PayOnService(), []);

  /**
   * Updates the global wpwlOptions object when the script has been loaded/is ready
   */

  const onReady = useCallback(() => {
    const wpwlOptions = {
      style: "plain",
      iframeStyles: {
        "card-number-placeholder": {
          "font-size": "14px",
          color: "#8a94a6",
          "font-family": "Open Sans",
        },
        "cvv-placeholder": {
          "font-size": "14px",
          color: "#8a94a6",
          "font-family": "Open Sans",
        },
      },
      autofocus: "card.number",
      brandDetection: true,
      errorMessages: {
        cardHolderError: "Invalid card holder name",
        cvvError: "Invalid CVV entered",
        cardNumberError: "Invalid card number",
        expiryMonthError: "Invalid expiry date",
        expiryYearError: "Invalid expiry date",
      },
      onReady: () => {
        // Not the cleanest approach
        // This event fires first
        // We need to stop showing the Loading Modal here if the widget does not render

        if ($("button:contains('Pay now')").length === 0) {
          domElementCheckInterval = setInterval(() => {
            // The only way to know if it has not rendered properly is by looking for this phrase
            if ($("div:contains('Payment cannot be completed')").length !== 0) {
              // If this phrase is present the onReadyIframeCommunication will never fire
              // and the loader will never stop
              checkout.publish([{ type: "status", value: undefined }]);
              clearInterval(domElementCheckInterval as any);
            }
          }, 100);
        } else {
          checkout.publish([{ type: "status", value: undefined }]);
        }
      },
      onReadyIframeCommunication: () => {
        if (domElementCheckInterval) {
          clearInterval(domElementCheckInterval as any);
        }

        domElementCheckInterval = setInterval(() => {
          if ($(".spinner").length === 0) {
            clearInterval(domElementCheckInterval as any);
          }
        }, 100);
      },
      onBeforeSubmitCard: () => {},
      onBeforeSubmitVirtualAccount: () => {},
      onError: (error: OnReadyError) => {
        if (cardPaymentErrorNames.indexOf(error.name) !== -1) {
          checkout.ga.trackEvent({
            action: checkout.ga.events.error,
            category: "Card",
            label: error.response.response_code,
          });
        }
      },
      onAfterSubmit: () => {
        checkout.ga.trackEvent({
          action: checkout.ga.events.pay,
          category: "Card",
        });
        checkout.publish;
      },
    };

    wpwlOptions.onReady();
    wpwlOptions.onBeforeSubmitCard = () => {
      checkout.ga.trackEvent({
        action: checkout.ga.events.pay,
        category: "Card",
      });
      return true;
    };

    // Attach to the local card setup object to the global window object
    // @ts-ignore
    window.wpwlOptions = wpwlOptions;
  }, []);

  /**
   * CopyAndPay widget requires a global object for it to be initialized
   * The global setup object is attached to the Javascript object and passed to CopyAndPay to allow customization
   * via a HTML script tag
   * @param checkoutId
   */
  const loadCopyAndPayWidgetJS = useCallback(
    (checkoutId: string) => {
      onReady();

      const script = document.createElement("script");

      script.setAttribute("async", "");
      script.onerror = (): void => {};
      script.src = `${config.payonBaseUrl}/v1/paymentWidgets.js?checkoutId=${checkoutId}`;

      document.body.appendChild(script);
    },
    [onReady, config.payonBaseUrl]
  );

  /**
   * Fetches the payon checkout id from the Checkout backend on success
   * @param checkoutId
   */
  const getCheckoutId = useCallback(
    (checkoutId: string): void => {
      checkout.publish([
        {
          type: "status",
          value: "loading",
          heading: "Loading",
          message: "Get ready to pay by credit or debit card",
        },
      ]);
      const postData = { checkout_id: checkoutId };
      const successCallback = (result: PayOnCheckoutSuccessCallBack): void => {
        if (!result?.response?.result?.code) {
          loadCopyAndPayWidgetJS(result.payon_checkout_id);
        }
      };

      const errorCallback = (data: PayOnCheckoutErrorCallBack): void => {
        /**
         * TODO: INVESTIGATE FURTHER AND WATCH OUT FOR GOTCHAS as error 800.110.100 is for expired payment attempt
         * This code block below is potentially redundant, it seems like the structure of the error object
         * has changed and no longer contains the property responseJSON. Use and invalid checkout id to trigger
         * the errorcallback. The error response object is similar to PayOnCheckoutErrorCallBack
         * */
        if (data?.responseJSON?.response?.code === "800.110.100") {
          return;
        }

        checkout.publish([
          {
            type: "status",
            value: "error",
            code: data.code,
            message: data.error,
          },
        ]);
      };

      checkoutService.postFormEncodedContent(
        `${config.baseUrl}payon-checkout`,
        postData,
        successCallback,
        errorCallback
      );
    },
    [loadCopyAndPayWidgetJS, config.baseUrl, checkoutService]
  );

  useEffect(() => {
    getCheckoutId(config.checkoutId);
  }, [
    getCheckoutId,
    config.checkoutId,
    config.isNativeCardForm,
    checkoutService,
  ]);

  return (
    <>
      <PayOnCardHtml checkout={config} />
    </>
  );
};

export default Card;
