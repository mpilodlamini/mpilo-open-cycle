export {};

/* eslint-disable */
var _encodingString = "&#39;";
var merchant_payment_methods_object =
  "[&#39;American Express&#39;, &#39;EFTSecure&#39;, &#39;Diners Club&#39;, &#39;Visa&#39;, &#39;Mastercard&#39;, &#39;1ForYou&#39;, &#39;OZOW&#39;, &#39;Mobicred&#39;, &#39;Masterpass&#39;]";

/**
 * Has to be pre ES6 functionality for IE 11 to work
 * since the index.html file does not get transpiled
 * Converts the Python dictionary for the payment methods to a JavaScript array
 * @param paymentMethodsArray
 * @returns {string[]}
 */
var createPaymentsMethodsArray = function (paymentMethodsArray) {
  var arrayWithoutEncoding = paymentMethodsArray
    .split(_encodingString)
    .join("");
  var arrayWithoutLeftBracket = arrayWithoutEncoding.replace("[", "");
  var arrayWithoutRightBracket = arrayWithoutLeftBracket.replace("]", "");

  var arrayWithoutSpecialCharacters = arrayWithoutRightBracket.split(",");

  return arrayWithoutSpecialCharacters.map(function (payment) {
    return payment.trim();
  });
};

describe("Test index.html functionality", () => {
  it("createPaymentsMethodsArray should parse the payments object correctly to a string array", () => {
    const matchArray = [
      "American Express",
      "EFTSecure",
      "Diners Club",
      "Visa",
      "Mastercard",
      "1ForYou",
      "OZOW",
      "Mobicred",
      "Masterpass",
    ];
    expect(createPaymentsMethodsArray(merchant_payment_methods_object)).toEqual(
      matchArray
    );
  });
});
