import { Box, Grid, makeStyles } from "@material-ui/core";
import React from "react";
import { FeedbackIcon, FeedbackType, IconSize } from "../feedback";

export interface SnackbarStaticProps {
  title: string;
  label: string;

  dataTestId?: string;
  timed?: boolean;
  time?: string | number;
  feedback?: FeedbackType;
}

const useStyles = makeStyles(() => ({
  snackbarStatic: {
    borderRadius: "4px",
    padding: "16px 16px 16px 0",
  },
  warning: {
    background: "#FFF6E4",
    color: "#F07300",
  },
}));

export const SnackbarStatic = (props: SnackbarStaticProps) => {
  const classes = useStyles();

  return (
    <Box className={`${classes.snackbarStatic} ${classes.warning}`}>
      <Grid container alignItems="center">
        <Grid item xs={2} sm={1}>
          <Box
            alignItems="center"
            justifyContent="center"
            display="flex"
            width={1}
            height={1}
          >
            <FeedbackIcon
              size={IconSize.SMALL}
              feedback={props.feedback || FeedbackType.WARNING}
            />
          </Box>
        </Grid>
        <Grid item xs={10} sm={11} style={{ height: "100%" }}>
          <Box>
            {props.title && (
              <Box
                width={1}
                fontWeight="bold"
                fontSize="15px"
                data-testid={props.dataTestId}
              >
                {props.title}
              </Box>
            )}
            {props.label && (
              <Box
                width={1}
                fontSize="13px"
                display="flex"
                justifyContent="center"
              >
                {props.label}
                {props.timed && (
                  <Box component="span" ml={1}>
                    {props.time}
                  </Box>
                )}
              </Box>
            )}
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};
