import React, { ReactNode } from "react";
import ReportProblemOutlinedIcon from "@material-ui/icons/ReportProblemOutlined";
import { Box, BoxProps, Button } from "@material-ui/core";
import { useStyles } from "./support.styles";
import { warning } from "@peach/checkout-design-system/dist/variables";
import { Dialog } from "./dialog/Dialog";

export interface CancelTransactionProps extends BoxProps {
  children?: ReactNode;
  onConfirm: () => void;
  onCancel: () => void;
}

export const CancelTransaction = ({
  children,
  className,
  onConfirm,
  onCancel,
  ...props
}: CancelTransactionProps) => {
  const styles = useStyles();

  return (
    <>
      <Dialog
        title="Cancel Transaction?"
        icon={
          <ReportProblemOutlinedIcon
            style={{ color: warning, height: 50, width: 50 }}
          />
        }
        subtitle={
          <>
            You are about to cancel the transaction and return to the merchant.
            Are you sure you want to do this?
          </>
        }
        {...props}
      >
        <Box
          display="flex"
          flex
          flexDirection="column"
          justifyContent="center"
          gridGap={6}
          alignItems="center"
          width="100%"
          marginBottom={3}
        >
          <Button
            data-testid="dialog-cancel"
            className={styles.button}
            variant="contained"
            disableElevation
            onClick={onConfirm}
          >
            Cancel
          </Button>
          <Button
            data-testid="dialog-go-back"
            className={styles.goBackButton}
            onClick={onCancel}
          >
            Go Back
          </Button>
        </Box>
        {children}
      </Dialog>
    </>
  );
};
