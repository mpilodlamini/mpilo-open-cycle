import * as yup from "yup";
import { isValidLuhnNumber } from "../../../utils/validator";
import { CARD } from "../../../utils/constants";
import { getAvailableCardBrands } from "./cardFormUtils";
import { CHECKOUT } from "../../../contexts/CheckoutContext";

export const validateCardNumberUsingLuhn = (
  cardNumber: string | null | undefined = ""
): boolean => {
  if (!cardNumber) {
    return false;
  }

  return isValidLuhnNumber(cardNumber);
};

export const validateExpiryDate = (
  date: string | null | undefined = ""
): boolean => {
  if (!date) {
    return false;
  }

  if (date.length < 4) {
    return false;
  }

  const todaysDate = new Date();
  const todaysMonth = todaysDate.getMonth() + 1;
  const todaysYear = todaysDate.getFullYear();
  const month = parseInt(date.substring(0, 2), 10);
  const year = parseInt(
    `20${date.substring(date.length - 2, date.length)}`,
    10
  );

  let isValidMonth = month < 13;
  let isValidYear = false;

  if (year === todaysYear) {
    isValidMonth = month >= todaysMonth;
  }

  isValidYear = year >= todaysYear;

  return isValidMonth && isValidYear;
};

const cardBrands = getAvailableCardBrands(CHECKOUT.paymentMethods);

export const validateCardNumber = ({
  number,
  handleCreateError,
  handleValidateCardNumberUsingLuhn,
}: {
  number: string;
  handleValidateCardNumberUsingLuhn: any;
  handleCreateError: any;
}) => {
  let message = "";

  if (!handleValidateCardNumberUsingLuhn(number)) {
    message = "Invalid card number";
  }

  if (number.length < 13) message = "Too short";

  if (
    CARD.BRAND_DETECTION.AMERICAN_EXPRESS.test(number) &&
    cardBrands.indexOf(CARD.ENUM.AMERICAN_EXPRESS) === -1
  ) {
    message = "Amex not supported";
  }

  if (
    CARD.BRAND_DETECTION.VISA.test(number) &&
    cardBrands.indexOf(CARD.ENUM.VISA) === -1
  ) {
    message = "Visa not supported";
  }

  if (
    CARD.BRAND_DETECTION.MASTERCARD.test(number) &&
    cardBrands.indexOf(CARD.ENUM.MASTERCARD) === -1
  ) {
    message = "Mastercard not supported";
  }

  if (
    CARD.BRAND_DETECTION.DINERS_CLUB.test(number) &&
    cardBrands.indexOf(CARD.ENUM.DINERS_CLUB) === -1
  ) {
    message = "Diners Club not supported";
  }

  if (!message) {
    return true;
  }

  return handleCreateError({ message });
};

const validNameRegex = /^[\w'\-,.`][^0-9_!¡?÷¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/gim;

export const cardSchema = yup.object({
  isAmex: yup.boolean().required(),
  brand: yup.string().required(),
  number: yup
    .string()
    .required("Required")
    .test(
      "luhn",
      "Invalid card number",
      (value: string | undefined, { createError }) =>
        validateCardNumber({
          number: value || "",
          handleCreateError: createError,
          handleValidateCardNumberUsingLuhn: validateCardNumberUsingLuhn,
        })
    ),
  holderName: yup
    .string()
    .min(3, "Too Short")
    .max(128, "Too long")
    .matches(validNameRegex, "Only Aa-Zz ,-’~` allowed")
    .required("Required"),
  expiryDate: yup
    .string()
    .ensure()
    .required("Required")
    .test("customValidateExpiryDate", "Invalid date", validateExpiryDate),
  cvv: yup
    .string()
    .when("isAmex", {
      is: true,
      then: yup.string().min(4, "Too short"),
      otherwise: yup.string().min(3, "Too short"),
    })
    .required("Required"),
  street: yup
    .string()
    .required("Required")
    .min(1, "Too short")
    .max(95, "Too long")
    .matches(/[\s\S]{1,95}/)
    .trim(),
  city: yup.string().required("Required").min(3, "Too short").max(48),
  country: yup.string().required("Required").min(2, "Too short"),
  postalCode: yup
    .string()
    .required("Required")
    .min(2, "Too short")
    .max(20, "Too long")
    .matches(/[A-Za-z0-9]{1,20}/, "Invalid Postal Code: No special characters"),
});

export type CardType = yup.InferType<typeof cardSchema>;
