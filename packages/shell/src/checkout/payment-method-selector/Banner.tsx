// src/components/banners/payment.options.banner.jsx
import axios from "axios";
import React, { useCallback, useContext, useState } from "react";
import { Box, Grid, makeStyles, Theme, Button } from "@material-ui/core";
import { TitleBar } from "@peach/checkout-design-system/dist/controls/title-bar";
import {
  white,
  error,
  border,
} from "@peach/checkout-design-system/dist/variables";
import { Modal } from "../support/modal/Modal";
import { CancelTransaction } from "../support/CancelTransaction";
import { MerchantRedirect } from "../support/MerchanRedirect";
import { CheckoutContext } from "../../contexts/CheckoutContext";
import { SelectedPaymentContext } from "../../contexts/SelectedPaymentContext";
import { CancelButton } from "../CancelButton";

interface BannerProps {
  title: string;
}

const useStyles = makeStyles((theme: Theme) => ({
  wrapper: {
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
  button: {
    border: `1px solid ${border}`,
    backgroundColor: white,
    color: "#8A94A6",
    textTransform: "none",
    paddingLeft: "24px",
    paddingRight: "24px",

    "&:hover": {
      backgroundColor: error,
      borderColor: error,
      color: white,
    },

    [theme.breakpoints.down("xs")]: {
      fontSize: "13px",
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
}));

// TODO: confirm this redirect logic
const Banner = ({ title }: BannerProps) => {
  const styles = useStyles();

  return (
    <>
      <Box className={styles.wrapper} pt={3} pb={3} pl={4} pr={4}>
        <Grid container alignItems="center">
          <Grid item xs={8}>
            <TitleBar title={title} />
          </Grid>
          <Grid item xs={4}>
            <Box display="flex" flexDirection="row-reverse">
              <CancelButton />
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default Banner;
