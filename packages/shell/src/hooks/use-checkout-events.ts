import { CheckoutEvent } from "@peach/checkout-lib";
import { useContext, useEffect } from "react";
import { CheckoutContext, IFrameMethod } from "../contexts/CheckoutContext";
import { SelectedPaymentContext } from "../contexts/SelectedPaymentContext";

export const useCheckoutEvents = (
  handler: (event: CheckoutEvent) => void,
  deps: any[]
) => {
  const config = useContext(CheckoutContext);
  const { select } = useContext(SelectedPaymentContext);

  useEffect(() => {
    const listener = (e: MessageEvent) => {
      if (
        config.pluginPaymentMethods
          .map((m) => (m as IFrameMethod).origin)
          .includes(e.origin) &&
        e.data.message === "push-checkout-events"
      ) {
        e.data.events.forEach((e) => {
          switch (e.type) {
            case "reset":
              select();
              break;
          }
          handler(e);
        });
      }
    };
    window.addEventListener("message", listener);
    return () => {
      window.removeEventListener("message", listener);
    };
  }, deps);
};
