import { FeedbackType, ModalDialog } from "@peach/checkout-design-system";
import React, { ReactNode } from "react";
import withTimer from "../../timed";

export interface LoaderModalProps {
  open: boolean;
  title: string;
  label: ReactNode;
  onCancel: () => void;

  timed?: boolean;
  continueText?: string;
  cancelText?: string;
  onClick?: () => void;
}

function createModal(props: LoaderModalProps) {
  return (
    <ModalDialog
      open={props.open}
      type={FeedbackType.WARNING}
      title={props.title}
      label={props.label}
      onClick={props.onClick}
      onCancel={props.onCancel}
      continueText={props.continueText}
      cancelText={props.cancelText}
      forceBottom={false}
    />
  );
}

function getModal(props: LoaderModalProps) {
  if (props.timed) {
    return withTimer(ModalDialog)({
      onTimeout: props.onClick,
      forceBottom: false,
      ...props,
    });
  } else {
    return createModal(props);
  }
}

const ErrorModal: React.FunctionComponent<LoaderModalProps> = (
  props: LoaderModalProps
) => {
  return getModal(props);
};

export default ErrorModal;
