import CONSTANTS from "../../../../constantsUtil";
import { When, Then } from "cypress-cucumber-preprocessor/steps";

const enterPhoneNumber = (phoneNumber: string) => {
  cy.intercept(
    CONSTANTS.AJAX_REQUEST_VERB.POST,
    `${Cypress.env("paymentBaseUrl")}/payment`
  ).as("paymentMethodRequest");

  cy.getElementByDataTestId(CONSTANTS.MPESA.MOBILE_INPUT.ID).clear().type(phoneNumber);
  cy.getElementByDataTestId(CONSTANTS.MPESA.SUBMIT_BUTTON.ID).click();
  cy.contains(CONSTANTS.MPESA.VERIFYING_DIALOG_TEXT);
  cy.wait("@paymentMethodRequest");
};

When("I enter a valid number and verify", () => {
  enterPhoneNumber(CONSTANTS.MPESA.SUCCESS_NUMBER);
});

Then("I should see awaiting phone verify notification", () => {
  cy.contains(CONSTANTS.MPESA.VERIFY_MESSAGE.LABEL);
  cy.contains(CONSTANTS.MPESA.TIMER_LABEL);
});

When("I enter an invalid number and verify", () => {
  enterPhoneNumber(CONSTANTS.MPESA.UNSUCCESSFUL_NUMBER);
});
