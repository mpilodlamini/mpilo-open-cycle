import { PaymentMethod } from "../../../contexts/CheckoutContext";
import { CARD, CARD_BRANDS } from "../../../utils/constants";
import cardSpacingFormats from "./cardSpacingFormats";
import countryList, { CountryType } from "./countryList";

export const getAvailableCardBrands = (
  paymentMethods: Array<PaymentMethod> = []
): typeof CARD_BRANDS => {
  const cardBrands: string[] = [];

  for (let i = 0; i < paymentMethods.length; i++) {
    const paymentMethod = paymentMethods[i];

    if (!paymentMethod) continue;

    const sanitizedPaymentMethod = paymentMethod
      .replace(/\s/g, "")
      .toUpperCase();

    if (CARD.BRAND_COLLECTION.indexOf(sanitizedPaymentMethod) !== -1) {
      cardBrands.push(sanitizedPaymentMethod);
    }
  }

  return cardBrands;
};

export const findCardBrandType = (
  number: string,
  cardBrands: string[],
  regexPatterns: typeof CARD.BRAND_DETECTION = CARD.BRAND_DETECTION
): string => {
  if (
    regexPatterns.AMERICAN_EXPRESS.test(number) &&
    cardBrands.indexOf(CARD.ENUM.AMERICAN_EXPRESS) !== -1
  ) {
    return CARD.ENUM.AMERICAN_EXPRESS;
  }

  if (
    regexPatterns.VISA.test(number) &&
    cardBrands.indexOf(CARD.ENUM.VISA) !== -1
  ) {
    return CARD.ENUM.VISA;
  }

  if (
    regexPatterns.MASTERCARD.test(number) &&
    cardBrands.indexOf(CARD.ENUM.MASTERCARD) !== -1
  ) {
    return CARD.ENUM.MASTERCARD;
  }

  if (
    regexPatterns.DINERS_CLUB.test(number) &&
    cardBrands.indexOf(CARD.ENUM.DINERS_CLUB) !== -1
  ) {
    return CARD.ENUM.DINERS_CLUB;
  }

  return "All Cards";
};

export const getCountryFromValue = (
  selectedValue: string,
  list: CountryType[] = countryList
) => {
  if (!selectedValue || list.length === 0) {
    return {
      value: "",
      code: "",
      icon: "",
      label: "",
    };
  }

  return list.find(({ value }) => value === selectedValue);
};

export const getCardNumberFormat = ({
  selectedCardBrand,
  spacingFormatList = cardSpacingFormats,
  brandNames = CARD.ENUM,
}: {
  selectedCardBrand: string;
  spacingFormatList?: typeof cardSpacingFormats;
  brandNames?: typeof CARD.ENUM;
}): string => {
  const { amex, visa, masterCard, dinersClub, generic } = spacingFormatList;

  const { AMERICAN_EXPRESS, VISA, MASTERCARD, DINERS_CLUB } = brandNames;

  switch (selectedCardBrand) {
    case AMERICAN_EXPRESS:
      return amex.default.format;
    case VISA:
      return visa.default.format;
    case MASTERCARD:
      return masterCard.default.format;
    case DINERS_CLUB:
      return dinersClub.default.format;
    default:
      return generic.default.format;
  }
};

export default {
  getAvailableCardBrands,
  findCardBrandType,
};
