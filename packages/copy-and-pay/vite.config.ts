import { defineConfig } from "vite";
import reactRefresh from "@vitejs/plugin-react-refresh";
import legacy from "@vitejs/plugin-legacy";

// https://vitejs.dev/config/
export default defineConfig({
  base: process.env.NODE_ENV === "development" ? "/copy-and-pay" : "/",
  plugins: [
    reactRefresh(),
    legacy({
      targets: ["ie 11", ">0.2%", "not dead", "not op_mini all"],
      additionalLegacyPolyfills: ["regenerator-runtime/runtime"],
    }),
  ],
  resolve: {
    dedupe: [
      "react",
      "react-dom",
      "@material-ui/core",
      "@material-ui/styles",
      "@material-ui/icons",
      "@material-ui/utils",
    ],
  },
});
