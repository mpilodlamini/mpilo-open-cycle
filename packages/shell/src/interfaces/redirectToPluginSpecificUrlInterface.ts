import { ClientData } from "./clientData";

export interface RedirectToPluginSpecificUrl {
  [key: string]: any;

  baseUrl: string;

  pluginName: string;
  source: string;
  redirectUrl: string;
  cancelUrl: string;
  isCheckoutInTest: boolean;
  clientData: ClientData;
  redirectPostData: {};
}
