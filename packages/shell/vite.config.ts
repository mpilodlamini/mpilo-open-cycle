import { defineConfig } from "vite";
import reactRefresh from "@vitejs/plugin-react-refresh";
import legacy from "@vitejs/plugin-legacy";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  base: process.env.NODE_ENV == "production" ? "{{ assets_base_path }}" : "/",
  plugins: [
    reactRefresh(),
    legacy({
      targets: ["ie 11", ">0.2%", "not dead", "not op_mini all"],
      additionalLegacyPolyfills: ["regenerator-runtime/runtime"],
    }),
  ],
  build: {
    rollupOptions: {
      input:
        process.env.NODE_ENV == "production"
          ? resolve(__dirname, "index.prod.html")
          : resolve(__dirname, "index.html"),
      external: [
        "{{ assets_base_path }}static/css/main.css?v={{ hash_value }}",
      ],
    },
  },
  resolve: {
    dedupe: [
      "react",
      "react-dom",
      "@material-ui/core",
      "@material-ui/styles",
      "@material-ui/icons",
      "@material-ui/utils",
    ],
  },
});
