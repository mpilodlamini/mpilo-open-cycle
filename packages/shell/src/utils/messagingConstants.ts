export const MESSAGING = Object.freeze({
  DEFAULT_PAYMENT_FAILED_ERROR_HEADER: "Payment Failed",
  DEFAULT_ERROR_HEADING: "Payment Error",
  DEFAULT_ERROR_MESSAGE:
    "Transaction failed due to an internal error. Please go back to try again or select a different payment method.",
  DEFAULT_LOADING_HEADING: "Processing Payment",
  DEFAULT_LOADING_MESSAGE: "Please wait while we process your payment.",
  DEFAULT_ERROR_MESSAGE_REDIRECT:
    "Your payment has been unsuccessful. We are navigating you back to the store",
  DEFAULT_CANCEL_TRANSACTION_HEADER: "Cancel Transaction?",
  DEFAULT_CANCEL_TRANSACTION_MESSAGE:
    "You are about to cancel the transaction and return to the merchant. Are you sure you want to do this?",
  ONE_FOR_YOU: {
    DEFAULT_LOADING_MESSAGE: "Please wait while we verify your Voucher Pin.",
    DEFAULT_LOADING_HEADING: "Verifying your Voucher",
    INVALID_PIN_ERROR_HEADING: "Invalid Voucher PIN",
    INVALID_PIN_ERROR_MESSAGE:
      "Please make sure you have entered your PIN correctly. Please go back to try again or select a different payment method.",
    EXPIRED_VOUCHER_ERROR_HEADING: "Expired Voucher",
    EXPIRED_VOUCHER_ERROR_MESSAGE:
      "Please contact 1ForYou or try again with a different voucher or select a different payment method.",
    NO_AVAILABLE_BALANCE_HEADING: "No Available Balance",
    NO_AVAILABLE_BALANCE_MESSAGING:
      "Your voucher has already been redeemed. Please try again with a different voucher or select a different payment method.",
  },
  MPESA: {
    DEFAULT_LOADING_HEADING: "Verifying Account",
    DEFAULT_LOADING_MESSAGE:
      "Please wait while we verify your mpesa account details.",
  },
});

export const BUTTON_LABELS = Object.freeze({
  BLANK_LABEL: "",
  GO_BACK: "Go Back",
  RETURN_TO_STORE: "Return to Store",
  CONTINUE: "Continue",
  TRY_AGAIN: "Try Again",
  CANCEL_PAYMENT: "Cancel Payment",
  SIGN_IN: "Sign In",
  VERIFY: "Verify",
  CANCEL: "Cancel",
  BACK: "Back",
});
