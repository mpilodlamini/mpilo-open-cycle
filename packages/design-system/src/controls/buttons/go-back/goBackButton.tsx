import React, { HTMLAttributes } from "react";
import { Button, makeStyles } from "@material-ui/core";

import { text } from "../../../variables";

export type GoBackButtonProps = HTMLAttributes<HTMLButtonElement> & {
  onClick: () => void;
  label?: string;
};

const customStyles = makeStyles(() => ({
  button: {
    color: text,
    textTransform: "none",
    textDecoration: "underline",
  },
}));

const defaultText = "Go Back";

export const GoBackButton = (props: GoBackButtonProps) => {
  const classes = customStyles();

  return (
    <Button className={classes.button} onClick={props.onClick}>
      {props.label || defaultText}
    </Button>
  );
};
