export interface ClientData {
  checkoutId: string;
  merchantTransactionId: string;
  merchantInvoiceId: string;
}
