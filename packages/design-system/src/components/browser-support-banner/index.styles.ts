import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  browserBanner: {
    background: "#F5F6F8",
    width: "100%",
    textAlign: "center",
    fontSize: "12px",
    zIndex: 999,
    borderBottom: "1px solid #EEEEEE",
    position: "fixed",

    [theme.breakpoints.down("xs")]: {
      position: "relative",
    },
  },
}));

export default useStyles;
