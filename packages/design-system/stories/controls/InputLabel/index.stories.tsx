import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import { InputLabel } from "../../../";
import { InputLabelProps } from "@material-ui/core/InputLabel";

export default {
  title: "Controls/InputLabel",
} as Meta;

export const Default: Story<InputLabelProps> = (props) => {
  return <InputLabel {...props}>Label</InputLabel>;
};

Default.args = {
  htmlFor: "<control_id>",
  required: true,
};
