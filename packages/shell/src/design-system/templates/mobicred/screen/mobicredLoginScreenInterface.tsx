export interface MobicredLoginScreenProps {
  onClick: (email: string, password: string) => void;
}

export interface MobicredFormValues {
  email: string;
  password: string;
}
