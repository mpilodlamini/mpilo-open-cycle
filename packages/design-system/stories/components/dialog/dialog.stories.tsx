import { Story } from "@storybook/react/types-6-0";
import React from "react";
import { FeedbackType, Dialog, DialogProps } from "../../../";

export default {
  title: "Components / Dialog",
  component: Dialog,
};

export const Default: Story<DialogProps> = (props: DialogProps) => {
  props = { ...props };
  return <Dialog {...props} />;
};

Default.args = {
  type: FeedbackType.INFO,
  title: "Dialog Title",

  errorMessages: [],
  forceBottom: false,
  continueText: null,
  cancelText: null,

  label: <p>Dialog Label</p>,
  children: <p>Dialog body</p>,

  onClick: () => {},
  onCancel: () => {},
};

export const Error: Story<DialogProps> = (props: DialogProps) => (
  <Dialog {...props} />
);

Error.args = {
  type: FeedbackType.ERROR,
  title: "Error",

  errorMessages: [
    "An error has occurred.",
    "Please contact your administrator.",
  ],
  forceBottom: false,
  continueText: null,
  cancelText: null,

  label: <p>Dialog Label</p>,
  children: <p>Dialog body</p>,

  onClick: () => {},
  onCancel: () => {},
};
