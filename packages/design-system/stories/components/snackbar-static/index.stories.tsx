import React from "react";
import { SnackbarStatic, SnackbarStaticProps } from "../../../";

export default {
  title: "Components / SnackBarStatic",
};

export const Default = (props: SnackbarStaticProps) => (
  <SnackbarStatic {...props} />
);

Default.args = {
  title: "Confirm",
  label: "Awaiting payment confirmation.",
  timed: true,
  time: "10",
};

Default.parameters = {
  docs: {
    storyDescription:
      "This is static and cannot be dismissed. It is also used for the timer",
  },
};
