// src/components/banners/payment.options.banner.jsx
import { Box, Grid, makeStyles, Theme } from "@material-ui/core";
import { CancelButton, TitleBar } from "@peach/checkout-design-system";
import React from "react";

interface PaymentOptionsProps {
  title: string;
  onCancel: () => void;
}

const useStyles = makeStyles((theme: Theme) => ({
  wrapper: {
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
}));

const PaymentOptionsBanner: React.FunctionComponent<PaymentOptionsProps> = (
  props: PaymentOptionsProps
) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper} pt={3} pb={3} pl={4} pr={4}>
      <Grid container alignItems="center">
        <Grid item xs={8}>
          <TitleBar title={props.title} />
        </Grid>
        <Grid item xs={4}>
          <Box display="flex" flexDirection="row-reverse">
            <CancelButton onClick={props.onCancel} />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default PaymentOptionsBanner;
