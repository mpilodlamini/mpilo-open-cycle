import { Box, Button, Collapse } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React from "react";
import useStyles from "./index.styles";

export interface FeedbackMessageProps {
  messages: string[];
}

const createMessages = (messages: string[]): React.ReactNode => {
  return messages.map((message) => <li key={message}>{message}</li>);
};

export const FeedbackMessage = (props: FeedbackMessageProps) => {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  return (
    <Box mt={2} minHeight="100px">
      <Collapse
        className={checked ? classes.active : classes.inactive}
        in={checked}
        collapsedHeight="40px"
        timeout={1000}
      >
        <Button onClick={handleChange} className={classes.button}>
          Error Message <ExpandMoreIcon className="expandIcon" />
        </Button>

        <ul className={classes.expand}>{createMessages(props.messages)}</ul>
      </Collapse>
    </Box>
  );
};
