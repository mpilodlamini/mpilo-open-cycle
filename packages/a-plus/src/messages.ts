export const A_PLUS = {
  DEFAULT_LOADING_HEADING: "Verifying",
  DEFAULT_LOADING_MESSAGE: "Please wait while we verify your details",

  LOADING_OTP_VERIFY_HEADING: "Verifying OTP",
  LOADING_OTP_VERIFY_MESSAGE: "Please wait while we verify your One Time Pin",

  LOADING_RESEND_OTP_HEADING: "Resending OTP",
  LOADING_RESEND_OTP_MESSAGE: "Please wait while we resend your One Time Pin",

  PAYMENT_ERROR_HEADING: "Payment Error",

  PAYMENT_DECLINED_HEADING: "Payment Declined",

  PAYMENT_FAILED_HEADING: "Payment Failed",

  TIMEOUT_HEADING: "Payment Timeout",

  INSUFFICIENT_FUNDS_HEADING: "Insufficient Funds",

  INVALID_CREDENTIALS_HEADING: "Invalid Credentials",

  PAYMENT_DETAILS_ERROR_HEADING: "Incorrect Payment Details",

  OTP_INCORRECT_HEADING: "Incorrect OTP",

  MERCHANT_ERROR_HEADING: "A+ unavailable",
};

export const messages = {
  "200.300.404": {
    // System error, field validation failed.
    heading: A_PLUS.PAYMENT_DETAILS_ERROR_HEADING,
  },
  "100.350.200": {
    //Session invalid
    heading: A_PLUS.PAYMENT_FAILED_HEADING,
  },
  "900.100.202": {
    //internal error auth token
    heading: A_PLUS.PAYMENT_ERROR_HEADING,
  },
  "900.300.600": {
    //Session Expired in EFT Purchase request
    heading: A_PLUS.TIMEOUT_HEADING,
  },
  "800.100.100": {
    //Daily online transaction volume exceeded.
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
  "800.100.152": {
    //Account Blocked
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
  "800.120.200": {
    //Daily online transaction volume exceeded.
    heading: A_PLUS.PAYMENT_FAILED_HEADING,
  },
  "100.550.310": {
    //Daily online transaction value exceeded.
    heading: A_PLUS.PAYMENT_FAILED_HEADING,
  },
  "800.100.190": {
    //Unable to generate or send the OTP.
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
  "100.380.401": {
    //Invalid cell phone number
    heading: A_PLUS.INVALID_CREDENTIALS_HEADING,
    cancelText: "Go Back",
  },
  "800.100.157": {
    //invalid expiry
    heading: A_PLUS.INVALID_CREDENTIALS_HEADING,
    cancelText: "Go Back",
  },
  "800.100.151": {
    //invalid card
    heading: A_PLUS.INVALID_CREDENTIALS_HEADING,
    cancelText: "Go Back",
  },
  "800.100.172": {
    //Account Blocked
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
  "800.100.160": {
    //Account Blocked
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
  "800.100.203": {
    //Account Blocked
    heading: A_PLUS.INSUFFICIENT_FUNDS_HEADING,
  },
  "600.200.201": {
    //Retailer store not found.
    heading: A_PLUS.MERCHANT_ERROR_HEADING,
  },
  "900.100.200": {
    //An exception on tenacity’s side
    heading: A_PLUS.MERCHANT_ERROR_HEADING,
  },
  "100.350.400": {
    //An exception on tenacity’s side
    heading: A_PLUS.OTP_INCORRECT_HEADING,
  },
  "800.100.161": {
    //OTP validation retries exceeded
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
  "300.100.100": {
    //Maximum number of OTP retries reached, with OTP resend possible
    heading: A_PLUS.OTP_INCORRECT_HEADING,
  },
  "900.100.301": {
    //timeout
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
  "800.100.162": {
    //Maximum number of OTP resends reached
    heading: A_PLUS.PAYMENT_DECLINED_HEADING,
  },
};
