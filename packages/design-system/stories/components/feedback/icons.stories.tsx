import { Story } from "@storybook/react/types-6-0";
import React from "react";
import {
  FeedbackIcon,
  FeedbackIconProps,
  FeedbackType,
  IconSize,
} from "../../../";

export default {
  title: "Components / FeedbackIcon",
  component: FeedbackIcon,

  parameters: {
    docs: {
      storyDescription:
        "This is a re-usable component that will change dependent on selected feedback type. E.g Warning, Success etc",
    },
  },
};

export const Default: Story<FeedbackIconProps> = (props: FeedbackIconProps) => (
  <FeedbackIcon {...props} />
);

Default.args = {
  feedback: FeedbackType.INFO,
  size: IconSize.SMALL,
};
