import React from "react";
import { createContext, ReactNode, useEffect, useState } from "react";
import { checkout, checkoutOrigin } from "../lib/checkout";
import { CheckoutConfig as BaseConfig } from "@peach/checkout-lib";

export type CheckoutConfig = {
  config: BaseConfig;
};

export const ConfigContext = createContext<CheckoutConfig>({
  config: {} as BaseConfig,
});

export const ConfigProvider = ({ children }: { children: ReactNode }) => {
  const [loading, setLoading] = useState(true);
  const [config, setConfig] = useState<any>({});

  useEffect(() => {
    checkout.publish([{ type: "status", value: "initializing" }]);
    const listener = async (e: MessageEvent) => {
      if (
        e.origin === checkoutOrigin &&
        e.data.message === "checkout-bootstrap"
      ) {
        setConfig(e.data);
        setLoading(false);
        checkout.publish([{ type: "status", value: undefined }]);
      }
    };
    window.addEventListener("message", listener);
    return () => {
      window.removeEventListener("message", listener);
    };
  }, [setLoading, setConfig]);

  if (loading) {
    return null;
  }

  return (
    <ConfigContext.Provider value={config}>{children}</ConfigContext.Provider>
  );
};
