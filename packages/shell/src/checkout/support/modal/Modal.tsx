import React from "react";
import classnames from "classnames";
import { Modal as MuiModal, Box, ModalProps } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { white } from "@peach/checkout-design-system/dist/variables";

const useStyles = makeStyles(() => {
  const top = 50;
  const left = 50;
  return {
    box: {
      position: "absolute",
      outline: 0,
      borderRadius: "8px",
      width: "auto",
      minWidth: "290px",
      paddingTop: "24px",
      paddingBottom: "24px",
      paddingLeft: "48px",
      paddingRight: "48px",
      background: white,
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    },
  };
});

export const Modal = ({
  children,
  className,
  classes = {},
  ...props
}: ModalProps & { classes?: { root?: string; box?: string } }) => {
  const styles = useStyles();

  return (
    <Box className={classes.root}>
      <MuiModal {...props}>
        <Box className={classnames(styles.box, classes.box)}>{children}</Box>
      </MuiModal>
    </Box>
  );
};
