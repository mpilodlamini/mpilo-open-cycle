/// <reference types="Cypress" />
/// <reference types="cypress-iframe" />
import { When, Then } from "cypress-cucumber-preprocessor/steps";
import "cypress-iframe";

When("I switch to the iframe", () => {
  cy.frameLoaded("iframe");
});

// ******* CLICKING
When("I click on {string} in the iframe", (element) => {
  cy.getIFrameElementByDataTestId(element).click();
});

// ******** ASSERTION
Then(
  "I should see that the {string} button is disabled in the iframe",
  (element) => {
    cy.getIFrameElementByDataTestId(element).should("be.disabled");
  }
);

Then(
  "I should see {string} containing the text {string} in the iframe",
  (element, warnText) => {
    cy.getIFrameElementByDataTestId(element).contains(warnText);
  }
);

// ******** TYPING
When(
  "I enter {string} on the field {string} in the iframe",
  (inputText, element) => {
    cy.getIFrameElementByDataTestId(element).type(inputText);
  }
);

When("I enter {string} in the iframe", (inputText) => {
  //TODO: DELETE
  cy.iframe().find("input#1ForYou").type(inputText);
});
