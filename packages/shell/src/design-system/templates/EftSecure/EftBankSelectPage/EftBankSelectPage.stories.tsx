import React from "react";
import EftBankSelectPage from "./EftBankSelectPage";

export default {
  title: "payments / Eft bank select page",
  component: EftBankSelectPage,
  parameters: {
    componentSubtitle: "This is a subtitle",
    docs: {
      storyDescription:
        "In the hosted payment page, if the customer selects EFT Secure then trigger a payment request to the Callpay API to get the payment key using the username and password configured in the Merchant Account",
    },
  },
};

export const select = (args: any) => <EftBankSelectPage {...args} />;
