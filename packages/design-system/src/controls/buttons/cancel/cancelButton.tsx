import { Button, makeStyles, Theme } from "@material-ui/core";
import React, { HTMLAttributes } from "react";
import { border, error, white } from "../../../variables";

const customStyles = makeStyles((theme: Theme) => ({
  button: {
    border: `1px solid ${border}`,
    backgroundColor: white,
    color: "#8A94A6",
    textTransform: "none",
    paddingLeft: "24px",
    paddingRight: "24px",

    "&:hover": {
      backgroundColor: error,
      borderColor: error,
      color: white,
    },

    [theme.breakpoints.down("xs")]: {
      fontSize: "13px",
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
}));

export type CancelButtonProps = HTMLAttributes<HTMLButtonElement> & {
  onClick: () => void;
};

export const CancelButton = (props: CancelButtonProps) => {
  const classes = customStyles();

  return (
    <Button
      className={classes.button}
      variant="contained"
      size="small"
      disableElevation
      onClick={props.onClick}
    >
      Cancel
    </Button>
  );
};
