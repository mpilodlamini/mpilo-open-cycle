export const MIDDLEWARE_SHOPIFY_CONSTANTS = {
  //Mandatory fields
  secretKey: 'input[id="secret_key"]',
  "authentication.entityId": 'input[id="id_x_account_id"]',
  amount: 'input[id="id_x_amount"]',
  currency: 'input[id="id_x_currency"]',
  reference: 'input[id="id_x_reference"]',
  shopCountry: 'input[id="id_x_shop_country"]',
  shopName: 'input[id="id_x_shop_name"]',
  test: 'input[id="id_x_test"]',
  urlCallback: 'input[id="id_x_url_callback"]',
  urlCancel: 'input[id="id_x_url_cancel"]',
  urlComplete: 'input[id="id_x_url_complete"]',

  //Optional fields
  transactionType: 'input[id="id_x_transaction_type"]',
  ////Customer Billing
  customerBillingAddress1: 'input[id="id_x_customer_billing_address1"]',
  customerBillingAddress2: 'input[id="id_x_customer_billing_address2"]',
  customerBillingCity: 'input[id="id_x_customer_billing_city"]',
  customerBillingCompany: 'input[id="id_x_customer_billing_company"]',
  customerBillingCountry: 'input[id="id_x_customer_billing_country"]',
  customerBillingPhone: 'input[id="id_x_customer_billing_phone"]',
  customerBillingState: 'input[id="id_x_customer_billing_state"]',
  customerBillingZip: 'input[id="id_x_customer_billing_zip"]',
  ////Customer
  customerEmail: 'input[id="id_x_customer_email"]',
  customerFirstName: 'input[id="id_x_customer_first_name"]',
  customerLastName: 'input[id="id_x_customer_last_name"]',
  customerPhone: 'input[id="id_x_customer_phone"]',
  ////Customer Shipping
  customerShippingAddress1: 'input[id="id_x_customer_shipping_address1"]',
  customerShippingAddress2: 'input[id="id_x_customer_shipping_address2"]',
  customerShippingCity: 'input[id="id_x_customer_shipping_city"]',
  customerShippingCompany: 'input[id="id_x_customer_shipping_company"]',
  customerShippingCountry: 'input[id="id_x_customer_shipping_country"]',
  customerShippingFirstName: 'input[id="id_x_customer_shipping_first_name"]',
  customerShippingLastName: 'input[id="id_x_customer_shipping_last_name"]',
  customerShippingPhone: 'input[id="id_x_customer_shipping_phone"]',
  customerShippingState: 'input[id="id_x_customer_shipping_state"]',
  customerShippingZip: 'input[id="id_x_customer_shipping_zip"]',

  description: 'input[id="id_x_description"]',
  invoice: 'input[id="id_x_invoice"]',
  shopifyOrderId: 'input[id="id_x_shopify_order_id"]',
};

export interface MiddlewareShopifyObject {
  secretKey?: string;
  "authentication.entityId"?: string;
  amount?: string;
  currency?: string;
  reference?: string;
  shopCountry?: string;
  shopName?: string;
  test?: string;
  urlCallback?: string;
  urlCancel?: string;
  urlComplete?: string;

  transactionType?: string;
  customerBillingAddress1?: string;
  customerBillingAddress2?: string;
  customerBillingCity?: string;
  customerBillingCompany?: string;
  customerBillingCountry?: string;
  customerBillingPhone?: string;
  customerBillingState?: string;
  customerBillingZip?: string;

  customerEmail?: string;
  customerFirstName?: string;
  customerLastName?: string;
  customerPhone?: string;

  customerShippingAddress1?: string;
  customerShippingAddress2?: string;
  customerShippingCity?: string;
  customerShippingCompany?: string;
  customerShippingCountry?: string;
  customerShippingFirstName?: string;
  customerShippingLastName?: string;
  customerShippingPhone?: string;
  customerShippingState?: string;
  customerShippingZip?: string;

  description?: string;
  invoice?: string;
  shopifyOrderId?: string;

  buttonId?: string;
}