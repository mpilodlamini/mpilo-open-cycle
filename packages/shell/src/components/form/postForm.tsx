import React, { useEffect, useRef } from "react";
import PostFormInputs from "./postFormInputs";

interface PostFormProps {
  redirectUrl: string;
  fieldsCollection: { name: string; value: string }[];
}

const PostForm = (props: PostFormProps) => {
  const formRef = useRef(null);

  useEffect(() => {
    if (formRef?.current) {
      // @ts-ignore
      formRef.current.submit();
    }
  }, [formRef]);

  return (
    <form
      style={{ display: "none" }}
      ref={formRef}
      action={props.redirectUrl}
      id="postForm"
    >
      <PostFormInputs fieldsCollection={props.fieldsCollection} />
      <button type="submit"> Submit</button>
    </form>
  );
};

export default PostForm;
