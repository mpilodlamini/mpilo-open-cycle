import React, { useContext, useMemo } from "react";
import { checkout } from "./lib/checkout";
import { ConfigContext } from "./contexts/ConfigContext";
import { OneForYouForm } from "./OneForYouForm";

export const OneForYouContainer = () => {
  const { config } = useContext(ConfigContext);

  const ONE_FOR_YOU = useMemo(() => {
    return {
      DEFAULT_LOADING_MESSAGE: "Please wait while we verify your Voucher Pin.",
      DEFAULT_LOADING_HEADING: "Verifying your Voucher",
      INVALID_PIN_ERROR_HEADING: "Invalid Voucher PIN",
      INVALID_PIN_ERROR_MESSAGE:
        "Please make sure you have entered your PIN correctly. Please go back to try again or select a different payment method.",
      EXPIRED_VOUCHER_ERROR_HEADING: "Expired Voucher",
      EXPIRED_VOUCHER_ERROR_MESSAGE:
        "Please contact 1ForYou or try again with a different voucher or select a different payment method.",
      NO_AVAILABLE_BALANCE_HEADING: "No Available Balance",
      NO_AVAILABLE_BALANCE_MESSAGING:
        "Your voucher has already been redeemed. Please try again with a different voucher or select a different payment method.",
      REVERSAL_PENDING_PAYMENT_ERROR_HEADING: "Payment Pending",
      REVERSAL_PENDING_PAYMENT_ERROR_MESSAGING: `Processing of this payment has failed due to an internal error. Please contact ${config.merchant} for support.`,
    };
  }, [config]);

  const errorMessages: {
    [key: string]: {
      heading: string;
      message: string;
    };
  } = useMemo(() => {
    return {
      "800.100.156": {
        heading: ONE_FOR_YOU.INVALID_PIN_ERROR_HEADING,
        message: ONE_FOR_YOU.INVALID_PIN_ERROR_MESSAGE,
      },
      "100.100.303": {
        heading: ONE_FOR_YOU.EXPIRED_VOUCHER_ERROR_HEADING,
        message: ONE_FOR_YOU.EXPIRED_VOUCHER_ERROR_MESSAGE,
      },
      "800.110.100": {
        heading: ONE_FOR_YOU.NO_AVAILABLE_BALANCE_HEADING,
        message: ONE_FOR_YOU.NO_AVAILABLE_BALANCE_MESSAGING,
      },
      "900.100.400": {
        heading: ONE_FOR_YOU.REVERSAL_PENDING_PAYMENT_ERROR_HEADING,
        message: ONE_FOR_YOU.REVERSAL_PENDING_PAYMENT_ERROR_MESSAGING,
      },
    };
  }, []);

  return (
    <OneForYouForm
      onSubmit={async (voucherValue: string) => {
        try {
          checkout.ga.trackPaymentInformation();
          checkout.ga.trackPage("1ForYou");
          checkout.ga.trackEvent({
            action: checkout.ga.events.pay,
            category: "1ForYou",
          });
          checkout.ga.trackEvent({
            action: checkout.ga.events.pay,
            category: "1ForYou",
          });

          checkout.publish([
            {
              type: "status",
              value: "loading",
              heading: ONE_FOR_YOU.DEFAULT_LOADING_HEADING,
              message: ONE_FOR_YOU.DEFAULT_LOADING_MESSAGE,
            },
          ]);

          const response = await fetch(`${config.baseUrl}payment`, {
            method: "POST",
            body: new URLSearchParams({
              merchant_key: config.merchantKey,
              checkout_id: config.checkoutId,
              payment_brand: "1FORYOU",
              password: voucherValue,
            }),
          });

          const data = await response.json();

          if (data.status === "success") {
            checkout.ga.trackEvent({
              action: checkout.ga.events.payment_success,
              category: "1ForYou",
              label: data.response.response_code,
            });
            checkout.publish([
              {
                type: "status",
                value: "success",
                code: data.response.response_code,
                heading: errorMessages[data.response.response_code]?.heading,
                message: errorMessages[data.response.response_code]?.message,
              },
              {
                type: "checkout-redirect",
                data: data.response.redirect_post_data,
                url: data.response.redirect_url,
                method: "POST",
              },
            ]);
          } else if (data.status === "pending") {
            checkout.ga.trackEvent({
              action: checkout.ga.events.payment_pending,
              category: "1ForYou",
              label: data.response.response_code,
            });
            checkout.publish([
              {
                type: "status",
                value: "pending",
                code: data.response.response_code,
              },
              {
                type: "checkout-redirect",
                data: data.response.redirect_post_data,
                url: data.response.redirect_url,
                method: "POST",
              },
            ]);
          } else if (data.response.response_code === "900.100.400") {
            checkout.ga.trackEvent({
              action: checkout.ga.events.payment_pending,
              category: "1ForYou",
              label: data.response.response_code,
            });
            checkout.publish([
              {
                type: "status",
                value: "pending",
                code: data.response.response_code,
                heading: errorMessages[data.response.response_code]?.heading,
                message: errorMessages[data.response.response_code]?.message,
              },
              {
                type: "checkout-redirect",
                data: data.response.redirect_post_data,
                url: data.response.redirect_url,
                method: "POST",
              },
            ]);
          } else {
            checkout.ga.trackEvent({
              action: "error",
              category: "1ForYou",
              label: data.response.response_code,
            });
            checkout.publish([
              {
                type: "status",
                value: "error",
                code: data.response.response_code,
                heading: errorMessages[data.response.response_code]?.heading,
                message: errorMessages[data.response.response_code]?.message,
              },
              {
                type: "checkout-redirect",
                data: data.response.redirect_post_data,
                url: data.response.redirect_url,
                method: "POST",
              },
            ]);
          }
        } catch (e) {
          checkout.ga.trackEvent({
            action: "error",
            category: "1ForYou",
          });
          checkout.publish([
            {
              type: "status",
              value: "error",
              heading: "",
              message: "",
            },
            {
              type: "checkout-redirect",
              data: {},
              url: "",
              method: "POST",
            },
          ]);
        }
      }}
    />
  );
};
