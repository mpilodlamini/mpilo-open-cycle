import { Then, When } from "cypress-cucumber-preprocessor/steps";

When("I click on the pay now button in the frame", () => {
  cy.iframe().find(".wpwl-wrapper-submit").contains("Pay now").click();
});

Then(
  "I should see that the {string} contains the text {string} inside the payon form",
  (wpwlClass, fieldText) => {
    cy.iframe().find(`.${wpwlClass}`).contains(fieldText);
  }
);
