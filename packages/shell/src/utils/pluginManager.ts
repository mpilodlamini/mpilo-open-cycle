// TODO: this is probably the most brittle part of this system

import $ from "jquery";
import pluginConstants from "./pluginConstants";
import CheckoutService from "../services/checkoutService";

import TimeUtil from "./timeUtil";
import { AJAX_DATA_RESULT, AJAX_RESULT_STATUS } from "./constants";
import { CHECKOUT } from "../contexts/CheckoutContext";
import GoogleAnalyticsUtil from "./googleAnalyticsUtil";
import { printInfo, printWarning } from "./logger";

interface TransactionStatus {
  checkout_id: string;
  checkout_status: string;

  transaction_id?: string;
  result_code?: string;
}

interface PluginManager {
  redirectToPluginSpecificUrl(
    url?: string | undefined,
    postData?: {} | undefined
  ): void;

  redirectWithPostData(url: string, postData: {}): void;

  redirectWithPostDataFormSubmission(url: string, postData: any): void;

  redirectUsingAnchorTag(returnRedirectUrl: string): void;

  createPollingListener(): void;

  stopPollingListener(): void;

  updateTransactionStatus(
    status: string,
    transactionId?: string,
    code?: string
  ): void;
}

const className: string = "PluginManager";

class PluginManager implements PluginManager {
  private readonly _checkoutService: CheckoutService = new CheckoutService();

  private readonly _timeUtil: TimeUtil | undefined;

  private readonly _googleAnalytics: GoogleAnalyticsUtil;

  private _pollingInterval: NodeJS.Timeout | undefined;

  private _pollingRequestInterval: NodeJS.Timeout | undefined;

  constructor(timeUtil?: TimeUtil) {
    this._timeUtil = timeUtil;
    this._googleAnalytics = new GoogleAnalyticsUtil();
  }

  redirectUsingAnchorTag = (returnRedirectUrl: string): void => {
    printInfo(className, "redirectUsingAnchorTag", "Redirecting via GET");

    const referLink = document.createElement("a");
    referLink.href = returnRedirectUrl;
    referLink.target = "_self";
    document.body.appendChild(referLink);
    referLink.click();
  };

  redirectToPluginSpecificUrl = (
    url?: string | undefined,
    postData?: {} | undefined
  ): void => {
    const functionName: string = "redirectToPluginSpecificUrl";

    if (CHECKOUT.isCheckoutInTest) {
      printInfo(className, functionName, "No redirect for local testing...");
      return;
    }

    printInfo(className, functionName, "Redirecting...");

    const redirectPostData = postData || CHECKOUT.redirectPostData;
    const returnRedirectUrl = url || CHECKOUT.shopperResultUrl;

    if (
      CHECKOUT.source !== pluginConstants.NAMES.SHOPIFY &&
      CHECKOUT.source !== pluginConstants.NAMES.SBTECH &&
      CHECKOUT.source !== pluginConstants.NAMES.WIX &&
      redirectPostData
    ) {
      this.redirectWithPostDataFormSubmission(
        returnRedirectUrl,
        redirectPostData
      );
    } else {
      this.redirectUsingAnchorTag(returnRedirectUrl);
    }
  };

  redirectWithPostData = (url: string, postData: {}): void => {
    const functionName: string = "redirectWithPostData";

    if (!postData) {
      printWarning(className, functionName, "No post data present");
    }

    const successCallback = (response: any) => {
      printInfo(className, functionName, "Redirecting with post");
      printInfo(className, functionName, response);
      this._checkoutService.redirect(url);
    };

    const cancelCallback = (): void => {
      printInfo(className, functionName, "Redirecting on cancel click");
      this._checkoutService.redirect(url);
    };

    this._checkoutService.postFormEncodedContent(
      url,
      postData,
      successCallback,
      cancelCallback
    );
  };

  redirectWithPostDataFormSubmission = (url: string, postData: any): void => {
    printInfo(
      className,
      "redirectWithPostDataFormSubmission",
      "Redirecting with post"
    );

    const postForm = $(`<form id = "postForm" action="${url}" method="post"/>`);

    let postFormInputs: string = "";

    Object.keys(postData).forEach((key) => {
      postFormInputs += `<input type="hidden" name="${key}" value="${postData[key]}"/>`;
    });

    postForm.append(postFormInputs);

    $("#redirectFormContainer").append(postForm);
    $("#postForm").trigger("submit");
  };

  createPollingListener = (): void => {
    printInfo(className, "createPollingListener", "Polling the back end");
    const successCallback = (result: any): void => {
      if (result.status === "expired") {
        this.stopPollingListener();
        const redirectPostData = result.redirect_post_data;
        this.redirectToPluginSpecificUrl(
          CHECKOUT.shopperResultUrl,
          redirectPostData
        );
      }
    };

    this._pollingInterval = setInterval(() => {
      if (this._timeUtil && this._timeUtil.getCheckoutTimeToExpire() === 0) {
        this._pollingRequestInterval = setInterval(() => {
          this._checkoutService.startPolling(
            CHECKOUT.baseUrl,
            CHECKOUT.checkoutId,
            successCallback
          );
          this._googleAnalytics.trackEvent(
            "timeout_modal",
            CHECKOUT.paymentBrand
          );
        }, 5000);
      }
    }, 1000);
  };

  stopPollingListener = (): void => {
    printInfo(className, "stopPollingListener", "Stop all active pollers");
    if (!this._pollingInterval) return;
    clearInterval(this._pollingInterval);
    if (!this._pollingRequestInterval) return;
    clearInterval(this._pollingRequestInterval);
  };

  updateTransactionStatus = (
    status: string,
    transactionId?: string,
    code?: string
  ): void => {
    printInfo(
      className,
      "updateTransactionStatus",
      "Update switch transaction status"
    );
    // Setup post object
    const postObject: TransactionStatus = {
      checkout_id: CHECKOUT.checkoutId,
      checkout_status: status,
    };

    if (transactionId) {
      postObject.transaction_id = transactionId;
    }

    if (code) {
      postObject.result_code = code;
    }

    const successCallback = (data: any): void => {
      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        const redirectPostData = data.response.redirect_post_data
          ? JSON.parse(data.response.redirect_post_data)
          : "";

        // Setup the redirect url
        let url = CHECKOUT.shopperResultUrl;

        if (status === AJAX_RESULT_STATUS.CANCELLED && CHECKOUT.cancelUrl) {
          url = CHECKOUT.cancelUrl;
        }

        if (data.response && data.response.redirect_url) {
          url = data.response.redirect_url;
        }

        if (status === AJAX_RESULT_STATUS.CANCELLED) {
          this.redirectToPluginSpecificUrl(url, redirectPostData);
        } else if (status === AJAX_RESULT_STATUS.SUCCESSFUL) {
          setTimeout(() => {
            this.redirectToPluginSpecificUrl(url, redirectPostData);
          }, 10000);
        }
      }
    };

    const cancelCallback = (): void => {
      printInfo(className, "cancelCallback", "Redirecting on cancel click");
      this._checkoutService.redirect(CHECKOUT.shopperResultUrl);
    };

    this._checkoutService.postFormEncodedContent(
      `${CHECKOUT.baseUrl}checkout-status`,
      postObject,
      successCallback,
      cancelCallback
    );
  };
}

export default PluginManager;
