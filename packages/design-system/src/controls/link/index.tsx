import { Button, makeStyles } from "@material-ui/core";
import React from "react";
import { text } from "../../variables";

export interface LinkProps {
  label: string;
  href: string;
}

const customStyles = makeStyles(() => ({
  button: {
    color: text,
    textTransform: "none",
    textDecoration: "underline",
  },
}));

export const Link = (props: LinkProps) => {
  const classes = customStyles();

  return (
    <Button className={classes.button} {...props}>
      {props.label}
    </Button>
  );
};
