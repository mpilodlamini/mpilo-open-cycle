Feature: A+ Successful Resend & Verify OTP
    In order to make a purchase
    As a customer
    I want to be able to pay using my A+ account

    Background: A+ Verify OTP Success Scenario Tests
        Given I Generate Checkout via API using default values
        When I click on "aplus-radio-card"

        And I switch to the iframe
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        When I Login to access my A+ account
        And it finishes "loading" in the iframe
        Then I should see the text "One Time Password sent to" in the iframe
        And I should see the text "Please enter your OTP below." in the iframe
        And I should see that the "aplus-otp-btn" button is disabled in the iframe
        And I should see "aplus-otp-timer" containing the text "Session Expires in" in the iframe

    Scenario: Enter OTP and verify to make successful payment
        Given I enter OTP "1111" and click verify
        Then I should see that the element with id "success-screen" contains the text "Return to Store" to "to.visible"

    Scenario: Resend OTP and remain on A+ otp screen
        Given I click resend otp in iframe
        When it finishes "loading" in the iframe
        Then I should see the text "One Time Password sent to" in the iframe
        And I should see the text "Please enter your OTP below." in the iframe
        And I should see "aplus-otp-timer" containing the text "Session Expires in" in the iframe
