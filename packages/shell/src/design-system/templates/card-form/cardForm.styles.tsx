import { createStyles, Theme, makeStyles } from "@material-ui/core";

import {
  border,
  info,
  white,
  orange,
  tertiaryGreen,
  secondaryGreen,
  secondaryBackgroundGrey,
} from "@peach/checkout-design-system";

const CONTENT_MAX_WIDTH: string = "547px";

export const useStyles = makeStyles(({ breakpoints }: Theme) =>
  createStyles({
    button: {
      border: "1px solid currentColor",
      backgroundColor: tertiaryGreen,
      color: white,
      textTransform: "none",
      fontSize: "14px",

      "&:hover": {
        backgroundColor: secondaryGreen,
      },
      "&.Mui-disabled, &:disabled": {
        borderColor: border,
        backgroundColor: border,
        color: white,
        cursor: "not-allowed",
      },
    },
    cvvField: {
      position: "relative",
      letterSpacing: "3.5px",
      "& .MuiFormHelperText-root": {
        letterSpacing: "initial",
      },
    },
    cvvFieldIsAmex: {
      letterSpacing: "5px",
    },
    tooltipIconContainer: {
      color: info,
      "& svg": {
        color: "currentColor",
        "& path": {
          fill: "currentColor",
        },
        "& circle": {
          stroke: "currentColor",
        },
      },
      "&:hover": {
        color: orange,
      },
    },
    tooltipPsuedoButton: {
      position: "absolute",
      top: 0,
      right: 0,
      height: "100%",
      width: "40px",
      "&:hover": {
        cursor: "pointer",
      },
    },
    paper: {
      width: "100%",
      maxWidth: CONTENT_MAX_WIDTH,
      margin: "auto",
      backgroundColor: white,
      padding: 0,
      boxShadow: "none",
      [breakpoints.up("sm")]: {
        backgroundColor: secondaryBackgroundGrey,
        padding: "inherit",
        boxShadow:
          "0px 3px 3px -2px rgba(0,0,0,0.2), 0px 3px 4px 0px rgba(0,0,0,0.14), 0px 1px 8px 0px rgba(0,0,0,0.12)",
      },
    },
    helpIcon: {
      color: info,
      "&:hover": {
        color: orange,
      },
    },
    layoutFooter: {
      maxWidth: CONTENT_MAX_WIDTH,
      margin: "auto",
    },
    textFieldContainer: {
      [breakpoints.up("sm")]: {
        minHeight: "74px",
      },
    },
    brandLogosContainer: {
      display: "none",
      [breakpoints.up("sm")]: {
        display: "block",
      },
    },
    numberFieldIsAmex: {
      "& input[name='number']": {
        width: "258px",
        "@media (min-width: 320px)": {
          width: "100%",
          letterSpacing: "1.7vw",
        },
        "@media (min-width: 360px)": {
          letterSpacing: "2.2vw",
        },
        "@media (min-width: 460px)": {
          letterSpacing: "2.9vw",
        },
        "@media (min-width: 551px)": {
          letterSpacing: "3.3vw",
        },
        [breakpoints.up("sm")]: {
          letterSpacing: "15px",
          maxWidth: "450px",
        },
        "@media (min-width: 638px)": {
          letterSpacing: "17px",
        },
      },
    },
  })
);

export default useStyles;
