/**
  Makes a pattern that react-format number will accept eg. `${"#".repeat(4)}  ${"#".repeat(4)}  ${"#".repeat(4)}  ${"#".repeat(4)}`
* @param {string} format
* @return {string} `####  ####  ####  ####`
*/
export const createFormatPattern = (format: string): string => {
  if (!format) return "";

  return format
    .split("-")
    .map((num) => new Array(parseInt(num, 10)).fill("#").join(""))
    .join("  ");
};

export default {
  amex: {
    default: {
      length: 15,
      format: createFormatPattern("4-6-5"),
    },
  },
  visa: {
    default: {
      length: 16,
      format: createFormatPattern("4-4-4-4"),
    },
    when13: {
      length: 13,
      format: createFormatPattern("4-4-4-3"),
    },
    when19: {
      length: 19,
      format: createFormatPattern("4-4-4-4-3"),
    },
  },
  dinersClub: {
    default: {
      length: 19,
      format: createFormatPattern("4-6-4-5"),
    },
    when14: {
      length: 14,
      format: createFormatPattern("4-6-4"),
    },
  },
  masterCard: {
    default: {
      length: 16,
      format: createFormatPattern("4-4-4-4"),
    },
  },
  ChinaUnionPay: {
    default: {
      length: 16,
      format: createFormatPattern("4-4-4-4"),
    },
    when19: {
      length: 19,
      format: createFormatPattern("6-13"),
    },
  },
  maestro: {
    default: {
      length: 19,
      format: createFormatPattern("4-4-4-4-3"),
    },
    when13: {
      length: 13,
      format: createFormatPattern("4-4-5"),
    },
    when15: {
      length: 15,
      format: createFormatPattern("4-6-5"),
    },
    when16: {
      length: 16,
      format: createFormatPattern("4-4-4-4"),
    },
  },
  discover: {
    default: {
      length: 16,
      format: createFormatPattern("4-4-4-4"),
    },
  },
  generic: {
    default: {
      format: createFormatPattern("4-4-4-4"),
    },
  },
};
