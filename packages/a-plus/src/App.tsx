import React from "react";
import { ConfigProvider } from "./contexts/ConfigContext";
import { APlusContainer } from "./a-plus";
import { ThemeProvider } from "@peach/checkout-design-system";

function App() {
  return (
    <ConfigProvider>
      <ThemeProvider>
        <APlusContainer />
      </ThemeProvider>
    </ConfigProvider>
  );
}

export default App;
