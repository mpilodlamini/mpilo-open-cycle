import * as CryptoJS from "crypto-js";
import {MiddlewareObject, MIDDLEWARE_CONSTANTS} from "../pages/middleware/middleware";
import {MiddlewareWixObject, MIDDLEWARE_WIX_CONSTANTS} from "../pages/middleware/middleware_wix";
import {MiddlewareSBTechObject, MIDDLEWARE_SBTECH_CONSTANTS} from "../pages/middleware/middleware_sbtech";
import {MiddlewareShopifyObject, MIDDLEWARE_SHOPIFY_CONSTANTS} from "../pages/middleware/middleware_shopify";

  Cypress.Commands.add('generateAPICheckout', (args: MiddlewareObject) => {
    
    const options = {
      secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
      "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
      amount: "10.00",
      currency: "ZAR",
      paymentType: "DB",
      shopperResultUrl: "https://httpbin.org/post",
      merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8),
      nonce: Cypress._.random(0, 1e10),
      signature: "",
      ... args,
    }
    
    //loop through all available body attributes and create an array with the populated values (not empty strings)
    const body = Object.keys(options)
      .map((key) => options[key] && `${key}${options[key]}`)
      .sort();
    //joins all the elements of the array into a single string with no comma separators
    const hash = CryptoJS.HmacSHA256(
      body.join(""), 
      args?.secretKey
    );
    //update signature parameter with signed value
    options.signature = hash.toString(CryptoJS.enc.Hex);

    cy.visit({
      url: Cypress.config().baseUrl+'/checkout',
      method: 'POST',
      body: options,
      headers: {
        Accept: "*/*",
        Referer: "34.247.96.206",
        Origin: "34.247.96.206",
      }
    });
  });

  Cypress.Commands.add('generateMiddlewareCheckout', (args: MiddlewareObject) => {
    cy.visit(Cypress.env('middlewareBaseUrl'));

    Object.keys(args).forEach((key) => {
      if (key === "predefinedMerchants") {
        cy.get(MIDDLEWARE_CONSTANTS[key]).select(args[key]);
      } else {
        cy.get(MIDDLEWARE_CONSTANTS[key]).clear().type(args[key]);
      }
    });

    if (args.merchantTransactionId === "") {
      cy.get(MIDDLEWARE_CONSTANTS.merchantTransactionId)
      .clear()
      .type("Cy-Checkout-"+ Cypress._.random(0, 1e8));
    }
    
    cy.get(MIDDLEWARE_CONSTANTS.nonce).clear().type(""+ Cypress._.random(0, 1e10));

    cy.get("form").submit();
  });

  Cypress.Commands.add('generateMiddlewareWixCheckout', (args: MiddlewareWixObject) => {
    cy.visit(Cypress.env('middlewareBaseUrl')+'qa/wix/');

    Object.keys(args).forEach((key) => {
      cy.get(MIDDLEWARE_WIX_CONSTANTS[key]).clear().type(args[key]);
    });

    if (args.wixTransactionId === "") {
      cy.get(MIDDLEWARE_WIX_CONSTANTS.wixTransactionId)
      .clear()
      .type("Cy-Checkout-"+ Cypress._.random(0, 1e8));
    } 
    
    cy.get("form").submit();
  });

  Cypress.Commands.add('generateMiddlewareSbTechCheckout', (args: MiddlewareSBTechObject) => {
    cy.visit(Cypress.env('middlewareBaseUrl')+'sbtech/');

    Object.keys(args).forEach((key) => {
      cy.get(MIDDLEWARE_SBTECH_CONSTANTS[key]).clear().type(args[key]);
    });

    if (args.merchantTransactionId === "") {
      cy.get(MIDDLEWARE_SBTECH_CONSTANTS.merchantTransactionId)
      .clear()
      .type("Cy-Checkout-"+ Cypress._.random(0, 1e8));
    }

    if (args.notificationUrl === "") {
      cy.get(MIDDLEWARE_SBTECH_CONSTANTS.notificationUrl)
      .clear()
      .type("https://httpbin.org/get");
    }
    
    cy.get(MIDDLEWARE_SBTECH_CONSTANTS.nonce).clear().type(""+ Cypress._.random(0, 1e10));
    
    cy.get("form").submit();
  });

  Cypress.Commands.add('generateMiddlewareShopifyCheckout', (args: MiddlewareShopifyObject) => {
    cy.visit(Cypress.env('middlewareBaseUrl')+'shopify/');

    Object.keys(args).forEach((key) => {
      cy.get(MIDDLEWARE_SHOPIFY_CONSTANTS[key]).clear().type(args[key]);
    });

    if (args.reference === "") {
      cy.get(MIDDLEWARE_SHOPIFY_CONSTANTS.reference)
      .clear()
      .type("Cy-Checkout-"+ Cypress._.random(0, 1e8));
    }

    if (args.test === "") {
      cy.get(MIDDLEWARE_SHOPIFY_CONSTANTS.test)
      .clear()
      .type(""+ Cypress._.random(0, 1e5));
    }
    
    if (args.urlCallback === "") {
      cy.get(MIDDLEWARE_SHOPIFY_CONSTANTS.urlCallback)
      .clear()
      .type("https://httpbin.org/get");
    }

    cy.get("form").submit();
  });

  Cypress.Commands.add('getElementById', (Id: string, options = {}) => {
    return cy.get(`[id="${Id}"]`, options);
  });

  Cypress.Commands.add('getElementByDataTestId', (dataTestId: string, options = {}) => {
    return cy.get(`[data-testid="${dataTestId}"]`, options);
  });

  Cypress.Commands.add('getIFrameElementByDataTestId', (dataTestId: string, options = {}) => {
    return cy.iframe().find(`[data-testid="${dataTestId}"]`, options);
  });
