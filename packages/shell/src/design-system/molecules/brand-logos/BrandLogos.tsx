import React, { useContext, useMemo } from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";

import { CARD } from "../../../utils/constants";
import useStyles from "./brandLogos.styles";
import { CheckoutContext } from "../../../contexts/CheckoutContext";

export interface BrandLogosPropInterface {
  readonly selectedBrand?: string;
  readonly cardBrands?: Array<string>;
}

/**
 * Generates the brand image based on the provided selected brand
 * @param selectedBrand
 * @param classes
 */
const generateBrandImage = (
  selectedBrand: string,
  classes: any,
  images: any
): React.ReactNode => {
  switch (selectedBrand) {
    case CARD.ENUM.AMERICAN_EXPRESS:
      return (
        <img className={classes.amexIcon} src={images.amexSrcUrl} alt="Amex" />
      );
    case CARD.ENUM.VISA:
      return (
        <img className={classes.visaIcon} src={images.visaSrcUrl} alt="Visa" />
      );
    case CARD.ENUM.MASTERCARD:
      return (
        <img
          className={classes.masterCardIcon}
          src={images.masterCardSrcUrl}
          alt="Master Card"
        />
      );
    case CARD.ENUM.DINERS_CLUB:
      return (
        <img
          className={classes.dinersClubIcon}
          src={images.dinersClubSrcUrl}
          alt="Diners Club"
        />
      );
    default:
      return null;
  }
};

const displayBrandLogos = (
  cardBrands: Array<string> | undefined,
  classes: any,
  images: any
): React.ReactNode => {
  if (!cardBrands) {
    return <></>;
  }

  return (
    <>
      {cardBrands.includes(CARD.ENUM.AMERICAN_EXPRESS) && (
        <img className={classes.amexIcon} src={images.amexSrcUrl} alt="Amex" />
      )}

      {cardBrands.includes(CARD.ENUM.VISA) && (
        <img className={classes.visaIcon} src={images.visaSrcUrl} alt="Visa" />
      )}

      {cardBrands.includes(CARD.ENUM.MASTERCARD) && (
        <img
          className={classes.masterCardIcon}
          src={images.masterCardSrcUrl}
          alt="Master Card"
        />
      )}

      {cardBrands.includes(CARD.ENUM.DINERS_CLUB) && (
        <img
          className={classes.dinersClubIcon}
          src={images.dinersClubSrcUrl}
          alt="Diners Club"
        />
      )}
    </>
  );
};

export const BrandLogos = (props: BrandLogosPropInterface) => {
  const config = useContext(CheckoutContext);
  const classes = useStyles();
  const theme = useTheme();
  const isXsBp = useMediaQuery(theme.breakpoints.down("xs"));

  const images = useMemo(() => {
    return {
      amexSrcUrl: `${config.assetsBasePath}images/AmexAlt.svg`,
      visaSrcUrl: `${config.assetsBasePath}images/VisaAlt.svg`,
      masterCardSrcUrl: `${config.assetsBasePath}images/Mastercard.svg`,
      dinersClubSrcUrl: `${config.assetsBasePath}images/diners-club-icon.svg`,
    };
  }, [config]);

  return (
    <Box position="relative">
      <Box
        position="absolute"
        maxWidth="152px"
        top={isXsBp ? "-20px" : "16px"}
        zIndex={100}
        right={isXsBp ? "0" : "32px"}
      >
        <Box display="flex" alignItems="center" justifyContent="center">
          {!props.selectedBrand || props.selectedBrand === "All Cards" ? (
            <>{displayBrandLogos(props.cardBrands, classes, images)}</>
          ) : (
            <>{generateBrandImage(props.selectedBrand, classes, images)}</>
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default BrandLogos;
