import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import { Total, TotalProps } from "../../../";

export default {
  title: "Controls/Total",
} as Meta;

export const Default: Story<TotalProps> = (props) => {
  return <Total {...props} />;
};

Default.args = {
  currency: "ZAR",
  total: "100.00",
};
