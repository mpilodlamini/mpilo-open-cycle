/*global ApplePaySession, a*/
// TODO: talk to apple about the a11y of their button
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
// @ts-ignore
import ApplePayJS from "applepayjs";
import axios from "axios";
import React, {
  HTMLAttributes,
  useCallback,
  useContext,
  useState,
} from "react";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";
import { SelectedPaymentContext } from "../../../../contexts/SelectedPaymentContext";
import { StatusContext } from "../../../../contexts/StatusContext";
import "../../../../scss/applePay.scss";
import {
  AJAX_DATA_RESULT,
  CARD,
  PEACH_STATE,
} from "../../../../utils/constants";
import "./applePay.styles.css";
import { PaymentsResponse } from "./response";

const isValidSuccessCode = new RegExp(
  "^(000.000.|000.100.1|000.[36])|^(000.400.0[^3]|000.400.100)"
);

export interface ApplePayButtonProps extends HTMLAttributes<HTMLDivElement> {}

export const ApplePayButton = ({}: ApplePayButtonProps) => {
  const config = useContext(CheckoutContext);
  const status = useContext(StatusContext);
  const payment = useContext(SelectedPaymentContext);
  const [_, setSession] = useState<ApplePaySession>();

  const createSession = useCallback(() => {
    // eslint-disable-next-line no-undef
    const session = new ApplePaySession(10, {
      countryCode: config.billingCountry || "ZA",
      currencyCode: config.merchantCurrencyCode,
      supportedNetworks: config.paymentMethods.reduce(
        (support: string[], method) => {
          /**
           * Maps the values for the card names to Apple Pay specific values
           * https://developer.apple.com/documentation/apple_pay_on_the_web/applepaypaymentrequest/1916122-supportednetworks
           */
          switch (method.toUpperCase().replace(/\s/g, "")) {
            case CARD.ENUM.VISA:
              support.push("visa");
              break;
            case CARD.ENUM.MASTERCARD:
              support.push("masterCard");
              break;
            case CARD.ENUM.AMERICAN_EXPRESS:
              support.push("amex");
              break;
            default:
              break;
          }
          return support;
        },
        []
      ),
      merchantCapabilities: ["supports3DS"],
      total: {
        label: config.merchant,
        amount: config.amount,
      },
    });

    session.onvalidatemerchant = () => {
      (async () => {
        try {
          const response = await axios.post(
            `${config.baseUrl}apple-pay-validation`,
            new URLSearchParams({
              checkoutId: config.checkoutId,
            })
          );

          if (response.data?.status === AJAX_DATA_RESULT.SUCCESS) {
            session?.completeMerchantValidation(response.data.response);
          }
        } catch (e) {
          status.set("error");
        }
      })();
    };

    session.onpaymentauthorized = (
      event: ApplePayJS.ApplePayPaymentAuthorizedEvent
    ) => {
      (async () => {
        try {
          const response = await axios.post<PaymentsResponse>(
            `${config.baseUrl}payment`,
            new URLSearchParams({
              checkout_id: config.checkoutId,
              payment_brand: "APPLEPAY",
              paymentType: "DB",
              token: JSON.stringify(event?.payment?.token || {}),
            })
          );

          if (response.data.status !== AJAX_DATA_RESULT.SUCCESS) {
            throw new Error(`Payment failed ${response.data.message}`);
          }

          const code: string =
            response.data.response?.response_data.result.code || "";

          if (isValidSuccessCode.test(code)) {
            session.completePayment({ status: 0 });
            status.set(PEACH_STATE.SUCCESS as any, {
              data: response.data.response?.redirect_post_data,
              url: response.data.response?.redirect_url,
            });

            payment.select({ id: "APPLEPAY", name: "APPLEPAY", label: "Pay" });
          } else {
            session.completePayment({ status: 1 });
            status.set(PEACH_STATE.FAILED as any, {
              code,
              data: { paymentBrand: "ApplePay" },
            });
          }
        } catch (e) {
          session.completePayment({ status: 1 });
          status.set(PEACH_STATE.FAILED as any, {
            data: { ...e, paymentBrand: "ApplePay" },
          });
        }
      })();
    };

    session.begin();

    setSession(session);
  }, [config]);

  return (
    <div
      className="apple-pay-button apple-pay-button-black"
      onClick={createSession}
      title="Pay with Apple Pay"
    />
  );
};
