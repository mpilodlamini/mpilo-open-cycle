import React from "react";
import { createContext, ReactNode, useEffect, useState } from "react";
import { checkout, checkoutOrigin } from "../lib/checkout";

export type CheckoutConfig = any;

export const ConfigContext = createContext<CheckoutConfig>({});

export const ConfigProvider = ({ children }: { children: ReactNode }) => {
  const [loading, setLoading] = useState(true);
  const [config, setConfig] = useState<CheckoutConfig>({});

  useEffect(() => {
    checkout.publish([{ type: "status", value: "loading" }]);
    const listener = (e: MessageEvent<{ config: any; message: string }>) => {
      if (e.origin === checkoutOrigin && e.data.message === "checkout-config") {
        setConfig(e.data.config);
        setLoading(false);
        checkout.publish([{ type: "status", value: undefined }]);
      }
    };
    window.addEventListener("message", listener);
    return () => {
      window.removeEventListener("message", listener);
    };
  }, [setLoading, setConfig]);

  if (loading) {
    return null;
  }

  return (
    <ConfigContext.Provider value={config}>{children}</ConfigContext.Provider>
  );
};
