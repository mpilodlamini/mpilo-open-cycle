import { makeStyles } from "@material-ui/core";
import { white } from "@peach/checkout-design-system/dist/variables";

export const useStyles = makeStyles((theme) => ({
  button: {
    textTransform: "capitalize",
    minWidth: "200px",
    justifyContent: "flex-start",
    padding: "0",

    [theme.breakpoints.down("xs")]: {
      textAlign: "left",
      paddingLeft: "0",
    },
  },
  screen: {
    backgroundColor: white,
    borderTopLeftRadius: "8px",
    borderTopRightRadius: "8px",
    boxShadow: "0 -2px 4px rgba(0,0,0,0.12)",
    zIndex: 999,
    position: "absolute",
    bottom: "0",

    [theme.breakpoints.down("xs")]: {
      paddingLeft: "16px",
      paddingRight: "16px",
    },
  },
  scroll: {
    height: "calc((100vh - 10vh) - (72px + 25px + 16px + 16px + 10px))",
    overflowY: "auto",
    maxWidth: "100%",
    [theme.breakpoints.up("sm")]: {
      height: "calc((100vh - 10vh) - (85px + 35px + 16px + 16px + 10px))",
    },
  },
  active: {
    "& .expandIcon": {
      transform: "rotate(180deg)",
      transition: "all 0.8s ease",
    },
  },
  inactive: {
    "& .expandIcon": {
      transition: "all 0.8s ease",
    },
  },
}));
