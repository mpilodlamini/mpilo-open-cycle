Feature: Checkout Payment Methods Sanity Test
    @Smoke
    Scenario Outline: Check if payment methods are available and working
        Given I Generate Checkout via API using default values
        When I click on "<paymentMethod>"
        And it finishes "loading"
        Then I should see the text "<paymentTypeText>"
        Examples:
            | paymentMethod         | paymentTypeText                                                             | 
            | card-btn              | Please enter your card details below to complete the payment.               | 
            | eft-secure-btn        | Please select your bank from the options below                              | 
            | masterpass-btn        | Go on to the Masterpass App on your phone and enter or scan the code below: | 
            | mobicred-btn          | Please sign in to your Mobicred account.                                    | 


    Scenario Outline: Check if Plugin payment methods are available and working
        Given I Generate Checkout via API using default values
        When I click on "<paymentMethod>"
        And I switch to the iframe
        Then I should see the text "<paymentTypeText>" in the iframe
        Examples:
            | paymentMethod         | paymentTypeText                                                             |  
            | 1ForYou-radio-card    | Please enter your 16 digit Voucher Pin                                      |
            | aplus-radio-card      | Please enter your A+ Account card details.                                  |