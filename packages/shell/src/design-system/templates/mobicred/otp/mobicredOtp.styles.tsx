import { withStyles, TextField, makeStyles } from "@material-ui/core";
import { border, info, text, white } from "@peach/checkout-design-system";

export const CssTextField = withStyles({
  root: {
    "& .MuiOutlinedInput-root": {
      fontSize: "15px",
      paddingLeft: "calc(48% - 60px)",
      paddingRight: "calc(48% - 60px)",

      "@media (max-width: 420px)": {
        paddingLeft: "calc(48% - 67px)",
        paddingRight: "calc(48% - 67px)",
      },

      "& fieldset": {
        borderColor: border,
        color: "#53627C",
      },
      "&:hover fieldset": {
        borderColor: info,
      },
      "&.Mui-focused fieldset": {
        borderColor: info,
      },
    },
  },
})(TextField);

export const useStyles = makeStyles(() => ({
  label: {
    fontSize: "14px",
    fontWeight: 600,
    color: "#53627C",
    marginBottom: "8px",
  },
  link: {
    color: text,
    textTransform: "none",
    textDecoration: "underline",
    fontSize: "12px",
    fontWeight: 600,
  },
  resend: {
    color: text,
    padding: "0 4px",
    marginTop: "-1px",
    textDecoration: "none",
    fontWeight: 600,
    textTransform: "none",
    fontSize: "12px",

    "&:hover": {
      background: "transparent",
    },
  },
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },
    "&:disabled": {
      borderColor: border,
    },
  },
}));
