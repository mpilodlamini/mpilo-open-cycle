import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  button: {
    textDecoration: "underline",
    textTransform: "capitalize",
    width: "100%",
  },
  active: {
    "& .expandIcon": {
      transform: "rotate(180deg)",
      transition: "all 0.8s ease",
    },
  },
  inactive: {
    "& .expandIcon": {
      transition: "all 0.8s ease",
    },
  },
  expand: {
    minHeight: "0",
    maxHeight: "80px",
    overflow: "auto",
  },
}));

export default useStyles;
