data "aws_eks_cluster" "cluster" {
  name = var.eks_cluster_name
}

data "aws_eks_cluster_auth" "cluster" {
  name = var.eks_cluster_name
}


# data "aws_efs_file_system" "efs" {
#   file_system_id = var.file_system_id
# }

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

resource "kubernetes_deployment" "this" {
  metadata {
    name = "${var.app}-${var.env}"
    labels = {
      name = "${var.app}-${var.env}"
      app = var.app
    }
  }

  spec {
    replicas = var.replicas_amount
    selector {
      match_labels = {
        name = "${var.app}-${var.env}"
        app = var.app
      }
    }
    template {
      metadata {
        labels = {
          name = "${var.app}-${var.env}"
          app = var.app
        }
      }
      spec {
        container {
          image             = var.container_image
          name              = "${var.app}-${var.env}-container"
          image_pull_policy = var.container_image_pull_policy

          port {
            container_port = var.container_port
          }

          resources {
            limits = {
              cpu    = var.cpu_limit
              memory = var.memory_limit
            }
            requests = {
              cpu    = var.resource_request_cpu
              memory = var.resource_request_memory
            }
          }

          # volume_mount {
          #   name       = "persistent-storage"
          #   mount_path = "/data"
          #   sub_path   = "${var.app}-${var.env}-data"
          # }

          dynamic "env" {
            for_each = var.env_vars
            content {
              name  = env.value["name"]
              value = env.value["value"]
            }
          }
        }
        # volume {
        #   name = "persistent-storage"
        #   persistent_volume_claim {
        #     claim_name = local.volume_claim_name
        #   }
        # }
      }
    }
  }
}

resource "kubernetes_service" "this" {
  metadata {
    name = "${var.app}-${var.env}"
    annotations = {
      "service.beta.kubernetes.io/aws-load-balancer-ssl-cert" = var.acm_cert_arn
      "service.beta.kubernetes.io/aws-load-balancer-ssl-ports" = "https"
      "service.beta.kubernetes.io/aws-load-balancer-backend-protocol" = "http"
    }
  }
  spec {
    selector = {
      name = "${var.app}-${var.env}"
    }
    type = "LoadBalancer"
    port {
      name        = "https"
      port        = 443
      target_port = var.container_port
    }
    port {
      name        = "http"
      port        = 80
      target_port = var.container_port
    }
  }
}

# resource "kubernetes_persistent_volume" "this" {
#   metadata {
#     name = local.volume_name
#     labels = {
#       name = local.volume_name
#     }
#   }
#   spec {
#     capacity = {
#       storage = var.volume_size
#     }
#     access_modes       = ["ReadWriteMany"]
#     storage_class_name = "efs-sc"
#     persistent_volume_source {
#       csi {
#         driver        = "efs.csi.aws.com"
#         volume_handle = var.file_system_id
#       }
#     }
#   }
# }

# resource "kubernetes_persistent_volume_claim" "this" {
#   metadata {
#     name = local.volume_claim_name
#   }
#   wait_until_bound = true
#   spec {
#     access_modes       = ["ReadWriteMany"]
#     storage_class_name = "efs-sc"
#     resources {
#       requests = {
#         storage = var.volume_size
#       }

#     }
#     selector {
#       match_labels = {
#         name = local.volume_name
#       }
#     }

#     volume_name = kubernetes_persistent_volume.this.metadata.0.name
#   }
# }
