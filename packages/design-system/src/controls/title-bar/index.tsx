import React from "react";
import { Box, makeStyles } from "@material-ui/core";
import { bold } from "../../variables";

export interface TitleProps {
  title: string;
}

const useStyles = makeStyles((theme) => ({
  pageTitle: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px",
    },
  },
}));

export const TitleBar = (props: TitleProps) => {
  const classes = useStyles();

  return (
    <Box component="span" className={classes.pageTitle} fontWeight={bold}>
      {props.title}
    </Box>
  );
};
