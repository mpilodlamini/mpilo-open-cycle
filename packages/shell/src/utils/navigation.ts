/* eslint-disable */
import constants from "./constants";

import GoogleAnalyticsUtil from "./googleAnalyticsUtil";
import { CHECKOUT_GA_INFORMATION } from "./pluginConstants";
import { GA } from "./googleAnalyticsConstants";
import { printInfo } from "./logger";

const className: string = "NavigationUtil";
const message: string =
  "This page that you're looking for used information that you entered. Returning to that page might cause any action that you took to be repeated. Do you want to continue?";
let isBackNavigationEventAdded: boolean = false;

export const setupBackNavigation = (
  googleAnalytics: GoogleAnalyticsUtil
): void => {
  printInfo(
    className,
    "setupBackNavigation",
    "Attaching event listeners for back navigation on browsers"
  );

  if (isBackNavigationEventAdded) return;
  if (window.history && history.pushState) {
    addEventListener(constants.JAVASCRIPT_EVENTS.LOAD, () => {
      history.pushState(null, "Back Navigation", null); // creates new history entry with same URL
      addEventListener(constants.JAVASCRIPT_EVENTS.POPSTATE, () => {
        const shouldStayOnPage = confirm(message);

        if (shouldStayOnPage) {
          googleAnalytics.trackEvent(
            GA.EVENTS.CANCEL_BROWSER_BACK,
            CHECKOUT_GA_INFORMATION.NAME
          );
        } else {
          googleAnalytics.trackEvent(
            GA.EVENTS.SUCCESS_BROWSER_BACK,
            CHECKOUT_GA_INFORMATION.NAME
          );

          history.pushState(null, "Back Navigation", null);
        }
      });
    });
  }

  isBackNavigationEventAdded = true;
};
