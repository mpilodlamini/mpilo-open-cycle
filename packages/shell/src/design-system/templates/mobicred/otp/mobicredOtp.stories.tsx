import React from "react";
import TimeUtil from "../../../../utils/timeUtil";
import MobicredOtp from "./mobicredOtp";

export default {
  title: "Payments / Mobicred OTP",
  parameters: {
    component: MobicredOtp,
  },
};

export const otp = (args: any) => <MobicredOtp {...args} />;

otp.args = {
  timeUtil: new TimeUtil(),
};
