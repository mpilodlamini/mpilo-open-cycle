import { Box } from "@material-ui/core";
import React from "react";
import {
  FeedbackIcon,
  FeedbackMessage,
  FeedbackType,
  IconSize,
} from "../feedback";
import { GoBackButton, PrimaryButton, TitleBar } from "../../controls";
import useStyles from "./index.styles";

export type DialogProps = {
  onClick?: () => void;
  continueText?: string;
  onCancel?: () => void;
  cancelText?: string;

  title: string;
  label?: React.ReactNode;
  type: FeedbackType;

  children?: React.ReactNode;

  forceBottom?: boolean;

  errorMessages?: string[];
};

export const Dialog = (props: DialogProps) => {
  const classes = useStyles();

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="flex-start"
      className={classes.wrapper}
      data-testid={props.type}
    >
      <Box alignSelf="flex-start" width={1} mb={3}>
        <FeedbackIcon size={IconSize.LARGE} feedback={props.type} />
      </Box>

      <Box textAlign="center" width={1} mb={2}>
        <TitleBar title={props.title} />
      </Box>

      {props.label && (
        <Box
          className={classes.message}
          fontSize="14px"
          textAlign="center"
          alignSelf="center"
        >
          {props.label}
        </Box>
      )}

      {props.children}

      {props.errorMessages && props.errorMessages.length > 0 && (
        <Box width={1} px={3}>
          <FeedbackMessage messages={props.errorMessages} />
        </Box>
      )}

      {(props.forceBottom ?? false) && <Box className={classes.anchor} />}
      <Box
        width={1}
        className={props.forceBottom ? classes.bottom : classes.modal}
      >
        {props.onClick && props.continueText !== "" && (
          <Box display="flex" justifyContent="center">
            <PrimaryButton
              onClick={props.onClick}
              label={props.continueText ?? "Continue"}
              fullWidth={!props.onCancel}
            />
          </Box>
        )}
        {props.onCancel && props.cancelText !== "" && (
          <Box display="flex" justifyContent="center" mt={1} width={1} mb={3}>
            <GoBackButton onClick={props.onCancel} label={props.cancelText} />
          </Box>
        )}
      </Box>
    </Box>
  );
};
