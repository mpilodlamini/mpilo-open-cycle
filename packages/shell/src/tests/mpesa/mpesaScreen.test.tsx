import React from "react";
import renderer from "react-test-renderer";
import { fireEvent, render, screen } from "@testing-library/react";

import Mpesa from "../../design-system/templates/mpesa/mpesa";

const { getByText, getByPlaceholderText } = screen;

describe("Tests for mpesa component", () => {
  it("Renders the correct elements", () => {
    const onClick = jest.fn();
    render(<Mpesa onClick={onClick} />);
    const verifyButton = document.querySelector("button.MuiButtonBase-root");
    expect(
      document.querySelector('img[src="/images/m-pesa.svg"]')
    ).toBeInTheDocument();
    expect(verifyButton).toBeDisabled();
    expect(verifyButton?.textContent).toEqual("Verify");
    expect(
      getByText(
        "To begin your payment please enter the mobile number linked with your Mpesa account"
      )
    ).toBeInTheDocument();
    expect(getByText("Mobile Number")).toBeInTheDocument();
    expect(document.querySelector("#Mpesa")).toBeInTheDocument();
  });

  it("Toggles Verify button enabled state when a valid or invalid number is entered on input field", () => {
    const onClick = jest.fn();
    render(<Mpesa onClick={onClick} />);
    const verifyButton = document.querySelector("button.MuiButtonBase-root");
    const mpesaTextInput = getByPlaceholderText(/\+254/);

    expect(verifyButton).toBeDisabled();
    fireEvent.change(mpesaTextInput, { target: { value: "136" } });
    expect(verifyButton).toBeDisabled();
    fireEvent.change(mpesaTextInput, { target: { value: "1234567890" } });
    expect(verifyButton).toBeEnabled();
  });
});
