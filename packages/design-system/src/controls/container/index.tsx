import React from "react";
import MuiContainer, { ContainerProps } from "@material-ui/core/Container";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Box, { BoxProps } from "@material-ui/core/Box";

export type Props = ContainerProps & { banner?: boolean };

const useStyles = makeStyles((theme) => ({
  wrapper: {
    minHeight: "100vh",

    [theme.breakpoints.down("xs")]: {
      alignItems: "flex-start",
    },
  },
  container: {
    borderRadius: "8px",
    height: "calc(100vh - 10vh)",
    minHeight: "calc(100vh - 10vh)",
    overflow: "hidden",
    boxShadow: "0px 0px 20px 0px rgba(10,31,68,0.1)",
    maxWidth: "764px",
    position: "relative",

    [theme.breakpoints.down("xs")]: {
      height: "calc(100vh - 10vh)",
      minHeight: "calc(100vh - 10vh)",
    },
  },
  bannerAdjustment: {
    height: "calc(100vh - (10vh + 34px))",
    minHeight: "calc(100vh - (10vh + 34px))",
  },
}));

export const Container = ({ className, banner, ...props }: Props) => {
  const styles = useStyles();
  return (
    <MuiContainer
      {...props}
      className={clsx(className, styles.container, {
        [styles.bannerAdjustment]: banner,
      })}
      disableGutters
      maxWidth="lg"
    />
  );
};

export const Wrapper = ({ className, ...props }: BoxProps) => {
  const styles = useStyles();
  return (
    <Box
      {...props}
      display="flex"
      alignItems="center"
      className={clsx(styles.wrapper, className)}
    />
  );
};
