import { Box, Container, ContainerProps } from "@material-ui/core";
import { FeedbackType, Header } from "@peach/checkout-design-system";
import classnames from "classnames";
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { CheckoutContext } from "../contexts/CheckoutContext";
import { GoogleAnalyticsContext } from "../contexts/GoogleAnalyticsContext";
import { SelectedPaymentContext } from "../contexts/SelectedPaymentContext";
import { StatusContext } from "../contexts/StatusContext";
import { UtilContext } from "../contexts/UtilContext";
import FeedbackModal from "../design-system/organisims/feedback/modal/FeedbackModal";
import { PEACH_STATE } from "../utils/constants";
import { GA } from "../utils/googleAnalyticsConstants";
import MessagesUtils from "../utils/messagesUtil";
import { MESSAGING } from "../utils/messagingConstants";
import { useStyles } from "./checkout.styles";
import { PaymentMethodSelector } from "./payment-method-selector";
import { SelectedPaymentMethod } from "./selected-payment-method";

export type CheckoutProps = ContainerProps & {};

export const Checkout = ({ className, children, ...props }: CheckoutProps) => {
  const styles = useStyles();
  const config = useContext(CheckoutContext);
  const status = useContext(StatusContext);
  const ga = useContext(GoogleAnalyticsContext);
  const { timeUtil, pluginManager } = useContext(UtilContext);
  const selectedMethod = useContext(SelectedPaymentContext);

  useEffect(() => {
    timeUtil.startCheckoutExpireTimer(() => {
      setLoadingModal({
        feedbackTitle: "Checkout Session Expired",
        message: `We're taking you back to ${config.merchant}`,
      });
      setShowLoadingModal(true);

      setTimeout(() => {
        ga.trackEvent(
          "timeout_modal",
          selectedMethod?.selected?.name as string
        );
        pluginManager.redirectToPluginSpecificUrl();
      }, 3000);
    });
  }, []);

  const [loadingModal, setLoadingModal] = useState({
    feedbackTitle: `Redirecting to ${config.merchant}`,
    message: `We're taking you back to ${config.merchant}`,
  });

  const errorMessages = useMemo(
    () =>
      new MessagesUtils(
        config.amount,
        config.merchantCurrencyCode,
        config.merchant
      ).getErrorMessages(),
    [config]
  );

  const [showErrorModal, setShowErrorModal] = useState(
    config.status === PEACH_STATE.FAILED
  );

  const [showLoadingModal, setShowLoadingModal] = useState(false);

  const goBackFromError = useCallback(() => {
    const paymentBrand = config.paymentBrand || status.data?.data?.paymentBrand;

    status.set(undefined);
    setShowErrorModal(false);
    if (!config.forceDefaultMethod && config.defaultPaymentMethod) {
      ga.trackEvent(GA.EVENTS.COLLAPSE, paymentBrand);
    }
    ga.trackEvent(GA.EVENTS.ERROR_GO_BACK, paymentBrand);
  }, []);

  return (
    <Box display="flex" alignItems="center" className={styles.wrapper}>
      <Container
        {...props}
        disableGutters
        className={classnames(styles.container, className)}
        maxWidth="lg"
      >
        <Header
          alt={config.merchant}
          src={config.merchantLogo}
          currency={config.merchantCurrencyCode}
          total={config.amount}
        />

        {!selectedMethod.selected && <PaymentMethodSelector />}
        <SelectedPaymentMethod />

        {/* TODO: as is, this is awful, we need to decompose FeedbackModal and properly document this funky redirect nonsnse */}
        <FeedbackModal
          isOpen={status.status === PEACH_STATE.FAILED || showErrorModal}
          feedback={FeedbackType.WARNING}
          feedbackTitle={
            (errorMessages[config.code || status.data.code!] &&
              errorMessages[config.code || status.data.code!].heading) ||
            MESSAGING.DEFAULT_ERROR_HEADING
          }
          message={
            errorMessages[config.code || status.data.code!] &&
            errorMessages[config.code || status.data.code!].message
              ? errorMessages[config.code || status.data.code!].message
              : MESSAGING.DEFAULT_ERROR_MESSAGE
          }
          buttonAction=""
          text="Go Back"
          clickCallbackGoBack={goBackFromError}
          fullWidthButton={true}
        />

        <FeedbackModal
          isOpen={showLoadingModal}
          feedback={FeedbackType.LOADING}
          buttonAction=""
          text=""
          {...loadingModal}
        />
      </Container>
    </Box>
  );
};
