/* eslint-disable */
// @ts-nocheck

import $ from "jquery";
export const version = "v1.0.0";

const GLOBAL_CHECKOUT_OBJECT = window.GLOBAL_CHECKOUT_OBJECT || {};
let globalTransactionId;

function addLogs(data) {
  $.ajax({
    url: GLOBAL_CHECKOUT_OBJECT.baseUrl + "log-event",
    type: "POST",
    data: prepareDataForLogs(data),
    contentType: "application/x-www-form-urlencoded",
    success: function (result) {
      console.log("logs added");
    },
    error: function (data, jqXhr, textStatus, errorThrown) {
      console.log("error in logs");
    },
  });
}

function prepareDataForLogs(data) {
  const logData = {};
  const bankData = {};

  logData["checkout_id"] = GLOBAL_CHECKOUT_OBJECT.checkoutId;
  logData["payment_brand"] = "OZOW";
  logData["transaction_id"] = globalTransactionId;
  bankData["modalType"] = data.modalType;
  bankData["userBank"] = data.userBank;
  bankData["title"] = data.title;
  bankData["text"] = data.text;
  logData.log_data = JSON.stringify(bankData);
  return logData;
}

function showErrorPopUpWithLogo(ozowData) {
  var banks = ["absa", "capitec", "standard", "investec", "fnb", "nedbank"];
  if (
    ozowData.text.charAt(0) === "*" ||
    ozowData.text.charAt(0) === "x" ||
    /\r|\n/.exec(ozowData.text.substr(0, 2))
  ) {
    ozowData.text = ozowData.text.substr(1);
  }

  $("#global-error-model").removeClass("d-none");
  $("#error-message").html(ozowData.text);
  $("#error-heading").text(ozowData.title);

  if (!$("#action-button").hasClass("d-none")) {
    $("#action-button").on("click", (event) => {
      event.preventDefault();
      $("#global-error-model").addClass("d-none");
    });
  }

  var img = $("#error-logo");
  var index = banks.indexOf(ozowData.userBank.toLowerCase());
  if (index > -1) {
    img.attr(
      "src",
      GLOBAL_CHECKOUT_OBJECT.assetsBasePath + "images/" + banks[index] + ".png"
    );
  } else if (ozowData.userBank === "Capitec Bank") {
    img.attr(
      "src",
      GLOBAL_CHECKOUT_OBJECT.assetsBasePath + "images/" + "capitec" + ".png"
    );
  } else if (ozowData.userBank === "Standard Bank") {
    img.attr(
      "src",
      GLOBAL_CHECKOUT_OBJECT.assetsBasePath + "images/" + "standard" + ".png"
    );
  } else {
    img.attr(
      "src",
      GLOBAL_CHECKOUT_OBJECT.assetsBasePath +
        "images/" +
        "icon/warning-icon.png"
    );
  }

  //Hide Go Back button on error popun in case of preventUserClose is true
  console.log("preventUserClose ---->>> ", ozowData.preventUserClose);
  if (ozowData.preventUserClose) {
    $("#global-error-go-back").addClass("d-none");
  } else {
    $("#global-error-go-back").removeClass("d-none");

    $("#go-back-anchor").on("click", function (event) {
      event.preventDefault();
      $("#global-error-model").addClass("d-none");
    });
  }
}

var Ozow = (function (window, document, undefined) {
  var eventMethod = window.addEventListener
    ? "addEventListener"
    : "attachEvent";
  var eventer = window[eventMethod];
  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
  var redirectPrefix = "/payment/iframeredirect?redirecturl=";
  var paymentErrorMessage =
    "Payment could not be completed, please contact the site administrator";

  function ozow() {
    var self = this;
    (self.createPaymentFrame = function (
      elementSelector,
      paymentUrl,
      postData,
      transactionId
    ) {
      if (
        !elementSelector ||
        elementSelector == "" ||
        elementSelector.length == 0
      ) {
        console.log(
          "The createPaymentFrame method parameter elementSelector cannot be empty or null."
        );
        alert(paymentErrorMessage);
        return;
      }

      if (!paymentUrl || paymentUrl == "" || paymentUrl.length == 0) {
        console.log(
          "The createPaymentFrame method parameter paymentUrl cannot be empty or null."
        );
        alert(paymentErrorMessage);
        return;
      }

      if (document.getElementById("paymentFrame")) {
        var iframe = document.getElementById("paymentFrame");
        iframe.parentNode.removeChild(iframe);
      }

      globalTransactionId = transactionId;

      if (typeof postData === "object") {
        var paymentData = setRedirectPrefix(postData);
        var params = Object.keys(postData)
          .map(function (k) {
            return (
              encodeURIComponent(k) + "=" + encodeURIComponent(postData[k])
            );
          })
          .join("&");
        var postUrl = paymentUrl + "?viewName=JsInjection&" + params;
        var iframe = document.createElement("iframe");
        iframe.setAttribute("id", "paymentFrame");
        iframe.setAttribute("src", postUrl);
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("scrolling", "no");
        iframe.setAttribute("height", "100%");
        iframe.setAttribute("width", "100%");
        iframe.style.overflow = "hidden";
        iframe.style.height = "100%";
        iframe.style.width = "100%";
        document.getElementById(elementSelector).appendChild(iframe);
      } else {
        console.log(
          "The createPaymentFrame method expects a JSON object for the postData parameter."
        );
        alert(paymentErrorMessage);
      }
    }),
      (self.createPaymentModal = function (paymentUrl, postData) {
        if (!paymentUrl || paymentUrl == "" || paymentUrl.length == 0) {
          console.log(
            "The createPaymentModal method parameter paymentUrl cannot be empty or null."
          );
          alert(paymentErrorMessage);
          return;
        }

        if (document.getElementById("paymentFrame")) {
          var iframe = document.getElementById("paymentFrame");
          iframe.parentNode.removeChild(iframe);
        }

        if (typeof postData === "object") {
          var paymentData = setRedirectPrefix(postData);
          var params = Object.keys(postData)
            .map(function (k) {
              return (
                encodeURIComponent(k) + "=" + encodeURIComponent(postData[k])
              );
            })
            .join("&");
          var postUrl = paymentUrl + "?viewName=JsPopup&" + params;
          var iframe = document.createElement("iframe");
          iframe.setAttribute("id", "paymentFrame");
          iframe.setAttribute("src", postUrl);
          iframe.setAttribute("frameborder", "0");
          iframe.setAttribute("scrolling", "no");
          iframe.style.position = "fixed";
          iframe.style.left = "0";
          iframe.style.right = "0";
          iframe.style.bottom = "0";
          iframe.style.top = "0";
          iframe.style.width = "100%";
          iframe.style.height = "175%";
          document.body.appendChild(iframe);
        } else {
          console.log(
            "The createPaymentModal method expects a JSON object for the postData parameter."
          );
          alert(paymentErrorMessage);
        }
      }),
      (self.cancelFramePayment = function () {
        var paymentFrame = document.getElementById("paymentFrame");
        paymentFrame.contentWindow.postMessage(
          { event: "ipayCancelPayment" },
          "*"
        );
      });
  }

  function setRedirectPrefix(data) {
    if (data.CancelUrl && data.CancelUrl.length > 0) {
      data.CancelUrl = redirectPrefix + encodeURI(data.CancelUrl);
    }

    if (data.ErrorUrl && data.ErrorUrl.length > 0) {
      data.ErrorUrl = redirectPrefix + encodeURI(data.ErrorUrl);
    }

    if (data.SuccessUrl && data.SuccessUrl.length > 0) {
      data.SuccessUrl = redirectPrefix + encodeURI(data.SuccessUrl);
    }

    return data;
  }

  eventer(
    messageEvent,
    function (e) {
      if (
        !e.data ||
        !e.data.event ||
        (e.data.event != "ipayMessage" &&
          e.data.event != "ipayResize" &&
          e.data.event != "ipayShowModal" &&
          e.data.event != "ipayHideModal")
      ) {
        return;
      }

      if (e.data.event == "ipayMessage") {
        var params = Object.keys(e.data.postData)
          .map(function (k) {
            return (
              encodeURIComponent(k) +
              "=" +
              encodeURIComponent(e.data.postData[k])
            );
          })
          .join("&");
        window.location =
          e.data.url +
          (e.data.url.indexOf("?") == -1 ? "?" : "&") +
          (params || {});
      } else if (e.data.event == "ipayResize") {
        document.getElementById("paymentFrame").style.height =
          e.data.height + "px";
      } else if (e.data.event == "ipayShowModal") {
        if (e.data.props) {
          addLogs(e.data.props);
        }

        if (e.data.props.modalType !== "PaymentSuccess") {
          showErrorPopUpWithLogo(e.data.props);
        }
        if (e.data.props.preventUserClose) {
          $("#closeModalBtn").hide();
        } else {
          $("#closeModalBtn").show();
        }

        $("#ipayModal .modalTitle").text(
          e.data.props.modalType + " - " + e.data.props.title
        );

        if (e.data.props.currentStep) {
          $("#ipayModal .modalStep").text(
            "Step: " +
              e.data.props.currentStep +
              " of " +
              e.data.props.totalSteps
          );
        } else {
          $("#ipayModal .modalStep").empty();
        }

        if (e.data.props.userBank) {
          $("#ipayModal .modalBank").text("Bank: " + e.data.props.userBank);
        } else {
          $("#ipayModal .modalBank").empty();
        }

        if (e.data.props.timeoutTimestamp) {
          $("#ipayModal .modalTimeout").text(
            "Timeout Timestamp: " + e.data.props.timeoutTimestamp
          );
        } else {
          $("#ipayModal .modalTimeout").empty();
        }

        if (e.data.props.text) {
          $("#ipayModal .modalText").text("Text: " + e.data.props.text);
        } else {
          $("#ipayModal .modalText").empty();
        }
      } else if (e.data.event === "ipayHideModal") {
        console.log("ipayHideModal data ---->>>> ", e.data);
        $("#global-errors-model").addClass("d-none");
      }
    },
    false
  );

  return ozow;
})(window, document);

export default Ozow;
