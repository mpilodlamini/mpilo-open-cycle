import React, { useContext, useState } from "react";
import { Box, InputLabel, Grid, Button } from "@material-ui/core";
import NumberFormat from "react-number-format";
import {
  info,
  white,
  border,
  SnackbarStatic,
} from "@peach/checkout-design-system";
import { makeStyles, withStyles, TextField, Theme } from "@material-ui/core";
import { ConfigContext } from "./contexts/ConfigContext";

const buttonAction = "Redeem";
const title = "1ForYou";
const maxLength = 16;
const inputProps: object = {
  style: {
    paddingTop: "10px",
    paddingBottom: "10px",
  },
  maxLength,
};

export const useStyles = makeStyles((theme: Theme) => ({
  label: {
    fontSize: "14px",
    fontWeight: 600,
    color: "#53627C",
    marginBottom: "8px",
  },
  textField: {
    marginRight: "8px",

    [theme.breakpoints.down("xs")]: {
      marginRight: "0",
      marginBottom: "8px",
    },
  },
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },
    "&:disabled": {
      borderColor: "#C9CED6",
      cursor: "not-allowed",
    },
  },
}));

export const CssTextField = withStyles({
  root: {
    "& .MuiOutlinedInput-root": {
      paddingLeft: "calc(48% - 100px)",
      paddingRight: "calc(48% - 100px)",

      "@media (max-width: 420px)": {
        paddingLeft: "calc(48% - 100px)",
        paddingRight: "calc(48% - 100px)",
      },
      "& fieldset": {
        borderColor: border,
        color: "#53627C",
      },
      "&:hover fieldset": {
        borderColor: info,
      },
      "&.Mui-focused fieldset": {
        borderColor: info,
      },
    },
  },
})(TextField);

export const OneForYouForm = (props: { onSubmit: (pin: string) => void }) => {
  const { config: checkout } = useContext(ConfigContext);
  const [pinValue, setPinValue] = useState("");
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const classes = useStyles();

  const onChange = (event: any) => {
    setIsButtonDisabled(true);
    const pinOnChangeValue = event.target.value;
    setPinValue(pinOnChangeValue);

    if (pinOnChangeValue.length === maxLength) {
      setIsButtonDisabled(false);
    }
  };

  const processPinContent = (): void => {
    if (isButtonDisabled) return;
    props.onSubmit(pinValue);
  };

  const onSubmit = (event: any): void => {
    event.preventDefault();
    processPinContent();
  };

  return (
    <Box width={1} height={1}>
      <Box display="flex" justifyContent="center" mb={3}>
        <img
          data-testid="1ForYou-logo"
          src="images/1foryou.svg"
          alt="1foryou"
        />
      </Box>

      <SnackbarStatic
        title="Partial Payments Not Supported"
        label="Your voucher amount must match the purchase amount or your payment will be declined. You will not be charged."
        dataTestId={`${title}-warning`}
      />

      <Box mt={3}>
        <form onSubmit={onSubmit} noValidate autoComplete="off">
          <InputLabel
            data-testid={`${title}-label`}
            className={classes.label}
            htmlFor={title}
          >
            Please enter your 16 digit Voucher Pin
          </InputLabel>

          <Grid container>
            <Grid item xs={12}>
              <Box mb={1}>
                <NumberFormat
                  type="tel"
                  customInput={CssTextField}
                  required
                  autoFocus
                  fullWidth
                  id={title}
                  data-testid={`${title}-pin`}
                  aria-describedby={title}
                  onChange={onChange}
                  variant="outlined"
                  placeholder="1234 5678 8909 6806"
                  inputProps={inputProps}
                />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Button
                type="button"
                className={classes.button}
                onClick={processPinContent}
                autoFocus
                disabled={isButtonDisabled}
                variant="contained"
                disableElevation
                fullWidth
                data-testid={`${title}-submit`}
              >
                {buttonAction}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Box>
  );
};
