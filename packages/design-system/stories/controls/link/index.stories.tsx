import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import { Link, LinkProps } from "../../../";

export default {
  title: "Controls/Link",
} as Meta;

export const Default: Story<LinkProps> = (props) => {
  return <Link {...props} />;
};

Default.args = {
  href: "https://peachpayments.com",
  label: "Peach Payments",
};
