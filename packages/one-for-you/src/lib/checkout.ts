import { Checkout } from "@peach/checkout-lib";

export const checkoutOrigin: string =
  (import.meta.env.VITE_CHECKOUT_ORIGIN as string) || "https://localhost:3000";

export const checkout = Checkout(checkoutOrigin);
