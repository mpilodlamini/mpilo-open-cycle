Feature: EFT Secure
    In order to make a purchase
    As a customer
    I want to pay using an EFT transaction

    Background: Launch Eft Secure
        Given I Generate Checkout via API using default values
        When I click on "eft-secure-btn"
        Then I should see "eftx-subtitle" containing the text "Please select your bank from the options below"

    Scenario Outline: <bankScenario> <simulationResponse> payment
        When I scroll to data-testid "<bank>"
        And I click on "<bank>"
        And I enter "<inputValue1>" in "<inputField1>" within "eftx-form"
        And I enter "<inputValue2>" in "<inputField2>" within "eftx-form"
        And I click on "submit" within "eftx-button-container"
        Then I should see that the element with id "eftx-button-container" contains the text "Failed Response"
        And I should see that the element with id "eftx-button-container" contains the text "Cancelled Response"
        When I click on "<simulation>" inside "eftx-button-container" id
        Then I should see that the element with id "eftx-subtitle" contains the text "<simulationResponse>"

        Examples:
            | bankScenario        | bank             | inputValue1 | inputField1 | inputValue2 | inputField2 | simulation         | simulationResponse    |
            | Nedbank             | nedbank-button   | s           | username    | a           | password    | Failed Response    | Insufficient funds    |
            | Capitec             | capitec-button   | e           | username    | s           | password    | Cancelled Response | Transaction Cancelled |


    Scenario Outline: <bank> <simulationResponse> payment
        When I scroll to data-testid "<bank>"
        And I click on "<bank>"
        And I enter "<inputValue1>" in "<inputField1>" within "eftx-form"
        And I enter "<inputValue2>" in "<inputField2>" within "eftx-form"
        And I click on "submit" within "eftx-button-container"
        Then I should see that the element with id "eftx-button-container" contains the text "Failed Response"
        And I should see that the element with id "eftx-button-container" contains the text "Cancelled Response"
        When I click on "<simulation>" inside "eftx-button-container" id
        Then I should see the text "<simulationResponse>"

        Examples:
            | bankScenario        | bank       | inputValue1 | inputField1 | inputValue2 | inputField2 | simulation    | simulationResponse |
            | First National Bank | fnb-button | u           | user        | p           | pass        | Paid Response | Payment successful |

    Scenario: Return to Bank selection screen
        When I click on "absa-button"
        Then I should see that the element with id "eftx-title" contains the text "Login"
        When I scroll to "Login"
        And I click on "selectAnotherBank" id
        Then I should see "eftx-subtitle" containing the text "Pay quickly and securely"
