import { Box, FormControl, Grid, RadioGroup } from "@material-ui/core";
import React from "react";
import { TitleBar } from "../../../controls";
import { CancelButton } from "../../../controls/buttons";
import { RadioCard, RadioCardProps } from "../../radio-card";

export interface PaymentSelectProps extends RadioCardProps {
  items: PaymentItem[];
  onCancel?: () => void;
}

export interface PaymentItem {
  label: string;
  name: string;
  id: string;
  src: string[];
  alt: string;
  checked: boolean;

  onClick: () => void;
}

export const PaymentMethodSelect = (props: PaymentSelectProps) => {
  // Control state for RadioButtonCard
  const [paymentMethodId, setValuePaymentMethodId] = React.useState("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValuePaymentMethodId((event.target as HTMLInputElement).value);
  };

  return (
    <Box style={{ overflow: "auto", height: "calc(100% - 180px)" }}>
      <Box p={2} pl={3}>
        <Grid container alignItems="center">
          <Grid item xs={8}>
            <TitleBar title={props.title} />
          </Grid>
          <Grid item xs={4}>
            <Box display="flex" flexDirection="row-reverse" mr={1}>
              <CancelButton onClick={props.onCancel || (() => {})} />
            </Box>
          </Grid>
        </Grid>
      </Box>

      <FormControl component="fieldset">
        <RadioGroup
          name={props.label}
          value={paymentMethodId}
          aria-label={props.label}
          onChange={handleChange}
        >
          <Box p={3} pt={0}>
            <Grid container spacing={2}>
              {props.items.map((plugin) => (
                <Grid item xs={12} lg={6} key={plugin.id}>
                  <RadioCard
                    key={plugin.id}
                    id={plugin.id}
                    title={plugin.label}
                    alt={plugin.alt}
                    src={plugin.src}
                    label={plugin.label}
                    onClick={plugin.onClick}
                    checked={plugin.checked}
                    dataTestId={plugin.id}
                  />
                </Grid>
              ))}
            </Grid>
          </Box>
        </RadioGroup>
      </FormControl>
    </Box>
  );
};
