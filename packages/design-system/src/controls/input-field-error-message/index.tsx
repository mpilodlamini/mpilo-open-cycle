import { withStyles, FormHelperText } from "@material-ui/core";
import { error } from "../../variables";

export const InputFieldErrorMessage = withStyles(() => ({
  root: {
    color: error,
    marginLeft: "14px",
  },
}))(FormHelperText);
