const JAVASCRIPT_EVENTS = Object.freeze({
  LOAD: "load",
  POPSTATE: "popstate",
});

export interface GlobalSetup {
  timeout: string;
}

export const GLOBAL_SETUP_OBJECT: GlobalSetup =
  (window as any).GLOBAL_SETUP_OBJECT || {};

export const REDIRECT_TIMER: number = 3;
const successful: string = "successful";
const cancelled: string = "cancelled";

export const MASTERPASS_QR_TOKEN_EXPIRATION_TIME: number = 30; // minutes
export const MOBICRED_OTP_EXPIRATION_TIME: number = 5; // minutes
export const MPESA_OTP_EXPIRATION_TIME: number = 2; // minutes

export const PEACH_STATE = Object.freeze({
  SUCCESS: "Success",
  FAILED: "Failed",
  PENDING: "Pending",
});

export const TRANSACTION_STATUS = Object.freeze({
  SUCCESSFUL: successful,
  CANCELLED: cancelled,
});

export const AJAX_RESULT_STATUS = Object.freeze({
  SUCCESSFUL: successful,
  CANCELLED: cancelled,
});

export const AJAX_DATA_RESULT = Object.freeze({
  SUCCESS: "success",
  FAILURE: "failure",
});

export const CARD = Object.freeze({
  REGEX_PATTERN: {
    AMERICAN_EXPRESS: /^(?:3[47][0-9]{13})$/,
    VISA: /^(?:4[0-9]{12}(?:[0-9]{3})?)$/,
    MASTERCARD: /^(?:5[1-5][0-9]{14})$/,
    DINERS_CLUB: /^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$/,
  },
  BRAND_DETECTION: {
    AMERICAN_EXPRESS: /^(?:3[47][0-9]{2})/,
    VISA: /^(?:4[0-9]{3})/,
    MASTERCARD: /^(?:5[1-5][0-9]{2})/,
    DINERS_CLUB: /^(?:3(?:0[0-5]|[68][0-9])[0-9])/,
  },
  ENUM: {
    AMERICAN_EXPRESS: "AMERICANEXPRESS",
    VISA: "VISA",
    MASTERCARD: "MASTERCARD",
    DINERS_CLUB: "DINERSCLUB",
  },
  NAME: {
    AMERICAN_EXPRESS: "American Express",
    VISA: "Visa",
    MASTERCARD: "Mastercard",
    DINERS_CLUB: "Diners Club",
  },
  MAX_LENGTH: {
    AMERICAN_EXPRESS: 15,
    VISA: 16,
    MASTERCARD: 16,
    DINERS_CLUB: 14,
  },
  MAX_CVV_LENGTH: {
    AMERICAN_EXPRESS: 4,
    VISA: 3,
    MASTERCARD: 3,
    DINERS_CLUB: 3,
  },
  BRAND_COLLECTION: ["AMERICANEXPRESS", "VISA", "MASTERCARD", "DINERSCLUB"],
  transformBrandEnumToName: (enumValue: string) => {
    switch (enumValue) {
      case "AMERICANEXPRESS":
        return "American Express";
      case "VISA":
        return "Visa";
      case "MASTERCARD":
        return "Mastercard";
      case "DINERSCLUB":
        return "Diners Club";
      default:
        return "";
    }
  },
});

export const REQUIRED_TEXT: string = "Required";

export const CARD_BRANDS = [
  "VISA",
  "AMERICANEXPRESS",
  "MASTERCARD",
  "DINERSCLUB",
];

export default {
  JAVASCRIPT_EVENTS,
  REQUIRED_TEXT,
  PEACH_STATE,
  REDIRECT_TIMER,
  AJAX_RESULT_STATUS,
  AJAX_DATA_RESULT,
};
