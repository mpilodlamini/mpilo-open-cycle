import React, { useContext, useState } from "react";
import NumberFormat from "react-number-format";

import { Box, InputLabel, Button } from "@material-ui/core";

import SnackbarContainer from "../../../../checkout/selected-payment-method/payment-methods/shared/SnackbarContainer";
import TimeUtil from "../../../../utils/timeUtil";

import { MOBICRED_OTP_EXPIRATION_TIME } from "../../../../utils/constants";
import { BUTTON_LABELS } from "../../../../utils/messagingConstants";
import { useStyles, CssTextField } from "./mobicredOtp.styles";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";

export interface MobicredOtpProps {
  timeUtil: TimeUtil;

  cellphoneNumber: string;

  onVerifyOtp: (otp: string) => void;
  onResendOtp: (otp: string) => void;
  onReRenderLogin: () => void;
}

const title: string = "mobicred-otp";
const otpNotFoundMessage: string = "Didn't receive an OTP?";

const maxLength: number = 6;
const maxInputLength: number = "1 - 2 - 3 - 4 - 5 - 6".length;

const inputProps: object = {
  placeholder: "1 - 2 - 3 - 4 - 5 - 6",
  maxLength: maxInputLength,
};

const MobicredOtp = (props: MobicredOtpProps) => {
  const [otpValue, setOtpValue] = useState("");
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const checkout = useContext(CheckoutContext);

  const classes = useStyles();

  const onChange = (event: { target: { value: string } }) => {
    const value = event.target.value;

    setOtpValue(value);

    const otpChangeEventValue: string = value
      .replace(/\s/g, "")
      .replace(/-/g, "")
      .replace(/_/g, "");

    if (otpChangeEventValue.length > maxLength) return;
    setIsButtonDisabled(true);

    if (otpChangeEventValue.length === maxLength) {
      setIsButtonDisabled(false);
    }
  };

  const button = (
    <Button
      type="submit"
      className={classes.button}
      onClick={() => {
        if (isButtonDisabled) return;
        const cleanOtpValue = otpValue
          .replace(/\s/g, "")
          .replace(/-/g, "")
          .replace(/_/g, "");
        props.onVerifyOtp(cleanOtpValue);
      }}
      variant="contained"
      disableElevation
      fullWidth={false}
      disabled={isButtonDisabled}
      data-testid={`${title}-submit-verify`}
      size="small"
    >
      {BUTTON_LABELS.VERIFY}
    </Button>
  );

  return (
    <Box width={1}>
      <Box display="flex" justifyContent="center">
        <img
          src={`${checkout.assetsBasePath || "/"}images/mobicred_36.svg`}
          alt=""
          data-testid={`${title}-otp-image`}
        />
      </Box>
      <Box textAlign="center">
        One Time Password sent to
        <b style={{ marginLeft: "4px", whiteSpace: "nowrap" }}>
          {props.cellphoneNumber}
        </b>
      </Box>

      <Box mt={3}>
        <form onSubmit={(event) => event.preventDefault()} autoComplete="off">
          <InputLabel className={classes.label} htmlFor={title}>
            Please enter your OTP
          </InputLabel>

          <NumberFormat
            type="tel"
            customInput={CssTextField}
            onChange={onChange}
            value={otpValue}
            required
            autoFocus
            fullWidth
            id={title}
            data-testid={`${title}-input`}
            aria-describedby={title}
            variant="outlined"
            format="#  #  #  #  #  #"
            // @ts-ignore
            size="small"
            inputProps={inputProps}
            mask="_"
          />

          <Box
            mt={1}
            fontSize="small"
            textAlign="center"
            style={{ color: "#53627C" }}
          >
            {otpNotFoundMessage}
            <Button
              data-testid={`${title}-resend-otp`}
              className={classes.resend}
              onClick={() => {
                const cleanOtpValue = otpValue
                  .replace(/\s/g, "")
                  .replace(/-/g, "")
                  .replace(/_/g, "");
                props.onResendOtp(cleanOtpValue);
              }}
            >
              Resend OTP
            </Button>
          </Box>

          <Box mt={3} mb={1} display="flex" justifyContent="center">
            {button}
          </Box>
          <Box mb={2} fontSize="small" textAlign="center">
            <Button
              data-testid={`${title}-different-account-signin`}
              className={classes.link}
              onClick={() => {
                props.onReRenderLogin();
              }}
            >
              Sign In with a different Account
            </Button>
          </Box>
        </form>

        <SnackbarContainer
          timeUtil={props.timeUtil}
          time={MOBICRED_OTP_EXPIRATION_TIME}
        />
      </Box>
    </Box>
  );
};

export default MobicredOtp;
