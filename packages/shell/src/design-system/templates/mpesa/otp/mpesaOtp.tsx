import React, { useContext } from "react";

import { Box, makeStyles } from "@material-ui/core";

import TimeUtil from "../../../../utils/timeUtil";
import SnackbarContainer from "../../../../checkout/selected-payment-method/payment-methods/shared/SnackbarContainer";

import { MPESA_OTP_EXPIRATION_TIME } from "../../../../utils/constants";
import { backgroundGrey } from "@peach/checkout-design-system";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";

const useStyles = makeStyles(() => ({
  box: {
    background: backgroundGrey,
    borderRadius: "4px",
  },
}));

export interface MobicredOtpProps {
  timeUtil: TimeUtil;
  mobileValue: string;
}

const mobileCode: string = "+254";

const MpesaOtp = (props: MobicredOtpProps) => {
  const checkout = useContext(CheckoutContext);
  const classes = useStyles();

  return (
    <Box>
      <Box p={3} width={1}>
        <Box display="flex" justifyContent="center">
          <img
            src={`${checkout.assetsBasePath || "/"}images/m-pesa.svg`}
            alt=""
          />
        </Box>
        <Box
          display="flex"
          justifyContent="center"
          data-testid="mpesa-otp-label"
        >
          To verify your Mpesa payments please check the phone that matches this
          number.
        </Box>

        <Box mt={3}>
          <Box className={classes.box} mb={3} p={3} textAlign="center">
            <Box
              component="span"
              mr={1}
              style={{ fontWeight: 600 }}
              data-testid="mpesa-mobile-code"
            >
              {mobileCode}
            </Box>
            <Box
              component="span"
              style={{ color: "#53627C" }}
              data-testid="mpesa-mobile-value"
            >
              {props.mobileValue}
            </Box>
          </Box>

          <SnackbarContainer
            data-testid="mpesa-otp-timer"
            timeUtil={props.timeUtil}
            time={MPESA_OTP_EXPIRATION_TIME}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default MpesaOtp;
