import { CHECKOUT } from "../contexts/CheckoutContext";
import { printInfo } from "./logger";
import { GLOBAL_SETUP_OBJECT } from "./constants";

interface TimeUtil {
  startCheckoutExpireTimer(timeOutCallback: () => void): void;

  createTimer(time: number, timerUpdateCallBack: (time: number) => void): void;

  stopShortTermTimer(): void;

  resetShortTermTimer(): void;

  updateCheckoutTimer(): void;

  getCheckoutTimeToExpire(): number;
}

const className: string = "TimeUtil";

class TimeUtil implements TimeUtil {
  private readonly _maximumPermittedTime: number;

  private _checkoutTimeToExpiration: number;

  private _hasExpired: boolean;

  private readonly _millisecondTimeOutDelimiter: number;

  private _timer: NodeJS.Timeout | null;

  private _shortTermTimer: NodeJS.Timeout | null;

  private _shortTermTime: number;

  private _shortTermTimerDuration: number;

  constructor() {
    this._maximumPermittedTime =
      parseFloat(GLOBAL_SETUP_OBJECT.timeout) * 60000 || 3000; // Minutes to milliseconds
    this._checkoutTimeToExpiration = this._maximumPermittedTime;
    this._hasExpired = false;
    this._millisecondTimeOutDelimiter = 1000;
    this._timer = null;
    this._shortTermTime = 0;
    this._shortTermTimer = null;
    this._shortTermTimerDuration = 0;
  }

  startCheckoutExpireTimer = (timeOutCallback: () => void): void => {
    const functionName = "startCheckoutExpireTimer";
    printInfo(
      className,
      functionName,
      `Starting checkout time out timer, with ${this._checkoutTimeToExpiration} milliseconds`
    );

    this._timer = setInterval(() => {
      if (this._checkoutTimeToExpiration === 0) {
        printInfo(className, functionName, "Ending checkout instance");
        if (this._timer) {
          clearInterval(this._timer);
        }
        this._hasExpired = true;

        if (CHECKOUT.isCheckoutInTest) {
          printInfo(
            className,
            functionName,
            "No redirect for local testing..."
          );
        } else {
          timeOutCallback();
        }
      }

      this._checkoutTimeToExpiration -= 1000;
    }, this._millisecondTimeOutDelimiter);
  };

  createTimer = (
    time: number,
    timerUpdateCallBack: (time: number) => void
  ): void => {
    printInfo(
      className,
      "createTimer",
      `Starting checkout timer out timer, with ${time} seconds`
    );

    this._shortTermTimerDuration = time;
    this._shortTermTime = this._shortTermTimerDuration;

    this._shortTermTimer = setInterval(() => {
      if (this._shortTermTime === 0) {
        this.stopShortTermTimer();
      }
      timerUpdateCallBack(this._shortTermTime);
      this._shortTermTime--;
    }, this._millisecondTimeOutDelimiter);
  };

  stopShortTermTimer = (): void => {
    printInfo(className, "stopShortTermTimer", "Stopping short term timer");
    if (!this._shortTermTimer) return;
    clearInterval(this._shortTermTimer);
    this._shortTermTimer = null;
  };

  resetShortTermTimer = (): void => {
    printInfo(
      className,
      "resetShortTermTimer",
      `Resetting timer, with ${this._shortTermTimerDuration} milliseconds`
    );
    this._shortTermTime = this._shortTermTimerDuration;
  };

  updateCheckoutTimer = (): void => {
    printInfo(
      className,
      "updateCheckoutTimer",
      `Update timer, with ${this._maximumPermittedTime} milliseconds`
    );
    this._checkoutTimeToExpiration = this._maximumPermittedTime;
  };

  getCheckoutTimeToExpire = (): number => {
    printInfo(
      className,
      "getCheckoutTimeToExpire",
      "Get the checkout timer value"
    );
    return this._checkoutTimeToExpiration;
  };
}

export default TimeUtil;
