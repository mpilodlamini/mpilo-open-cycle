import { makeStyles } from "@material-ui/core";
import {
  white,
  info,
  text,
} from "@peach/checkout-design-system/dist/variables";

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    height: "100%",
  },
  message: {
    maxWidth: "500px",
  },
  margin: {
    marginTop: "24px",
  },
  bottom: {
    alignSelf: "flex-end",
    paddingTop: "24px",
    paddingBottom: "16px",
  },
  anchor: {
    display: "flex",
    height: "100%",
  },
  pageTitle: {
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px",
    },
  },
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",

    "&:hover": {
      borderColor: "#d5d5d5",
    },
  },
  goBackButton: {
    color: text,
    textTransform: "none",
    textDecoration: "underline",
  },
}));
