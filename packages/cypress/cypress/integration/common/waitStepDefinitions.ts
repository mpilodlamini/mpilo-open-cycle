import { When } from "cypress-cucumber-preprocessor/steps";

When("it finishes {string}", (element) => {
  cy.getElementByDataTestId(element, { timeout: 20000 }).should(
    "not.exist"
  );
});

When("it finishes {string} in the iframe", (element) => {
  cy.getIFrameElementByDataTestId(element, {
    timeout: 20000,
  }).should("not.exist");
});
