import React from "react";
import $ from "jquery";
import { AJAX_DATA_RESULT } from "../../../../utils/constants";
import PluginManager from "../../../../utils/pluginManager";
import CheckoutService from "../../../../services/checkoutService";
import LoadingScreen from "../../../../components/loaders/loadingScreen";
import { PAYMENT_METHOD_NAMES } from "../../../../utils/pluginConstants";
import ErrorScreen from "../../../../components/errors/screens/errorScreen";
import { GA } from "../../../../utils/googleAnalyticsConstants";
import { printError, printInfo } from "../../../../utils/logger";
import { CheckoutConfig } from "../../../../contexts/CheckoutContext";
import GoogleAnalyticsUtil from "../../../../utils/googleAnalyticsUtil";
import TimeUtil from "../../../../utils/timeUtil";

export interface EftSecureContainerProps {
  checkout: CheckoutConfig;
  googleAnalytics: GoogleAnalyticsUtil;
  timeUtil: TimeUtil;
}

export interface EftSecureContainerState {
  showLoadingScreen: boolean;
  showErrorScreen: boolean;
  isEftLandingPage: boolean;

  selectedBank: string;
  eftSecure: any;
}

export interface PostData {
  merchant_key: string;
  checkout_id: string;
  payment_brand: string;
}

export interface BeforeFormRenderData {
  isInputsButtonEnabled: boolean;
  errorMessage: string;
  category: string;
  inputs: Array<{
    options: Array<string>;
    html_options: {
      name: string;
    };
  }>;
}

export interface EftSecureSuccessCallbackData {
  status: string;
  response: { response_data: { resultDetails: { paymentKey: string } } };
}

const componentName = "EftSecureContainer";

const paymentBrand = "EFTSecure";

const banks: Array<string> = [
  "absa",
  "capitec",
  "standard",
  "investec",
  "fnb",
  "nedbank",
  "afribank",
  "tyme",
  "bidvest",
  "oldmutual",
];

const submitButtonLabels: Array<string> = ["Sign in", "Login"];

const fadeOutSpeed = "fast";

const hideClass = "hide";

const displayNoneClass = "d-none";

const jqueryClickEvent = "click";

const disableProp = "disabled";

class EftSecureContainer extends React.Component<
  EftSecureContainerProps,
  EftSecureContainerState
> {
  private readonly checkoutService: CheckoutService = new CheckoutService();

  private readonly _pluginManager: PluginManager = new PluginManager(
    this.props.timeUtil
  );

  private readonly _loadingScreen = (
    <LoadingScreen widgetName={PAYMENT_METHOD_NAMES.EFT_SECURE} />
  );

  private readonly _errorScreen = (
    <ErrorScreen
      title={`The ${PAYMENT_METHOD_NAMES.EFT_SECURE} payment method is unavailable`}
    />
  );

  constructor(props: EftSecureContainerProps) {
    super(props);

    this.state = {
      showLoadingScreen: true,
      isEftLandingPage: true,
      showErrorScreen: false,
      selectedBank: "",
      eftSecure: null,
    };
  }

  componentDidMount = (): void => {
    printInfo(componentName, "componentDidMount", "Mounted");

    this.getEftPayment();

    $(".eftx-cancel-transaction").on(jqueryClickEvent, () => {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.CANCEL_PAYMENT,
        PAYMENT_METHOD_NAMES.EFT_SECURE
      );
    });
  };

  componentWillUnmount = (): void => {
    this.checkoutService.cancelRequest();
  };

  getEftPayment = (): void => {
    const postData: PostData = {
      merchant_key: this.props.checkout.merchantKey,
      checkout_id: this.props.checkout.checkoutId,
      payment_brand: paymentBrand,
    };

    const successCallback = (data: EftSecureSuccessCallbackData): void => {
      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        this.props.timeUtil.updateCheckoutTimer();
        this._pluginManager.createPollingListener();

        const paymentKey =
          data?.response?.response_data?.resultDetails?.paymentKey || "";
        this.initializeEFTPayment(paymentKey);
      }
    };

    const errorCallback = (): void => {
      this.setState({ showLoadingScreen: false, showErrorScreen: true });
    };

    this.checkoutService.postFormEncodedContent(
      `${this.props.checkout.baseUrl}payment`,
      postData,
      successCallback,
      errorCallback
    );
  };

  beforeFormRender = (form: any, data: BeforeFormRenderData): boolean => {
    const categoryType: string = "select_account";

    // Renders the various accounts of the user
    if (
      data.category &&
      data.category === categoryType &&
      data.isInputsButtonEnabled &&
      !data.errorMessage
    ) {
      const inputName = data.inputs[0].html_options.name;
      const accounts = data.inputs[0].options;

      for (const accountNumber in accounts) {
        const accountDescription = accounts[accountNumber];
        const inputToRender = {
          html_options: {
            class: "eftx-submit eftx-account-button",
            name: inputName,
            value: accountNumber,
          },
          label: accountDescription,
          type: "submit",
        };
        // @ts-ignore
        window.eftXOptions.populateInput(form, inputToRender, false, data.step);
      }
      return false;
    }
    return true;
  };

  onLoadStart = (): void => {
    this.setState({ showLoadingScreen: false });
    const loader: JQuery = $(".loader_wrapper");

    $("#eftx-form").attr("data-testid", "eftx-form").hide();
    $("#eftx-button-container").hide();

    $(".submitBtn").prop(disableProp, true);

    if (this.state.showLoadingScreen) {
      $(".eft-global-loader").fadeIn(fadeOutSpeed);
    } else {
      loader.removeClass(displayNoneClass);
      loader.fadeIn(fadeOutSpeed);
    }
  };

  eftInputOnKeyPress = (inputs: JQuery): void => {
    const submitButton: JQuery = $(".submitBtn");
    let shouldEnable: boolean = true;

    // Disables and enables the submit button based on the input values
    // EFT Secure has inputs that are hidden, they should not be taken into account for validation
    // Some banks have more inputs than others, thus the need for a for loop
    for (let i = 0; i < inputs.length; i++) {
      const inputElement: JQuery = $(inputs[i]) as unknown as JQuery;
      if (inputElement.attr("type") === "hidden") continue;

      if (inputElement.val() === "") {
        shouldEnable = false;
        submitButton.prop(disableProp, true);
      }
    }

    if (shouldEnable) {
      submitButton.prop(disableProp, false);
    }
  };

  setBanksLogo = (): void => {
    const activeBank: string = $("body").attr("data-selected-bank") || "";

    if (activeBank) {
      for (let i = 0; i < banks.length; i++) {
        if (activeBank === banks[i]) {
          $(".eftx-cancel-transaction").hide();
          $(".eft_header").show();
          $(".eft_logo").attr(
            "src",
            `${this.props.checkout.assetsBasePath}images/${banks[i]}.svg`
          );
          return;
        }
      }
    } else {
      $(".eft_logo").attr(
        "src",
        `${this.props.checkout.assetsBasePath}images/eft.svg`
      );
    }
  };

  /**
   * Reinstates the EFT Secure plugin
   */
  callEftAgain = (): void => {
    $(".eft_header").hide();

    // @ts-ignore
    if (window.eftXOptions) {
      // @ts-ignore
      window.eftXOptions.init(() => {
        this.setState({ isEftLandingPage: true });
      });
    }
  };

  addBankButtonsEvents = (): void => {
    $(".eftx-capitec")
      .attr("data-testid", "capitec-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/capitec.svg" class="capitec_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Capitec" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });

    $(".eftx-fnb")
      .attr("data-testid", "fnb-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/fnb.svg" class="fnb_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Fnb" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-standard")
      .attr("data-testid", "standard-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/standard.svg" class="standard_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Standard Bank" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-nedbank")
      .attr("data-testid", "nedbank-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/nedbank.svg" class="nedbank_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Nedbank" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-absa")
      .attr("data-testid", "absa-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/absa.svg" class="absa_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Absa" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-investec")
      .attr("data-testid", "investec-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/investec.svg" class="investec_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Investec" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-tyme")
      .attr("data-testid", "tyme-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/tyme.svg" class="investec_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Tyme Bank" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-afribank")
      .attr("data-testid", "afribank-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/afribank.svg" class="investec_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Afribank" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-bidvest")
      .attr("data-testid", "bidvest-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/bidvest.svg" class="investec_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Bidvest" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
    $(".eftx-oldmutual")
      .attr("data-testid", "oldmutual-button")
      .append(
        `<img src="${this.props.checkout.assetsBasePath}images/oldmutual.svg" class="investec_logo">`
      )
      .off(jqueryClickEvent)
      .on(jqueryClickEvent, () => {
        this.setState({ selectedBank: "Old Mutual" }, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_EFT_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
        });
      });
  };

  updateLandingScreenText = (): void => {
    this.setState({ isEftLandingPage: false });

    $("#eftx-title").text("");
    $("#eftx-subtitle")
      .attr("data-testid", "eftx-subtitle")
      .text("Please select your bank from the options below");
  };

  addLogoOnSelectedInput = (
    allInputs: JQuery<HTMLInputElement>,
    initialValue: number,
    index: number
  ): void => {
    let counter: number = initialValue;

    if (allInputs[index].type !== "hidden") {
      counter++;

      if (counter === 2) {
        const firstInput: HTMLInputElement = allInputs[0];
        const secondInput: HTMLInputElement = allInputs[1];
        const screenWidth: number = $(window).width() || 0;

        if (screenWidth > 500 && screenWidth < 600) {
          this.addCSS(firstInput, "user-logo", 40, 17.5);
          this.addCSS(secondInput, "pass-logo", 40, 17.5);
        } else {
          this.addCSS(firstInput, "user-logo", -5, 17.5);
          this.addCSS(secondInput, "pass-logo", -5, 17.5);
        }
      } else if (counter === 3) {
        const thirdInput = allInputs[2];
        const screenWidth: number = $(window).width() || 0;

        if (screenWidth > 500 && screenWidth < 600) {
          this.addCSS(thirdInput, "number-logo", 40, 17.5);
        } else {
          this.addCSS(thirdInput, "number-logo", -5, 17.5);
        }
      }
    }
  };

  onLoadStop = (): void => {
    this.setBanksLogo();

    this.setState({
      showLoadingScreen: false,
    });

    $(".eft_header").fadeIn(fadeOutSpeed);
    $("#eftx-subtitle").fadeIn(fadeOutSpeed);
    $("#eftx-title").fadeIn(fadeOutSpeed);

    $(".loader_wrapper").fadeOut(fadeOutSpeed);

    $("#eftx-button-container").show();
    $("#eftx-form").fadeIn(fadeOutSpeed, () => {
      $(".eftx-bank").append(
        `<div class="circle_main">
                    <span class="active-con" />
                </div>`
      );

      this.addBankButtonsEvents();

      if (this.state.isEftLandingPage) {
        this.updateLandingScreenText();
      }
    });

    setTimeout(() => {
      const loaderWrapper: JQuery = $(".loader_wrapper");
      loaderWrapper.fadeOut(fadeOutSpeed).addClass(hideClass);
    }, 1000);

    setTimeout(() => {
      const eftxInput: JQuery = $(".eftx-input");
      const eftxButtonContainer: JQuery = $("#eftx-button-container");

      // Check if the input is visible
      if (eftxInput.offset()) {
        // If the input's are visible append the icons to the input and show select another bank button
        // Check if all inputs are empty
        eftxInput.on("keyup", () => {
          // check amount of inputs
          this.eftInputOnKeyPress(eftxInput);
        });

        eftxButtonContainer
          .show()
          .append(
            "<a id='selectAnotherBank' class='select_bank'>Select another bank</a>"
          );

        $("#selectAnotherBank").on(jqueryClickEvent, () => {
          this.props.googleAnalytics.trackEvent(
            GA.EVENTS.SELECT_NEW_BANK,
            PAYMENT_METHOD_NAMES.EFT_SECURE,
            this.state.selectedBank
          );
          this.callEftAgain();
        });

        // Hide cancel button
        $(".eftx-cancel-transaction").hide();

        const allInputs: JQuery<HTMLInputElement> = $("input");
        const submitButton: JQuery = $(".eftx-submit");

        // If the submit button is visible add class and change header text and subheader text
        let textContent: string = "";
        if (submitButton && submitButton[0] && submitButton[0].textContent) {
          textContent = submitButton[0].textContent;
        }

        if (submitButtonLabels.indexOf(textContent) !== -1) {
          // SUBMIT BUTTON
          submitButton.addClass("submitBtn");
          $(".submitBtn").prop(disableProp, true);
        }
        // Add the correct logo depending on which input is visible
        for (let i = 0; i < allInputs.length; i++) {
          this.addLogoOnSelectedInput(allInputs, 0, i);
        }
      }
    });
  };

  initializeEFTPayment = (paymentKey: string): void => {
    $("#eft").removeClass(displayNoneClass);

    // @ts-ignore
    if (window.eftXOptions) {
      this.setState({ showLoadingScreen: false });
      // @ts-ignore
      window.eftXOptions.init(() => {
        this.setState({ isEftLandingPage: true });
      });
      return;
    }

    if (!this.state.eftSecure) {
      // @ts-ignore
      // eslint-disable-next-line no-undef
      this.setState({ eftSecure: Eftx });

      try {
        this.state.eftSecure({
          key: paymentKey,
          formId: "eftx-form",
          titleId: "eftx-title",
          subTitleId: "eftx-subtitle",
          cssFramework: "plain",
          buttonContainerId: "eftx-button-container",
          beforeFormRender: this.beforeFormRender,
          onLoadStart: this.onLoadStart,
          onLoadStop: this.onLoadStop,
        });
      } catch (error) {
        printError(componentName, "initializeEFTPayment", error);
      }
    }
  };

  /**
   * Helper method for adding CSS left and top styling for selector class provided
   */
  addCSS = (
    data: HTMLInputElement,
    selector: string,
    left: number,
    top: number
  ): void => {
    $(`.${selector}`).css({
      left: data.clientWidth + left,
      top: data.offsetTop + top,
    });
  };

  render = ():
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined => {
    const eftSecureContent = (
      <div className="inner-content eft d-none" id="eft">
        <div className="eft_main">
          <div className="eft_content">
            <div className="loader_wrapper center-con-in d-none">
              <div className="loader-icon">
                <svg viewBox="22 22 44 44" style={{ height: 40, width: 40 }}>
                  <circle
                    className="MuiCircularProgress-circleIndeterminate-10"
                    cx="44"
                    cy="44"
                    r="18"
                    stroke="#1c7ed6"
                    strokeWidth="4"
                    fill="none"
                  />
                </svg>
                <p>Please wait...</p>
              </div>
            </div>

            <div className="eft_header">
              <img
                src={`${this.props.checkout.assetsBasePath}images/eft.svg`}
                className="eft_logo"
                alt=""
              />
            </div>
            <p id="eftx-title" />
            <p id="eftx-subtitle" />
            <form id="eftx-form" autoComplete="false" />
            <div id="eftx-button-container" />
          </div>
        </div>
      </div>
    );

    return (
      <>
        {this.state.showLoadingScreen &&
          !this.state.showErrorScreen &&
          this._loadingScreen}
        {!this.state.showLoadingScreen &&
          this.state.showErrorScreen &&
          this._errorScreen}
        {eftSecureContent}
      </>
    );
  };
}

export default EftSecureContainer;
