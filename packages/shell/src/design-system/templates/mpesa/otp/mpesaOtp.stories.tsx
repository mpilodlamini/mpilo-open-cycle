import React from "react";
import MpesaOtp from "./mpesaOtp";

export default {
  title: "Payments / Mpesa",
  parameters: {
    component: MpesaOtp,
  },
};

export const standard = (args: any) => <MpesaOtp {...args} />;
