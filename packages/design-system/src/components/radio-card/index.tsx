import React, { MouseEvent as ME } from "react";
import {
  Box,
  FormControlLabel,
  makeStyles,
  Paper,
  Radio,
  RadioProps,
} from "@material-ui/core";
import { info } from "../../variables";

export type RadioCardProps = RadioProps & {
  label: string;
  title: string;
  id: string;
  src: string[];
  alt: string;
  dataTestId?: string;
  checked: boolean;
  onClick: (e: ME<HTMLDivElement, MouseEvent>) => void;
};

const useStyles = makeStyles(() => ({
  radioCard: {
    width: "100%",
    marginRight: "0",
  },
  radio: {
    "&$checked": {
      color: info,
    },
  },
  checked: {},
  selected: {
    boxShadow: `0 0 0 2px ${info}`,
  },
  paymentLogo: {
    height: "auto",
    width: "auto",
    marginRight: "8px",
  },
  hover: {
    position: "relative",

    "&:after": {
      content: '" "',
      position: "absolute",
      boxShadow: `0 0 0 2px ${info}`,
      opacity: "0",
      transition: "opacity 0.2s ease-in-out",
      top: "0",
      left: "0",
      bottom: "0",
      right: "0",
      borderRadius: "4px",
      cursor: "pointer",
    },

    "&:hover": {
      "&:after": {
        opacity: "1",
      },
    },
  },
}));

export const RadioCard = ({
  src,
  alt,
  title,
  id,
  onClick,
  checked,
  label,
  dataTestId,
  ...props
}: RadioCardProps) => {
  const styles = useStyles();

  return (
    <Paper
      id={id}
      data-testid={dataTestId}
      title={title}
      onClick={onClick}
      className={checked ? styles.selected : styles.hover}
    >
      <Box
        pt={3}
        pb={3}
        pl={3}
        pr={3}
        display="flex"
        alignItems="center"
        width={1}
      >
        <Box flexGrow={1}>
          <FormControlLabel
            value={id}
            control={
              <Box pr={1}>
                <Radio
                  {...props}
                  id={id}
                  checked={checked}
                  classes={{ root: styles.radio, checked: styles.checked }}
                />
              </Box>
            }
            label={`${label}`}
            labelPlacement="end"
            className={styles.radioCard}
          />
        </Box>
        <Box display="flex" alignItems="center" justifyContent="flex-end">
          {(src || []).map((src) => {
            return (
              src && (
                <img
                  key={src}
                  src={src}
                  className={styles.paymentLogo}
                  alt={alt}
                />
              )
            );
          })}
        </Box>
      </Box>
    </Paper>
  );
};
