export interface CardPostData {
  brand: string;
  number: string;
  holder: string;
  expiryMonth: string;
  expiryYear: string;
  street1: string;
  city: string;
  postalCode: string;
  country: string;
  cvv: string;
}

export interface CardFormProps {
  cardBrands: string[];
  onSubmit: (parameters: CardPostData) => void;
}
