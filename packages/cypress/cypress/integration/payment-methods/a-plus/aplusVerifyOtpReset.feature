Feature: A+ Verify OTP requests for invalid scenarios
    In order to handle invalid verify attempts
    As a customer
    I want to to be notified accordingly

    Scenario Outline: A+ Verify OTP <otp> Failure Scenario Tests that reset the payment type on Fail
        Given I Generate Checkout via API using default values

        When I click on "aplus-radio-card"
        And I switch to the iframe
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        When I Login to access my A+ account
        And it finishes "loading" in the iframe
        Then I should see the text "One Time Password sent to" in the iframe
        And I should see the text "Please enter your OTP below." in the iframe
        And I should see that the "aplus-otp-btn" button is disabled in the iframe
        And I should see "aplus-otp-timer" containing the text "Session Expires in" in the iframe

        When I enter OTP "<otp>" and click verify
        And it finishes "loading" in the iframe
        Then I should see the text "<errorHeading>" inside "warning" modal
        And I should see the text "Continue" inside "warning" modal

        When I click the "Continue" inside "warning" modal
        And it finishes "loading" in the iframe
        Then I should see the text "Account Payment Method" in the iframe
        And I should see the text "Please enter your A+ Account card details." in the iframe
        And I should see that the "aplus-details-btn" button is disabled in the iframe
        And I should see "aplus-details-timer" containing the text "Session Expires in" in the iframe

        Examples:
            | otp  | errorHeading              |
            | 3333 | Payment Declined          |
            | 4444 | Incorrect Payment Details |
            | 6666 | Payment Timeout           |
            | 7777 | Payment Failed            |
            | 8888 | A+ unavailable            |
            | 9999 | Payment Declined          |
            | 1122 | A+ unavailable            |