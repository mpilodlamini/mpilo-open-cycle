import React, { useContext } from "react";

import { Box, InputLabel, InputAdornment, Button } from "@material-ui/core";
import PersonOutlineOutlinedIcon from "@material-ui/icons/PersonOutlineOutlined";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import { FormikHelpers, useFormik } from "formik";
import * as Yup from "yup";
import { BUTTON_LABELS } from "../../../../utils/messagingConstants";
import { useStyles, CssTextField } from "./mobicredLoginScreen.styles";
import {
  MobicredLoginScreenProps,
  MobicredFormValues,
} from "./mobicredLoginScreenInterface";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";
import { LanguageContext } from "../../../../contexts/LanguageContext";

const MobicredLoginScreen = (props: MobicredLoginScreenProps) => {
  const title = "mobicred";
  const checkout = useContext(CheckoutContext);
  const language = useContext(LanguageContext);
  const classes = useStyles();

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    // eslint-disable-next-line no-unused-vars
    onSubmit: (
      values: MobicredFormValues,
      { setSubmitting }: FormikHelpers<MobicredFormValues>
    ) => {
      // Has to be present does not get used
    },
    validationSchema: Yup.object({
      email: Yup.string().required("Required").email("Invalid email address"),
      password: Yup.string().required("Required"),
    }),
    validateOnMount: true, // For some reason the isValid is true on mount, this changes it to false
    initialErrors: {
      // Without this the values are perceived as valid
      email: "",
      password: "",
    },
  });

  const submit = () => {
    props.onClick(formik.values.email, formik.values.password);
  };

  const submitButton = (
    <Button
      type="submit"
      variant="contained"
      className={classes.button}
      disabled={!formik.isValid}
      disableElevation
      fullWidth={false}
      data-testid={`${title}-submit-login`}
    >
      {language.signIn}
    </Button>
  );

  return (
    <Box width={1} data-testid="mobicred-login-screen">
      <Box display="flex" justifyContent="center">
        <img
          src={`${checkout.assetsBasePath || "/"}images/mobicred_36.svg`}
          alt=""
        />
      </Box>

      {/* <Box display="flex" justifyContent="center" textAlign="center">
        Please sign in to your Mobicred account.
      </Box> */}

      <Box display="flex" justifyContent="center" textAlign="center">
        {language.pleaseSignIn}
      </Box>

      <Box mt={3}>
        <form
          onSubmit={(event) => {
            event.preventDefault();
            submit();
          }}
          autoComplete="off"
        >
          <InputLabel
            error={formik.touched.email && !!formik.errors.email}
            className={classes.label}
            htmlFor="email"
          >
            {language.emailAddress}*
          </InputLabel>

          <CssTextField
            error={formik.touched.email && !!formik.errors.email}
            FormHelperTextProps={{ className: classes.validation }}
            helperText={(formik.touched.email && formik.errors.email) || ""}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
            name="email"
            required
            fullWidth
            id="email"
            data-testid={`${title}-email`}
            aria-describedby="email"
            variant="outlined"
            size="small"
            type="email"
            InputProps={{
              placeholder: "email@address.co.za",
              endAdornment: (
                <InputAdornment position="end">
                  <PersonOutlineOutlinedIcon fontSize="small" />
                </InputAdornment>
              ),
            }}
          />

          <Box mt={2}>
            <InputLabel
              error={formik.touched.password && !!formik.errors.password}
              className={classes.label}
              htmlFor="password"
            >
              {language.password}*
            </InputLabel>
            <CssTextField
              name="password"
              FormHelperTextProps={{ className: classes.validation }}
              error={formik.touched.password && !!formik.errors.password}
              helperText={formik.touched.password && formik.errors.password}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.password}
              required
              fullWidth
              id="password"
              data-testid={`${title}-password`}
              aria-describedby="password"
              variant="outlined"
              size="small"
              type="password"
              InputProps={{
                placeholder: language.password,
                endAdornment: (
                  <InputAdornment position="end">
                    <LockOutlinedIcon fontSize="small" />
                  </InputAdornment>
                ),
              }}
            />
          </Box>

          <Box mt={2} display="flex" justifyContent="flex-end">
            {submitButton}
          </Box>
        </form>
      </Box>
    </Box>
  );
};

export default MobicredLoginScreen;
