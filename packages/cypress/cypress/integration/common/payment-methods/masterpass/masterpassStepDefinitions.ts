import { Then, When } from "cypress-cucumber-preprocessor/steps";
import { Decoder } from '@nuintun/qrcode';

Then("I should see a QR code", () => {
  cy.getElementByDataTestId("qr-code")
    .should("be.visible")
    .invoke("attr", "src")
    .then((src) =>
      cy.getElementByDataTestId("code")
        .should("be.visible")
        .invoke("text")
        .then((c) =>
          scanCode(src, c).then((result) => assert(result, "QR code matched"))
        )
    );
});

When("Masterpass finishes loading", () => {
  cy.getElementByDataTestId("loading", { timeout: 20000 }).should(
    "not.exist"
  );
});

When("QR code finishes generating", () => {
  cy.getElementByDataTestId("loading", { timeout: 20000 }).should(
    "not.exist"
  );
});

async function scanCode(qrImg: string, expectedCode: string) {
  
  expectedCode = expectedCode.replace(/\s/g, "");

  const qrcode = new Decoder();

  const decode = await qrcode.scan(qrImg);
  
  return decode.data === expectedCode;

}
