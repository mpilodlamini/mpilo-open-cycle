import React from "react";
import OneForYou from "./oneForYou";
import SnackbarStatic from "../../molecules/snackbar-static/snackbarStatic";
import { Meta } from "@storybook/react/types-6-0";

export default {
  title: "payments / OneForYou",
  component: OneForYou,
  subcomponents: { SnackbarStatic },

  parameters: {
    componentSubtitle:
      "A voucher based redemption system integrated into our Hosted Payments Page (Checkout)",
    docs: {
      storyDescription:
        "We will be taking a phased approach to the 1ForYou integration into Switch & Checkout with the v1 of this integration completely descoping us, the merchant and customers from managing change vouchers. We will only support full redemptions, no partial redemptions will be allowed for any merchants, to do this, we will send the transaction amount as 0 to 1ForYou to indicate a full redemption of the voucher. No refunds; partial or full, will be supported",
    },
  },
} as Meta;

export const voucher = (args: any) => <OneForYou {...args} />;
