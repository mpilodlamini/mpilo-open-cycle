//Any defined ID below refer to our ''data-testid' attributes within the project

const CONSTANTS = {
  WEBSITE: "https://www.google.com",
  EMAIL: "mail@mail.com",
  PASSWORD: "1234",
  PAYMENT_BANNER_HOME_SCREEN: "Select Payment Method",
  SUBMIT_BUTTON_LABEL: "Send POST Request",
  REDIRECT_BUTTON_LABEL: "Return to Store",

  POST_TEST_URL: "https://httpbin.org/post",

  AJAX_REQUEST_VERB: {
    POST: "POST",
    GET: "GET",
  },

  CHECKOUT: {
    CURRENCY_ID: "header-currency",
    AMOUNT_ID: "header-total",
    CANCEL_CHECKOUT_BUTTON_ID: "banner-cancel",
    FORM_HEADER_LABEL: "Select Payment Method",
    CANCEL_DIALOG: {
      HEADER_LABEL: "Cancel Transaction?",
      BODY_TEXT: "You are about to cancel the transaction and return to the merchant. Are you sure you want to do this?",
      CONFIRM_BUTTON_ID: "dialog-cancel",
      GO_BACK_BUTTON_ID: "dialog-go-back",
    },
  },

  PAYMENT_DECLINED_HEADER: "Payment Declined",
  PAYMENT_ERROR_HEADER: "Payment Error",
  PAYMENT_PENDING_HEADER: "Payment Pending",
  PAYMENT_SUCCESS_HEADER: "Payment Successful",
  PAYMENT_CANCELED_ERROR_HEADER: "Payment Cancelled",
  PAYMENT_FAILED_ERROR_HEADER: "Payment Failed",

  LOADING_DIALOG_ID: "loading",
  WARNING_DIALOG_ID: "warning",

  ONE_FOR_YOU: {
    PAYMENT_METHOD_SCREEN_BUTTON_TEST_ID: "1ForYou-radio-card",
    PAYMENT_METHOD_LABELS: "1ForYou",
    PAYMENT_METHOD_TITLE: "Pay with 1ForYou",
    PIN: "1111111111111111",
    WARNING:{
      TITLE_TEST_ID: "1ForYou-warning",
      TITLE_TEXT: "Partial Payments Not Supported",
      TEXT: "Your voucher amount must match the purchase amount or your payment will be declined. You will not be charged.",
    },
    PIN_INPUT:{
      LABEL: "Please enter your 16 digit Voucher Pin",
      ID: "1ForYou-pin",
    },
    SUBMIT_BUTTON:{
      LABEL: "Redeem",
      ID: "1ForYou-submit",
    },
    PAYMENT_VERIFYING: {
      DIALOG_HEADER: "Verifying your Voucher",
      DIALOG_TEXT: "Please wait while we verify your Voucher Pin.",
    },      
    NO_AVAILABLE_BALANCE: {
      //amount=0.01
      DIALOG_HEADER: "No Available Balance",
      DIALOG_TEXT: "Your voucher has already been redeemed. Please try again with a different voucher or select a different payment method.",
    }, 
    INVALID_VOUCHER: {
      //amount=0.02
      DIALOG_HEADER: "Invalid Voucher PIN",
      DIALOG_TEXT: "Please make sure you have entered your PIN correctly. Please go back to try again or select a different payment method.",
    },  
    EXPIRED_VOUCHER: {
      //amount=0.04
      DIALOG_HEADER: "Expired Voucher",
      DIALOG_TEXT: "Please contact 1ForYou or try again with a different voucher or select a different payment method."
    },
    REVERSAL_SUCCESSFUL: {
      //amount=1.00 || amount=4.00
      DIALOG_HEADER: "Payment Declined",
      DIALOG_TEXT: "Your voucher credit does not match your purchase amount. Your payment has been declined. You can try again or select another payment method."
    },
  },

  MOBICRED: {
    PAYMENT_METHOD_SCREEN_BUTTON_ID: "mobicred-btn",
    SIGN_IN_MESSAGE: "Please sign in to your Mobicred account.",
    EMAIL_INPUT: {
      LABEL:"Email Address*",
      ID: "mobicred-email",
    },
    PASSWORD_INPUT: {
      LABEL:"Password*",
      ID: "mobicred-password",
    },
    SUBMIT_BUTTON: {
      LABEL:"Sign In",
      ID:"mobicred-submit-login",
    },
    OTP_MESSAGE: "One Time Password sent to",
    SIGN_OUT_BUTTON: {
      LABEL: "Sign In with a different Account",
      ID: "mobicred-otp-different-account-signin",
    },
    RESEND_OTP: {
      LABEL: "Resend OTP",
      ID: "mobicred-otp-resend-otp",
    },
    PIN_INPUT: {
      LABEL: "Please enter your OTP",
      ID: "mobicred-otp-input",
    },    
    VERIFY_BUTTON: {
      LABEL: "Verify",
      ID: "mobicred-otp-submit-verify",
    },
    SUCCESS_PIN: "123456",
    PENDING_PIN: "111111",
    ERROR_PIN: "654321",
  },

  MASTERPASS: {
    PAYMENT_METHOD_SCREEN_BUTTON_ID: "masterpass-btn",
    HOME_SCREEN_MESSAGE:
      "Go on to the Masterpass App on your phone and enter or scan the code below:",
    CODE_ID: "code",
    COPY_CODE_BUTTON_TEXT: "Copy Code",
    COPIED_CODE_BUTTON_TEXT: "Code Copied",
    REFRESH_CODE_BUTTON: {
      LABEL: "Refresh Code",
      ID: "RefreshCode",
    },
  },

  MPESA: {
    PAYMENT_METHOD_SCREEN_BUTTON_ID: "mpesa-secure-btn",
    HOME_SCREEN_MESSAGE: {
      LABEL: "To begin your payment please enter the mobile number linked with your Mpesa account",
      ID: "mpesa-mobile-label",
    },
    MOBILE_INPUT: {
      LABEL: "Mobile Number",
      ID:"mpesa-mobile-input",
    },
    SUBMIT_BUTTON: {
      LABEL: "Verify",
      ID: "mpesa-mobile-btn",
    },
    SUCCESS_NUMBER: "111111111",
    UNSUCCESSFUL_NUMBER: "123456789",
    VERIFYING_DIALOG_TEXT: "Please wait while we verify your mpesa account details.",
    VERIFY_MESSAGE: {
      LABEL: "To verify your Mpesa payments please check the phone that matches this number.",
      ID: "mpesa-otp-label",
    },
    OTP_NUMBER_ID: "mpesa-mobile-value",
    TIMER_LABEL: "Code expires in",
    PAYMENT_DECLINED_MESSAGE:
      "Authentication of this payment has failed. Please go back to try again or select a different payment method.",
    
  },

  OZOW: {
    PAYMENT_METHOD_SCREEN_BUTTON_ID: "#ozow-btn",
    TEST_SUCCESSFUL_RESPONSE_TEXT: "Test Successful Response",
    TEST_ERROR_RESPONSE_TEXT: "Test Error Response",
    TEST_PENDING_INVESTIGATION_RESPONSE_TEXT:
      "Test Pending Investigation Response",
    TEST_ABANDONED_RESPONSE_TEXT: "Test Abandoned Response",
    TEST_PENDING_RESPONSE_TEXT: "Test Pending Response",
  },

  PAYON_CARD: {
    ENTITY_ID: "8ac7a4c76b8d18ee016b926a7b790d6e",
    SECRET_KEY: "fb55db37967a11e9baa702de7a5a0a6e",
    CARD_NUMBER: "411111111111",
  },

  NATIVE_CARD: {
    PAYMENT_METHOD_SCREEN_BUTTON_ID: "card-btn",
    BILLING: {
      COUNTRY: "ZA",
      CITY: "Stellenbosch",
      STREET1: "01 Billing Street",
      POSTCODE: "7600",
    },
    ENTITY_ID: "8a82941764697b1401646e7f10a61d82",
    USER_NOT_ENROLLED_ERROR_TEXT: "user not enrolled",
    NUMBERS: {
      NO_REDIRECT_SUCCESS: "4024007179651566",
      UNAVAILABLE: "4916399301569589",
      NOT_ENROLLED_3D: "5457116362416571",
      CARD_HOLDER_NOT_FOUND: "4263982640269299",
      INVALID_PAYER: "4917484589897107",
      UNSUPPORTED_USER_DEVICE: "4012 8888 8888 1881",
      MALFORMED: "5506 9004 9000 0436",
    },
    ERROR_MESSAGES: {
      UNAVAILABLE: "card not participating/authentication unavailable",
      NOT_ENROLLED_3D: "Card is not enrolled for 3DS version 2",
      CARD_HOLDER_NOT_FOUND:
        "Cardholder Not Found - card number provided is not found in the ranges of the issuer",
      INVALID_PAYER:
        "invalid payer authentication response(PARes) in threeDSecure Transaction",
      UNSUPPORTED_USER_DEVICE:
        "Unsupported User Device - Authentication not possible",
      MALFORMED: "Missing or malformed threeDSecure Configuration for Channel",
    },
    PAY_NOW_BUTTON: "Pay Now",
    CARD: {
      NUMBER: {
        LABEL: "Card Number",
        ID: "#number",
      },
      HOLDER: {
        VALUE: "Ewald Van Rooyen",
        ID: "#holderName",
        LABEL: "Card Holder Name",
      },
      EXPIRY_DATE: {
        LABEL: "Expiry Date",
        ID: "#expiryDate",
        VALUE: "1224",
      },
      CVV: {
        LABEL: "Security Code",
        ID: "#cvv",
        VALUE: "123",
      },
      STREET: {
        LABEL: "Street",
        ID: "#street",
      },
      CITY: {
        LABEL: "City",
        ID: "#city",
      },
      POSTAL_CODE: {
        LABEL: "Postal Code",
        ID: "#postalCode",
      },
      COUNTRY: {
        LABEL: "Country",
        ID: "#country",
      },
    },
  },
  EFT_SECURE: {
    PAYMENT_METHOD_SCREEN_BUTTON_ID: "eft-secure-btn",
    WIDGET_URL: "https://eftsecure.callpay.com/api/v1/eft/",
    OLD_SITE: "Old Site",
    NEW_SITE: "New Site",
    DESCRIPTION:
      "None of your credentials are stored. We simply facilitate the transaction on your behalf to confirm the transaction instantly.",
    BANKS: {
      STANDARD_BANK: "Standard Bank",
      FNB: "First National Bank",
      ABSA: "ABSA",
      NEDBANK: "Nedbank",
      CAPITEC: "Capitec",
      INVESTEC: "Investec",
      TYME_BANK: "Tyme Bank",
      AFRICAN_BANK: "African Bank",
      BIDVEST_BANK: "Bidvest Bank",
      OLD_MUTUAL_BANK: "Old Mutual Bank",
    },
  },
  APLUS: {
    PAYMENT_METHOD_SCREEN_BUTTON_ID: "aplus-radio-card",
    SIGN_IN_MESSAGE_1: "Account Payment Method",
    SIGN_IN_MESSAGE_2: "Please enter your A+ Account card details.",
    CARD_NUMBER: {
      LABEL: "Card Number",
      ID: "aplus-details-cardno",
      VALUE: "1111111111111111",
    },
    EXPIRY_DATE: {
      LABEL: "Expiry Date",
      ID: "aplus-details-cardexpiry",
      VALUE: "0130",
    },
    CELLPHONE_NUMBER: {
      LABEL: "Cellphone Number",
      ID: "aplus-details-cellphone",
      VALUE: "0821234567",
    },
    VERIFY_BUTTON_LABEL: "Verify",
    OTP_MESSAGE: "One Time Password sent to",
    RESEND_OTP: "Resend OTP",
    OTP: {
      LABEL: "",
      ID: "aplus-otp-input",
    },
    PAY_NOW_BUTTON: "Pay Now",
    OTP_INPUT_ID: "aplus-otp-input",
  },
};

export default CONSTANTS;
