import "./style.css";
import { Checkout } from "@peach/checkout-lib";
import { html, render } from "lit-html";

// Get the instance of checkout to communicate with
// This value will change between the demo, sandbox and live environments
// These values are
// demo: https://checkout-demo.ppay.io
// sandbox: https://testsecure.peachpayments.com
// live: https://secure.peachpayments.com
// We reccomend making this a build step configuration
const checkout = Checkout("https://localhost:3000");

// The first thing you need to do is subscribe to the bootstrap
// This will provide your plugin with the information it requires to make a payment
checkout.on("checkout-bootstrap", (event) => {
  // This is our pay method
  const pay = async () => {
    try {
      // Tell checkout to inform the user that something is busy loading
      checkout.loading();

      // This is where you would make a call to your API to complete the payment from your side
      // An example would be:
      // const result = await fetch("/your/api/payment", {
      //   method: "POST",
      //   body: JSON.stringify({
      //     amount: event.config.amount,
      //     transactionId: event.config.transactionId,
      //   }),
      // });

      // Tell checkout the payment has been successful,
      // this will display feedback to the user
      // and redirect them to their store
      checkout.success();
    } catch (e) {
      // Tell checkout the payment has failed
      // This will end the plugin session and allow the user to restart the flow
      checkout.error();
    }
  };
  // Here we are rendering the most basic example plugin
  // It displays the amount being paid and has a button to make the payment
  render(
    html`
      <h1>Paying amount: ${event.data.config.amount}</h1>
      <button @click=${pay}>Pay</button>
    `,
    document.querySelector("#app")
  );

  // Once we have finished rendering our
  // application we inform checkout that we are ready
  // this will stop the loading indicator and show your application
  checkout.initialized();
});

// The second thing you need to do is notify checkout
// that your plugin has finished loading and is
// ready to receive its configuration
// this method will do two things
// 1. Indicate to the user that the plugin is loading
// 2. Send the checkout bootstrap event to your plugin
checkout.initialize();
