import React, { useContext, useMemo } from "react";
import LoadingScreen from "../../../../components/loaders/loadingScreen";
import { PAYMENT_METHOD_NAMES } from "../../../../utils/pluginConstants";
import ErrorScreen from "../../../../components/errors/screens/errorScreen";
import FeedbackPage from "../shared/FeedbackPage";
import { PEACH_STATE } from "../../../../utils/constants";
import CardForm from "../../../../design-system/templates/card-form/cardForm";
import PayOnCardHtml from "../../../../components/html/payOnCardHtml";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";
import { GoogleAnalyticsContext } from "../../../../contexts/GoogleAnalyticsContext";
import { getAvailableCardBrands } from "../../../../design-system/templates/card-form/cardFormUtils";
import { CardPostData } from "../../../../design-system/templates/card-form/cardFormInterfaces";

export interface CardPaymentProps {
  showLoadingScreen: boolean;
  showErrorScreen: boolean;
  showSuccessScreen: boolean;
  showCardCaptureScreen: boolean;

  code: string;
  redirectUrl: string;

  redirectPostData: {};

  redirectToClient: (state: string) => void;
  postNativeCardContent: (parameters: CardPostData) => void;
}

const CardPayment = (props: CardPaymentProps) => {
  const checkout = useContext(CheckoutContext);
  const googleAnalytics = useContext(GoogleAnalyticsContext);

  const cardBrands = useMemo(
    () => getAvailableCardBrands(checkout.paymentMethods),
    [checkout.paymentMethods]
  );

  if (props.showLoadingScreen) {
    return <LoadingScreen widgetName={PAYMENT_METHOD_NAMES.CARD} />;
  }

  if (props.showErrorScreen) {
    return (
      <ErrorScreen
        title={`The ${PAYMENT_METHOD_NAMES.CARD} payment method is unavailable`}
      />
    );
  }

  if (props.showSuccessScreen) {
    return (
      <FeedbackPage
        paymentMethod={PAYMENT_METHOD_NAMES.CARD}
        code={props.code}
        status={PEACH_STATE.SUCCESS}
        isTimed
        onRedirect={() => {
          props.redirectToClient(PEACH_STATE.SUCCESS);
        }}
        redirectPostData={props.redirectPostData}
        redirectUrl={props.redirectUrl}
      />
    );
  }

  if (props.showCardCaptureScreen) {
    return (
      <CardForm
        cardBrands={cardBrands}
        onSubmit={props.postNativeCardContent}
      />
    );
  }
  return <PayOnCardHtml />;
};

export default CardPayment;
