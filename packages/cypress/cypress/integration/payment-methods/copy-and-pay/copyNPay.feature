Feature: Copy and Pay
    In order to make a purchase
    As a customer
    I want to pay using my bank card

    Background: Launch Copy and Pay widget
        Given I Generate Checkout via API using with merchant billing details
        When I click on "CopyNPay-radio-card"
        Then I should see that the element with data-testid "display-label-go-back" contains the text "Go Back" to "be.visible"
        When I switch to the iframe
        Then I should see "card-form-title" containing the text "Please enter your card details below to complete the payment." in the iframe
        And I should see that the "wpwl-label-brand" contains the text "Brand" inside the payon form
        And I should see that the "wpwl-label-cardNumber" contains the text "Card Number" inside the payon form
        And I should see that the "wpwl-label-expiry" contains the text "Expiry Date" inside the payon form
        And I should see that the "wpwl-group-cardHolder" contains the text "Card holder" inside the payon form
        And I should see that the "wpwl-label-cvv" contains the text "CVV" inside the payon form

    Scenario: Field Validation
        When I click on the pay now button in the frame
        Then I should see that the "wpwl-hint-cardNumberError" contains the text "Invalid card number" inside the payon form
        And I should see that the "wpwl-hint-expiryMonthError" contains the text "Invalid expiry date" inside the payon form
        And I should see that the "wpwl-hint-cvvError" contains the text "Invalid CVV entered" inside the payon form