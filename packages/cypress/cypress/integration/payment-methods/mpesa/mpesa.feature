Feature: M-Pesa
    In order to make a purchase
    As a customer
    I want to pay using my M-Pesa account

    Background: Launch M-Pesa modal and login
        Given I Generate Checkout via API using "1" as my test amount as a Kenyan merchant        
        When I click on "mpesa-secure-btn"
        Then I should see the text "To begin your payment please enter the mobile number linked with your Mpesa account"
        And I should see that the "mpesa-mobile-btn" button is disabled

    Scenario: Enter number and verify with a valid number
        When I enter "111111111" on the field "mpesa-mobile-input"
        And I click on "mpesa-mobile-btn"
        Then I should see awaiting phone verify notification

    Scenario: Enter number and verify to make unsuccessful payment
        When I enter "123456789" on the field "mpesa-mobile-input"
        And I click on "mpesa-mobile-btn"
        Then I should see the text "Payment Declined"
        And I should see the text "Authentication of this payment has failed. Please go back to try again or select a different payment method."
        When I click the "Go Back" inside "warning" modal
        
        Then I should see the text "To begin your payment please enter the mobile number linked with your Mpesa account"
