import { makeStyles } from "@material-ui/core/styles";
import { white } from "@peach/checkout-design-system/dist/variables";

const useStyles = makeStyles(() => ({
  paper: {
    position: "absolute",
    outline: 0,
    borderRadius: "8px",
    width: "auto",
    minWidth: "290px",
    paddingTop: "24px",
    paddingBottom: "24px",
    paddingLeft: "48px",
    paddingRight: "48px",
    background: white,
    height: "auto",
  },
}));

export default useStyles;
