import { Box } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckCircleOutlinedIcon from "@material-ui/icons/CheckCircleOutlined";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import ReportProblemOutlinedIcon from "@material-ui/icons/ReportProblemOutlined";
import React from "react";
import { info, success, warning } from "../../../variables";

export interface FeedbackIconProps {
  // Feedback Type will set style and icon
  feedback: FeedbackType;
  // Controls the size of the icon
  size?: IconSize;
}

export enum FeedbackType {
  INFO = "info",
  WARNING = "warning",
  SUCCESS = "success",
  LOADING = "loading",
  PENDING = "pending",
  TIMER = "timer",
  ERROR = "error",
  NONE = "none",
}

export enum IconSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large",
}

const getFeedbackIconSize = (size: string | undefined): string => {
  switch (size) {
    case IconSize.SMALL:
      return "24px";
    case IconSize.MEDIUM:
      return "30px";
    case IconSize.LARGE:
      return "50px";
    default:
      return "24px";
  }
};

const renderFeedbackIconJsx = (
  feedback: string,
  iconDiameters: string
): React.ReactNode => {
  switch (feedback) {
    case FeedbackType.ERROR:
      return (
        <ErrorOutlineIcon
          style={{ height: iconDiameters, width: iconDiameters }}
          color="error"
        />
      );
    case FeedbackType.WARNING:
      return (
        <ReportProblemOutlinedIcon
          style={{
            color: warning,
            height: iconDiameters,
            width: iconDiameters,
          }}
        />
      );
    case FeedbackType.INFO:
      return (
        <InfoOutlinedIcon
          style={{ color: info, height: iconDiameters, width: iconDiameters }}
        />
      );
    case FeedbackType.SUCCESS:
      return (
        <CheckCircleOutlinedIcon
          style={{
            color: success,
            height: iconDiameters,
            width: iconDiameters,
          }}
        />
      );
    case FeedbackType.LOADING:
      return (
        <CircularProgress
          style={{ color: info, height: iconDiameters, width: iconDiameters }}
        />
      );
    case FeedbackType.PENDING:
      return (
        <HourglassEmptyIcon
          style={{ color: info, height: iconDiameters, width: iconDiameters }}
        />
      );
    case FeedbackType.TIMER:
      return (
        <HourglassEmptyIcon
          style={{
            color: warning,
            height: iconDiameters,
            width: iconDiameters,
          }}
        />
      );
    default:
      return null;
  }
};

export const FeedbackIcon = (props: FeedbackIconProps) => {
  const size = getFeedbackIconSize(props.size);

  return (
    <Box display="flex" justifyContent="center">
      {renderFeedbackIconJsx(props.feedback, size)}
    </Box>
  );
};
