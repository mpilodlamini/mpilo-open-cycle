import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  wrapper: {
    minHeight: "100vh",

    [theme.breakpoints.down("xs")]: {
      alignItems: "flex-start",
    },
  },
  container: {
    borderRadius: "8px",
    height: "calc(100vh - 10vh)",
    minHeight: "calc(100vh - 10vh)",
    overflow: "hidden",
    boxShadow: "0px 0px 20px 0px rgba(10,31,68,0.1)",
    maxWidth: "764px",
    position: "relative",

    [theme.breakpoints.down("xs")]: {
      height: "100vh",
      minHeight: "100vh",
    },
  },
}));

export default useStyles;
