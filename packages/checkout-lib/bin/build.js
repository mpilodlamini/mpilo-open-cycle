const esbuild = require("esbuild");
const fs = require("fs/promises");
const path = require("path");
const { nodeExternalsPlugin } = require("esbuild-node-externals");

const base = {
  entryPoints: ["src/index.ts"],
  bundle: true,
  platform: "browser",
  watch: process.env.NODE_ENV === "production" ? false : true,
  plugins: [nodeExternalsPlugin()],
  sourcemap: true,
};

const main = async () => {
  Promise.all([
    // esbuild.build({ ...base, format: "cjs", outfile: "dist/index.js" }),
    esbuild.build({ ...base, format: "esm", outfile: "dist/index.esm.js" }),
  ]).catch(() => process.exit(1));
};

main();
