import { FeedbackType, ModalDialog } from "@peach/checkout-design-system";
import React from "react";
import Mpesa from "../../../../design-system/templates/mpesa/mpesa";
import CheckoutService from "../../../../services/checkoutService";
import PluginManager from "../../../../utils/pluginManager";
import MessagesUtils from "../../../../utils/messagesUtil";
import MpesaOtp from "../../../../design-system/templates/mpesa/otp/mpesaOtp";
import FeedbackPage from "../shared/FeedbackPage";
import {
  AJAX_DATA_RESULT,
  PEACH_STATE,
  TRANSACTION_STATUS,
} from "../../../../utils/constants";
import {
  PAYMENT_METHOD_ENUMS,
  PAYMENT_METHOD_NAMES,
} from "../../../../utils/pluginConstants";
import { AJAX } from "../../../../utils/ajaxConstants";
import { GA } from "../../../../utils/googleAnalyticsConstants";
import LoadingScreen from "../../../../components/loaders/loadingScreen";
import { BUTTON_LABELS, MESSAGING } from "../../../../utils/messagingConstants";
import { printInfo } from "../../../../utils/logger";
import TimeUtil from "../../../../utils/timeUtil";
import { CheckoutConfig } from "../../../../contexts/CheckoutContext";
import GoogleAnalyticsUtil from "../../../../utils/googleAnalyticsUtil";
import ErrorModal from "../../../../components/errors/modals/errorModal";

export interface MpesaContainerProps {
  timeUtil: TimeUtil;

  isDefaultAndForced: boolean;

  setCancelVisibility: (state: boolean) => void;
  resetSelected: () => void;

  checkout: CheckoutConfig;
  googleAnalytics: GoogleAnalyticsUtil;
}

export interface MpesaContainerState {
  showLoadingModal: boolean;
  showErrorModal: boolean;
  showLoadingScreen: boolean;
  showSuccessScreen: boolean;
  showMpesaOtpScreen: boolean;

  shouldErrorModalRedirect: boolean;
  errorMessage: string;
  errorModalBackButtonLabel: string;
  errorModalActionButtonLabel: string;

  loaderHeading: string;
  loaderMessage: string;

  code: string;
  status: string;
  feedbackTitle: string;
  transactionId: string;
  phoneNumber: string;
}

const componentName = "MpesaContainer";
const paymentBrand = PAYMENT_METHOD_ENUMS.MPESA;

const messagesUtils: MessagesUtils = new MessagesUtils();
const errorMessageObject = messagesUtils.getErrorMessages();
const successCodes = messagesUtils.getSuccessCodes();

/**
 * Container component for all mpesa child components.
 * mpesa and MpesaOtp and also handles all the logic for the payment method
 */
export class MpesaContainer extends React.Component<
  MpesaContainerProps,
  MpesaContainerState
> {
  private readonly checkoutService: CheckoutService = new CheckoutService();

  private readonly _pluginManager: PluginManager = new PluginManager(
    this.props.timeUtil
  );

  private _mpesaTimer: NodeJS.Timeout | null = null;

  private readonly _loadingScreen = (
    <LoadingScreen widgetName={PAYMENT_METHOD_NAMES.MPESA} />
  );

  constructor(props: MpesaContainerProps) {
    super(props);

    this.state = {
      showLoadingModal: false,
      showErrorModal: false,
      showSuccessScreen: false,
      showMpesaOtpScreen: false,
      shouldErrorModalRedirect: false,
      showLoadingScreen: true,

      loaderHeading: MESSAGING.MPESA.DEFAULT_LOADING_HEADING,
      loaderMessage: MESSAGING.MPESA.DEFAULT_LOADING_MESSAGE,
      feedbackTitle: MESSAGING.DEFAULT_ERROR_HEADING,
      errorMessage: MESSAGING.DEFAULT_ERROR_MESSAGE,
      errorModalBackButtonLabel: BUTTON_LABELS.GO_BACK,

      errorModalActionButtonLabel: BUTTON_LABELS.BLANK_LABEL,
      transactionId: "",
      code: "",
      status: "",
      phoneNumber: "",
    };
  }

  componentDidMount = (): void => {
    printInfo(componentName, "componentDidMount", "Mounted");

    const loaderTimeout = setTimeout(() => {
      this.setState({ showLoadingScreen: false });
      clearInterval(loaderTimeout);
    }, 1000);
  };

  componentWillUnmount = (): void => {
    this.checkoutService.cancelRequest();
    this._pluginManager.stopPollingListener();
  };

  postWidgetData = (phoneNumber: string): void => {
    this.props.googleAnalytics.trackPaymentInformation();
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.VERIFY,
      PAYMENT_METHOD_ENUMS.MPESA,
      "mobile"
    );
    this.setState({ phoneNumber, showLoadingModal: true });
    this.submitMpesaNumber(phoneNumber);
  };

  submitMpesaNumber = (phoneNumber: string): void => {
    const url: string = `${this.props.checkout.baseUrl}payment`;

    const postData = {
      merchant_key: this.props.checkout.merchantKey,
      checkout_id: this.props.checkout.checkoutId,
      payment_brand: paymentBrand,
      accountId: `254${phoneNumber}`,
    };

    const successCallback = (data: any) => {
      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        this.props.timeUtil.updateCheckoutTimer();
        this._pluginManager.createPollingListener();

        const code: string = data.response.response_data.result.code || "";
        const status: string = PEACH_STATE.SUCCESS;
        const transactionId: string = data.response.response_data.id || "";

        this.setState(
          {
            code,
            status,
            transactionId,
            showMpesaOtpScreen: true,
            showLoadingModal: false,
          },
          () => {
            this.getMPesaStatus();
          }
        );
      }
    };

    const errorCallback = (data: any) => {
      this.setState({ showLoadingModal: false });

      let code: string =
        data && data.responseJSON && data.responseJSON.response.code;
      const status: string = PEACH_STATE.FAILED;
      let isRedirectError: boolean = false;
      let redirectBackButton: string = BUTTON_LABELS.GO_BACK;
      let redirectActionButton: string = BUTTON_LABELS.BLANK_LABEL;

      if (!code && data && data.response && data.response.response_code) {
        code = data.response.response_code;
      }

      if (
        !code &&
        data.responseJSON &&
        data.responseJSON.response &&
        data.responseJSON.response.response_data
      ) {
        code = data.responseJSON.response.response_data.result.code;
      }

      if (!code || messagesUtils.getErrorCodes().indexOf(code) === -1) {
        code = "900.100";
      }

      if (code === "800.110.100") {
        isRedirectError = true;
        redirectBackButton = "";
        redirectActionButton = BUTTON_LABELS.RETURN_TO_STORE;
      }

      const feedbackTitle: string = errorMessageObject[code]?.heading || "";
      const errorMessage: string = errorMessageObject[code]?.message || "";

      this.setState({
        code,
        status,
        errorMessage,
        feedbackTitle,
        showErrorModal: true,
        shouldErrorModalRedirect: isRedirectError,
        errorModalBackButtonLabel: redirectBackButton,
        errorModalActionButtonLabel: redirectActionButton,
      });

      this.logGoogleErrorMessages(feedbackTitle, code);
    };

    this.checkoutService.postFormEncodedContent(
      url,
      postData,
      successCallback,
      errorCallback
    );
  };

  logGoogleErrorMessages = (errorHeader: string, code: string): void => {
    if (messagesUtils.errorHeaders.indexOf(errorHeader) !== -1) {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.ERROR,
        PAYMENT_METHOD_ENUMS.MPESA,
        code
      );
      this.props.googleAnalytics.trackErrorEvents(
        errorHeader,
        PAYMENT_METHOD_ENUMS.MPESA,
        code
      );
    } else {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.ERROR,
        PAYMENT_METHOD_ENUMS.MPESA,
        code
      );
    }
  };

  closeErrorModal = (): void => {
    this.props.googleAnalytics.trackEvent(
      GA.EVENTS.ERROR_GO_BACK,
      PAYMENT_METHOD_ENUMS.MPESA
    );
    this.setState({ showErrorModal: false });

    if (!this.props.isDefaultAndForced) {
      this.props.googleAnalytics.trackEvent(
        GA.EVENTS.COLLAPSE,
        PAYMENT_METHOD_ENUMS.MPESA
      );
      this.props.resetSelected();
    }
  };

  successScreenRedirectCallBack = (): void => {
    this.redirectLoaderOnAction(TRANSACTION_STATUS.SUCCESSFUL);
  };

  cancelCheckout = (): void => {
    this.redirectLoaderOnAction(TRANSACTION_STATUS.CANCELLED);
  };

  getMPesaStatus = (): void => {
    this.props.googleAnalytics.trackPage(GA.PAGES.MPESA_VERIFICATION);
    const url: string = `${this.props.checkout.switchBaseUrl}/internal/verify/${this.state.transactionId}/status/`;

    const successCallback = (data: any) => {
      if (data.status === AJAX.CODES.SUCCESS) {
        if (this._mpesaTimer) {
          clearInterval(this._mpesaTimer);
        }

        const code: string = (data && data.data && data.data.result_code) || "";

        if (successCodes.indexOf(code) !== -1) {
          this.setState({
            code,
            status: PEACH_STATE.SUCCESS,
            showSuccessScreen: true,
          });
        } else if (code === "100.380.501") {
          const status: string = PEACH_STATE.FAILED;
          const feedbackTitle = errorMessageObject[code].heading;
          const errorMessage = errorMessageObject[code].message;

          this.setState({
            code,
            status,
            feedbackTitle,
            errorMessage,
            showErrorModal: true,
            showSuccessScreen: false,
            showMpesaOtpScreen: false,
          });
          this.logGoogleErrorMessages(feedbackTitle, code);
        } else if (code === "100.396.101") {
          const isRedirectError = true;
          this.setState({
            code,
            errorModalBackButtonLabel: BUTTON_LABELS.BLANK_LABEL,
            feedbackTitle: MESSAGING.DEFAULT_PAYMENT_FAILED_ERROR_HEADER,
            errorMessage: MESSAGING.DEFAULT_ERROR_MESSAGE_REDIRECT,
            errorModalActionButtonLabel: BUTTON_LABELS.RETURN_TO_STORE,
            shouldErrorModalRedirect: isRedirectError,
            showErrorModal: true,
            showLoadingModal: false,
          });
          this.logGoogleErrorMessages("Payment Failed", code);
        } else {
          const status: string = PEACH_STATE.FAILED;
          const feedbackTitle: string =
            (errorMessageObject[code] && errorMessageObject[code].heading) ||
            MESSAGING.DEFAULT_ERROR_HEADING;
          const errorMessage: string =
            (errorMessageObject[code] && errorMessageObject[code].message) ||
            MESSAGING.DEFAULT_ERROR_MESSAGE;

          this.setState({
            code,
            status,
            feedbackTitle,
            errorMessage,
            showErrorModal: true,
            shouldErrorModalRedirect: false,
            showMpesaOtpScreen: false,
            showSuccessScreen: false,
          });
          this.logGoogleErrorMessages(feedbackTitle, code);
        }
      }
    };

    this._mpesaTimer = setInterval(() => {
      this.checkoutService.getContent(url, successCallback);
    }, 5000);
  };

  redirectLoaderOnAction = (transactionStatus: string): void => {
    const loaderHeading: string = `Redirecting to ${this.props.checkout.merchant}`;
    const loaderMessage: string = "Please wait you are being redirected";

    this.setState({ loaderHeading, loaderMessage, showLoadingModal: true });
    this._pluginManager.updateTransactionStatus(
      transactionStatus,
      this.state.transactionId,
      this.state.code
    );
  };

  render = () => {
    const errorModal = (
      <ErrorModal
        open={this.state.showErrorModal}
        title={this.state.feedbackTitle}
        label={this.state.errorMessage}
        continueText={this.state.errorModalActionButtonLabel}
        cancelText={this.state.errorModalBackButtonLabel}
        onClick={this.cancelCheckout}
        onCancel={this.closeErrorModal}
        timed={this.state.shouldErrorModalRedirect}
      />
    );

    const feedbackPage = (
      <FeedbackPage
        paymentMethod={PAYMENT_METHOD_NAMES.MPESA}
        code={this.state.code}
        status={this.state.status}
        isTimed
        onRedirect={this.successScreenRedirectCallBack}
      />
    );

    const loadingModal = (
      <ModalDialog
        type={FeedbackType.LOADING}
        title={this.state.loaderHeading}
        label={this.state.loaderMessage}
        open={this.state.showLoadingModal}
        forceBottom
      />
    );

    const mpesaScreen = <Mpesa onClick={this.postWidgetData} />;

    const mpesaOtpScreen = (
      <MpesaOtp
        mobileValue={this.state.phoneNumber}
        timeUtil={this.props.timeUtil}
      />
    );

    return (
      <>
        {!this.state.showLoadingScreen &&
          !this.state.showMpesaOtpScreen &&
          !this.state.showSuccessScreen &&
          mpesaScreen}
        {!this.state.showLoadingScreen &&
          !this.state.showSuccessScreen &&
          this.state.showMpesaOtpScreen &&
          mpesaOtpScreen}
        {this.state.showLoadingScreen && this._loadingScreen}
        {!this.state.showLoadingScreen && errorModal}
        {this.state.showSuccessScreen && feedbackPage}
        {loadingModal}
      </>
    );
  };
}

export default MpesaContainer;
