import React from "react";
import { ConfigProvider, ConfigContext } from "./contexts/ConfigContext";
import { checkout } from "./lib/checkout";
import { OneForYouContainer } from "./one-for-you";
import { ThemeProvider } from "@peach/checkout-design-system";

function App() {
  return (
    <ConfigProvider>
      <ThemeProvider>
        <OneForYouContainer />
      </ThemeProvider>
    </ConfigProvider>
  );
}

export default App;
