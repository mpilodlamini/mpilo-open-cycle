import React from "react";

export interface WidgetWrapper {
  postWidgetData(stringParam?: string): void;

  componentDidMount(): void;

  render():
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined;
}
