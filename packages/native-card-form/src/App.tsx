import React, { useEffect, useState } from "react";
import { ConfigProvider } from "./contexts/ConfigContext";
import { CardForm } from "./form";
import { checkout } from "./lib/checkout";

const wait = (ms = 1000) =>
  new Promise((resolve) =>
    setTimeout(() => {
      resolve(undefined);
    }, ms)
  );

function App() {
  return (
    <ConfigProvider>
      <CardForm
        handleOnSubmit={async (data) => {
          checkout.publish([{ type: "status", value: "loading" }]);
          await wait();
          checkout.publish([{ type: "status", value: "success" }]);
        }}
      />
    </ConfigProvider>
  );
}

export default App;
