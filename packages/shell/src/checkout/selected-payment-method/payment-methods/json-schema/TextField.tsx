import * as React from "react";
import { TextFieldProps as MuiTextFieldProps } from "@material-ui/core/TextField";
import { TextField as BaseTextField } from "@peach/checkout-design-system/dist/controls/text-field";
import { InputLabel } from "@peach/checkout-design-system/dist/controls/input-label";

import { FieldProps, getIn } from "formik";
import { Box } from "@material-ui/core";

export interface TextFieldProps
  extends FieldProps,
    Omit<MuiTextFieldProps, "name" | "value" | "error"> {}

export function fieldToTextField({
  disabled,
  field: { onBlur: fieldOnBlur, ...field },
  form: { isSubmitting, touched, errors },
  onBlur,
  helperText,
  ...props
}: TextFieldProps): MuiTextFieldProps {
  const fieldError = getIn(errors, field.name);
  const showError = getIn(touched, field.name) && !!fieldError;

  return {
    variant: props.variant,
    error: showError,
    helperText: showError ? fieldError : helperText,
    disabled: disabled ?? isSubmitting,
    onBlur:
      onBlur ??
      function (e) {
        fieldOnBlur(e ?? field.name);
      },
    ...field,
    ...props,
  };
}

export function TextField({ children, label, ...props }: TextFieldProps) {
  const innerProps = fieldToTextField(props);
  // @ts-ignore
  return (
    <Box>
      <InputLabel error={innerProps.error} htmlFor={props.field.name}>
        {label}
      </InputLabel>
      <BaseTextField {...innerProps} variant="outlined">
        {children}
      </BaseTextField>
    </Box>
  );
}

TextField.displayName = "FormikMaterialUITextField";
