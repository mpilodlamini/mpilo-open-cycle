import { makeStyles } from "@material-ui/core/styles";
import {
  border,
  info,
  primaryBlue,
  secondaryBackgroundGrey,
  secondaryGrey,
  tertiaryGrey,
  white,
} from "@peach/checkout-design-system";

export const useStyles = makeStyles((theme) => ({
  backCardContainer: {
    marginTop: "24px",
    maxWidth: "320px",
    margin: "auto",
  },
  backCard: {
    paddingTop: "30px",
  },
  backCardPaper: {
    width: "100%",
    background: secondaryBackgroundGrey,
  },
  backCardStrip: {
    backgroundColor: secondaryGrey,
    height: "40px",
  },
  backCardWrapper: {
    paddingRight: "16px",
    paddingLeft: "16px",
    [theme.breakpoints.up("sm")]: {
      paddingRight: "24px",
      paddingLeft: "24px",
    },
  },
  backCardBody: {
    paddingTop: "16px",
    paddingBottom: "16px",
    [theme.breakpoints.up("sm")]: {
      paddingTop: "32px",
      paddingBottom: "48px",
    },
  },
  backCardSignature: {
    fontStyle: "italic",
    fontWeight: 400,
    lineHeight: "20px",
    border: `1px solid ${border}`,
    borderRadius: "4px",
    color: tertiaryGrey,
    background: white,

    "& .MuiBox-root": {
      fontSize: "11px",
      "@media (min-width: 650px)": {
        fontSize: "14px",
      },
    },
  },
  backCardCVV: {
    margin: "auto",
    fontSize: "14px",
    fontWeight: 600,
    letterSpacing: "2.5px",
    textAlign: "center",
    color: primaryBlue,
    border: `2px solid ${info}`,
    background: white,
    borderRadius: "4px",
    paddingLeft: "calc(48% - 100px)",
    paddingRight: "calc(48% - 100px)",
  },
}));

export default useStyles;
