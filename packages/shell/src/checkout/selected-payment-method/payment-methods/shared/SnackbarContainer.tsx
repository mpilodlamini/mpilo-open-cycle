import { SnackbarStatic } from "@peach/checkout-design-system";
import React, { useCallback, useEffect, useState } from "react";
import { printInfo } from "../../../../utils/logger";
import TimeUtil from "../../../../utils/timeUtil";

interface SnackbarContainerProps {
  timeUtil: TimeUtil;
  time: number;
}

const componentName: string = "SnackbarContainer";

const SnackbarContainer = (props: SnackbarContainerProps) => {
  const [time, setTime] = useState(`${props.time.toString()}:00`); // Always provide minutes

  const updateTime = useCallback(
    (newTime: number): void => {
      const minutes = Math.floor(newTime / 60);
      const seconds = newTime % 60;
      const minutesDisplay = minutes < 10 ? `0${minutes}` : minutes;
      const secondsDisplay = seconds < 10 ? `0${seconds}` : seconds;
      setTime(`${minutesDisplay}:${secondsDisplay}`);

      if (newTime === 0) {
        printInfo(
          componentName,
          "updateTime",
          `${props.time.toString()}:00 timer has expired`
        );
      }
    },
    [props.time]
  );

  useEffect(() => {
    printInfo(componentName, "componentDidMount", "Mounted");

    props.timeUtil.createTimer(props.time * 60, updateTime);

    return () => {
      props.timeUtil.stopShortTermTimer();
    };
  }, [props.time, props.timeUtil, updateTime]);

  return (
    <SnackbarStatic
      dataTestId="snackbar-container-warning"
      title=""
      label="Code expires in"
      time={time}
      timed
    />
  );
};

export default SnackbarContainer;
