export const isValidEmailAddress = (email: string): boolean => {
  const emailReg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailReg.test(email);
};

export const isNumerical = (value: string): boolean => {
  if (value === "") return true;
  const numericalRegexPattern = /^\d+$/;
  return numericalRegexPattern.test(value);
};

export const isValidLuhnNumber = (cardNumber: string): boolean => {
  let sum: number = 0;
  let isEvenNumber: boolean = false;

  for (let i = cardNumber.length - 1; i >= 0; i--) {
    let currentValue: number = parseInt(cardNumber[i], 10);

    if (isEvenNumber) {
      if ((currentValue *= 2) > 9) currentValue -= 9;
    }

    sum += currentValue;
    isEvenNumber = !isEvenNumber;
  }

  return sum % 10 === 0;
};
