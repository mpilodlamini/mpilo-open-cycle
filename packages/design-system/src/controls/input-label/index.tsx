import React from "react";
import { InputLabel as Base, InputLabelProps } from "@material-ui/core";
import useStyles from "./input-label.styles";

export const InputLabel = (props: InputLabelProps) => {
  const { root } = useStyles(props);

  return <Base className={root} {...props} />;
};
