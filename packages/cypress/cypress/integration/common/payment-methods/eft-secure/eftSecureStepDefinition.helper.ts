import { When, Then } from "cypress-cucumber-preprocessor/steps";

const createBankLoginScreenMessage = (bankName: string): string => {
  return `Login using your ${bankName} internet banking profile.`;
};

// ******** INPUTKEYS
When(
  "I enter {string} in {string} within {string}",
  (testData, textInput, container) => {
    cy.getElementById(container)
      .find(`[name="${textInput}"]`)
      .type(testData);
  }
);

// ******** CLICKING
When("I click on {string} within {string}", (element, container) =>
  cy.getElementById(container).find(`[name="${element}"]`).click()
);

// ******** ASSERTIONS
Then(
  "I should see that the element with id {string} contains subtitle for {string} internet banking",
  (container, bankName) => {
    cy.getElementById(container)
      .contains(createBankLoginScreenMessage(bankName))
      .should("be.visible");
  }
);

Then("I should see the internet banking data privacy disclaimer", () => {
  cy.getElementById("eftx-subtitle")
    .contains(
      "None of your credentials are stored. We simply facilitate the transaction on your behalf to confirm the transaction instantly."
    )
    .should("be.visible");
});
