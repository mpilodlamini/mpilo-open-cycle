Feature: One For You Payment Plugin Notifications

    Scenario Outline: Testing various plugin payment notifications
        Given I Generate Checkout via API using "<testAmount>" as my test amount
        When I click on "1ForYou-radio-card"
        And I switch to the iframe
        Then I should see "1ForYou-warning" containing the text "Partial Payments Not Supported" in the iframe
        And I should see that the "1ForYou-submit" button is disabled in the iframe
        When I enter "<voucherPin>" on the field "1ForYou-pin" in the iframe
        And I click on "1ForYou-submit" in the iframe
        And it finishes "loading"
        Then I should see payment "<paymentNotification>" notification
        Examples:
            | testAmount | voucherPin       | paymentNotification |
            | 10.00      | 1212121212121212 | payment successful  |
            | 14.00      | 1111111111111111 | payment declined    |
            | 0.01       | 1212121212121212 | no balance          |
            | 0.02       | 1212121212121212 | invalid voucher     |
            | 0.03       | 1212121212121212 | payment error       |
            | 0.04       | 1212121212121212 | voucher expired     |
            | 1.00       | 1212121212121212 | amount not matching |
            | 3.00       | 1212121212121212 | payment pending     | 