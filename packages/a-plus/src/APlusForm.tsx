import React, { useContext } from "react";
import {
  info,
  white,
  SnackbarStatic,
  FeedbackType,
} from "@peach/checkout-design-system";
import {
  makeStyles,
  Theme,
  Box,
  Grid,
  Button,
  InputAdornment,
} from "@material-ui/core";
import { ConfigContext } from "./contexts/ConfigContext";
import { Formik, Field } from "formik";
import * as yup from "yup";
import { NumberFormat } from "./MUITextField";
import CallIcon from "@material-ui/icons/Call";

export const useStyles = makeStyles((theme: Theme) => ({
  textField: {
    marginRight: "8px",

    [theme.breakpoints.down("xs")]: {
      marginRight: "0",
      marginBottom: "8px",
    },
  },
  button: {
    border: `1px solid ${info}`,
    backgroundColor: info,
    color: white,
    textTransform: "none",
    margin: "auto 0 auto auto",
    width: "auto",

    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },

    "&:hover": {
      borderColor: "#d5d5d5",
    },
    "&:disabled": {
      borderColor: "#C9CED6",
      cursor: "not-allowed",
    },
  },
  inputFormat: {
    position: "absolute",
    top: "41px",
    left: "15px",
    color: "black",
    fontSize: "0.9rem",
    fontWeight: "bold",
  },
  snackbarTimer: {
    width: "100%",
    position: "relative",
    marginTop: "30px",
  },
  aplusIcon: {
    width: "68px",
    height: "42px",
    marginLeft: "8px",
  },
}));

export const APlusForm = ({
  onSubmit,
}: {
  onSubmit: (values: {
    cardNo: string;
    cardExpiry: string;
    cellPhone: string;
  }) => void;
}) => {
  const classes = useStyles();
  const {
    config: checkout,
    time: { timeLeft },
  } = useContext(ConfigContext);

  return (
    <Box width={1} height={1}>
      <Box display="flex" justifyContent="center" mb={3}>
        <img
          data-testid="aplus-details-logo"
          src="images/a+.svg"
          alt="A+"
          className={classes.aplusIcon}
        />
      </Box>

      <Box mt={3}>
        <Formik
          enableReinitialize
          validationSchema={yup.object({
            cardNo: yup
              .string()
              .required("Required")
              .min(19, "Too short. Must be 16 digits")
              .matches(
                /^([123456789][0-9]{3})\s([0-9]{4})\s([0-9]{4})\s([0-9]{4})$/,
                "Please capture a valid card number of 16 digits"
              ),
            cardExpiry: yup
              .string()
              .required("Required")
              .min(5)
              .matches(
                /^((0[1-9])|(1[0-2]))[\\/]*(^(1[0-9])|([2-9][0-9]))$/,
                "Please capture valid expiry date in the format MM/YY."
              ),
            cellPhone: yup
              .string()
              .required("Required")
              .min(12, "Too short. Must be 10 digits")
              .matches(
                /^(0)([0-9]{2})\s([0-9]){3}\s([0-9]){4}$/,
                "Please capture a valid cell phone number of 10 digits."
              ),
          })}
          initialValues={{ cellPhone: "", cardNo: "", cardExpiry: "" }}
          onSubmit={onSubmit}
        >
          {(props) => (
            <form onSubmit={props.handleSubmit}>
              <Box
                mb={2}
                className={classes.textField}
                data-testid="aplus-details-label"
              >
                <Box textAlign="center">Account Payment Method</Box>
                <Box mb={4} textAlign="center">
                  Please enter your A+ Account card details.
                </Box>
              </Box>
              <Grid container>
                <Grid item sm={10} xs={12}>
                  <Box mb={2} className={classes.textField}>
                    <Field
                      data-testid="aplus-details-cardno"
                      autoFocus
                      label="Card Number"
                      type="text"
                      name="cardNo"
                      component={NumberFormat}
                      fullWidth
                      id="cardNo"
                      format="#### #### #### ####"
                      aria-describedby="cardNo"
                      variant="outlined"
                      placeholder="1234 5678 8765 4321"
                      autoComplete="cc-number"
                    />
                  </Box>
                </Grid>
                <Grid item sm={2} xs={12}>
                  <Box mb={2}>
                    <Field
                      data-testid="aplus-details-cardexpiry"
                      label="Expiry Date"
                      type="text"
                      name="cardExpiry"
                      component={NumberFormat}
                      format="##/##"
                      placeholder="MM / YY"
                      fullWidth
                      id="cardExpiry"
                      aria-describedby="cardExpiry"
                      variant="outlined"
                    />
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box mb={2}>
                    <Field
                      data-testid="aplus-details-cellphone"
                      type="text"
                      label="Cell Phone Number"
                      sublabel="Please use the cell phone number associated with your A+ Account"
                      allowLeadingZeros
                      name="cellPhone"
                      component={NumberFormat}
                      fullWidth
                      id="cellPhone"
                      aria-describedby="cellPhone"
                      variant="outlined"
                      format="### ### ####"
                      placeholder="072 123 4567"
                      countrycode={true}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <CallIcon />
                          </InputAdornment>
                        ),
                      }}
                    />
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <Box style={{ textAlign: "right", width: "100%" }}>
                    <Button
                      data-testid="aplus-details-btn"
                      type="submit"
                      className={classes.button}
                      variant="contained"
                      disableElevation
                      disabled={
                        !props.dirty || !props.isValid || props.isSubmitting
                      }
                    >
                      Verify
                    </Button>
                  </Box>
                </Grid>
                <Box
                  data-testid="aplus-details-timer"
                  className={classes.snackbarTimer}
                >
                  <SnackbarStatic
                    label="Session Expires in"
                    time={timeLeft}
                    title=""
                    timed
                    feedback={FeedbackType.TIMER}
                  />
                </Box>
              </Grid>
            </form>
          )}
        </Formik>
      </Box>
    </Box>
  );
};
