provider "aws" {
  region  = "eu-west-1"
  profile = "dev"
}

terraform {
  backend "s3" {
    bucket = "peach-payments-tf-state"
    key    = "checkout-plugins/dev"
    region = "eu-west-1"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

module "a_plus_demo" {
  container_image             = "997694320602.dkr.ecr.eu-west-1.amazonaws.com/checkout-dev:a-plus-demo"
  source                      = "./modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-8753e1b3"
  eks_cluster_name            = "eks-cluster-dev"
  app                         = "a-plus"
  env                         = "dev"
  replicas_amount             = 1

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn = "arn:aws:acm:eu-west-1:997694320602:certificate/5b0a371c-e906-4ed6-9cf6-c14eddc04dd6"

  env_vars = [
  ]
}

module "one_for_you_demo" {
  container_image             = "997694320602.dkr.ecr.eu-west-1.amazonaws.com/checkout-dev:one-for-you-demo"
  source                      = "./modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-8753e1b3"
  eks_cluster_name            = "eks-cluster-dev"
  app                         = "one-for-you"
  env                         = "dev"
  replicas_amount             = 1

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn = "arn:aws:acm:eu-west-1:997694320602:certificate/5b0a371c-e906-4ed6-9cf6-c14eddc04dd6"

  env_vars = [
  ]
}

module "copy_and_pay_demo" {
  container_image             = "997694320602.dkr.ecr.eu-west-1.amazonaws.com/checkout-dev:copy-and-pay-demo"
  source                      = "./modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-8753e1b3"
  eks_cluster_name            = "eks-cluster-dev"
  app                         = "copy-and-pay"
  env                         = "dev"
  replicas_amount             = 1

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn = "arn:aws:acm:eu-west-1:997694320602:certificate/5b0a371c-e906-4ed6-9cf6-c14eddc04dd6"

  env_vars = [
  ]
}

module "a_plus_qa" {
  container_image             = "997694320602.dkr.ecr.eu-west-1.amazonaws.com/checkout-dev:a-plus-qa"
  source                      = "./modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-8753e1b3"
  eks_cluster_name            = "eks-cluster-dev"
  app                         = "a-plus"
  env                         = "qa"
  replicas_amount             = 1

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn = "arn:aws:acm:eu-west-1:997694320602:certificate/5b0a371c-e906-4ed6-9cf6-c14eddc04dd6"


  env_vars = [
  ]
}

module "one_for_you_qa" {
  container_image             = "997694320602.dkr.ecr.eu-west-1.amazonaws.com/checkout-dev:one-for-you-qa"
  source                      = "./modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-8753e1b3"
  eks_cluster_name            = "eks-cluster-dev"
  app                         = "one-for-you"
  env                         = "qa"
  replicas_amount             = 1

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn = "arn:aws:acm:eu-west-1:997694320602:certificate/5b0a371c-e906-4ed6-9cf6-c14eddc04dd6"

  env_vars = [
  ]
}

module "copy_and_pay_qa" {
  container_image             = "997694320602.dkr.ecr.eu-west-1.amazonaws.com/checkout-dev:copy-and-pay-qa"
  source                      = "./modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-8753e1b3"
  eks_cluster_name            = "eks-cluster-dev"
  app                         = "copy-and-pay"
  env                         = "qa"
  replicas_amount             = 1

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn = "arn:aws:acm:eu-west-1:997694320602:certificate/5b0a371c-e906-4ed6-9cf6-c14eddc04dd6"

  env_vars = [
  ]
}
