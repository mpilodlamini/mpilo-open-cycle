export interface SelectedPaymentResponse {
  status?: string;
  response?: {
    result?: {
      code: string;
      description: string;
    };
    response_code?: string;
    id?: string;
    currency?: string;
    brand?: string;
    amount?: number;
    result_details?: {
      ExtendedDescription: string;
      AcquirerResponse: string;
      ConnectorTxID1: string;
    };
    redirect_url?: string;
    redirect_post_data?: {
      amount: string;
      checkoutId: string;
      currency: string;
      id: string;
      merchantTransactionId: string;
      paymentBrand: string;
      paymentType: string;
      signature: string;
      timestamp: string;
    };
  };
  error?: string;
  responseJSON?: any;
}
