import {
  browserName,
  browserVersion,
  isBrowser,
  isMobile,
  isTablet,
  osName,
} from "react-device-detect";
import { printInfo, printWarning } from "./logger";

/**
 * Browser support util to check if the current user browser version is supported by Peach Payments
 * Uses https://www.npmjs.com/package/react-device-detect to do all the detection
 */

const className: string = "BrowserUtils";

const supportedBrowsersDesktop: {
  [index: string]: { [index: string]: number };
} = {
  Chrome: {
    Windows: 75,
    "Mac OS": 79,
    Linux: 78,
    "Chrome Os": 81,
  },
  Safari: {
    "Mac OS": 10,
  },
  Firefox: {
    Windows: 60,
    "Mac OS": 48,
    Ubuntu: 52,
  },
  IE: {
    Windows: 11,
  },
  Edge: {
    Windows: 81,
    "Mac OS": 85,
    Xbox: 18,
  },
  Samsung: {
    Linux: 12,
  },
};

const supportedBrowsersTablet: {
  [index: string]: { [index: string]: number };
} = {
  Chrome: {
    Android: 81,
    iOS: 79,
  },
  "Mobile Safari": {
    iOS: 10,
  },
  Safari: {
    "Mac OS": 10,
  },
  "Samsung Browser": {
    Android: 11,
  },
  Opera: {
    Android: 53,
  },
};

const supportedBrowsersMobile: {
  [index: string]: { [index: string]: number };
} = {
  Chrome: {
    Android: 60,
    iOS: 70,
  },
  "Mobile Safari": {
    iOS: 13,
  },
  Firefox: {
    Android: 67,
  },
  IE: {
    Windows: 11,
  },
  Edge: {
    Windows: 13,
  },
  "Samsung Browser": {
    Android: 4,
    Tizen: 2,
  },
  Opera: {
    Android: 26,
  },
  Android: {
    Android: 55,
  },
};

const isValidDesktopBrowser = (): boolean => {
  if (
    !supportedBrowsersDesktop[browserName] ||
    !supportedBrowsersDesktop[browserName][osName]
  )
    return false;
  return (
    supportedBrowsersDesktop[browserName][osName] <=
    parseInt(browserVersion || "", 10)
  );
};

const isValidTabletBrowser = (): boolean => {
  if (
    !supportedBrowsersTablet[browserName] ||
    !supportedBrowsersTablet[browserName][osName]
  )
    return false;
  return (
    supportedBrowsersTablet[browserName][osName] <=
    parseInt(browserVersion || "", 10)
  );
};

const isValidMobileBrowser = (): boolean => {
  if (
    !supportedBrowsersMobile[browserName] ||
    !supportedBrowsersMobile[browserName][osName]
  )
    return false;
  return (
    supportedBrowsersMobile[browserName][osName] <=
    parseInt(browserVersion || "", 10)
  );
};

export const isValidBrowser = (): boolean => {
  const functionName: string = "detectBrowser";

  if (!browserName) {
    printWarning(className, functionName, "browserName does not exist");
    return false;
  }

  if (!browserVersion) {
    printWarning(className, functionName, "browserVersion does not exist");
    return false;
  }

  if (!osName) {
    printWarning(className, functionName, "osName does not exist");
    return false;
  }

  printInfo(className, functionName, browserName);
  printInfo(className, functionName, browserVersion);
  printInfo(className, functionName, osName);

  if (isTablet) {
    return isValidTabletBrowser();
  }

  if (isMobile) {
    return isValidMobileBrowser();
  }

  if (isBrowser) {
    return isValidDesktopBrowser();
  }

  return false;
};

export const getBrowserSpecificInformation = (): object => ({
  "customer.browser.language": navigator.language,
  "customer.browser.screenHeight": window.innerHeight,
  "customer.browser.screenWidth": window.innerWidth,
  "customer.browser.timezone": new Date().getTimezoneOffset(),
  "customer.browser.userAgent": navigator.userAgent,
  "customer.browser.javaEnabled": navigator.javaEnabled(),
  // eslint-disable-next-line no-restricted-globals
  "customer.browser.screenColorDepth": screen.colorDepth,
  "customer.browser.javascriptEnabled": true,
});
