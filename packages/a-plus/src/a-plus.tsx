import React, { useContext, useMemo, useState } from "react";
import { APlusForm } from "./APlusForm";
import { APlusOtpForm } from "./APlusOtpForm";
import { ConfigContext } from "./contexts/ConfigContext";
import { checkout } from "./lib/checkout";
import { A_PLUS, messages } from "./messages";

export const APlusContainer = () => {
  const {
    config,
    sessionId,
    time: { setTimer },
    transaction: { transactionId, setTransactionId },
  } = useContext(ConfigContext);

  const [confirmOtp, setConfirmOtp] = useState(false);
  const [otpResend, setOtpResend] = useState(1);
  const [otpVerify, setOtpVerify] = useState(true);
  const [cellNumber, setCellNumber] = useState("");

  const errorMessages: {
    [key: string]: {
      heading: string;
      continueText?: string;
      cancelText?: string;
    };
  } = useMemo(() => {
    return messages;
  }, []);

  // APlusOTPForm

  const otpSuccess = async (data: any) => {
    checkout.ga.trackPage("Payment success");
    checkout.publish([
      {
        type: "status",
        value: "success",
      },
      {
        type: "checkout-redirect",
        data: JSON.parse(data.response.redirect_post_data || "{}"),
        url: data.response.redirect_url,
        method: "POST",
      },
    ]);
  };

  const merchantError = async (code: string, message: string) => {
    checkout.publish([
      {
        type: "status",
        value: "error",
        code,
        heading: A_PLUS.MERCHANT_ERROR_HEADING,
        message,
      },
    ]);
    checkout.ga.trackErrorEvents({
      heading: A_PLUS.PAYMENT_DECLINED_HEADING,
      category: "aplus",
      code,
    });
    checkout.ga.trackEvent({
      action: checkout.ga.events.error,
      category: "aplus",
      label: code,
    });
  };

  const resendOtpAvail = async (code: string, message: string) => {
    checkout.publish([
      {
        type: "status",
        value: "error",
        code,
        heading: errorMessages[code]?.heading,
        message,
        cancelText: "Go Back",
      },
    ]);
    checkout.ga.trackErrorEvents({
      heading: errorMessages[code]?.heading,
      category: "aplus",
      code,
    });
  };

  const otpVerifyFailed = async (code: string, message: string) => {
    checkout.publish([
      {
        type: "status",
        value: "error",
        code,
        heading: errorMessages[code]?.heading,
        message,
      },
    ]);
    checkout.ga.trackErrorEvents({
      heading: errorMessages[code]?.heading,
      category: "aplus",
      code,
    });
    checkout.ga.trackEvent({
      action: checkout.ga.events.failed_modal,
      category: "aplus",
      label: code,
    });
  };

  const retryVerifyAvail = async (code: string, message: string) => {
    checkout.publish([
      {
        type: "status",
        value: "error",
        code,
        heading: errorMessages[code]?.heading,
        message,
        cancelText: "Go Back",
      },
    ]);
    checkout.ga.trackErrorEvents({
      heading: A_PLUS.OTP_INCORRECT_HEADING,
      category: "aplus",
      code,
    });
    checkout.ga.trackEvent({
      action: checkout.ga.events.error,
      category: "aplus",
      label: code,
    });
  };

  const otpVerifyError = async (code: string, message: string) => {
    checkout.publish([
      {
        type: "status",
        value: "error",
        code,
        heading: errorMessages[code]?.heading,
        message,
      },
    ]);
    checkout.ga.trackErrorEvents({
      heading: errorMessages[code]?.heading,
      category: "aplus",
      code,
    });
    checkout.ga.trackEvent({
      action: checkout.ga.events.error,
      category: "aplus",
      label: code,
    });
  };

  const otpResent = async (code: string, otp_resends_left: number) => {
    setOtpResend(otp_resends_left);
    setOtpVerify(true);
    checkout.publish([
      {
        type: "status",
        value: undefined,
      },
    ]);
    checkout.ga.trackEvent({
      action: checkout.ga.events.opt_resend_modal,
      category: "aplus",
      label: code,
    });
  };

  const otpResendError = async (code: string, message: string) => {
    checkout.publish([
      {
        type: "status",
        value: "error",
        code,
        heading: errorMessages[code]?.heading,
        message,
      },
    ]);
    checkout.ga.trackEvent({
      action: checkout.ga.events.error,
      category: "aplus",
      label: code,
    });
  };

  const errorCatch = async () => {
    checkout.ga.trackEvent({
      action: checkout.ga.events.error,
      category: "aplus",
    });
    checkout.publish([
      {
        type: "status",
        value: "error",
        heading: "",
        message: "",
      },
      {
        type: "checkout-redirect",
        data: {},
        url: "",
        method: "POST",
      },
    ]);
  };

  if (confirmOtp) {
    return (
      <APlusOtpForm
        cellNumber={cellNumber}
        otpResend={otpResend}
        otpVerify={otpVerify}
        onSubmit={async (values) => {
          try {
            checkout.publish([
              {
                type: "status",
                value: "loading",
                heading: A_PLUS.LOADING_OTP_VERIFY_HEADING,
                message: A_PLUS.LOADING_OTP_VERIFY_MESSAGE,
              },
            ]);
            checkout.ga.trackEvent({
              action: checkout.ga.events.pay,
              category: "aplus",
            });

            let otp = values.otp.replace(/\s/g, "");

            const response = await fetch(`${config.baseUrl}verify-otp`, {
              method: "POST",
              body: new URLSearchParams({
                checkout_id: config.checkoutId,
                transaction_id: transactionId || "",
                otp,
              }),
            });
            const data = await response.json();

            const code =
              data.response.response_data?.result?.code ||
              data.response.result?.code;

            const message =
              data.response.response_data?.result_details?.UserDisplayMessage ||
              data.response.response_data?.resultDetails?.UserDisplayMessage ||
              data.response.result_details?.UserDisplayMessage;

            if (data.status === "success") {
              otpSuccess(data);
            } else if (code === "900.100.200") {
              // merchant error
              merchantError(code, message);
            } else if (code === "100.350.400" && otpResend) {
              // retries and resend avail
              resendOtpAvail(code, message);
              checkout.ga.trackEvent({
                action: checkout.ga.events.error,
                category: "aplus",
                label: code,
              });
            } else if (code === "300.100.100") {
              // no retries but resend avail
              resendOtpAvail(code, message);
              setOtpVerify(false);
              checkout.ga.trackEvent({
                action: checkout.ga.events.otp_limit_modal,
                category: "aplus",
                label: code,
              });
            } else if (code === "100.350.400") {
              // retries and no resend avail
              retryVerifyAvail(code, message);
            } else if (code === "800.100.161") {
              // no retries and no resend avail
              otpVerifyFailed(code, message);
            } else {
              // any other result
              otpVerifyError(code, message);
            }
          } catch (e) {
            errorCatch();
          }
        }}
        onResend={async () => {
          try {
            checkout.publish([
              {
                type: "status",
                value: "loading",
                heading: A_PLUS.LOADING_RESEND_OTP_HEADING,
                message: A_PLUS.LOADING_RESEND_OTP_MESSAGE,
              },
            ]);
            checkout.ga.trackEvent({
              action: checkout.ga.events.resend,
              category: "aplus",
            });
            const response = await fetch(`${config.baseUrl}resend-otp`, {
              method: "POST",
              body: new URLSearchParams({
                checkout_id: config.checkoutId,
                transaction_id: transactionId || "",
              }),
            });

            const data = await response.json();

            const code =
              data.response.response_data?.result?.code ||
              data.response.result?.code;

            const message =
              data.response.response_data?.result_details?.UserDisplayMessage ||
              data.response.response_data?.resultDetails?.UserDisplayMessage ||
              data.response.result_details?.UserDisplayMessage;

            const otp_resends_left =
              data.response.response_data?.result_details?.OTPResendsLeft ||
              data.response.response_data?.resultDetails?.OTPResendsLeft ||
              data.response.result_details?.OTPResendsLeft ||
              0;

            if (data.status === "success") {
              otpResent(code, otp_resends_left);
            } else if (code === "900.100.200") {
              // merchant error
              merchantError(code, message);
            } else {
              otpResendError(code, message);
            }
          } catch (e) {
            errorCatch();
          }
        }}
      />
    );
  }

  // APlusForm

  const detailsSuccess = async (data: any) => {
    setConfirmOtp(true);
    setTimer(() => 1 * 60 * 15);
    setTransactionId(data.response.response_data?.id);
    checkout.ga.trackPage("A+ verification");
    checkout.publish([
      {
        type: "status",
        value: undefined,
      },
      {
        type: "set-checkout-confirm",
        confirm: true,
      },
    ]);
  };

  const detailsError = async (code: string, message: string) => {
    checkout.ga.trackErrorEvents({
      heading: errorMessages[code]?.heading,
      category: "aplus",
      code,
    });
    checkout.publish([
      {
        type: "status",
        value: "error",
        code,
        heading: errorMessages[code]?.heading,
        message,
        cancelText: errorMessages[code]?.cancelText,
      },
    ]);
  };

  return (
    <APlusForm
      onSubmit={async (values) => {
        try {
          checkout.publish([
            {
              type: "status",
              value: "loading",
              heading: A_PLUS.DEFAULT_LOADING_HEADING,
              message: A_PLUS.DEFAULT_LOADING_MESSAGE,
            },
          ]);
          checkout.ga.trackEvent({
            action: checkout.ga.events.verify,
            category: "aplus",
          });

          const card_no = values.cardNo.replace(/\s/g, "");
          const card_expiry = values.cardExpiry;
          const cell_phone = values.cellPhone.replace(/\s/g, "");

          const response = await fetch(`${config.baseUrl}payment`, {
            method: "POST",
            body: new URLSearchParams({
              merchant_key: config.merchantKey,
              checkout_id: config.checkoutId,
              payment_brand: "aplus",
              card_no,
              card_expiry,
              cell_phone,
              session_id: sessionId || "",
            }),
          });

          const data = await response.json();

          const code =
            data.response.response_data?.result?.code ||
            data.response.result?.code;

          const message =
            data.response.response_data?.result_details?.UserDisplayMessage ||
            data.response.response_data?.resultDetails?.UserDisplayMessage ||
            data.response.result_details?.UserDisplayMessage;

          if (data.status === "success") {
            detailsSuccess(data);
            setCellNumber(values.cellPhone);
          } else if (code === "900.100.200") {
            // merchant error
            merchantError(code, message);
          } else if (code === "800.100.203") {
            // insufficient funds
            detailsError(code, message);
            checkout.ga.trackEvent({
              action: checkout.ga.events.declined_modal,
              category: "aplus",
              label: code,
            });
          } else {
            detailsError(code, message);
          }
        } catch (e) {
          errorCatch();
        }
      }}
    />
  );
};
