import React, { HTMLAttributes, forwardRef } from "react";

export interface ManualRedirectFormProps
  extends HTMLAttributes<HTMLFormElement> {
  url: string;
  data: { [index: string]: any };
  method: "GET" | "POST";
}

export const RedirectForm = forwardRef<
  HTMLFormElement,
  ManualRedirectFormProps
>(({ url, data, method, children, ...props }: ManualRedirectFormProps, ref) => (
  <form {...props} method={method} action={url} ref={ref}>
    {Object.entries(data || {}).map(([k, v]) => (
      <input hidden style={{ display: "none" }} name={k} value={v} key={k} />
    ))}
    {children}
  </form>
));
