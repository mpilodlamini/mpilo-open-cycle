import { Box, Modal } from "@material-ui/core";
import React, { ReactNode } from "react";
import { Dialog } from "..";
import { FeedbackType } from "../../feedback";
import useStyles from "./index.styles";

const getModalStyle = (): object => {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
};

export type ModalDialogProps = {
  open: boolean;
  /**
   * Select variation e.g Warning
   */
  type: FeedbackType;
  title: string;
  label?: ReactNode;
  forceBottom?: boolean;

  /**
   * States the action the button will take onClick and is button text
   */
  continueText?: string;
  onClick?: () => void;
  cancelText?: string;
  onCancel?: () => void;

  errorMessages?: string[];
  children?: ReactNode;
};

export const ModalDialog = (props: ModalDialogProps) => {
  const classes = useStyles();

  const body = (
    <Box style={getModalStyle()} className={classes.paper} height="60%">
      <Dialog
        type={props.type}
        title={props.title}
        label={props.label}
        continueText={props.continueText}
        onClick={props.onClick}
        onCancel={props.onCancel}
        cancelText={props.cancelText}
        errorMessages={props.errorMessages}
        forceBottom={props.forceBottom}
      >
        {props.children}
      </Dialog>
    </Box>
  );

  return (
    <Box>
      <Modal open={props.open} aria-labelledby={props.title}>
        {body}
      </Modal>
    </Box>
  );
};
