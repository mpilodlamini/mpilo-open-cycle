import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import { Props, Container, Wrapper } from "../../../";

export default {
  title: "Controls/Container",
} as Meta;

export const Default: Story<Props> = (props) => {
  return <Container {...props} />;
};

Default.args = {};

export const WrapperContainer: Story<Props> = (props) => {
  return <Wrapper {...props} />;
};

WrapperContainer.args = {};
