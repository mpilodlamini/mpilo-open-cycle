import React from "react";
import $ from "jquery";
import CheckoutService from "../../../../services/checkoutService";
import {
  PAYMENT_METHOD_ENUMS,
  PAYMENT_METHOD_NAMES,
  URLS,
} from "../../../../utils/pluginConstants";
import { AJAX_DATA_RESULT } from "../../../../utils/constants";
import PluginManager from "../../../../utils/pluginManager";
import LoadingScreen from "../../../../components/loaders/loadingScreen";
import ErrorScreen from "../../../../components/errors/screens/errorScreen";
// @ts-ignore
import Ozow from "../../../../libraries/ipay-jsintegration-1.1";
import { printInfo } from "../../../../utils/logger";
import TimeUtil from "../../../../utils/timeUtil";
import { CheckoutConfig } from "../../../../contexts/CheckoutContext";

export interface OzowContainerProps {
  checkout: CheckoutConfig;
  timeUtil: TimeUtil;
  selectedPaymentMethod: string;
}

export interface OzowContainerState {
  showErrorScreen: boolean;
  showLoadingScreen: boolean;
  isOzowLoaded: boolean;
}
export {};

const componentName: string = "OzowWrapper";

const ozowContainerId = "ozowContainer";

const paymentBrand = PAYMENT_METHOD_ENUMS.OZOW;

export class OzowContainer extends React.Component<
  OzowContainerProps,
  OzowContainerState
> {
  private readonly checkoutService: CheckoutService = new CheckoutService();

  private readonly _pluginManager = new PluginManager(this.props.timeUtil);

  private readonly _loadingScreen = (
    <LoadingScreen widgetName={PAYMENT_METHOD_NAMES.OZOW} />
  );

  private readonly _errorScreen = (
    <ErrorScreen
      title={`The ${PAYMENT_METHOD_NAMES.OZOW} payment method is unavailable`}
    />
  );

  constructor(props: OzowContainerProps) {
    super(props);

    this.state = {
      showErrorScreen: false,
      showLoadingScreen: true,
      isOzowLoaded: false,
    };
  }

  componentDidMount = (): void => {
    printInfo(componentName, "componentDidMount", "Mounted");

    this.initiateOzowPlugin();
  };

  initiateOzowPlugin = (): void => {
    if (this.state.isOzowLoaded) return;

    const postData = {
      merchant_key: this.props.checkout.merchantKey,
      checkout_id: this.props.checkout.checkoutId,
      payment_brand: paymentBrand,
    };

    const successCallback = (data: any) => {
      const ozowPostData = JSON.parse(data.response.post_data);

      if (data.status === AJAX_DATA_RESULT.SUCCESS) {
        this.props.timeUtil.updateCheckoutTimer();
        this._pluginManager.createPollingListener();

        const transactionId = data.response.response_data.id;

        // Initiate the Ozow plugin from the middleware javascript
        // @ts-ignore
        const ozow = new Ozow();
        ozow.createPaymentFrame(
          ozowContainerId,
          URLS.OZOW_URL,
          ozowPostData,
          transactionId
        );

        $("#paymentFrame").on("load", () => {
          this.setState({ showLoadingScreen: false, isOzowLoaded: true });
        });
      }
    };

    const errorCallback = (): void => {
      this.setState({
        showLoadingScreen: false,
        isOzowLoaded: true,
        showErrorScreen: true,
      });
    };

    printInfo(
      componentName,
      "initiateOzowPlugin",
      `Making contact with ${PAYMENT_METHOD_NAMES.OZOW} data:${postData}`
    );

    this.checkoutService.postFormEncodedContent(
      `${this.props.checkout.baseUrl}payment`,
      postData,
      successCallback,
      errorCallback
    );
  };

  componentWillUnmount = (): void => {
    this.checkoutService.cancelRequest();
    this._pluginManager.stopPollingListener();
  };

  render = () => {
    const ozowErrorModalAnchor = (
      <p className="refresh-code m-t-16" id="global-error-go-back">
        {/* eslint-disable-next-line */}
        <a id="go-back-anchor" className="go-back-anchor">
          Go Back
        </a>
      </p>
    );

    const ozowErrorModal = (
      <div className="full-screen-modal d-none" id="global-error-model">
        <div className="center-con">
          <div className="icon-con">
            <img
              id="error-logo"
              src={`${
                this.props.checkout.assetsBasePath || "/"
              }images/icon/warning-icon.png`}
              alt=""
            />
          </div>
          <h2 className="text-content" id="error-heading">
            Error Message
          </h2>
          <p id="error-message">
            Transaction failed due to an internal error. Please go back to try
            again or select a different payment method.
          </p>
          <button
            type="button"
            className="btn-primary btn-blue d-none"
            id="action-button"
          >
            Action Button
          </button>
          {ozowErrorModalAnchor}
        </div>
      </div>
    );

    return (
      <>
        {ozowErrorModal}
        <div id={ozowContainerId}>
          {!this.state.showLoadingScreen &&
            this.state.showErrorScreen &&
            this._errorScreen}
          {this.state.showLoadingScreen &&
            !this.state.showErrorScreen &&
            this._loadingScreen}
        </div>
      </>
    );
  };
}

export default OzowContainer;
