import React from "react";
import EftLoader from "./EftLoader";

export default { title: "payments / Eft loader" };

export const Loader = () => <EftLoader />;
