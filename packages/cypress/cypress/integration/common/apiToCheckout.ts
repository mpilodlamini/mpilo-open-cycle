import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";

When(
    "I Generate Checkout via API using default values",
    () => {
      cy.generateAPICheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount: "10.00",
        currency: "ZAR",
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/post",
        merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8)
      });
    }
  );

When(
  "I Generate Checkout via API using {string} as my test amount",
  (amount) => {
    cy.generateAPICheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount,
        currency: "ZAR",
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/post",
        merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8)
      });
  }
);

When(
  "I Generate Checkout via API using {string} as my test amount and {string} as my currency",
  (amount, currency) => {
    cy.generateAPICheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount,
        currency,
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/post",
        merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8)
      });
  }
);

When(
    "I Generate Checkout via API using {string} as my test amount as a Kenyan merchant",
    (amount) => {
      cy.generateAPICheckout({
          secretKey: "ca3d3f623a7111e9baa702de7a5a0a6e",
          "authentication.entityId": "8ac7a4ca694cec2601694cf5f9360026",
          amount,
          currency: "KES",
          paymentType: "DB",
          shopperResultUrl: "https://httpbin.org/post",
          merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8)
        });
    }
  );

When(
  "I Generate Checkout via API using {string} as my merchant Transaction Id",
  (merchantTransactionId) => {
    cy.generateAPICheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount: "10.00",
        currency: "ZAR",
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/post",
        merchantTransactionId
      });
  }
);

When(
  "I Generate Checkout via API using with merchant billing details",
  () => {
        cy.generateAPICheckout({
        secretKey: "e098c65a50e411eb88ef02de7a5a0a6e",
        "authentication.entityId": "8ac7a4ca77a64c9c0177af52972c13bd",
        amount: "10.00",
        currency: "ZAR",
        paymentType: "DB",
        shopperResultUrl: "https://httpbin.org/post",
        merchantTransactionId: "Cy-Checkout-" + Cypress._.random(0, 1e8),
        customerEmail: "test@test.com",
        billingStreet1: "1 Test lane",
        billingCountry: "ZA",
        billingCity: "Cape Town",
        billingPostcode: "7700",
      });
  }
);