import { Box } from "@material-ui/core";
import React, { useMemo } from "react";
import Select, { Props } from "react-select";
import customStyles, { CustomInputLabel } from "./index.styles";

export type SelectProps = Props & {
  label: string;
  isXsBp?: boolean;
  touched?: boolean;
  hasValue?: boolean;
  isError?: boolean;
  errorMessage?: string;
};

/**
 This Select component extends the React-Select with styling to match MUI textfield
 @see https://react-select.com/home
 */
export const CustomSelect = (props: SelectProps) => {
  const {
    isXsBp,
    label,
    touched,
    hasValue,
    isError,
    errorMessage,
  }: SelectProps = props;

  const isLabelPresent = useMemo(() => {
    if (errorMessage === "Required") {
      return false;
    }

    return isXsBp && (hasValue || touched);
  }, [touched, hasValue, isXsBp, errorMessage]);

  return (
    <>
      {isLabelPresent && (
        <Box position="relative">
          <CustomInputLabel error={isError} shrink={touched}>
            {errorMessage || label}
          </CustomInputLabel>
        </Box>
      )}

      <Select {...props} isError={isError} styles={customStyles} />
    </>
  );
};
