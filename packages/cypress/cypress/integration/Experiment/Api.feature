Feature: Checkout via API
  Background: Navigate to Checkout
    Given I Generate Checkout via API using default values
        
  Scenario: Checkout should render correctly
    Then I should see the text "Select Payment Method"
    And I should see "header-currency" containing the text "ZAR"
    And I should see "header-total" containing the text "10.00"
    And I should see "banner-cancel" containing the text "Cancel"
    And I should see the text "Secured by"
    And I should see the text "Peach Payments"
    