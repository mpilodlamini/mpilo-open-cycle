import { useContext, useReducer } from "react";
import { BUTTON_LABELS, MESSAGING } from "../../../../utils/messagingConstants";
import MessagesUtils from "../../../../utils/messagesUtil";
import { CheckoutContext } from "../../../../contexts/CheckoutContext";
import { StatusContext } from "../../../../contexts/StatusContext";
import { SelectedPaymentContext } from "../../../../contexts/SelectedPaymentContext";

const messagesUtils: MessagesUtils = new MessagesUtils();

export interface CardState {
  showCardCaptureScreen: boolean;
  showLoadingScreen: boolean;
  showLoadingModal: boolean;
  showErrorModal: boolean;
  showErrorScreen: boolean;
  showSuccessScreen: boolean;
  loadingModalHeading: string;
  loadingModalMessage: string;
  errorModalHeading: string;
  errorModalMessage: string;
  errorModalActionButton: string;
  errorModalBackButton: string;
  threeDSecureRedirectUrl: string;
  threeDSecurePostData: {
    name: string;
    value: string;
  }[];
  code: string;
  transactionId: string;
  redirectUrl: string;
  redirectPostData: any;
}

export type CardAction =
  | { type: "stop-loading-screen" }
  | { type: "stop-loading-modal" }
  | { type: "show-error-screen" }
  | {
      type: "server-error";
      code: string;
    }
  | { type: "hide-error-modal" }
  | {
      type: "start-loading-modal";
      heading?: string;
      message?: string;
    }
  | {
      type: "start-loading-screen";
    }
  | {
      type: "start-3ds-redirect";
      code: string;
      transactionId: string;
      url: string;
      postData: any;
    }
  | {
      type: "3ds-success";
      code: string;
      postData: any;
      url: string;
    };

export const useCardState = () => {
  const checkout = useContext(CheckoutContext);
  const payment = useContext(SelectedPaymentContext);

  const [containerState, dispatch] = useReducer(
    (state: CardState, action: CardAction) => {
      switch (action.type) {
        case "start-loading-screen":
          return { ...state, showLoadingScreen: true };
        case "start-loading-modal":
          return {
            ...state,
            loadingModalHeading: action.heading || "Loading",
            loadingModalMessage:
              action.message || "Please wait while payment is processing",
            showLoadingModal: true,
          };
        case "stop-loading-modal":
          return { ...state, showLoadingModal: false };
        case "stop-loading-screen":
          return { ...state, showLoadingScreen: false };
        case "hide-error-modal":
          return { ...state, showErrorModal: false };
        case "show-error-screen":
          return {
            ...state,
            showLoadingScreen: false,
            showLoadingModal: false,
            showErrorScreen: true,
          };
        case "server-error": {
          const errorMessagesObject =
            messagesUtils.getNativeCardErrorMessages()[action.code];
          const heading: string =
            errorMessagesObject?.heading || MESSAGING.DEFAULT_ERROR_HEADING;
          const message: string =
            errorMessagesObject?.message || MESSAGING.DEFAULT_ERROR_MESSAGE;
          const backAction: string = BUTTON_LABELS.GO_BACK;
          payment.setCancelVisibility(false);
          return {
            ...state,
            code: action.code,
            errorModalHeading: heading,
            errorModalMessage: message,
            errorModalActionButton: "",
            errorModalBackButton: backAction,
            showLoadingModal: false,
            showErrorModal: true,
          };
        }
        case "start-3ds-redirect":
          return {
            ...state,
            code: action.code,
            transactionId: action.transactionId,
            threeDSecureRedirectUrl: action.url,
            threeDSecurePostData: action.postData,
            showCardCaptureScreen: false,
          };
        case "3ds-success":
          payment.setCancelVisibility(false);
          return {
            ...state,
            code: action.code,
            redirectUrl: action.url,
            redirectPostData: action.postData,
            showCardCaptureScreen: false,
            showSuccessScreen: true,
            showLoadingModal: false,
          };
        default:
          return state;
      }
    },
    {
      showCardCaptureScreen: checkout.isNativeCardForm,
      showLoadingScreen: checkout.isNativeCardForm,
      showLoadingModal: !checkout.isNativeCardForm,
      showErrorModal: false,
      showErrorScreen: false,
      showSuccessScreen: false,
      loadingModalHeading: "Get ready to pay by credit or debit card",
      loadingModalMessage: "",
      errorModalHeading: MESSAGING.DEFAULT_ERROR_HEADING,
      errorModalMessage: MESSAGING.DEFAULT_ERROR_MESSAGE,
      errorModalActionButton: "",
      errorModalBackButton: BUTTON_LABELS.GO_BACK,
      threeDSecureRedirectUrl: "",
      threeDSecurePostData: [],
      code: "",
      transactionId: "",
      redirectUrl: "",
      redirectPostData: {},
    }
  );

  return [containerState, dispatch] as [
    CardState,
    (action: CardAction) => void
  ];
};
