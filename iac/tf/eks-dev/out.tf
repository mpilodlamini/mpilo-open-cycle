output "one_for_you_demo" {
  value = module.one_for_you_demo.load_balancer_ingress
}

output "a_plus_demo" {
  value = module.a_plus_demo.load_balancer_ingress
}

output "copy_and_pay_demo" {
  value = module.copy_and_pay_demo.load_balancer_ingress
}

output "one_for_you_qa" {
  value = module.one_for_you_qa.load_balancer_ingress
}

output "a_plus_qa" {
  value = module.a_plus_qa.load_balancer_ingress
}

output "copy_and_pay_qa" {
  value = module.copy_and_pay_qa.load_balancer_ingress
}
