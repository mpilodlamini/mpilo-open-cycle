provider "aws" {
  region  = "af-south-1"
  profile = "peach-prod"
}

terraform {
  backend "s3" {
    bucket = "peach-payments-state"
    key    = "checkout-plugins/af-south-1/prod-temp"
    region = "eu-west-1"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

module "a_plus_sandbox" {
  container_image             = "285408586721.dkr.ecr.af-south-1.amazonaws.com/checkout-prod:a-plus-sandbox"
  source                      = "../modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-2f6fe58a"
  eks_cluster_name            = "eks-cluster-test"
  app                         = "a-plus"
  env                         = "sandbox"
  replicas_amount             = 2

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn            = "arn:aws:acm:af-south-1:285408586721:certificate/bd645330-0d96-41e2-bdce-fec238584a41"

  env_vars = [
  ]
}

module "one_for_you_sandbox" {
  container_image             = "285408586721.dkr.ecr.af-south-1.amazonaws.com/checkout-prod:one-for-you-sandbox"
  source                      = "../modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-2f6fe58a"
  eks_cluster_name            = "eks-cluster-test"
  app                         = "one-for-you"
  env                         = "sandbox"
  replicas_amount             = 2

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn            = "arn:aws:acm:af-south-1:285408586721:certificate/bd645330-0d96-41e2-bdce-fec238584a41"

  env_vars = [
  ]
}

module "a_plus_live" {
  container_image             = "285408586721.dkr.ecr.af-south-1.amazonaws.com/checkout-prod:a-plus-live"
  source                      = "../modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-2f6fe58a"
  eks_cluster_name            = "eks-cluster-test"
  app                         = "a-plus"
  env                         = "live"
  replicas_amount             = 2

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn            = "arn:aws:acm:af-south-1:285408586721:certificate/bd645330-0d96-41e2-bdce-fec238584a41"


  env_vars = [
  ]
}

module "one_for_you_live" {
  container_image             = "285408586721.dkr.ecr.af-south-1.amazonaws.com/checkout-prod:one-for-you-live"
  source                      = "../modules/eks-resources"
  container_image_pull_policy = "Always"
  file_system_id              = "fs-2f6fe58a"
  eks_cluster_name            = "eks-cluster-test"
  app                         = "one-for-you"
  env                         = "live"
  replicas_amount             = 2

  cpu_limit               = "0.5"
  memory_limit            = "512Mi"
  resource_request_cpu    = "0.2"
  resource_request_memory = "256Mi"
  acm_cert_arn            = "arn:aws:acm:af-south-1:285408586721:certificate/bd645330-0d96-41e2-bdce-fec238584a41"

  env_vars = [
  ]
}
