import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  amexIcon: {
    width: "50px",
    height: "10px",
    marginLeft: "8px",
  },
  visaIcon: {
    width: "36px",
    height: "11px",
    marginLeft: "8px",
  },
  masterCardIcon: {
    width: "24px",
    height: "14px",
    marginLeft: "8px",
  },
  dinersClubIcon: {
    width: "21px",
    height: "16px",
    marginLeft: "8px",
  },
}));

export default useStyles;
