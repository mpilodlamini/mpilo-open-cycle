# Peach checkout lib

This library is used to interact with the Checkout shell from a plugin (payment method).

## Getting started

Install the dependency

```bash
(yarn | npm) install @peach/checkout-lib
```

Import the checkout constructor

```javascript
import { Checkout } from "@peach/checkout-lib";
// or
const { Checkout } = require("@peach/checkout-lib");
```

Initialize checkout for the environment you are hosting the plugin on

```javascript
const checkout = Checkout("https://testsecure.peachpayments.com");
```

Subscribe to an event

```javascript
checkout.on("checkout-bootstrap", (event) => {
  console.log(event.data.config.currency); // ZAR
  console.log(event.data.config.amount); // 10.00
});
```

Send a message to checkout

```javascript
checkout.initalize();
// or
checkout.publish([
  { type: "status", value: "initializing" }
]);
```

Send multiple messages to Checkout

```javascript
checkout.publish([
  { type: "status", value: "loading" },
  { type: "status", value: "redirect", url: "https://example.com", data: { test: "value" } },
]);
```

## API

### `on/subscribe(event: Event, handler: (event: CheckoutEvent) => `void) => handler

This allows you to subscribe to events from Checkout, this returns the instance of your handler if you need to unsubscribe.

#### `Available events`

```typescript
"checkout-bootstrap": {
  data: {
    message: "checkout-bootstrap";
    config: CheckoutConfig;
  };
}
```

### `off/unsubscribe(handler)`

This allows you to unsubscribe your handler.

### `redirect`

This allows you to redirect your user

#### `get(url: string)`

This allows you to redirect your user with a get request

#### `post(url: string, data: any)`

This allows you to redirect your user with a post request

### `GA`

Google analytics utils

#### `trackPage(title: string)`

Tracks a page

#### `trackEvent(action: string, category: string, label?: string)`

Tracks a event

#### `trackPurchase(event: { value: string; currency: string; `transactionId: string; })`

Tracks a purchase

#### `trackPaymentInformation()`

Tracks a user entering payment information

#### `trackErrorEvents(event: { heading: string; category: string`code: string; })`

Tracks a user encoutering an error

#### `events`

A map of available checkout events

### `publish(events: Event[]) => void`

This is the low level mechanism that plugins use to communicate with Checkout.

You will rarely need to drop down to this level unless you are doing something complicated.

You can publish multiple events at once.

#### `Available events`

```typescript
export type StatusEvent = {
  type: "status";
  value:
    | "loading"
    | "error"
    | "success"
    | "pending"
    | "initializing"
    | undefined;
  code?: string;
  heading?: string;
  message?: string | string[];
  continueText?: string;
  cancelText?: string;
};

export type FeedbackEvent = {
  type: "update-feedback";
  feedback: {
    heading?: string;
    message?: string | string[];
    continueText?: string;
    cancelText?: string;
  };
};

export type CheckoutConfirm = {
  type: "set-checkout-confirm";
  confirm: boolean;
};

export type GAEvent =
  | { type: "track-page"; title: string }
  | {
      type: "track-event";
      event: { action: string; category: string; label?: string };
    }
  | {
      type: "track-purchase";
      event: {
        value: string;
        currency: string;
        transactionId: string;
      };
    }
  | { type: "track-payment-information" }
  | {
      type: "track-error-events";
      event: {
        heading:
          | "Login Failed"
          | "Payment Timeout"
          | "Payment Error"
          | "Payment Cancelled"
          | "Payment Declined"
          | "OTP Limit Exceeded"
          | "Payment Failed"
          | string;
        category: string;
        code: string;
      };
    };

export type CheckoutRedirectEvent = {
  type: "checkout-redirect";
  url: string;
  data?: any;
} & ({ method: "GET" } | { method: "POST" });
```

## Example plugin

```javascript
import "./style.css";
import { Checkout } from "@peach/checkout-lib";
import { html, render } from "lit-html";

// Get the instance of checkout to communicate with
// This value will change between the demo, sandbox and live environments
// These values are
// demo: https://checkout-demo.ppay.io
// sandbox: https://testsecure.peachpayments.com
// live: https://secure.peachpayments.com
// We recommend making this a build step configuration
const checkout = Checkout("https://localhost:3000");

// The first thing you need to do is subscribe to the bootstrap
// This will provide your plugin with the information it requires to make a payment
checkout.on("checkout-bootstrap", (event) => {
  // This is our pay method
  const pay = async () => {
    try {
      // Tell Checkout to inform the user that something is busy loading
      checkout.loading();

      // This is where you would make a call to your API to complete the payment from your side
      // An example would be:
      // const result = await fetch("/your/api/payment", {
      //   method: "POST",
      //   body: JSON.stringify({
      //     amount: event.config.amount,
      //     transactionId: event.config.transactionId,
      //   }),
      // });

      // Tell Checkout the payment has been successful,
      // this will display feedback to the user
      // and redirect them to their store
      checkout.success();
    } catch (e) {
      // Tell Checkout the payment has failed
      // This will end the plugin session and allow the user to restart the flow
      checkout.error();
    }
  };
  // Here we are rendering the most basic example plugin
  // It displays the amount being paid and has a button to make the payment
  render(
    html`
      <h1>Paying amount: ${event.data.config.amount}</h1>
      <button @click=${pay}>Pay</button>
    `,
    document.querySelector("#app")
  );

  // Once we have finished rendering our
  // application we inform Checkout that we are ready
  // this will stop the loading indicator and show your application
  checkout.initialized();
});

// The second thing you need to do is notify Checkout
// that your plugin has finished loading and is
// ready to receive its configuration
// this method will do two things
// 1. Indicate to the user that the plugin is loading
// 2. Send the checkout bootstrap event to your plugin
checkout.initialize();
```
