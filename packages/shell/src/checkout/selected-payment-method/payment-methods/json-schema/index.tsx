import { Button, Grid, makeStyles, Paper } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { SnackbarStatic } from "@peach/checkout-design-system";
import {
  border,
  secondaryBackgroundGrey,
  secondaryGreen,
  tertiaryGreen,
  white,
} from "@peach/checkout-design-system/dist/variables";
import { Field, Formik } from "formik";
import { JSONSchema7, JSONSchema7Definition } from "json-schema";
import React, { useContext, useState } from "react";
import * as yup from "yup";
import { ObjectShape } from "yup/lib/object";
import ErrorModal from "../../../../components/errors/modals/errorModal";
import LoaderModal from "../../../../components/loaders/loadingModal";
import {
  CheckoutContext,
  JSONSchemaMethod,
} from "../../../../contexts/CheckoutContext";
import { MESSAGING } from "../../../../utils/messagingConstants";
import { PAYMENT_METHOD_NAMES } from "../../../../utils/pluginConstants";
import FeedbackPage from "../shared/FeedbackPage";
import { TextField } from "./TextField";

export interface JsonSchemaPluginProps {
  method: JSONSchemaMethod;
}

const useStyles = makeStyles(({ breakpoints }) => {
  return {
    paper: {
      width: "100%",
      maxWidth: "547px",
      margin: "auto",
      backgroundColor: white,
      padding: "24px 32px 16px",
      boxShadow: "none",
      [breakpoints.up("sm")]: {
        backgroundColor: secondaryBackgroundGrey,
        boxShadow:
          "0px 3px 3px -2px rgba(0,0,0,0.2), 0px 3px 4px 0px rgba(0,0,0,0.14), 0px 1px 8px 0px rgba(0,0,0,0.12)",
      },
    },
    layoutFooter: {
      maxWidth: "547px",
      margin: "auto",
    },
    button: {
      border: "1px solid currentColor",
      backgroundColor: tertiaryGreen,
      color: white,
      textTransform: "none",
      fontSize: "14px",

      "&:hover": {
        backgroundColor: secondaryGreen,
      },
      "&.Mui-disabled, &:disabled": {
        borderColor: border,
        backgroundColor: border,
        color: white,
        cursor: "not-allowed",
      },
    },
  };
});

export const JsonSchemaPlugin = ({
  method: { schema, image, submit_btn },
}: JsonSchemaPluginProps) => {
  const classes = useStyles();
  const config = useContext(CheckoutContext);
  const [status, setStatus] = useState<
    "loading" | "success" | "error" | undefined
  >();

  if (status === "loading") {
    return (
      <LoaderModal
        open={true}
        title="Loading"
        label="Please wait while payment is processing."
      />
    );
  }

  if (status === "error") {
    return (
      <ErrorModal
        open={true}
        title={MESSAGING.DEFAULT_ERROR_HEADING}
        label={MESSAGING.DEFAULT_ERROR_MESSAGE}
        continueText=""
        cancelText="Go Back"
        onClick={() => {}}
        onCancel={() => {}}
      />
    );
  }

  if (status === "success") {
    return (
      <FeedbackPage
        paymentMethod={PAYMENT_METHOD_NAMES.ONE_FOR_YOU}
        code="000.000.000"
        status="Success"
        isTimed={true}
        onRedirect={() => {}}
        redirectPostData={{}}
        redirectUrl={config.redirectUrl}
      />
    );
  }
  return (
    <Formik
      enableReinitialize
      initialValues={initialValuesForJsonSchema(schema, schema)}
      validationSchema={yupForJsonSchema(schema, schema)}
      onSubmit={async (values) => {
        try {
          setStatus("loading");
        } catch (e) {
          setStatus("error");
        }
      }}
    >
      {({ handleSubmit, isSubmitting, isValid, errors }) => {
        return (
          <form onSubmit={handleSubmit}>
            <Paper className={classes.paper} elevation={3}>
              <FieldsForSchema
                image={image}
                schema={schema as JSONSchema7}
                object={schema as JSONSchema7}
                parents={[]}
              />
              <Grid item xs={12} className={classes.layoutFooter}>
                <Box display="flex" justifyContent="flex-end" mt={2}>
                  <Button
                    type="submit"
                    data-testid="submit-button"
                    className={classes.button}
                    disabled={!isValid || isSubmitting}
                    variant="contained"
                    disableElevation
                  >
                    {submit_btn as any}
                  </Button>
                </Box>
              </Grid>
            </Paper>
          </form>
        );
      }}
    </Formik>
  );
};

const yupForJsonSchema = (object: JSONSchema7, schema: JSONSchema7) => {
  const toValidate = yup.object(
    Object.entries(object.properties || {}).reduce(
      (rules: ObjectShape, [k, v]: any) => {
        if (v.$ref) {
          rules[k] = yupForJsonSchema(
            get(v.$ref.slice(2).split("/"), schema),
            schema
          );
        } else if (v.type === "object") {
          rules[k] = yupForJsonSchema(v, schema);
        } else {
          let rule = (yup as any)[v.type]();
          if (v.maxLength) {
            rule = rule.max(v.maxLength);
          }
          if (v.minLength) {
            rule = rule.min(v.minLength);
          }
          if (object.required?.includes(k)) {
            rules[k] = rule.required();
          } else {
            rules[k] = rule;
          }
        }

        return rules;
      },
      {}
    )
  );
  return toValidate;
};

const initialValuesForJsonSchema = (
  object: JSONSchema7,
  schema: JSONSchema7
) => {
  return Object.entries(object.properties || {}).reduce(
    (values: any, [k, v]: any) => {
      if (v.$ref) {
        values[k] = initialValuesForJsonSchema(
          get(v.$ref.slice(2).split("/"), schema),
          schema
        );
      } else if (v.type === "object") {
        values[k] = initialValuesForJsonSchema(v, schema);
      } else {
        values[k] = v.default || "";
      }

      return values;
    },
    {}
  );
};

const get = (path: string[], schema: any, defaultValue: any = {}) => {
  try {
    let node = schema;
    while (path.length > 0) {
      node = node[path.shift() || ""];
    }
    return node || defaultValue;
  } catch (e) {
    return defaultValue;
  }
};

const FieldsForSchema = ({
  object,
  image,
  schema,
  parents,
  depth = 0,
}: {
  image?: string;
  submit_btn?: string;
  schema: JSONSchema7;
  object: JSONSchema7;
  parents: string[];
  depth?: number;
}) => {
  if (object.$ref) {
    return (
      <FieldsForSchema
        object={get(object.$ref.slice(2).split("/"), schema)}
        schema={schema}
        parents={parents}
        depth={depth}
      />
    );
  }

  if (object.type === "object") {
    return (
      <Box
        marginLeft={depth}
        paddingLeft={depth > 0 ? 1 : 0}
        style={{
          borderLeft: depth > 0 ? "solid 4px #cacaca" : "none",
        }}
      >
        <Box textAlign="center" mb={3}>
          <img src={image} />
        </Box>

        <Box bgcolor="#FFF6E4">
          <SnackbarStatic
            title={object.title as string}
            label={object.description as string}
            dataTestId="json-schema-warning"
          />
        </Box>

        <Box mt={3}>
          {Object.entries(object.properties || {}).map(
            ([k, v]: [string, JSONSchema7Definition]) => {
              return (
                <FieldsForSchema
                  key={k}
                  object={v as JSONSchema7}
                  parents={[...parents, k]}
                  schema={schema}
                  depth={depth + 1}
                />
              );
            }
          )}
        </Box>
      </Box>
    );
  }

  if (object.type === "string" || object.type === "number") {
    return (
      <Field
        fullWidth={true}
        label={object.title || parents[parents.length - 1]}
        helperText={object.description}
        type={typeMap[object.type]}
        placeholder={(object as any).placeholder}
        name={parents.join(".")}
        component={TextField}
      />
    );
  }

  return <div>{JSON.stringify(object)} is an unsupported json-schema type</div>;
};
const typeMap = { string: "text", number: "number" };
