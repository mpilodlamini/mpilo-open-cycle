import React, { useEffect, useRef } from "react";

export interface RedirectorProps {
  url: string;
  data: { [index: string]: any };
  method: "GET" | "POST";
}

/**
 * Component that redirect using a form
 * Can redirect on a GET or POST determined by {method}
 * @param url to redirect to
 * @param data post data
 * @param method {GET | POST}
 * @constructor
 */
const Redirector = ({ url, data, method }: RedirectorProps) => {
  const formRef = useRef<HTMLFormElement>(null);

  useEffect(() => {
    if (formRef?.current) {
      formRef.current.submit();
    }
  }, [formRef.current]);

  return (
    <form method={method} action={url} ref={formRef}>
      {Object.entries(data).map(([k, v]) => {
        return <input name={k} value={v} key={k} />;
      })}
    </form>
  );
};

export default Redirector;
